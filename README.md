**KeyStash (Core):**
 
 --------------------

 **Overview:**

 KeyStash is a simple to use and highly versatile open source software to manage your and customers Nxt accounts. The ease of maintenance and multiplatform support makes it an users's favorite. Built in front and backend allows especially enterprises to build services on it. Plugins contributed by the fantastic Nxt community allows for a broad range of use cases. 
 
 All versions with e in it will be recommended to use with test Nxt. Visit wiki.keystash.org for further documentation.

 --------------------

 **Running software:**

* Windows: keystash.bat
* Linux: ./keystash.sh
* Headless systems: Command line supported. 
* Windows: keystashd.bat
* Linux: ./keystashd.sh
* NOTE: To start the daemon with a encrypted wallet file use for example:
  
* ./keystashd.sh -walletpassphrase "password"
   Edit config file keystash/conf/keystash.properties to run keystash as a daemon.
   keystash.enableDaemon=true
   
* NOTE: Java(Oracle JVM) 7 or later needs to be installed first.   
 
 ---------------------

 **Technical details:**

 KeyStash generates and encrypts(AES 128,*256bit) private pass phrases. 
 You can enable a second layer of security by encrypting the database via AES 128 bit.
 If you enable the included api you can find your wallet @"http://localhost:7801/keystash" (jsonp formatted). 
 The api offers you also the possibility to use custom plugins. 
 With offline signing you can prepare transactions on platforms without internet connection.
 
* WARNING: Always take wallet backups on a secure place. When do I have to backup? When ever you create a new account! 
* If you forget your primary/ master password you could lose all your access.
 
 --------------------

 **Customization:**

 * If you want to enable 256 bit encryption for primary keys, search for "removing the 128-bit key restriction in Java".
 * Infos where you can see the component -status/version/paths you will find in the GUI-About tab.  
 
 --------------------

 **Command line:**

 Command line is enabled on all headless systems. Insert -? for help.


```
#!java

 Usage:          
 [help]          
 [options] <command> 
 [command] <cancel> <start> <ip> <stop> <allow>  

 Options:        
 -?                        Help message. 
 -info                     Status report. 
 -list                     List accounts in wallet file. 
 -new                      Create new account. 
 -import                   Import account. 
 -show                     Show account details 
 -encrypt                  Encrypt wallet file. 
 -sign                     Sign offline transaction. 
 -backup                   Backup wallet file on a specific place. 
 -api                      <start> <ip> <allow> <ip> <*> or <stop> api. 
 -exit                     Stop software and exit. 
```

 ---------------------

 **Build:**

 * KeyStash developer using Netbeans IDE. The default NetBeans IDE project system is built directly on top of the Ant build system.

 ---------------------

 **Roadmap:**

 * Auto installer
 * to continue...
  
 ---------------------

 **Want to contribute?**

 * create pull requests
 * review pull requests
 * review existing code
 * create issues 
 * answer issues