package keystash;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFrame;
import keystash.command.Connect;
import keystash.command.Interface;
import keystash.command.Options;
import keystash.gui.About;
import keystash.gui.AccountDetails;
import keystash.gui.AddressSearch;
import keystash.gui.BackupWallet;
import keystash.gui.ClipBoard;
import keystash.gui.Complexity;
import keystash.gui.ConfirmImport;
import keystash.gui.ConfirmNewAccount;
import keystash.gui.Console;
import keystash.gui.DecryptWallet;
import keystash.gui.DisplayQR;
import keystash.gui.EncryptWallet;
import keystash.gui.FeelAndLook;
import keystash.gui.GenerateToken;
import keystash.gui.Icon;
import keystash.gui.Import;
import keystash.gui.Loader;
import keystash.gui.Memorable;
import keystash.gui.MenuItem;
import keystash.gui.NewAccount;
import keystash.gui.OfflineSigning;
import keystash.gui.PopUpMenu;
import keystash.gui.Preferences;
import keystash.gui.Reset;
import keystash.gui.Welcome;
import keystash.http.Api;
import keystash.http.ApiRefresh;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Debug;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.SystemUtils;

public class KeyStashGUI extends javax.swing.JFrame {

    public static char[] secretToken;
    public static char[] secretSigning;
    public static char[] globalPassword;
    public static char[] secret;
    public int tmpList;
    public static int backgroundMenu = 0;
    public static Integer row = null;
    public List<Integer> list = new ArrayList<>();
    public static boolean isWalletEncrypted = false;
    public static boolean closeCommandLine = false;
    public static boolean lockClearClipboardButton = false;
    public static boolean enableApi = false;
    public static boolean guiShowWarnings = Constants.showWarnings;
    public static boolean runDaemon = false;

    public KeyStashGUI()
            throws SQLException,
            FontFormatException,
            IOException {

        initComponents();
        FeelAndLook.loadCustom();
        setIconAndFont();
        clearClipBoardWhenExit();
        Loader.walletStructure();
        Icon.initializeIcons(false);

    }

    @SuppressWarnings({"unchecked", "CallToPrintStackTrace"})
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        welcomeFrame = new javax.swing.JFrame();
        welcomeBackgroundLabel = new javax.swing.JLabel();
        welcomejSeparator = new javax.swing.JSeparator();
        welcomeOkButton = new javax.swing.JButton();
        welcomeShowOnStartupLabel = new javax.swing.JLabel();
        welcomeShowOnStartupCheckbox = new javax.swing.JCheckBox();
        newAccounIconBackgroundLabel = new javax.swing.JLabel();
        importIconBackgroundLabel = new javax.swing.JLabel();
        preferencesIconBackgroundLabel = new javax.swing.JLabel();
        showWarningsOnStartupLabel = new javax.swing.JLabel();
        showWarningsOnStartupCheckbox = new javax.swing.JCheckBox();
        newAccountFrame = new javax.swing.JFrame();
        newAccountOKButton = new javax.swing.JButton();
        newAccountGeneratePasswordButton = new javax.swing.JButton();
        newAccountSeparator = new javax.swing.JSeparator();
        newAccountCancelButton = new javax.swing.JButton();
        newAccountBackgroundLabel = new javax.swing.JLabel();
        newAccountLengthLabel = new javax.swing.JLabel();
        newAccountEntropyLabel = new javax.swing.JLabel();
        newAccountPasswordStrengthLabel = new javax.swing.JLabel();
        newAccountlPasswordConfirmFieldLabel = new javax.swing.JLabel();
        newAccountlPasswordFieldLabel = new javax.swing.JLabel();
        newAccountPasswordField = new javax.swing.JPasswordField();
        newAccountPasswordConfirmField = new javax.swing.JPasswordField();
        newAccountShowPhraseCheckBox = new javax.swing.JCheckBox();
        newAccountjProgressBar = new javax.swing.JProgressBar();
        newAccountEntropyValue = new javax.swing.JLabel();
        newAccountLengthValue = new javax.swing.JLabel();
        confirmNewAccountFrame = new javax.swing.JFrame();
        confirmNewAccountPhraseField = new javax.swing.JPasswordField();
        confirmNewAccountPhraseLabel = new javax.swing.JLabel();
        confirmNewAccountOKButton = new javax.swing.JButton();
        confirmNewAccountShowPhraseCheckBox = new javax.swing.JCheckBox();
        confirmNewAccountEntropyLabel = new javax.swing.JLabel();
        confirmNewAccountEntropyValue = new javax.swing.JLabel();
        confirmNewAccountLengthLabel = new javax.swing.JLabel();
        confirmNewAccountLengthValue = new javax.swing.JLabel();
        confirmNewAccountGenerateNewPhraseButton = new javax.swing.JButton();
        confirmNewAccountSearchAddressToggleButton = new javax.swing.JToggleButton();
        confirmNewAccountIncludeLettersField = new javax.swing.JFormattedTextField();
        confirmNewAccountAddressLabel = new javax.swing.JLabel();
        confirmNewAccountShowAddressRsLabel = new javax.swing.JLabel();
        confirmNewAccountAttemptLabel = new javax.swing.JLabel();
        confirmNewAccountAttemptValueLabel = new javax.swing.JLabel();
        confirmNewAccountBackgroundLabel = new javax.swing.JLabel();
        confirmNewAccountCancelButton = new javax.swing.JButton();
        confirmAccountSeparator = new javax.swing.JSeparator();
        showDetailPopup = new javax.swing.JPopupMenu();
        accountDetailsItem = new javax.swing.JMenuItem();
        generateTokenItem = new javax.swing.JMenuItem();
        offlineSigningItem = new javax.swing.JMenuItem();
        showDetailSeparator = new javax.swing.JPopupMenu.Separator();
        showQRCodeItem = new javax.swing.JMenuItem();
        copyAddressItem = new javax.swing.JMenuItem();
        enterPrimaryPasswordAccountDetailsFrame = new javax.swing.JFrame();
        enterPrimaryPasswordAccountDetailsPasswordField = new javax.swing.JPasswordField();
        enterPrimaryPasswordAccountDetailsPasswordFieldLabel = new javax.swing.JLabel();
        enterPrimaryPasswordAccountDetailsOKButton = new javax.swing.JButton();
        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox = new javax.swing.JCheckBox();
        enterPrimaryPasswordAccountDetailsBackgroundLabel = new javax.swing.JLabel();
        enterPrimaryPasswordAccountDetailsCancelButton = new javax.swing.JButton();
        enterPrimaryPasswordAccountDetailsSeparator = new javax.swing.JSeparator();
        enterPrimaryPasswordGenerateTokenFrame = new javax.swing.JFrame();
        enterPrimaryPasswordGenerateTokenPasswordField = new javax.swing.JPasswordField();
        enterPrimaryPasswordGenerateTokenPasswordFieldLabel = new javax.swing.JLabel();
        enterPrimaryPasswordGenerateTokenOKButton = new javax.swing.JButton();
        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox = new javax.swing.JCheckBox();
        enterPrimaryPasswordGenerateTokenBackgroundLabel = new javax.swing.JLabel();
        enterPrimaryPasswordGenerateTokenCancelButton = new javax.swing.JButton();
        enterPrimaryPasswordGenerateTokenSeparator = new javax.swing.JSeparator();
        enterPrimaryPasswordOfflineSigningFrame = new javax.swing.JFrame();
        enterPrimaryPasswordOfflineSigningPasswordField = new javax.swing.JPasswordField();
        enterPrimaryPasswordOfflineSigningPasswordFieldLabel = new javax.swing.JLabel();
        enterPrimaryPasswordOfflineSigningOKButton = new javax.swing.JButton();
        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox = new javax.swing.JCheckBox();
        enterPrimaryPasswordOfflineSigningBackgroundLabel = new javax.swing.JLabel();
        enterPrimaryPasswordOfflineSigningCancelButton = new javax.swing.JButton();
        enterPrimaryPasswordOfflineSigningSeparator = new javax.swing.JSeparator();
        accountDetailsFrame = new javax.swing.JFrame();
        accountDetailsPhraseField = new javax.swing.JPasswordField();
        accountDetailsPhraseLabel = new javax.swing.JLabel();
        accountDetailsShowPhraseCheckBox = new javax.swing.JCheckBox();
        accountDetailsPublicKeyLabel = new javax.swing.JLabel();
        accountDetailsPublicKeyField = new javax.swing.JTextField();
        accountDetailsCopyButton1 = new javax.swing.JButton();
        accountDetailsCopyButton2 = new javax.swing.JButton();
        accountDetailsClearLabel = new javax.swing.JLabel();
        accountDetailsjProgressBar = new javax.swing.JProgressBar();
        accountDetailsLabelLabel = new javax.swing.JLabel();
        accountDetailsBackgroundLabel = new javax.swing.JLabel();
        accountDetailsOKButton = new javax.swing.JButton();
        accountDetailsjSeparator = new javax.swing.JSeparator();
        accountDetailsLabelField = new javax.swing.JFormattedTextField();
        generateTokenFrame = new javax.swing.JFrame();
        generateTokenCopyButton = new javax.swing.JButton();
        generateTokenClearLabel = new javax.swing.JLabel();
        generateTokenTokenDataField = new javax.swing.JTextField();
        generateTokenTokenValueLabel = new javax.swing.JLabel();
        generateTokenTokenKeyField = new javax.swing.JTextField();
        generateTokenTokenKeyLabel = new javax.swing.JLabel();
        generateTokenGenerateTokenButton = new javax.swing.JButton();
        generateTokenBackgroundLabel = new javax.swing.JLabel();
        generateTokenOKButton = new javax.swing.JButton();
        generateTokenSeparator = new javax.swing.JSeparator();
        offlineSigningFrame = new javax.swing.JFrame();
        offlineSigningClearLabel = new javax.swing.JLabel();
        offlineSigningRecepientLabel = new javax.swing.JLabel();
        offlineSigningAmountLabel = new javax.swing.JLabel();
        offlineSigningSignButton = new javax.swing.JButton();
        offlineSigningBackgroundLabel = new javax.swing.JLabel();
        offlineSigningOKButton = new javax.swing.JButton();
        offlineSigningSeparator = new javax.swing.JSeparator();
        offlineSigningRecepientTextField = new javax.swing.JFormattedTextField();
        offlineSigningFeeLabel = new javax.swing.JLabel();
        offlineSigningFeeSpinner = new javax.swing.JSpinner();
        offlineSigningDeadlineLabel = new javax.swing.JLabel();
        offlineSigningDeadlineSpinner = new javax.swing.JSpinner();
        offlineSigningAmountTextField = new javax.swing.JTextField();
        showPreferencesFrame = new javax.swing.JFrame();
        showPreferencesWarningsLabel = new javax.swing.JLabel();
        showPreferencesWarningsCheckbox = new javax.swing.JCheckBox();
        showPreferencesClearClipboardSpinner = new javax.swing.JSpinner();
        showPreferencesClearClipboardLabel = new javax.swing.JLabel();
        showPreferencesPhraseBlocksSpinner = new javax.swing.JSpinner();
        showPreferencesPhraseBlocksLabel = new javax.swing.JLabel();
        showPreferencesRandomGenerationLabel = new javax.swing.JLabel();
        showPreferencesRandomGenerationCheckbox = new javax.swing.JCheckBox();
        showPreferencesLengthMinimumLabel = new javax.swing.JLabel();
        showPreferencesLengthMinimumMinSpinner = new javax.swing.JSpinner();
        showPreferencesLengthMinimumMaxSpinner = new javax.swing.JSpinner();
        showPreferencesLengthMaximumLabel = new javax.swing.JLabel();
        showPreferencesLengthMaximumMaxSpinner = new javax.swing.JSpinner();
        showPreferencesLengthMaximumMinSpinner = new javax.swing.JSpinner();
        showPreferencesLowerCaseLabel = new javax.swing.JLabel();
        showPreferencesLowerCaseMinSpinner = new javax.swing.JSpinner();
        showPreferencesLowerCaseMaxSpinner = new javax.swing.JSpinner();
        showPreferencesUpperCaseLabel = new javax.swing.JLabel();
        showPreferencesUpperCaseMinSpinner = new javax.swing.JSpinner();
        showPreferencesUpperCaseMaxSpinner = new javax.swing.JSpinner();
        showPreferencesNumeraryLabel = new javax.swing.JLabel();
        showPreferencesNumeraryMinSpinner = new javax.swing.JSpinner();
        showPreferencesNumeraryMaxSpinner = new javax.swing.JSpinner();
        showPreferencesSpecialCharLabel = new javax.swing.JLabel();
        showPreferencesSpecialCharMinSpinner = new javax.swing.JSpinner();
        showPreferencesSpecialCharMaxSpinner = new javax.swing.JSpinner();
        showPreferencesResetSettingsButton = new javax.swing.JButton();
        showPreferencesWhitespaceLabel = new javax.swing.JLabel();
        showPreferencesWhitespaceCheckbox = new javax.swing.JCheckBox();
        showPreferencesIterationLabel = new javax.swing.JLabel();
        showPreferencesIterationSpinner = new javax.swing.JSpinner();
        showPreferenceKeySizesLabel = new javax.swing.JLabel();
        showPreferencesKeySizeComboBox = new javax.swing.JComboBox();
        showPreferecesBackgroundLabel = new javax.swing.JLabel();
        showPreferencesjSeparator = new javax.swing.JSeparator();
        showPreferencesOKButton = new javax.swing.JButton();
        showPreferencesANSILabel = new javax.swing.JLabel();
        showPreferencesANSICheckbox = new javax.swing.JCheckBox();
        aboutFrame = new javax.swing.JFrame();
        aboutBackgroundLabel = new javax.swing.JLabel();
        aboutScrollPane = new javax.swing.JScrollPane();
        aboutTable = new javax.swing.JTable();
        aboutjSeparator = new javax.swing.JSeparator();
        aboutOkButton = new javax.swing.JButton();
        aboutKeyStashDevelopersLabel = new javax.swing.JLabel();
        aboutKeyStashLicenseLabel = new javax.swing.JLabel();
        aboutKeyStashLicenseLinkLabel = new javax.swing.JLabel();
        aboutKeyStashExperimentelLabel = new javax.swing.JLabel();
        aboutKeyStashIncludesSoftwareLabel = new javax.swing.JLabel();
        aboutKeyStashIncludesSoftwareLinkLabel = new javax.swing.JLabel();
        encryptDatabaseFrame = new javax.swing.JFrame();
        encryptDatabasePasswordField = new javax.swing.JPasswordField();
        encryptDatabasePasswordFieldLabel = new javax.swing.JLabel();
        encryptDatabaseOKButton = new javax.swing.JButton();
        encryptDatabasePasswordConfirmField = new javax.swing.JPasswordField();
        encryptDatabasePasswordConfirmFieldLabel = new javax.swing.JLabel();
        encryptDatabaseShowPhraseCheckBox = new javax.swing.JCheckBox();
        encryptDatabasejProgressBar = new javax.swing.JProgressBar();
        encryptDatabasePasswordStrengthLabel = new javax.swing.JLabel();
        encryptDatabaseEntropyLabel = new javax.swing.JLabel();
        encryptDatabaseEntropyValue = new javax.swing.JLabel();
        encryptDatabaseLengthLabel = new javax.swing.JLabel();
        encryptDatabaseLengthValue = new javax.swing.JLabel();
        encryptDatabaseGeneratePasswordButton = new javax.swing.JButton();
        encryptDatabaseBackgroundLabel = new javax.swing.JLabel();
        encryptDatabaseSeparator = new javax.swing.JSeparator();
        encryptDatabaseCancelButton = new javax.swing.JButton();
        decryptDatabaseFrame = new javax.swing.JFrame();
        decryptDatabasePasswordField = new javax.swing.JPasswordField();
        decryptDatabasePasswordFieldLabel = new javax.swing.JLabel();
        decryptDatabaseOKButton = new javax.swing.JButton();
        decryptDatabaseShowPhraseCheckBox = new javax.swing.JCheckBox();
        decryptDatabaseBackgroundLabel = new javax.swing.JLabel();
        decryptDatabaseSeparator = new javax.swing.JSeparator();
        decryptDatabaseCancelButton = new javax.swing.JButton();
        importFrame = new javax.swing.JFrame();
        importOKButton = new javax.swing.JButton();
        importGeneratePasswordButton = new javax.swing.JButton();
        importSeparator = new javax.swing.JSeparator();
        importCancelButton = new javax.swing.JButton();
        importBackgroundLabel = new javax.swing.JLabel();
        importLengthLabel = new javax.swing.JLabel();
        importEntropyLabel = new javax.swing.JLabel();
        importPasswordStrengthLabel = new javax.swing.JLabel();
        importPasswordConfirmFieldLabel = new javax.swing.JLabel();
        importPasswordFieldLabel = new javax.swing.JLabel();
        importPasswordField = new javax.swing.JPasswordField();
        importPasswordConfirmField = new javax.swing.JPasswordField();
        importShowPhraseCheckBox = new javax.swing.JCheckBox();
        importjProgressBar = new javax.swing.JProgressBar();
        importEntropyValue = new javax.swing.JLabel();
        importLengthValue = new javax.swing.JLabel();
        confirmImportFrame = new javax.swing.JFrame();
        confirmImportPhraseField = new javax.swing.JPasswordField();
        confirmImportPhraseLabel = new javax.swing.JLabel();
        confirmImportOKButton = new javax.swing.JButton();
        confirmImportShowPhraseCheckBox = new javax.swing.JCheckBox();
        confirmImportEntropyLabel = new javax.swing.JLabel();
        confirmImportEntropyValue = new javax.swing.JLabel();
        confirmImportLengthLabel = new javax.swing.JLabel();
        confirmImportLengthValue = new javax.swing.JLabel();
        confirmImportAddressLabel = new javax.swing.JLabel();
        confirmImportShowAddressRsLabel = new javax.swing.JLabel();
        confirmImportBackgroundLabel = new javax.swing.JLabel();
        confirmImportCancelButton = new javax.swing.JButton();
        confirmImportSeparator = new javax.swing.JSeparator();
        qrCodeFrame = new javax.swing.JFrame();
        qrCodeBackgroundLabel = new javax.swing.JLabel();
        accountScrollPane = new javax.swing.JScrollPane();
        accountTable = new javax.swing.JTable();
        iconPane = new javax.swing.JPanel();
        iconPaneAccountjLabel = new javax.swing.JLabel();
        iconPaneLockjLabel = new javax.swing.JLabel();
        iconPaneApijLabel = new javax.swing.JLabel();
        mainMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        accountMenuItem = new javax.swing.JMenuItem();
        importMenuItem = new javax.swing.JMenuItem();
        backupMenuItem = new javax.swing.JMenuItem();
        fileMenuSeperator = new javax.swing.JPopupMenu.Separator();
        exitMenuItem = new javax.swing.JMenuItem();
        settingsMenu = new javax.swing.JMenu();
        enableApiMenuItem = new javax.swing.JCheckBoxMenuItem();
        encryptWalletFileMenuItem = new javax.swing.JMenuItem();
        preferencesMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        startPageMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        welcomeFrame.setTitle("Start Page");
        welcomeFrame.setAlwaysOnTop(true);
        welcomeFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        welcomeFrame.setResizable(false);

        welcomeBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/welcomeScreenBG.png"))); // NOI18N
        welcomeBackgroundLabel.setToolTipText(null);

        welcomejSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        welcomejSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        welcomejSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        welcomeOkButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        welcomeOkButton.setMnemonic('O');
        welcomeOkButton.setText("OK");
        welcomeOkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                welcomeOkButtonActionPerformed(evt);
            }
        });

        welcomeShowOnStartupLabel.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        welcomeShowOnStartupLabel.setText("Show On Startup");
        welcomeShowOnStartupLabel.setMaximumSize(new java.awt.Dimension(110, 18));
        welcomeShowOnStartupLabel.setMinimumSize(new java.awt.Dimension(110, 18));
        welcomeShowOnStartupLabel.setPreferredSize(new java.awt.Dimension(110, 18));

        welcomeShowOnStartupCheckbox.setSelected(true);
        welcomeShowOnStartupCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                welcomeShowOnStartupCheckboxActionPerformed(evt);
            }
        });

        newAccounIconBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/newAccountIconBG.png"))); // NOI18N
        newAccounIconBackgroundLabel.setToolTipText(null);
        newAccounIconBackgroundLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        newAccounIconBackgroundLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                newAccounIconBackgroundLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                newAccounIconBackgroundLabelMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                newAccounIconBackgroundLabelMouseReleased(evt);
            }
        });

        importIconBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/importIconBG.png"))); // NOI18N
        importIconBackgroundLabel.setToolTipText(null);
        importIconBackgroundLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        importIconBackgroundLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                importIconBackgroundLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                importIconBackgroundLabelMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                importIconBackgroundLabelMouseReleased(evt);
            }
        });

        preferencesIconBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/preferencesIconBG.png"))); // NOI18N
        preferencesIconBackgroundLabel.setToolTipText(null);
        preferencesIconBackgroundLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        preferencesIconBackgroundLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                preferencesIconBackgroundLabelMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                preferencesIconBackgroundLabelMouseExited(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                preferencesIconBackgroundLabelMouseReleased(evt);
            }
        });

        showWarningsOnStartupLabel.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showWarningsOnStartupLabel.setText("Show Warnings");
        showWarningsOnStartupLabel.setMaximumSize(new java.awt.Dimension(110, 18));
        showWarningsOnStartupLabel.setMinimumSize(new java.awt.Dimension(110, 18));
        showWarningsOnStartupLabel.setPreferredSize(new java.awt.Dimension(110, 18));

        showWarningsOnStartupCheckbox.setSelected(true);
        showWarningsOnStartupCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showWarningsOnStartupCheckboxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout welcomeFrameLayout = new javax.swing.GroupLayout(welcomeFrame.getContentPane());
        welcomeFrame.getContentPane().setLayout(welcomeFrameLayout);
        welcomeFrameLayout.setHorizontalGroup(
            welcomeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(welcomeFrameLayout.createSequentialGroup()
                .addGroup(welcomeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(welcomeBackgroundLabel)
                    .addComponent(welcomejSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(welcomeFrameLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(welcomeShowOnStartupLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(welcomeShowOnStartupCheckbox)
                .addGap(12, 12, 12)
                .addComponent(showWarningsOnStartupLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(showWarningsOnStartupCheckbox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(welcomeOkButton)
                .addGap(32, 32, 32))
            .addGroup(welcomeFrameLayout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(newAccounIconBackgroundLabel)
                .addGap(53, 53, 53)
                .addComponent(importIconBackgroundLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(preferencesIconBackgroundLabel)
                .addGap(52, 52, 52))
        );
        welcomeFrameLayout.setVerticalGroup(
            welcomeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(welcomeFrameLayout.createSequentialGroup()
                .addComponent(welcomeBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(welcomeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(newAccounIconBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importIconBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(preferencesIconBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(welcomejSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(welcomeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(welcomeShowOnStartupCheckbox)
                    .addGroup(welcomeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(welcomeOkButton)
                        .addComponent(welcomeShowOnStartupLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(welcomeFrameLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(showWarningsOnStartupLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(showWarningsOnStartupCheckbox))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        newAccountFrame.setTitle("New Account");
        newAccountFrame.setAlwaysOnTop(true);
        newAccountFrame.setBackground(new java.awt.Color(255, 255, 255));
        newAccountFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        newAccountFrame.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        newAccountFrame.setResizable(false);

        newAccountOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountOKButton.setMnemonic('O');
        newAccountOKButton.setText("OK");
        newAccountOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newAccountOKButtonActionPerformed(evt);
            }
        });

        newAccountGeneratePasswordButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountGeneratePasswordButton.setMnemonic('G');
        newAccountGeneratePasswordButton.setText("Generate Memorable");
        newAccountGeneratePasswordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newAccountGeneratePasswordButtonActionPerformed(evt);
            }
        });

        newAccountSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        newAccountSeparator.setMinimumSize(new java.awt.Dimension(520, 5));

        newAccountCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountCancelButton.setMnemonic('C');
        newAccountCancelButton.setText("Cancel");
        newAccountCancelButton.setFocusable(false);
        newAccountCancelButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        newAccountCancelButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        newAccountCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newAccountCancelButtonActionPerformed(evt);
            }
        });

        newAccountBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/newAccountBG.png"))); // NOI18N
        newAccountBackgroundLabel.setToolTipText(null);

        newAccountLengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountLengthLabel.setText("Length:");
        newAccountLengthLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        newAccountLengthLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        newAccountLengthLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        newAccountEntropyLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountEntropyLabel.setText("Entropy Bits:");
        newAccountEntropyLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        newAccountEntropyLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        newAccountEntropyLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        newAccountPasswordStrengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountPasswordStrengthLabel.setText("Complexity: ");
        newAccountPasswordStrengthLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        newAccountPasswordStrengthLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        newAccountPasswordStrengthLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        newAccountlPasswordConfirmFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        newAccountlPasswordConfirmFieldLabel.setText("Confirm Password:");
        newAccountlPasswordConfirmFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        newAccountlPasswordConfirmFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        newAccountlPasswordConfirmFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        newAccountlPasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        newAccountlPasswordFieldLabel.setText("Primary Password:");
        newAccountlPasswordFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        newAccountlPasswordFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        newAccountlPasswordFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        newAccountPasswordField.setColumns(50);
        newAccountPasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        newAccountPasswordField.setMaximumSize(new java.awt.Dimension(290, 30));
        newAccountPasswordField.setMinimumSize(new java.awt.Dimension(290, 30));
        newAccountPasswordField.setPreferredSize(new java.awt.Dimension(290, 30));
        newAccountPasswordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                newAccountPasswordFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                newAccountPasswordFieldKeyTyped(evt);
            }
        });

        newAccountPasswordConfirmField.setColumns(50);
        newAccountPasswordConfirmField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        newAccountPasswordConfirmField.setMaximumSize(new java.awt.Dimension(290, 30));
        newAccountPasswordConfirmField.setMinimumSize(new java.awt.Dimension(290, 30));
        newAccountPasswordConfirmField.setPreferredSize(new java.awt.Dimension(290, 30));

        newAccountShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        newAccountShowPhraseCheckBox.setText("Show");
        newAccountShowPhraseCheckBox.setFocusable(false);
        newAccountShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        newAccountShowPhraseCheckBox.setMaximumSize(new java.awt.Dimension(37, 30));
        newAccountShowPhraseCheckBox.setMinimumSize(new java.awt.Dimension(37, 30));
        newAccountShowPhraseCheckBox.setPreferredSize(new java.awt.Dimension(37, 30));
        newAccountShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        newAccountShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newAccountShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        newAccountjProgressBar.setFont(new java.awt.Font("Open Sans", 1, 11)); // NOI18N
        newAccountjProgressBar.setBorderPainted(true);
        newAccountjProgressBar.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        newAccountjProgressBar.setMaximumSize(new java.awt.Dimension(290, 20));
        newAccountjProgressBar.setMinimumSize(new java.awt.Dimension(290, 20));
        newAccountjProgressBar.setPreferredSize(new java.awt.Dimension(290, 20));

        newAccountEntropyValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountEntropyValue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        newAccountEntropyValue.setText("0.0");
        newAccountEntropyValue.setToolTipText(null);
        newAccountEntropyValue.setMaximumSize(new java.awt.Dimension(290, 18));
        newAccountEntropyValue.setMinimumSize(new java.awt.Dimension(290, 18));
        newAccountEntropyValue.setPreferredSize(new java.awt.Dimension(290, 18));
        newAccountEntropyValue.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        newAccountLengthValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        newAccountLengthValue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        newAccountLengthValue.setText("0");
        newAccountLengthValue.setMaximumSize(new java.awt.Dimension(290, 18));
        newAccountLengthValue.setMinimumSize(new java.awt.Dimension(290, 18));
        newAccountLengthValue.setPreferredSize(new java.awt.Dimension(290, 18));

        javax.swing.GroupLayout newAccountFrameLayout = new javax.swing.GroupLayout(newAccountFrame.getContentPane());
        newAccountFrame.getContentPane().setLayout(newAccountFrameLayout);
        newAccountFrameLayout.setHorizontalGroup(
            newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(newAccountFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(newAccountGeneratePasswordButton)
                .addGap(5, 5, 5)
                .addComponent(newAccountOKButton)
                .addGap(5, 5, 5)
                .addComponent(newAccountCancelButton)
                .addGap(32, 32, 32))
            .addGroup(newAccountFrameLayout.createSequentialGroup()
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(newAccountSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newAccountBackgroundLabel))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(newAccountFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(newAccountlPasswordFieldLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, newAccountFrameLayout.createSequentialGroup()
                        .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(newAccountEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(newAccountLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(newAccountLengthValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(newAccountEntropyValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, newAccountFrameLayout.createSequentialGroup()
                        .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(newAccountlPasswordConfirmFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(newAccountPasswordStrengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)
                        .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(newAccountPasswordConfirmField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(newAccountPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(newAccountjProgressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(10, 10, 10)
                .addComponent(newAccountShowPhraseCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        newAccountFrameLayout.setVerticalGroup(
            newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, newAccountFrameLayout.createSequentialGroup()
                .addComponent(newAccountBackgroundLabel)
                .addGap(10, 10, 10)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(newAccountPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(newAccountlPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(newAccountShowPhraseCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newAccountPasswordConfirmField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newAccountlPasswordConfirmFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(newAccountPasswordStrengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newAccountjProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(newAccountEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newAccountEntropyValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(newAccountLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newAccountLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(newAccountSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(newAccountCancelButton)
                    .addGroup(newAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(newAccountOKButton)
                        .addComponent(newAccountGeneratePasswordButton)))
                .addGap(10, 10, 10))
        );

        newAccountjProgressBar.setBackground(Color.red);
        newAccountjProgressBar.setForeground(Color.red);

        confirmNewAccountFrame.setTitle("Confirm Account");
        confirmNewAccountFrame.setAlwaysOnTop(true);
        confirmNewAccountFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        confirmNewAccountFrame.setResizable(false);

        confirmNewAccountPhraseField.setColumns(50);
        confirmNewAccountPhraseField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        confirmNewAccountPhraseField.setMaximumSize(new java.awt.Dimension(290, 30));
        confirmNewAccountPhraseField.setMinimumSize(new java.awt.Dimension(290, 30));
        confirmNewAccountPhraseField.setPreferredSize(new java.awt.Dimension(290, 30));
        confirmNewAccountPhraseField.setVerifyInputWhenFocusTarget(false);
        confirmNewAccountPhraseField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                confirmNewAccountPhraseFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                confirmNewAccountPhraseFieldKeyTyped(evt);
            }
        });

        confirmNewAccountPhraseLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmNewAccountPhraseLabel.setText("Private Phrase:");
        confirmNewAccountPhraseLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountPhraseLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountPhraseLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmNewAccountOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountOKButton.setMnemonic('O');
        confirmNewAccountOKButton.setText("OK");
        confirmNewAccountOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmNewAccountOKButtonActionPerformed(evt);
            }
        });

        confirmNewAccountShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        confirmNewAccountShowPhraseCheckBox.setText("Show");
        confirmNewAccountShowPhraseCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        confirmNewAccountShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        confirmNewAccountShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        confirmNewAccountShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmNewAccountShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        confirmNewAccountEntropyLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountEntropyLabel.setText("Entropy Bits:");
        confirmNewAccountEntropyLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountEntropyLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountEntropyLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmNewAccountEntropyValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountEntropyValue.setText("0.0");
        confirmNewAccountEntropyValue.setToolTipText(null);
        confirmNewAccountEntropyValue.setMaximumSize(new java.awt.Dimension(290, 18));
        confirmNewAccountEntropyValue.setMinimumSize(new java.awt.Dimension(290, 18));
        confirmNewAccountEntropyValue.setPreferredSize(new java.awt.Dimension(290, 18));

        confirmNewAccountLengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountLengthLabel.setText("Length:");
        confirmNewAccountLengthLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountLengthLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountLengthLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmNewAccountLengthValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountLengthValue.setText("0");
        confirmNewAccountLengthValue.setMaximumSize(new java.awt.Dimension(290, 18));
        confirmNewAccountLengthValue.setMinimumSize(new java.awt.Dimension(290, 18));
        confirmNewAccountLengthValue.setPreferredSize(new java.awt.Dimension(290, 18));

        confirmNewAccountGenerateNewPhraseButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountGenerateNewPhraseButton.setMnemonic('G');
        confirmNewAccountGenerateNewPhraseButton.setText("Generate new phrase");
        confirmNewAccountGenerateNewPhraseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmNewAccountGenerateNewPhraseButtonActionPerformed(evt);
            }
        });

        confirmNewAccountSearchAddressToggleButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountSearchAddressToggleButton.setMnemonic('S');
        confirmNewAccountSearchAddressToggleButton.setText("Search");
        confirmNewAccountSearchAddressToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmNewAccountSearchAddressToggleButtonActionPerformed(evt);
            }
        });

        try {
            confirmNewAccountIncludeLettersField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("NXT-*****")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        confirmNewAccountIncludeLettersField.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmNewAccountIncludeLettersField.setMaximumSize(new java.awt.Dimension(90, 30));
        confirmNewAccountIncludeLettersField.setMinimumSize(new java.awt.Dimension(90, 30));
        confirmNewAccountIncludeLettersField.setName(""); // NOI18N
        confirmNewAccountIncludeLettersField.setPreferredSize(new java.awt.Dimension(90, 30));

        confirmNewAccountAddressLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmNewAccountAddressLabel.setText("NXT Address:");
        confirmNewAccountAddressLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountAddressLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountAddressLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmNewAccountShowAddressRsLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmNewAccountShowAddressRsLabel.setMaximumSize(new java.awt.Dimension(290, 30));
        confirmNewAccountShowAddressRsLabel.setMinimumSize(new java.awt.Dimension(290, 30));

        confirmNewAccountAttemptLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountAttemptLabel.setText("Attempt:");
        confirmNewAccountAttemptLabel.setEnabled(false);
        confirmNewAccountAttemptLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountAttemptLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmNewAccountAttemptLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmNewAccountAttemptValueLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountAttemptValueLabel.setText("0");
        confirmNewAccountAttemptValueLabel.setEnabled(false);
        confirmNewAccountAttemptValueLabel.setMaximumSize(new java.awt.Dimension(290, 18));
        confirmNewAccountAttemptValueLabel.setMinimumSize(new java.awt.Dimension(290, 18));
        confirmNewAccountAttemptValueLabel.setPreferredSize(new java.awt.Dimension(290, 18));

        confirmNewAccountBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/confirmAccountBG.png"))); // NOI18N
        confirmNewAccountBackgroundLabel.setToolTipText(null);

        confirmNewAccountCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmNewAccountCancelButton.setMnemonic('C');
        confirmNewAccountCancelButton.setText("Cancel");
        confirmNewAccountCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmNewAccountCancelButtonActionPerformed(evt);
            }
        });

        confirmAccountSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        confirmAccountSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        confirmAccountSeparator.setName(""); // NOI18N
        confirmAccountSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        javax.swing.GroupLayout confirmNewAccountFrameLayout = new javax.swing.GroupLayout(confirmNewAccountFrame.getContentPane());
        confirmNewAccountFrame.getContentPane().setLayout(confirmNewAccountFrameLayout);
        confirmNewAccountFrameLayout.setHorizontalGroup(
            confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(confirmNewAccountBackgroundLabel)
                    .addComponent(confirmAccountSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                        .addComponent(confirmNewAccountIncludeLettersField, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(confirmNewAccountSearchAddressToggleButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(confirmNewAccountGenerateNewPhraseButton)
                        .addGap(5, 5, 5)
                        .addComponent(confirmNewAccountOKButton)
                        .addGap(5, 5, 5)
                        .addComponent(confirmNewAccountCancelButton))
                    .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                        .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(confirmNewAccountPhraseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmNewAccountEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmNewAccountLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmNewAccountAttemptLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmNewAccountAddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(confirmNewAccountLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmNewAccountAttemptValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                                .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(confirmNewAccountShowAddressRsLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(confirmNewAccountPhraseField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(confirmNewAccountEntropyValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(10, 10, 10)
                                .addComponent(confirmNewAccountShowPhraseCheckBox)))))
                .addGap(32, 32, 32))
        );

        confirmNewAccountFrameLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {confirmNewAccountAttemptValueLabel, confirmNewAccountLengthValue});

        confirmNewAccountFrameLayout.setVerticalGroup(
            confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, confirmNewAccountFrameLayout.createSequentialGroup()
                .addComponent(confirmNewAccountBackgroundLabel)
                .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(confirmNewAccountShowAddressRsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(confirmNewAccountAddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                        .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(confirmNewAccountPhraseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmNewAccountPhraseField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                                .addComponent(confirmNewAccountEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(confirmNewAccountLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(confirmNewAccountAttemptLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(confirmNewAccountFrameLayout.createSequentialGroup()
                                .addComponent(confirmNewAccountEntropyValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(confirmNewAccountLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(confirmNewAccountAttemptValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(confirmNewAccountShowPhraseCheckBox))
                .addGap(10, 10, 10)
                .addComponent(confirmAccountSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(confirmNewAccountFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmNewAccountOKButton)
                    .addComponent(confirmNewAccountGenerateNewPhraseButton)
                    .addComponent(confirmNewAccountCancelButton)
                    .addComponent(confirmNewAccountSearchAddressToggleButton)
                    .addComponent(confirmNewAccountIncludeLettersField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        showDetailPopup.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N

        accountDetailsItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        accountDetailsItem.setText("Account Details");
        accountDetailsItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountDetailsItemActionPerformed(evt);
            }
        });
        showDetailPopup.add(accountDetailsItem);

        generateTokenItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        generateTokenItem.setText("Generate Token");
        generateTokenItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateTokenItemActionPerformed(evt);
            }
        });
        showDetailPopup.add(generateTokenItem);

        offlineSigningItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        offlineSigningItem.setText("Offline Signing");
        offlineSigningItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                offlineSigningItemActionPerformed(evt);
            }
        });
        showDetailPopup.add(offlineSigningItem);
        showDetailPopup.add(showDetailSeparator);

        showQRCodeItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showQRCodeItem.setText("Show QR Code");
        showQRCodeItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showQRCodeItemActionPerformed(evt);
            }
        });
        showDetailPopup.add(showQRCodeItem);

        copyAddressItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        copyAddressItem.setText("Copy To Clipboard");
        copyAddressItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyAddressItemActionPerformed(evt);
            }
        });
        showDetailPopup.add(copyAddressItem);

        enterPrimaryPasswordAccountDetailsFrame.setTitle("Open Account Details");
        enterPrimaryPasswordAccountDetailsFrame.setAlwaysOnTop(true);
        enterPrimaryPasswordAccountDetailsFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        enterPrimaryPasswordAccountDetailsFrame.setResizable(false);

        enterPrimaryPasswordAccountDetailsPasswordField.setColumns(50);
        enterPrimaryPasswordAccountDetailsPasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        enterPrimaryPasswordAccountDetailsPasswordField.setPreferredSize(new java.awt.Dimension(290, 30));

        enterPrimaryPasswordAccountDetailsPasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        enterPrimaryPasswordAccountDetailsPasswordFieldLabel.setText("Primary Password:");
        enterPrimaryPasswordAccountDetailsPasswordFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        enterPrimaryPasswordAccountDetailsPasswordFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        enterPrimaryPasswordAccountDetailsPasswordFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        enterPrimaryPasswordAccountDetailsOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        enterPrimaryPasswordAccountDetailsOKButton.setMnemonic('O');
        enterPrimaryPasswordAccountDetailsOKButton.setText("OK");
        enterPrimaryPasswordAccountDetailsOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordAccountDetailsOKButtonActionPerformed(evt);
            }
        });

        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.setText("Show");
        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordAccountDetailsShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        enterPrimaryPasswordAccountDetailsBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/primaryPasswordAccountDetailsBG.png"))); // NOI18N
        enterPrimaryPasswordAccountDetailsBackgroundLabel.setToolTipText(null);

        enterPrimaryPasswordAccountDetailsCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        enterPrimaryPasswordAccountDetailsCancelButton.setMnemonic('C');
        enterPrimaryPasswordAccountDetailsCancelButton.setText("Cancel");
        enterPrimaryPasswordAccountDetailsCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordAccountDetailsCancelButtonActionPerformed(evt);
            }
        });

        enterPrimaryPasswordAccountDetailsSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        javax.swing.GroupLayout enterPrimaryPasswordAccountDetailsFrameLayout = new javax.swing.GroupLayout(enterPrimaryPasswordAccountDetailsFrame.getContentPane());
        enterPrimaryPasswordAccountDetailsFrame.getContentPane().setLayout(enterPrimaryPasswordAccountDetailsFrameLayout);
        enterPrimaryPasswordAccountDetailsFrameLayout.setHorizontalGroup(
            enterPrimaryPasswordAccountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(enterPrimaryPasswordAccountDetailsFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(enterPrimaryPasswordAccountDetailsPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(enterPrimaryPasswordAccountDetailsPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(enterPrimaryPasswordAccountDetailsShowPhraseCheckBox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(enterPrimaryPasswordAccountDetailsFrameLayout.createSequentialGroup()
                .addGroup(enterPrimaryPasswordAccountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(enterPrimaryPasswordAccountDetailsSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(enterPrimaryPasswordAccountDetailsBackgroundLabel))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, enterPrimaryPasswordAccountDetailsFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(enterPrimaryPasswordAccountDetailsOKButton)
                .addGap(5, 5, 5)
                .addComponent(enterPrimaryPasswordAccountDetailsCancelButton)
                .addGap(32, 32, 32))
        );
        enterPrimaryPasswordAccountDetailsFrameLayout.setVerticalGroup(
            enterPrimaryPasswordAccountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, enterPrimaryPasswordAccountDetailsFrameLayout.createSequentialGroup()
                .addComponent(enterPrimaryPasswordAccountDetailsBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(enterPrimaryPasswordAccountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(enterPrimaryPasswordAccountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(enterPrimaryPasswordAccountDetailsPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(enterPrimaryPasswordAccountDetailsPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(enterPrimaryPasswordAccountDetailsShowPhraseCheckBox))
                .addGap(10, 10, 10)
                .addComponent(enterPrimaryPasswordAccountDetailsSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(enterPrimaryPasswordAccountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enterPrimaryPasswordAccountDetailsOKButton)
                    .addComponent(enterPrimaryPasswordAccountDetailsCancelButton))
                .addGap(10, 10, 10))
        );

        enterPrimaryPasswordGenerateTokenFrame.setTitle("Open Generate Token");
        enterPrimaryPasswordGenerateTokenFrame.setAlwaysOnTop(true);
        enterPrimaryPasswordGenerateTokenFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        enterPrimaryPasswordGenerateTokenFrame.setResizable(false);

        enterPrimaryPasswordGenerateTokenPasswordField.setColumns(50);
        enterPrimaryPasswordGenerateTokenPasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        enterPrimaryPasswordGenerateTokenPasswordField.setMaximumSize(new java.awt.Dimension(290, 30));
        enterPrimaryPasswordGenerateTokenPasswordField.setMinimumSize(new java.awt.Dimension(290, 30));
        enterPrimaryPasswordGenerateTokenPasswordField.setPreferredSize(new java.awt.Dimension(290, 30));

        enterPrimaryPasswordGenerateTokenPasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        enterPrimaryPasswordGenerateTokenPasswordFieldLabel.setText("Primary Password:");
        enterPrimaryPasswordGenerateTokenPasswordFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        enterPrimaryPasswordGenerateTokenPasswordFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        enterPrimaryPasswordGenerateTokenPasswordFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        enterPrimaryPasswordGenerateTokenOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        enterPrimaryPasswordGenerateTokenOKButton.setMnemonic('O');
        enterPrimaryPasswordGenerateTokenOKButton.setText("OK");
        enterPrimaryPasswordGenerateTokenOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordGenerateTokenOKButtonActionPerformed(evt);
            }
        });

        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.setText("Show");
        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordGenerateTokenShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        enterPrimaryPasswordGenerateTokenBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/primaryPasswordTokenBG.png"))); // NOI18N
        enterPrimaryPasswordGenerateTokenBackgroundLabel.setToolTipText(null);

        enterPrimaryPasswordGenerateTokenCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        enterPrimaryPasswordGenerateTokenCancelButton.setMnemonic('C');
        enterPrimaryPasswordGenerateTokenCancelButton.setText("Cancel");
        enterPrimaryPasswordGenerateTokenCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordGenerateTokenCancelButtonActionPerformed(evt);
            }
        });

        enterPrimaryPasswordGenerateTokenSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        javax.swing.GroupLayout enterPrimaryPasswordGenerateTokenFrameLayout = new javax.swing.GroupLayout(enterPrimaryPasswordGenerateTokenFrame.getContentPane());
        enterPrimaryPasswordGenerateTokenFrame.getContentPane().setLayout(enterPrimaryPasswordGenerateTokenFrameLayout);
        enterPrimaryPasswordGenerateTokenFrameLayout.setHorizontalGroup(
            enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createSequentialGroup()
                .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(enterPrimaryPasswordGenerateTokenSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(enterPrimaryPasswordGenerateTokenBackgroundLabel))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createSequentialGroup()
                .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createSequentialGroup()
                        .addGap(367, 367, 367)
                        .addComponent(enterPrimaryPasswordGenerateTokenOKButton)
                        .addGap(5, 5, 5)
                        .addComponent(enterPrimaryPasswordGenerateTokenCancelButton))
                    .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(enterPrimaryPasswordGenerateTokenPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(enterPrimaryPasswordGenerateTokenPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(enterPrimaryPasswordGenerateTokenShowPhraseCheckBox)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        enterPrimaryPasswordGenerateTokenFrameLayout.setVerticalGroup(
            enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, enterPrimaryPasswordGenerateTokenFrameLayout.createSequentialGroup()
                .addComponent(enterPrimaryPasswordGenerateTokenBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(enterPrimaryPasswordGenerateTokenPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(enterPrimaryPasswordGenerateTokenPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(enterPrimaryPasswordGenerateTokenShowPhraseCheckBox))
                .addGap(10, 10, 10)
                .addComponent(enterPrimaryPasswordGenerateTokenSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(enterPrimaryPasswordGenerateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enterPrimaryPasswordGenerateTokenOKButton)
                    .addComponent(enterPrimaryPasswordGenerateTokenCancelButton))
                .addGap(10, 10, 10))
        );

        enterPrimaryPasswordOfflineSigningFrame.setTitle("Open Offline Signing");
        enterPrimaryPasswordOfflineSigningFrame.setAlwaysOnTop(true);
        enterPrimaryPasswordOfflineSigningFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        enterPrimaryPasswordOfflineSigningFrame.setResizable(false);

        enterPrimaryPasswordOfflineSigningPasswordField.setColumns(50);
        enterPrimaryPasswordOfflineSigningPasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        enterPrimaryPasswordOfflineSigningPasswordField.setMaximumSize(new java.awt.Dimension(290, 30));
        enterPrimaryPasswordOfflineSigningPasswordField.setMinimumSize(new java.awt.Dimension(290, 30));
        enterPrimaryPasswordOfflineSigningPasswordField.setPreferredSize(new java.awt.Dimension(290, 30));

        enterPrimaryPasswordOfflineSigningPasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        enterPrimaryPasswordOfflineSigningPasswordFieldLabel.setText("Primary Password:");
        enterPrimaryPasswordOfflineSigningPasswordFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        enterPrimaryPasswordOfflineSigningPasswordFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        enterPrimaryPasswordOfflineSigningPasswordFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        enterPrimaryPasswordOfflineSigningOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        enterPrimaryPasswordOfflineSigningOKButton.setMnemonic('O');
        enterPrimaryPasswordOfflineSigningOKButton.setText("OK");
        enterPrimaryPasswordOfflineSigningOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordOfflineSigningOKButtonActionPerformed(evt);
            }
        });

        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.setText("Show");
        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordOfflineSigningShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        enterPrimaryPasswordOfflineSigningBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/primaryPasswordOfflineSigningBG.png"))); // NOI18N
        enterPrimaryPasswordOfflineSigningBackgroundLabel.setToolTipText(null);

        enterPrimaryPasswordOfflineSigningCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        enterPrimaryPasswordOfflineSigningCancelButton.setMnemonic('C');
        enterPrimaryPasswordOfflineSigningCancelButton.setText("Cancel");
        enterPrimaryPasswordOfflineSigningCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enterPrimaryPasswordOfflineSigningCancelButtonActionPerformed(evt);
            }
        });

        enterPrimaryPasswordOfflineSigningSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        javax.swing.GroupLayout enterPrimaryPasswordOfflineSigningFrameLayout = new javax.swing.GroupLayout(enterPrimaryPasswordOfflineSigningFrame.getContentPane());
        enterPrimaryPasswordOfflineSigningFrame.getContentPane().setLayout(enterPrimaryPasswordOfflineSigningFrameLayout);
        enterPrimaryPasswordOfflineSigningFrameLayout.setHorizontalGroup(
            enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createSequentialGroup()
                .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(enterPrimaryPasswordOfflineSigningSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(enterPrimaryPasswordOfflineSigningBackgroundLabel))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createSequentialGroup()
                .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createSequentialGroup()
                        .addGap(367, 367, 367)
                        .addComponent(enterPrimaryPasswordOfflineSigningOKButton)
                        .addGap(5, 5, 5)
                        .addComponent(enterPrimaryPasswordOfflineSigningCancelButton))
                    .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(enterPrimaryPasswordOfflineSigningPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(enterPrimaryPasswordOfflineSigningPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(enterPrimaryPasswordOfflineSigningShowPhraseCheckBox)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        enterPrimaryPasswordOfflineSigningFrameLayout.setVerticalGroup(
            enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, enterPrimaryPasswordOfflineSigningFrameLayout.createSequentialGroup()
                .addComponent(enterPrimaryPasswordOfflineSigningBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(enterPrimaryPasswordOfflineSigningPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(enterPrimaryPasswordOfflineSigningPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(enterPrimaryPasswordOfflineSigningShowPhraseCheckBox))
                .addGap(10, 10, 10)
                .addComponent(enterPrimaryPasswordOfflineSigningSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(enterPrimaryPasswordOfflineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enterPrimaryPasswordOfflineSigningOKButton)
                    .addComponent(enterPrimaryPasswordOfflineSigningCancelButton))
                .addGap(10, 10, 10))
        );

        accountDetailsFrame.setTitle("Account Details");
        accountDetailsFrame.setAlwaysOnTop(true);
        accountDetailsFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        accountDetailsFrame.setResizable(false);

        accountDetailsPhraseField.setEditable(false);
        accountDetailsPhraseField.setColumns(30);
        accountDetailsPhraseField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        accountDetailsPhraseField.setMaximumSize(new java.awt.Dimension(290, 30));
        accountDetailsPhraseField.setMinimumSize(new java.awt.Dimension(290, 30));
        accountDetailsPhraseField.setPreferredSize(new java.awt.Dimension(290, 30));
        accountDetailsPhraseField.setVerifyInputWhenFocusTarget(false);

        accountDetailsPhraseLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        accountDetailsPhraseLabel.setText("Private Phrase:");
        accountDetailsPhraseLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        accountDetailsPhraseLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        accountDetailsPhraseLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        accountDetailsShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        accountDetailsShowPhraseCheckBox.setText("Show");
        accountDetailsShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        accountDetailsShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        accountDetailsShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountDetailsShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        accountDetailsPublicKeyLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        accountDetailsPublicKeyLabel.setText("Public Key:");
        accountDetailsPublicKeyLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        accountDetailsPublicKeyLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        accountDetailsPublicKeyLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        accountDetailsPublicKeyField.setEditable(false);
        accountDetailsPublicKeyField.setColumns(12);
        accountDetailsPublicKeyField.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        accountDetailsPublicKeyField.setMaximumSize(new java.awt.Dimension(290, 30));
        accountDetailsPublicKeyField.setMinimumSize(new java.awt.Dimension(290, 30));
        accountDetailsPublicKeyField.setPreferredSize(new java.awt.Dimension(290, 30));

        accountDetailsCopyButton1.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        accountDetailsCopyButton1.setForeground(new java.awt.Color(0, 102, 204));
        accountDetailsCopyButton1.setText("Copy Private Phrase");
        accountDetailsCopyButton1.setToolTipText(null);
        accountDetailsCopyButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountDetailsCopyButton1ActionPerformed(evt);
            }
        });

        accountDetailsCopyButton2.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        accountDetailsCopyButton2.setText("Copy Public Key");
        accountDetailsCopyButton2.setToolTipText(null);
        accountDetailsCopyButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountDetailsCopyButton2ActionPerformed(evt);
            }
        });

        accountDetailsjProgressBar.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        accountDetailsjProgressBar.setMaximumSize(new java.awt.Dimension(515, 20));
        accountDetailsjProgressBar.setMinimumSize(new java.awt.Dimension(515, 20));
        accountDetailsjProgressBar.setPreferredSize(new java.awt.Dimension(515, 20));
        accountDetailsjProgressBar.setString("");
        accountDetailsjProgressBar.setStringPainted(true);

        accountDetailsLabelLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        accountDetailsLabelLabel.setText("Label:");
        accountDetailsLabelLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        accountDetailsLabelLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        accountDetailsLabelLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        accountDetailsBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/accountDetailBG.png"))); // NOI18N
        accountDetailsBackgroundLabel.setToolTipText(null);

        accountDetailsOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        accountDetailsOKButton.setMnemonic('O');
        accountDetailsOKButton.setText("OK");
        accountDetailsOKButton.setToolTipText(null);
        accountDetailsOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountDetailsOKButtonActionPerformed(evt);
            }
        });

        accountDetailsjSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        accountDetailsjSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        accountDetailsjSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        try {
            accountDetailsLabelField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("***************")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        accountDetailsLabelField.setMaximumSize(new java.awt.Dimension(290, 30));
        accountDetailsLabelField.setMinimumSize(new java.awt.Dimension(290, 30));
        accountDetailsLabelField.setPreferredSize(new java.awt.Dimension(290, 30));

        javax.swing.GroupLayout accountDetailsFrameLayout = new javax.swing.GroupLayout(accountDetailsFrame.getContentPane());
        accountDetailsFrame.getContentPane().setLayout(accountDetailsFrameLayout);
        accountDetailsFrameLayout.setHorizontalGroup(
            accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountDetailsFrameLayout.createSequentialGroup()
                .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, accountDetailsFrameLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(accountDetailsPhraseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(accountDetailsPublicKeyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(accountDetailsLabelLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(accountDetailsFrameLayout.createSequentialGroup()
                                .addComponent(accountDetailsPhraseField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(accountDetailsShowPhraseCheckBox))
                            .addComponent(accountDetailsPublicKeyField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(accountDetailsLabelField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(accountDetailsBackgroundLabel, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(accountDetailsjSeparator, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(accountDetailsjProgressBar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, accountDetailsFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, accountDetailsFrameLayout.createSequentialGroup()
                        .addComponent(accountDetailsClearLabel)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, accountDetailsFrameLayout.createSequentialGroup()
                        .addComponent(accountDetailsCopyButton1)
                        .addGap(5, 5, 5)
                        .addComponent(accountDetailsCopyButton2)
                        .addGap(5, 5, 5)
                        .addComponent(accountDetailsOKButton)
                        .addGap(32, 32, 32))))
        );
        accountDetailsFrameLayout.setVerticalGroup(
            accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(accountDetailsFrameLayout.createSequentialGroup()
                .addComponent(accountDetailsBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(accountDetailsShowPhraseCheckBox)
                    .addGroup(accountDetailsFrameLayout.createSequentialGroup()
                        .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(accountDetailsPhraseField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(accountDetailsPhraseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(accountDetailsPublicKeyField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(accountDetailsPublicKeyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(accountDetailsLabelField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(accountDetailsLabelLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(accountDetailsjSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(accountDetailsFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(accountDetailsOKButton)
                    .addComponent(accountDetailsCopyButton1)
                    .addComponent(accountDetailsCopyButton2))
                .addGap(10, 10, 10)
                .addComponent(accountDetailsjProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(accountDetailsClearLabel)
                .addGap(63, 63, 63))
        );

        generateTokenFrame.setTitle("Account Details");
        generateTokenFrame.setAlwaysOnTop(true);
        generateTokenFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        generateTokenFrame.setResizable(false);

        generateTokenCopyButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        generateTokenCopyButton.setText("Copy Token");
        generateTokenCopyButton.setToolTipText(null);
        generateTokenCopyButton.setActionCommand("Copy To Clipboard");
        generateTokenCopyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateTokenCopyButtonActionPerformed(evt);
            }
        });

        generateTokenTokenDataField.setColumns(12);
        generateTokenTokenDataField.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        generateTokenTokenDataField.setText("Text Or Website");
        generateTokenTokenDataField.setPreferredSize(new java.awt.Dimension(290, 30));

        generateTokenTokenValueLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        generateTokenTokenValueLabel.setText("Token Data:");
        generateTokenTokenValueLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        generateTokenTokenValueLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        generateTokenTokenValueLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        generateTokenTokenKeyField.setEditable(false);
        generateTokenTokenKeyField.setColumns(12);
        generateTokenTokenKeyField.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        generateTokenTokenKeyField.setText("No Token Generated Yet");
        generateTokenTokenKeyField.setPreferredSize(new java.awt.Dimension(290, 30));

        generateTokenTokenKeyLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        generateTokenTokenKeyLabel.setText("Token:");
        generateTokenTokenKeyLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        generateTokenTokenKeyLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        generateTokenTokenKeyLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        generateTokenGenerateTokenButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        generateTokenGenerateTokenButton.setMnemonic('G');
        generateTokenGenerateTokenButton.setText("Generate Token");
        generateTokenGenerateTokenButton.setToolTipText(null);
        generateTokenGenerateTokenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateTokenGenerateTokenButtonActionPerformed(evt);
            }
        });

        generateTokenBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/generateTokenBG.png"))); // NOI18N
        generateTokenBackgroundLabel.setToolTipText(null);

        generateTokenOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        generateTokenOKButton.setMnemonic('O');
        generateTokenOKButton.setText("OK");
        generateTokenOKButton.setToolTipText(null);
        generateTokenOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateTokenOKButtonActionPerformed(evt);
            }
        });

        generateTokenSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        generateTokenSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        generateTokenSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        javax.swing.GroupLayout generateTokenFrameLayout = new javax.swing.GroupLayout(generateTokenFrame.getContentPane());
        generateTokenFrame.getContentPane().setLayout(generateTokenFrameLayout);
        generateTokenFrameLayout.setHorizontalGroup(
            generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateTokenFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, generateTokenFrameLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(generateTokenClearLabel))
                    .addGroup(generateTokenFrameLayout.createSequentialGroup()
                        .addGroup(generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(generateTokenFrameLayout.createSequentialGroup()
                                .addComponent(generateTokenTokenKeyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(generateTokenTokenKeyField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(generateTokenFrameLayout.createSequentialGroup()
                                .addComponent(generateTokenTokenValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(23, 23, 23)
                                .addComponent(generateTokenTokenDataField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, generateTokenFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(generateTokenCopyButton)
                .addGap(5, 5, 5)
                .addComponent(generateTokenGenerateTokenButton)
                .addGap(5, 5, 5)
                .addComponent(generateTokenOKButton)
                .addGap(32, 32, 32))
            .addGroup(generateTokenFrameLayout.createSequentialGroup()
                .addGroup(generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(generateTokenBackgroundLabel)
                    .addComponent(generateTokenSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        generateTokenFrameLayout.setVerticalGroup(
            generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(generateTokenFrameLayout.createSequentialGroup()
                .addComponent(generateTokenBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generateTokenTokenValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generateTokenTokenDataField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generateTokenTokenKeyField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(generateTokenTokenKeyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(generateTokenSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(generateTokenFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(generateTokenGenerateTokenButton)
                    .addComponent(generateTokenOKButton)
                    .addComponent(generateTokenCopyButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(generateTokenClearLabel)
                .addGap(37, 37, 37))
        );

        offlineSigningFrame.setTitle("Offline Signing");
        offlineSigningFrame.setAlwaysOnTop(true);
        offlineSigningFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        offlineSigningFrame.setResizable(false);

        offlineSigningRecepientLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        offlineSigningRecepientLabel.setText("Recipient:");
        offlineSigningRecepientLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        offlineSigningRecepientLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        offlineSigningRecepientLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        offlineSigningAmountLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        offlineSigningAmountLabel.setText("Amount:");
        offlineSigningAmountLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        offlineSigningAmountLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        offlineSigningAmountLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        offlineSigningSignButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        offlineSigningSignButton.setMnemonic('S');
        offlineSigningSignButton.setText("Sign And Save");
        offlineSigningSignButton.setToolTipText(null);
        offlineSigningSignButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                offlineSigningSignButtonActionPerformed(evt);
            }
        });

        offlineSigningBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/offlineSigningBG.png"))); // NOI18N
        offlineSigningBackgroundLabel.setToolTipText(null);

        offlineSigningOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        offlineSigningOKButton.setMnemonic('O');
        offlineSigningOKButton.setText("Cancel");
        offlineSigningOKButton.setToolTipText(null);
        offlineSigningOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                offlineSigningOKButtonActionPerformed(evt);
            }
        });

        offlineSigningSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        offlineSigningSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        offlineSigningSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        try {
            offlineSigningRecepientTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("***-****-****-****-*****")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        offlineSigningRecepientTextField.setMaximumSize(new java.awt.Dimension(290, 30));
        offlineSigningRecepientTextField.setMinimumSize(new java.awt.Dimension(290, 30));
        offlineSigningRecepientTextField.setPreferredSize(new java.awt.Dimension(290, 30));

        offlineSigningFeeLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        offlineSigningFeeLabel.setText("Fee:");
        offlineSigningFeeLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        offlineSigningFeeLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        offlineSigningFeeLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        offlineSigningFeeSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 999, 1));
        offlineSigningFeeSpinner.setMaximumSize(new java.awt.Dimension(85, 25));
        offlineSigningFeeSpinner.setMinimumSize(new java.awt.Dimension(85, 25));
        offlineSigningFeeSpinner.setPreferredSize(new java.awt.Dimension(85, 25));

        offlineSigningDeadlineLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        offlineSigningDeadlineLabel.setText("Deadline:");
        offlineSigningDeadlineLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        offlineSigningDeadlineLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        offlineSigningDeadlineLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        offlineSigningDeadlineSpinner.setModel(new javax.swing.SpinnerNumberModel(500, 100, 1440, 100));
        offlineSigningDeadlineSpinner.setMaximumSize(new java.awt.Dimension(85, 25));
        offlineSigningDeadlineSpinner.setMinimumSize(new java.awt.Dimension(85, 25));
        offlineSigningDeadlineSpinner.setPreferredSize(new java.awt.Dimension(85, 25));

        offlineSigningAmountTextField.setMaximumSize(new java.awt.Dimension(290, 30));
        offlineSigningAmountTextField.setMinimumSize(new java.awt.Dimension(290, 30));
        offlineSigningAmountTextField.setPreferredSize(new java.awt.Dimension(290, 30));

        javax.swing.GroupLayout offlineSigningFrameLayout = new javax.swing.GroupLayout(offlineSigningFrame.getContentPane());
        offlineSigningFrame.getContentPane().setLayout(offlineSigningFrameLayout);
        offlineSigningFrameLayout.setHorizontalGroup(
            offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, offlineSigningFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(offlineSigningSignButton)
                .addGap(5, 5, 5)
                .addComponent(offlineSigningOKButton)
                .addGap(32, 32, 32))
            .addGroup(offlineSigningFrameLayout.createSequentialGroup()
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(offlineSigningBackgroundLabel)
                    .addComponent(offlineSigningSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(offlineSigningFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(offlineSigningFrameLayout.createSequentialGroup()
                        .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(offlineSigningAmountLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(offlineSigningFeeLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(offlineSigningDeadlineLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(offlineSigningFeeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(offlineSigningDeadlineSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(offlineSigningAmountTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, offlineSigningFrameLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(offlineSigningClearLabel)
                        .addGap(10, 10, 10))
                    .addGroup(offlineSigningFrameLayout.createSequentialGroup()
                        .addComponent(offlineSigningRecepientLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addComponent(offlineSigningRecepientTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        offlineSigningFrameLayout.setVerticalGroup(
            offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(offlineSigningFrameLayout.createSequentialGroup()
                .addComponent(offlineSigningBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(offlineSigningRecepientLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(offlineSigningRecepientTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(offlineSigningAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(offlineSigningAmountTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(offlineSigningFeeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(offlineSigningFeeSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(offlineSigningDeadlineLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(offlineSigningDeadlineSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(offlineSigningSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(offlineSigningFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(offlineSigningSignButton)
                    .addComponent(offlineSigningOKButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(offlineSigningClearLabel)
                .addGap(37, 37, 37))
        );

        showPreferencesFrame.setTitle("Preferences");
        showPreferencesFrame.setAlwaysOnTop(true);
        showPreferencesFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        showPreferencesFrame.setResizable(false);

        showPreferencesWarningsLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesWarningsLabel.setText("Show Warnings:");
        showPreferencesWarningsLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        showPreferencesWarningsLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        showPreferencesWarningsLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        showPreferencesWarningsCheckbox.setSelected(true);
        showPreferencesWarningsCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesWarningsCheckboxActionPerformed(evt);
            }
        });

        showPreferencesClearClipboardSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesClearClipboardSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 20, 1));
        showPreferencesClearClipboardSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesClearClipboardSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesClearClipboardSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesClearClipboardSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesClearClipboardSpinnerStateChanged(evt);
            }
        });

        showPreferencesClearClipboardLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesClearClipboardLabel.setText("Clear Clipboard:");
        showPreferencesClearClipboardLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        showPreferencesClearClipboardLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        showPreferencesClearClipboardLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        showPreferencesPhraseBlocksSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesPhraseBlocksSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 99, 1));
        showPreferencesPhraseBlocksSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesPhraseBlocksSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesPhraseBlocksSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesPhraseBlocksSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesPhraseBlocksSpinnerStateChanged(evt);
            }
        });

        showPreferencesPhraseBlocksLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesPhraseBlocksLabel.setText("Phrase Blocks:");
        showPreferencesPhraseBlocksLabel.setMaximumSize(new java.awt.Dimension(110, 18));
        showPreferencesPhraseBlocksLabel.setMinimumSize(new java.awt.Dimension(110, 18));
        showPreferencesPhraseBlocksLabel.setPreferredSize(new java.awt.Dimension(110, 18));

        showPreferencesRandomGenerationLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesRandomGenerationLabel.setText("Expert Phrase Generation:");
        showPreferencesRandomGenerationLabel.setMaximumSize(new java.awt.Dimension(190, 18));
        showPreferencesRandomGenerationLabel.setMinimumSize(new java.awt.Dimension(190, 18));
        showPreferencesRandomGenerationLabel.setPreferredSize(new java.awt.Dimension(190, 18));

        showPreferencesRandomGenerationCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesRandomGenerationCheckboxActionPerformed(evt);
            }
        });

        showPreferencesLengthMinimumLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesLengthMinimumLabel.setText("Length Min:");
        showPreferencesLengthMinimumLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesLengthMinimumLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesLengthMinimumLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesLengthMinimumMinSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesLengthMinimumMinSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 99, 1));
        showPreferencesLengthMinimumMinSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMinimumMinSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMinimumMinSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMinimumMinSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesLengthMinimumMinSpinnerStateChanged(evt);
            }
        });

        showPreferencesLengthMinimumMaxSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesLengthMinimumMaxSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 99, 1));
        showPreferencesLengthMinimumMaxSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMinimumMaxSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMinimumMaxSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMinimumMaxSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesLengthMinimumMaxSpinnerStateChanged(evt);
            }
        });

        showPreferencesLengthMaximumLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesLengthMaximumLabel.setText("Length Max:");
        showPreferencesLengthMaximumLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesLengthMaximumLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesLengthMaximumLabel.setName(""); // NOI18N
        showPreferencesLengthMaximumLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesLengthMaximumMaxSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesLengthMaximumMaxSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 99, 1));
        showPreferencesLengthMaximumMaxSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMaximumMaxSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMaximumMaxSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMaximumMaxSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesLengthMaximumMaxSpinnerStateChanged(evt);
            }
        });

        showPreferencesLengthMaximumMinSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesLengthMaximumMinSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, 99, 1));
        showPreferencesLengthMaximumMinSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMaximumMinSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMaximumMinSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesLengthMaximumMinSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesLengthMaximumMinSpinnerStateChanged(evt);
            }
        });

        showPreferencesLowerCaseLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesLowerCaseLabel.setText("Lower Case:");
        showPreferencesLowerCaseLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesLowerCaseLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesLowerCaseLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesLowerCaseMinSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesLowerCaseMinSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesLowerCaseMinSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesLowerCaseMinSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesLowerCaseMinSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesLowerCaseMinSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesLowerCaseMinSpinnerStateChanged(evt);
            }
        });

        showPreferencesLowerCaseMaxSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesLowerCaseMaxSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesLowerCaseMaxSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesLowerCaseMaxSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesLowerCaseMaxSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesLowerCaseMaxSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesLowerCaseMaxSpinnerStateChanged(evt);
            }
        });

        showPreferencesUpperCaseLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesUpperCaseLabel.setText("Upper Case:");
        showPreferencesUpperCaseLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesUpperCaseLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesUpperCaseLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesUpperCaseMinSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesUpperCaseMinSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesUpperCaseMinSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesUpperCaseMinSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesUpperCaseMinSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesUpperCaseMinSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesUpperCaseMinSpinnerStateChanged(evt);
            }
        });

        showPreferencesUpperCaseMaxSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesUpperCaseMaxSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesUpperCaseMaxSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesUpperCaseMaxSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesUpperCaseMaxSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesUpperCaseMaxSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesUpperCaseMaxSpinnerStateChanged(evt);
            }
        });

        showPreferencesNumeraryLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesNumeraryLabel.setText("Numerary:");
        showPreferencesNumeraryLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesNumeraryLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesNumeraryLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesNumeraryMinSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesNumeraryMinSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesNumeraryMinSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesNumeraryMinSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesNumeraryMinSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesNumeraryMinSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesNumeraryMinSpinnerStateChanged(evt);
            }
        });

        showPreferencesNumeraryMaxSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesNumeraryMaxSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesNumeraryMaxSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesNumeraryMaxSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesNumeraryMaxSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesNumeraryMaxSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesNumeraryMaxSpinnerStateChanged(evt);
            }
        });

        showPreferencesSpecialCharLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesSpecialCharLabel.setText("Special/ ANSI:");
        showPreferencesSpecialCharLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesSpecialCharLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesSpecialCharLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesSpecialCharMinSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesSpecialCharMinSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesSpecialCharMinSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesSpecialCharMinSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesSpecialCharMinSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesSpecialCharMinSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesSpecialCharMinSpinnerStateChanged(evt);
            }
        });

        showPreferencesSpecialCharMaxSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesSpecialCharMaxSpinner.setModel(new javax.swing.SpinnerNumberModel(0, 0, 99, 1));
        showPreferencesSpecialCharMaxSpinner.setMaximumSize(new java.awt.Dimension(50, 25));
        showPreferencesSpecialCharMaxSpinner.setMinimumSize(new java.awt.Dimension(50, 25));
        showPreferencesSpecialCharMaxSpinner.setPreferredSize(new java.awt.Dimension(50, 25));
        showPreferencesSpecialCharMaxSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesSpecialCharMaxSpinnerStateChanged(evt);
            }
        });

        showPreferencesResetSettingsButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        showPreferencesResetSettingsButton.setMnemonic('D');
        showPreferencesResetSettingsButton.setText("Default Settings");
        showPreferencesResetSettingsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesResetSettingsButtonActionPerformed(evt);
            }
        });

        showPreferencesWhitespaceLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesWhitespaceLabel.setText("Whitespaces:");
        showPreferencesWhitespaceLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesWhitespaceLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesWhitespaceLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesWhitespaceCheckbox.setSelected(true);
        showPreferencesWhitespaceCheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesWhitespaceCheckboxActionPerformed(evt);
            }
        });

        showPreferencesIterationLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesIterationLabel.setText("Iteration");
        showPreferencesIterationLabel.setMaximumSize(new java.awt.Dimension(100, 18));
        showPreferencesIterationLabel.setMinimumSize(new java.awt.Dimension(100, 18));
        showPreferencesIterationLabel.setPreferredSize(new java.awt.Dimension(100, 18));

        showPreferencesIterationSpinner.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesIterationSpinner.setModel(new javax.swing.SpinnerNumberModel(1000, 10, 200000, 100));
        showPreferencesIterationSpinner.setMaximumSize(new java.awt.Dimension(110, 25));
        showPreferencesIterationSpinner.setMinimumSize(new java.awt.Dimension(110, 25));
        showPreferencesIterationSpinner.setPreferredSize(new java.awt.Dimension(110, 25));
        showPreferencesIterationSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                showPreferencesIterationSpinnerStateChanged(evt);
            }
        });

        showPreferenceKeySizesLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferenceKeySizesLabel.setText("Encryption Bit:");
        showPreferenceKeySizesLabel.setMaximumSize(new java.awt.Dimension(110, 18));
        showPreferenceKeySizesLabel.setMinimumSize(new java.awt.Dimension(110, 18));
        showPreferenceKeySizesLabel.setPreferredSize(new java.awt.Dimension(110, 18));

        showPreferencesKeySizeComboBox.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        showPreferencesKeySizeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "128", "256" }));
        showPreferencesKeySizeComboBox.setMaximumSize(new java.awt.Dimension(60, 25));
        showPreferencesKeySizeComboBox.setMinimumSize(new java.awt.Dimension(60, 25));
        showPreferencesKeySizeComboBox.setPreferredSize(new java.awt.Dimension(60, 25));
        showPreferencesKeySizeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesKeySizeComboBoxActionPerformed(evt);
            }
        });

        showPreferecesBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/preferencesBG.png"))); // NOI18N
        showPreferecesBackgroundLabel.setToolTipText(null);

        showPreferencesjSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        showPreferencesjSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        showPreferencesjSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        showPreferencesOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        showPreferencesOKButton.setMnemonic('O');
        showPreferencesOKButton.setText("OK");
        showPreferencesOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesOKButtonActionPerformed(evt);
            }
        });

        showPreferencesANSILabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        showPreferencesANSILabel.setText("High ANSI Chars:");
        showPreferencesANSILabel.setMaximumSize(new java.awt.Dimension(120, 18));
        showPreferencesANSILabel.setMinimumSize(new java.awt.Dimension(120, 18));
        showPreferencesANSILabel.setPreferredSize(new java.awt.Dimension(120, 18));

        showPreferencesANSICheckbox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showPreferencesANSICheckboxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout showPreferencesFrameLayout = new javax.swing.GroupLayout(showPreferencesFrame.getContentPane());
        showPreferencesFrame.getContentPane().setLayout(showPreferencesFrameLayout);
        showPreferencesFrameLayout.setHorizontalGroup(
            showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                .addComponent(showPreferecesBackgroundLabel)
                .addGap(0, 1, Short.MAX_VALUE))
            .addComponent(showPreferencesjSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                                .addComponent(showPreferencesRandomGenerationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(showPreferencesRandomGenerationCheckbox))
                            .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                                .addComponent(showPreferencesWarningsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(showPreferencesWarningsCheckbox))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(showPreferencesNumeraryLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(showPreferencesLengthMinimumLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(showPreferencesLowerCaseLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(showPreferencesPhraseBlocksLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE))
                                    .addComponent(showPreferencesIterationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                            .addComponent(showPreferencesLowerCaseMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(showPreferencesLowerCaseMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                            .addComponent(showPreferencesLengthMinimumMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(showPreferencesLengthMinimumMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                            .addComponent(showPreferencesNumeraryMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(showPreferencesNumeraryMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(showPreferencesIterationSpinner, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(showPreferencesPhraseBlocksSpinner, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(23, 23, 23)
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(showPreferenceKeySizesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                                    .addComponent(showPreferencesANSILabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(showPreferencesANSICheckbox))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, showPreferencesFrameLayout.createSequentialGroup()
                                    .addComponent(showPreferencesClearClipboardLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(showPreferencesClearClipboardSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, showPreferencesFrameLayout.createSequentialGroup()
                                    .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(showPreferencesSpecialCharLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(showPreferencesLengthMaximumLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(showPreferencesUpperCaseLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(showPreferencesWhitespaceLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                                    .addComponent(showPreferencesUpperCaseMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(showPreferencesUpperCaseMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                                    .addComponent(showPreferencesSpecialCharMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(showPreferencesSpecialCharMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                                            .addGap(18, 18, 18)
                                            .addComponent(showPreferencesLengthMaximumMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(showPreferencesLengthMaximumMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(showPreferencesWhitespaceCheckbox, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(showPreferencesKeySizeComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(showPreferencesResetSettingsButton)
                        .addGap(5, 5, 5)
                        .addComponent(showPreferencesOKButton)))
                .addGap(32, 32, 32))
        );
        showPreferencesFrameLayout.setVerticalGroup(
            showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showPreferencesFrameLayout.createSequentialGroup()
                .addComponent(showPreferecesBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(showPreferencesClearClipboardLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(showPreferencesClearClipboardSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(showPreferencesWarningsCheckbox)
                    .addComponent(showPreferencesWarningsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(showPreferencesRandomGenerationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(showPreferencesRandomGenerationCheckbox)
                    .addComponent(showPreferencesANSILabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(showPreferencesANSICheckbox))
                .addGap(12, 12, 12)
                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(showPreferencesPhraseBlocksSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesPhraseBlocksLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesWhitespaceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(showPreferencesLengthMinimumMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLengthMinimumLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLengthMinimumMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLengthMaximumLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLengthMaximumMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLengthMaximumMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(showPreferencesLowerCaseMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLowerCaseMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesLowerCaseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesUpperCaseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesUpperCaseMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesUpperCaseMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(showPreferencesNumeraryMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesNumeraryLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesNumeraryMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesSpecialCharMinSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesSpecialCharMaxSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesSpecialCharLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(showPreferencesWhitespaceCheckbox))
                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(showPreferencesIterationSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferencesIterationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(showPreferenceKeySizesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(showPreferencesFrameLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(showPreferencesKeySizeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addComponent(showPreferencesjSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(showPreferencesFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(showPreferencesResetSettingsButton)
                    .addComponent(showPreferencesOKButton))
                .addGap(10, 10, 10))
        );

        aboutFrame.setTitle("About KeyStash");
        aboutFrame.setAlwaysOnTop(true);
        aboutFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        aboutFrame.setResizable(false);

        aboutBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/aboutBG.png"))); // NOI18N
        aboutBackgroundLabel.setToolTipText(null);

        aboutScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        aboutScrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        aboutScrollPane.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        aboutScrollPane.setMaximumSize(new java.awt.Dimension(470, 92));
        aboutScrollPane.setMinimumSize(new java.awt.Dimension(470, 92));
        aboutScrollPane.setPreferredSize(new java.awt.Dimension(470, 92));

        aboutTable.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        aboutTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Component", "Directory, Status, Version"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        aboutTable.setMaximumSize(new java.awt.Dimension(470, 92));
        aboutTable.setMinimumSize(new java.awt.Dimension(470, 92));
        aboutTable.setPreferredSize(new java.awt.Dimension(470, 92));
        aboutTable.setRowSelectionAllowed(false);
        aboutScrollPane.setViewportView(aboutTable);

        aboutjSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        aboutjSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        aboutjSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        aboutOkButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        aboutOkButton.setMnemonic('O');
        aboutOkButton.setText("OK");
        aboutOkButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutOkButtonActionPerformed(evt);
            }
        });

        aboutKeyStashDevelopersLabel.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        aboutKeyStashDevelopersLabel.setText("Copyright © 2014 - 2015 The KeyStash developers");
        aboutKeyStashDevelopersLabel.setToolTipText(null);

        aboutKeyStashLicenseLabel.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        aboutKeyStashLicenseLabel.setText("This is experimental software. ");
        aboutKeyStashLicenseLabel.setToolTipText(null);

        aboutKeyStashLicenseLinkLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        aboutKeyStashLicenseLinkLabel.setText("file COPYING or http://www.opensource.org/licenses/MIT.");
        aboutKeyStashLicenseLinkLabel.setToolTipText(null);

        aboutKeyStashExperimentelLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        aboutKeyStashExperimentelLabel.setText("Distributed under the MIT/X11 software license, see the accompanying ");
        aboutKeyStashExperimentelLabel.setToolTipText(null);

        aboutKeyStashIncludesSoftwareLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        aboutKeyStashIncludesSoftwareLabel.setText("This product contains software developed by the Nxt cryptocurrency project.");
        aboutKeyStashIncludesSoftwareLabel.setToolTipText(null);

        aboutKeyStashIncludesSoftwareLinkLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        aboutKeyStashIncludesSoftwareLinkLabel.setText("http://www.bitbucket.org/JeanLucPicard/nxt/src");
        aboutKeyStashIncludesSoftwareLinkLabel.setToolTipText(null);

        javax.swing.GroupLayout aboutFrameLayout = new javax.swing.GroupLayout(aboutFrame.getContentPane());
        aboutFrame.getContentPane().setLayout(aboutFrameLayout);
        aboutFrameLayout.setHorizontalGroup(
            aboutFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutFrameLayout.createSequentialGroup()
                .addGroup(aboutFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(aboutBackgroundLabel)
                    .addComponent(aboutjSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, aboutFrameLayout.createSequentialGroup()
                .addGap(437, 437, 437)
                .addComponent(aboutOkButton)
                .addGap(32, 32, 32))
            .addGroup(aboutFrameLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(aboutFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(aboutScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aboutKeyStashDevelopersLabel)
                    .addComponent(aboutKeyStashLicenseLabel)
                    .addComponent(aboutKeyStashLicenseLinkLabel)
                    .addComponent(aboutKeyStashExperimentelLabel)
                    .addComponent(aboutKeyStashIncludesSoftwareLabel)
                    .addComponent(aboutKeyStashIncludesSoftwareLinkLabel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        aboutFrameLayout.setVerticalGroup(
            aboutFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(aboutFrameLayout.createSequentialGroup()
                .addComponent(aboutBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(aboutKeyStashDevelopersLabel)
                .addGap(12, 12, 12)
                .addComponent(aboutKeyStashLicenseLabel)
                .addGap(12, 12, 12)
                .addComponent(aboutKeyStashExperimentelLabel)
                .addGap(6, 6, 6)
                .addComponent(aboutKeyStashLicenseLinkLabel)
                .addGap(12, 12, 12)
                .addComponent(aboutKeyStashIncludesSoftwareLabel)
                .addGap(6, 6, 6)
                .addComponent(aboutKeyStashIncludesSoftwareLinkLabel)
                .addGap(10, 10, 10)
                .addComponent(aboutScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(aboutjSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(aboutOkButton)
                .addGap(10, 10, 10))
        );

        encryptDatabaseFrame.setTitle("Encrypt Wallet File");
        encryptDatabaseFrame.setAlwaysOnTop(true);
        encryptDatabaseFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        encryptDatabaseFrame.setResizable(false);

        encryptDatabasePasswordField.setColumns(50);
        encryptDatabasePasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        encryptDatabasePasswordField.setMaximumSize(new java.awt.Dimension(290, 30));
        encryptDatabasePasswordField.setMinimumSize(new java.awt.Dimension(290, 30));
        encryptDatabasePasswordField.setPreferredSize(new java.awt.Dimension(290, 30));
        encryptDatabasePasswordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                encryptDatabasePasswordFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                encryptDatabasePasswordFieldKeyTyped(evt);
            }
        });

        encryptDatabasePasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        encryptDatabasePasswordFieldLabel.setText("Master Password:");
        encryptDatabasePasswordFieldLabel.setMaximumSize(new java.awt.Dimension(135, 18));
        encryptDatabasePasswordFieldLabel.setMinimumSize(new java.awt.Dimension(135, 18));
        encryptDatabasePasswordFieldLabel.setPreferredSize(new java.awt.Dimension(135, 18));

        encryptDatabaseOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseOKButton.setMnemonic('O');
        encryptDatabaseOKButton.setText("OK");
        encryptDatabaseOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encryptDatabaseOKButtonActionPerformed(evt);
            }
        });

        encryptDatabasePasswordConfirmField.setColumns(50);
        encryptDatabasePasswordConfirmField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        encryptDatabasePasswordConfirmField.setMaximumSize(new java.awt.Dimension(290, 30));
        encryptDatabasePasswordConfirmField.setMinimumSize(new java.awt.Dimension(290, 30));
        encryptDatabasePasswordConfirmField.setPreferredSize(new java.awt.Dimension(290, 30));

        encryptDatabasePasswordConfirmFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        encryptDatabasePasswordConfirmFieldLabel.setText("Confirm Password:");
        encryptDatabasePasswordConfirmFieldLabel.setMaximumSize(new java.awt.Dimension(135, 18));
        encryptDatabasePasswordConfirmFieldLabel.setMinimumSize(new java.awt.Dimension(135, 18));
        encryptDatabasePasswordConfirmFieldLabel.setPreferredSize(new java.awt.Dimension(135, 18));

        encryptDatabaseShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        encryptDatabaseShowPhraseCheckBox.setText("Show");
        encryptDatabaseShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        encryptDatabaseShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        encryptDatabaseShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encryptDatabaseShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        encryptDatabasejProgressBar.setFont(new java.awt.Font("Open Sans", 1, 11)); // NOI18N
        encryptDatabasejProgressBar.setBorderPainted(true);
        encryptDatabasejProgressBar.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        encryptDatabasejProgressBar.setMaximumSize(new java.awt.Dimension(290, 20));
        encryptDatabasejProgressBar.setMinimumSize(new java.awt.Dimension(290, 20));
        encryptDatabasejProgressBar.setPreferredSize(new java.awt.Dimension(290, 20));

        encryptDatabasePasswordStrengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabasePasswordStrengthLabel.setText("Complexity:");
        encryptDatabasePasswordStrengthLabel.setMaximumSize(new java.awt.Dimension(135, 18));
        encryptDatabasePasswordStrengthLabel.setMinimumSize(new java.awt.Dimension(135, 18));
        encryptDatabasePasswordStrengthLabel.setPreferredSize(new java.awt.Dimension(135, 18));

        encryptDatabaseEntropyLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseEntropyLabel.setText("Entropy Bits:");
        encryptDatabaseEntropyLabel.setMaximumSize(new java.awt.Dimension(135, 18));
        encryptDatabaseEntropyLabel.setMinimumSize(new java.awt.Dimension(135, 18));
        encryptDatabaseEntropyLabel.setPreferredSize(new java.awt.Dimension(135, 18));

        encryptDatabaseEntropyValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseEntropyValue.setText("0.0");
        encryptDatabaseEntropyValue.setToolTipText(null);
        encryptDatabaseEntropyValue.setMaximumSize(new java.awt.Dimension(290, 18));
        encryptDatabaseEntropyValue.setMinimumSize(new java.awt.Dimension(290, 18));
        encryptDatabaseEntropyValue.setPreferredSize(new java.awt.Dimension(290, 18));

        encryptDatabaseLengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseLengthLabel.setText("Length:");
        encryptDatabaseLengthLabel.setMaximumSize(new java.awt.Dimension(135, 18));
        encryptDatabaseLengthLabel.setMinimumSize(new java.awt.Dimension(135, 18));
        encryptDatabaseLengthLabel.setPreferredSize(new java.awt.Dimension(135, 18));

        encryptDatabaseLengthValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseLengthValue.setText("0");
        encryptDatabaseLengthValue.setMaximumSize(new java.awt.Dimension(290, 18));
        encryptDatabaseLengthValue.setMinimumSize(new java.awt.Dimension(290, 18));
        encryptDatabaseLengthValue.setPreferredSize(new java.awt.Dimension(290, 18));

        encryptDatabaseGeneratePasswordButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseGeneratePasswordButton.setMnemonic('G');
        encryptDatabaseGeneratePasswordButton.setText("Generate Memorable");
        encryptDatabaseGeneratePasswordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encryptDatabaseGeneratePasswordButtonActionPerformed(evt);
            }
        });

        encryptDatabaseBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/encryptWalletBG.png"))); // NOI18N
        encryptDatabaseBackgroundLabel.setToolTipText(null);

        encryptDatabaseSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        encryptDatabaseSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        encryptDatabaseSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        encryptDatabaseCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        encryptDatabaseCancelButton.setMnemonic('C');
        encryptDatabaseCancelButton.setText("Cancel");
        encryptDatabaseCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encryptDatabaseCancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout encryptDatabaseFrameLayout = new javax.swing.GroupLayout(encryptDatabaseFrame.getContentPane());
        encryptDatabaseFrame.getContentPane().setLayout(encryptDatabaseFrameLayout);
        encryptDatabaseFrameLayout.setHorizontalGroup(
            encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(encryptDatabaseBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(encryptDatabaseSeparator, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(encryptDatabaseFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(encryptDatabaseFrameLayout.createSequentialGroup()
                        .addComponent(encryptDatabasePasswordConfirmFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(encryptDatabasePasswordConfirmField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, encryptDatabaseFrameLayout.createSequentialGroup()
                        .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(encryptDatabaseEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(encryptDatabaseLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(encryptDatabasePasswordFieldLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(encryptDatabasePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(encryptDatabaseLengthValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(encryptDatabaseEntropyValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, encryptDatabaseFrameLayout.createSequentialGroup()
                        .addComponent(encryptDatabasePasswordStrengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(encryptDatabasejProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addComponent(encryptDatabaseShowPhraseCheckBox)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, encryptDatabaseFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(encryptDatabaseGeneratePasswordButton)
                .addGap(5, 5, 5)
                .addComponent(encryptDatabaseOKButton)
                .addGap(5, 5, 5)
                .addComponent(encryptDatabaseCancelButton)
                .addGap(32, 32, 32))
        );
        encryptDatabaseFrameLayout.setVerticalGroup(
            encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(encryptDatabaseFrameLayout.createSequentialGroup()
                .addComponent(encryptDatabaseBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(encryptDatabaseShowPhraseCheckBox)
                    .addGroup(encryptDatabaseFrameLayout.createSequentialGroup()
                        .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(encryptDatabasePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(encryptDatabasePasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(encryptDatabasePasswordConfirmField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(encryptDatabasePasswordConfirmFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(encryptDatabasejProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(encryptDatabasePasswordStrengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(encryptDatabaseEntropyValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(encryptDatabaseEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(encryptDatabaseLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(encryptDatabaseLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(encryptDatabaseSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(encryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(encryptDatabaseGeneratePasswordButton)
                    .addComponent(encryptDatabaseOKButton)
                    .addComponent(encryptDatabaseCancelButton))
                .addGap(10, 10, 10))
        );

        newAccountjProgressBar.setBackground(Color.red);
        newAccountjProgressBar.setForeground(Color.red);

        decryptDatabaseFrame.setTitle("Open Wallet File");
        decryptDatabaseFrame.setAlwaysOnTop(true);
        decryptDatabaseFrame.setBackground(new java.awt.Color(255, 255, 255));
        decryptDatabaseFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        decryptDatabaseFrame.setResizable(false);

        decryptDatabasePasswordField.setColumns(50);
        decryptDatabasePasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        decryptDatabasePasswordField.setFocusCycleRoot(true);
        decryptDatabasePasswordField.setMaximumSize(new java.awt.Dimension(290, 30));
        decryptDatabasePasswordField.setMinimumSize(new java.awt.Dimension(290, 30));
        decryptDatabasePasswordField.setPreferredSize(new java.awt.Dimension(290, 30));

        decryptDatabasePasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        decryptDatabasePasswordFieldLabel.setText("Master Password:");
        decryptDatabasePasswordFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        decryptDatabasePasswordFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        decryptDatabasePasswordFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        decryptDatabaseOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        decryptDatabaseOKButton.setMnemonic('O');
        decryptDatabaseOKButton.setText("OK");
        decryptDatabaseOKButton.setFocusable(false);
        decryptDatabaseOKButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        decryptDatabaseOKButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        decryptDatabaseOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decryptDatabaseOKButtonActionPerformed(evt);
            }
        });

        decryptDatabaseShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        decryptDatabaseShowPhraseCheckBox.setText("Show");
        decryptDatabaseShowPhraseCheckBox.setFocusable(false);
        decryptDatabaseShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        decryptDatabaseShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        decryptDatabaseShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decryptDatabaseShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        decryptDatabaseBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/decryptWalletBG.png"))); // NOI18N
        decryptDatabaseBackgroundLabel.setToolTipText(null);

        decryptDatabaseSeparator.setBackground(new java.awt.Color(204, 204, 204));
        decryptDatabaseSeparator.setForeground(new java.awt.Color(204, 204, 204));
        decryptDatabaseSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        decryptDatabaseSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        decryptDatabaseSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        decryptDatabaseCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        decryptDatabaseCancelButton.setMnemonic('C');
        decryptDatabaseCancelButton.setText("Cancel");
        decryptDatabaseCancelButton.setFocusable(false);
        decryptDatabaseCancelButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        decryptDatabaseCancelButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        decryptDatabaseCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decryptDatabaseCancelButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout decryptDatabaseFrameLayout = new javax.swing.GroupLayout(decryptDatabaseFrame.getContentPane());
        decryptDatabaseFrame.getContentPane().setLayout(decryptDatabaseFrameLayout);
        decryptDatabaseFrameLayout.setHorizontalGroup(
            decryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(decryptDatabaseBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(decryptDatabaseSeparator, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(decryptDatabaseFrameLayout.createSequentialGroup()
                .addGroup(decryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(decryptDatabaseFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(decryptDatabasePasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(decryptDatabasePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(decryptDatabaseShowPhraseCheckBox))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, decryptDatabaseFrameLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(decryptDatabaseOKButton)
                        .addGap(5, 5, 5)
                        .addComponent(decryptDatabaseCancelButton)))
                .addGap(32, 32, 32))
        );
        decryptDatabaseFrameLayout.setVerticalGroup(
            decryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(decryptDatabaseFrameLayout.createSequentialGroup()
                .addComponent(decryptDatabaseBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(decryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(decryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(decryptDatabasePasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(decryptDatabasePasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(decryptDatabaseShowPhraseCheckBox))
                .addGap(10, 10, 10)
                .addComponent(decryptDatabaseSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(decryptDatabaseFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(decryptDatabaseOKButton)
                    .addComponent(decryptDatabaseCancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        importFrame.setTitle("Import");
        importFrame.setAlwaysOnTop(true);
        importFrame.setBackground(new java.awt.Color(255, 255, 255));
        importFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        importFrame.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        importFrame.setResizable(false);

        importOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importOKButton.setMnemonic('O');
        importOKButton.setText("OK");
        importOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importOKButtonActionPerformed(evt);
            }
        });

        importGeneratePasswordButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importGeneratePasswordButton.setMnemonic('G');
        importGeneratePasswordButton.setText("Generate Memorable");
        importGeneratePasswordButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importGeneratePasswordButtonActionPerformed(evt);
            }
        });

        importSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        importSeparator.setMinimumSize(new java.awt.Dimension(520, 5));

        importCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importCancelButton.setMnemonic('C');
        importCancelButton.setText("Cancel");
        importCancelButton.setFocusable(false);
        importCancelButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        importCancelButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        importCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importCancelButtonActionPerformed(evt);
            }
        });

        importBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/importBG.png"))); // NOI18N
        importBackgroundLabel.setToolTipText(null);

        importLengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importLengthLabel.setText("Length:");
        importLengthLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        importLengthLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        importLengthLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        importEntropyLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importEntropyLabel.setText("Entropy Bits:");
        importEntropyLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        importEntropyLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        importEntropyLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        importPasswordStrengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importPasswordStrengthLabel.setText("Complexity: ");
        importPasswordStrengthLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        importPasswordStrengthLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        importPasswordStrengthLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        importPasswordConfirmFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        importPasswordConfirmFieldLabel.setText("Confirm Password:");
        importPasswordConfirmFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        importPasswordConfirmFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        importPasswordConfirmFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        importPasswordFieldLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        importPasswordFieldLabel.setText("Primary Password:");
        importPasswordFieldLabel.setMaximumSize(new java.awt.Dimension(140, 18));
        importPasswordFieldLabel.setMinimumSize(new java.awt.Dimension(140, 18));
        importPasswordFieldLabel.setPreferredSize(new java.awt.Dimension(140, 18));

        importPasswordField.setColumns(50);
        importPasswordField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        importPasswordField.setMaximumSize(new java.awt.Dimension(290, 30));
        importPasswordField.setMinimumSize(new java.awt.Dimension(290, 30));
        importPasswordField.setPreferredSize(new java.awt.Dimension(290, 30));
        importPasswordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                importPasswordFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                importPasswordFieldKeyTyped(evt);
            }
        });

        importPasswordConfirmField.setColumns(50);
        importPasswordConfirmField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        importPasswordConfirmField.setMaximumSize(new java.awt.Dimension(290, 30));
        importPasswordConfirmField.setMinimumSize(new java.awt.Dimension(290, 30));
        importPasswordConfirmField.setPreferredSize(new java.awt.Dimension(290, 30));

        importShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        importShowPhraseCheckBox.setText("Show");
        importShowPhraseCheckBox.setFocusable(false);
        importShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        importShowPhraseCheckBox.setMaximumSize(new java.awt.Dimension(37, 30));
        importShowPhraseCheckBox.setMinimumSize(new java.awt.Dimension(37, 30));
        importShowPhraseCheckBox.setPreferredSize(new java.awt.Dimension(37, 30));
        importShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        importShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        importjProgressBar.setFont(new java.awt.Font("Open Sans", 1, 11)); // NOI18N
        importjProgressBar.setBorderPainted(true);
        importjProgressBar.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        importjProgressBar.setMaximumSize(new java.awt.Dimension(290, 20));
        importjProgressBar.setMinimumSize(new java.awt.Dimension(290, 20));
        importjProgressBar.setPreferredSize(new java.awt.Dimension(290, 20));

        importEntropyValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importEntropyValue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        importEntropyValue.setText("0.0");
        importEntropyValue.setToolTipText(null);
        importEntropyValue.setMaximumSize(new java.awt.Dimension(290, 18));
        importEntropyValue.setMinimumSize(new java.awt.Dimension(290, 18));
        importEntropyValue.setPreferredSize(new java.awt.Dimension(290, 18));
        importEntropyValue.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        importLengthValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        importLengthValue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        importLengthValue.setText("0");
        importLengthValue.setMaximumSize(new java.awt.Dimension(290, 18));
        importLengthValue.setMinimumSize(new java.awt.Dimension(290, 18));
        importLengthValue.setPreferredSize(new java.awt.Dimension(290, 18));

        javax.swing.GroupLayout importFrameLayout = new javax.swing.GroupLayout(importFrame.getContentPane());
        importFrame.getContentPane().setLayout(importFrameLayout);
        importFrameLayout.setHorizontalGroup(
            importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(importFrameLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(importGeneratePasswordButton)
                .addGap(5, 5, 5)
                .addComponent(importOKButton)
                .addGap(5, 5, 5)
                .addComponent(importCancelButton)
                .addGap(32, 32, 32))
            .addGroup(importFrameLayout.createSequentialGroup()
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(importSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importBackgroundLabel))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(importFrameLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(importPasswordFieldLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, importFrameLayout.createSequentialGroup()
                        .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(importEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(importLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(importLengthValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(importEntropyValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, importFrameLayout.createSequentialGroup()
                        .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(importPasswordConfirmFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(importPasswordStrengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)
                        .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(importPasswordConfirmField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(importPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(importjProgressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(10, 10, 10)
                .addComponent(importShowPhraseCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );
        importFrameLayout.setVerticalGroup(
            importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, importFrameLayout.createSequentialGroup()
                .addComponent(importBackgroundLabel)
                .addGap(10, 10, 10)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(importPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(importPasswordFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(importShowPhraseCheckBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(importPasswordConfirmField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importPasswordConfirmFieldLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(importPasswordStrengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importjProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(importEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importEntropyValue, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(importLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(importSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(importCancelButton)
                    .addGroup(importFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(importOKButton)
                        .addComponent(importGeneratePasswordButton)))
                .addGap(10, 10, 10))
        );

        newAccountjProgressBar.setBackground(Color.red);
        newAccountjProgressBar.setForeground(Color.red);

        confirmImportFrame.setTitle("Confirm Import");
        confirmImportFrame.setAlwaysOnTop(true);
        confirmImportFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        confirmImportFrame.setResizable(false);

        confirmImportPhraseField.setColumns(50);
        confirmImportPhraseField.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        confirmImportPhraseField.setMaximumSize(new java.awt.Dimension(290, 30));
        confirmImportPhraseField.setMinimumSize(new java.awt.Dimension(290, 30));
        confirmImportPhraseField.setPreferredSize(new java.awt.Dimension(290, 30));
        confirmImportPhraseField.setVerifyInputWhenFocusTarget(false);
        confirmImportPhraseField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                confirmImportPhraseFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                confirmImportPhraseFieldKeyTyped(evt);
            }
        });

        confirmImportPhraseLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmImportPhraseLabel.setText("Private Phrase:");
        confirmImportPhraseLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmImportPhraseLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmImportPhraseLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmImportOKButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmImportOKButton.setMnemonic('O');
        confirmImportOKButton.setText("OK");
        confirmImportOKButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmImportOKButtonActionPerformed(evt);
            }
        });

        confirmImportShowPhraseCheckBox.setFont(new java.awt.Font("Open Sans", 1, 9)); // NOI18N
        confirmImportShowPhraseCheckBox.setText("Show");
        confirmImportShowPhraseCheckBox.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        confirmImportShowPhraseCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        confirmImportShowPhraseCheckBox.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        confirmImportShowPhraseCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmImportShowPhraseCheckBoxActionPerformed(evt);
            }
        });

        confirmImportEntropyLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmImportEntropyLabel.setText("Entropy Bits:");
        confirmImportEntropyLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmImportEntropyLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmImportEntropyLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmImportEntropyValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmImportEntropyValue.setText("0.0");
        confirmImportEntropyValue.setToolTipText(null);
        confirmImportEntropyValue.setMaximumSize(new java.awt.Dimension(290, 18));
        confirmImportEntropyValue.setMinimumSize(new java.awt.Dimension(290, 18));
        confirmImportEntropyValue.setPreferredSize(new java.awt.Dimension(290, 18));

        confirmImportLengthLabel.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmImportLengthLabel.setText("Length:");
        confirmImportLengthLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmImportLengthLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmImportLengthLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmImportLengthValue.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmImportLengthValue.setText("0");
        confirmImportLengthValue.setMaximumSize(new java.awt.Dimension(290, 18));
        confirmImportLengthValue.setMinimumSize(new java.awt.Dimension(290, 18));
        confirmImportLengthValue.setPreferredSize(new java.awt.Dimension(290, 18));

        confirmImportAddressLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmImportAddressLabel.setText("NXT Address:");
        confirmImportAddressLabel.setMaximumSize(new java.awt.Dimension(120, 18));
        confirmImportAddressLabel.setMinimumSize(new java.awt.Dimension(120, 18));
        confirmImportAddressLabel.setPreferredSize(new java.awt.Dimension(120, 18));

        confirmImportShowAddressRsLabel.setFont(new java.awt.Font("Open Sans", 1, 12)); // NOI18N
        confirmImportShowAddressRsLabel.setMaximumSize(new java.awt.Dimension(290, 30));
        confirmImportShowAddressRsLabel.setMinimumSize(new java.awt.Dimension(290, 30));

        confirmImportBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/importConfirmBG.png"))); // NOI18N
        confirmImportBackgroundLabel.setToolTipText(null);

        confirmImportCancelButton.setFont(new java.awt.Font("Open Sans", 0, 11)); // NOI18N
        confirmImportCancelButton.setMnemonic('C');
        confirmImportCancelButton.setText("Cancel");
        confirmImportCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmImportCancelButtonActionPerformed(evt);
            }
        });

        confirmImportSeparator.setMaximumSize(new java.awt.Dimension(520, 5));
        confirmImportSeparator.setMinimumSize(new java.awt.Dimension(520, 5));
        confirmImportSeparator.setName(""); // NOI18N
        confirmImportSeparator.setPreferredSize(new java.awt.Dimension(520, 5));

        javax.swing.GroupLayout confirmImportFrameLayout = new javax.swing.GroupLayout(confirmImportFrame.getContentPane());
        confirmImportFrame.getContentPane().setLayout(confirmImportFrameLayout);
        confirmImportFrameLayout.setHorizontalGroup(
            confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(confirmImportFrameLayout.createSequentialGroup()
                .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(confirmImportBackgroundLabel)
                    .addComponent(confirmImportSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(confirmImportFrameLayout.createSequentialGroup()
                .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(confirmImportFrameLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(confirmImportOKButton)
                        .addGap(5, 5, 5)
                        .addComponent(confirmImportCancelButton))
                    .addGroup(confirmImportFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(confirmImportPhraseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmImportEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmImportLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmImportAddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(confirmImportLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(confirmImportFrameLayout.createSequentialGroup()
                                .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(confirmImportShowAddressRsLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(confirmImportPhraseField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                    .addComponent(confirmImportEntropyValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(10, 10, 10)
                                .addComponent(confirmImportShowPhraseCheckBox)))))
                .addGap(32, 32, 32))
        );
        confirmImportFrameLayout.setVerticalGroup(
            confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, confirmImportFrameLayout.createSequentialGroup()
                .addComponent(confirmImportBackgroundLabel)
                .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(confirmImportFrameLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(confirmImportShowAddressRsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(confirmImportFrameLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(confirmImportAddressLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(confirmImportFrameLayout.createSequentialGroup()
                        .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(confirmImportPhraseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmImportPhraseField, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(confirmImportFrameLayout.createSequentialGroup()
                                .addComponent(confirmImportEntropyLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(confirmImportLengthLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(confirmImportFrameLayout.createSequentialGroup()
                                .addComponent(confirmImportEntropyValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(12, 12, 12)
                                .addComponent(confirmImportLengthValue, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(confirmImportShowPhraseCheckBox))
                .addGap(10, 10, 10)
                .addComponent(confirmImportSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(confirmImportFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmImportOKButton)
                    .addComponent(confirmImportCancelButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        qrCodeFrame.setTitle("QR Code");
        qrCodeFrame.setAlwaysOnTop(true);
        qrCodeFrame.setBackground(new java.awt.Color(255, 255, 255));
        qrCodeFrame.setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        qrCodeFrame.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        qrCodeFrame.setMinimumSize(null);
        qrCodeFrame.setResizable(false);

        qrCodeBackgroundLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/keystash/resources/QR.png"))); // NOI18N
        qrCodeBackgroundLabel.setToolTipText(null);
        qrCodeBackgroundLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        qrCodeBackgroundLabel.setMaximumSize(new java.awt.Dimension(370, 370));
        qrCodeBackgroundLabel.setMinimumSize(new java.awt.Dimension(370, 370));
        qrCodeBackgroundLabel.setPreferredSize(new java.awt.Dimension(370, 370));

        javax.swing.GroupLayout qrCodeFrameLayout = new javax.swing.GroupLayout(qrCodeFrame.getContentPane());
        qrCodeFrame.getContentPane().setLayout(qrCodeFrameLayout);
        qrCodeFrameLayout.setHorizontalGroup(
            qrCodeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(qrCodeBackgroundLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        qrCodeFrameLayout.setVerticalGroup(
            qrCodeFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(qrCodeBackgroundLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("KeyStash");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setResizable(false);

        accountTable.setAutoCreateRowSorter(true);
        accountTable.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        accountTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "#", "Account Address", "Label", "Created"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        accountTable.setAutoscrolls(false);
        accountTable.setRowSelectionAllowed(false);
        accountTable.getTableHeader().setReorderingAllowed(false);
        accountTable.setUpdateSelectionOnSort(false);
        accountTable.setVerifyInputWhenFocusTarget(false);
        accountTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                accountTableMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                accountTableMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                accountTableMouseReleased(evt);
            }
        });
        accountScrollPane.setViewportView(accountTable);
        if (accountTable.getColumnModel().getColumnCount() > 0) {
            accountTable.getColumnModel().getColumn(0).setMinWidth(35);
            accountTable.getColumnModel().getColumn(0).setMaxWidth(35);
            accountTable.getColumnModel().getColumn(1).setMinWidth(190);
            accountTable.getColumnModel().getColumn(1).setMaxWidth(200);
            accountTable.getColumnModel().getColumn(2).setMinWidth(90);
            accountTable.getColumnModel().getColumn(2).setMaxWidth(90);
            accountTable.getColumnModel().getColumn(3).setMinWidth(220);
            accountTable.getColumnModel().getColumn(3).setMaxWidth(220);
        }

        iconPaneAccountjLabel.setBackground(new java.awt.Color(255, 255, 255));
        iconPaneAccountjLabel.setMaximumSize(new java.awt.Dimension(16, 16));
        iconPaneAccountjLabel.setMinimumSize(new java.awt.Dimension(16, 16));
        iconPaneAccountjLabel.setPreferredSize(new java.awt.Dimension(16, 16));

        iconPaneLockjLabel.setMaximumSize(new java.awt.Dimension(16, 16));
        iconPaneLockjLabel.setMinimumSize(new java.awt.Dimension(16, 16));
        iconPaneLockjLabel.setPreferredSize(new java.awt.Dimension(16, 16));

        iconPaneApijLabel.setMaximumSize(new java.awt.Dimension(16, 16));
        iconPaneApijLabel.setMinimumSize(new java.awt.Dimension(16, 16));
        iconPaneApijLabel.setPreferredSize(new java.awt.Dimension(16, 16));

        javax.swing.GroupLayout iconPaneLayout = new javax.swing.GroupLayout(iconPane);
        iconPane.setLayout(iconPaneLayout);
        iconPaneLayout.setHorizontalGroup(
            iconPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(iconPaneLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(iconPaneAccountjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(iconPaneLockjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(iconPaneApijLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        iconPaneLayout.setVerticalGroup(
            iconPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, iconPaneLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(iconPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iconPaneAccountjLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(iconPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(iconPaneLockjLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(iconPaneApijLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        mainMenuBar.setBorder(null);

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");
        fileMenu.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N

        accountMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        accountMenuItem.setMnemonic('N');
        accountMenuItem.setText("New Account");
        accountMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accountMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(accountMenuItem);

        importMenuItem.setMnemonic('I');
        importMenuItem.setText("Import");
        importMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(importMenuItem);

        backupMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        backupMenuItem.setMnemonic('B');
        backupMenuItem.setText("Backup");
        backupMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backupMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(backupMenuItem);
        fileMenu.add(fileMenuSeperator);

        exitMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        exitMenuItem.setMnemonic('E');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        mainMenuBar.add(fileMenu);

        settingsMenu.setMnemonic('S');
        settingsMenu.setText("Settings");
        settingsMenu.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N

        enableApiMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        enableApiMenuItem.setMnemonic('E');
        enableApiMenuItem.setText("Enable Api");
        enableApiMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enableApiMenuItemActionPerformed(evt);
            }
        });
        settingsMenu.add(enableApiMenuItem);

        encryptWalletFileMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        encryptWalletFileMenuItem.setText("Encrypt Wallet");
        encryptWalletFileMenuItem.setActionCommand("Encrypt Wallet File");
        encryptWalletFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encryptWalletFileMenuItemActionPerformed(evt);
            }
        });
        settingsMenu.add(encryptWalletFileMenuItem);

        preferencesMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        preferencesMenuItem.setMnemonic('P');
        preferencesMenuItem.setText("Preferences");
        preferencesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                preferencesMenuItemActionPerformed(evt);
            }
        });
        settingsMenu.add(preferencesMenuItem);

        mainMenuBar.add(settingsMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");
        helpMenu.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N

        startPageMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        startPageMenuItem.setMnemonic('S');
        startPageMenuItem.setText("Start Page");
        startPageMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startPageMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(startPageMenuItem);

        aboutMenuItem.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setText("About KeyStash");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        mainMenuBar.add(helpMenu);

        setJMenuBar(mainMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(accountScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(iconPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(accountScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(iconPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void accountMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountMenuItemActionPerformed

        MenuItem.showNewAccount();

    }//GEN-LAST:event_accountMenuItemActionPerformed

    private void newAccountOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newAccountOKButtonActionPerformed

        try {
            NewAccount.confirmPrimaryPassword();
        } catch (IOException ex) {
            Debug.addLoggMessage("Insert new account failed.");
        }

    }//GEN-LAST:event_newAccountOKButtonActionPerformed

    private void newAccountShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newAccountShowPhraseCheckBoxActionPerformed

        NewAccount.showPhrase();

    }//GEN-LAST:event_newAccountShowPhraseCheckBoxActionPerformed

    private void accountTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountTableMouseClicked

        row = accountTable.rowAtPoint(evt.getPoint());
        int col = accountTable.columnAtPoint(evt.getPoint());
        Object rowValue;

        if (row >= 0 && col >= 0) {
            list.add(row);
            rowValue = accountTable.getValueAt(row, col);

            if (list.size() > 1) {
                tmpList = list.get(list.size() - 2);
            }
        }

    }//GEN-LAST:event_accountTableMouseClicked

    private void accountTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountTableMousePressed

        if (row != null) {
            if (evt.isPopupTrigger()) {
                showDetailPopup.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        }

    }//GEN-LAST:event_accountTableMousePressed

    private void accountTableMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_accountTableMouseReleased

        row = accountTable.rowAtPoint(evt.getPoint());
        int col = accountTable.columnAtPoint(evt.getPoint());
        Object rowValue;

        if (row >= 0 && col >= 0) {
            list.add(row);
            rowValue = accountTable.getValueAt(row, col);

            if (list.size() > 1) {
                tmpList = list.get(list.size() - 2);
            }

        }
        if (row != null) {
            if (evt.isPopupTrigger()) {
                showDetailPopup.show(evt.getComponent(), evt.getX(), evt.getY());
            }
        }

    }//GEN-LAST:event_accountTableMouseReleased

    private void enterPrimaryPasswordAccountDetailsShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordAccountDetailsShowPhraseCheckBoxActionPerformed

        AccountDetails.primayPasswordAccountDetailCheckBox();

    }//GEN-LAST:event_enterPrimaryPasswordAccountDetailsShowPhraseCheckBoxActionPerformed

    private void accountDetailsShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountDetailsShowPhraseCheckBoxActionPerformed

        AccountDetails.showPhrase();

    }//GEN-LAST:event_accountDetailsShowPhraseCheckBoxActionPerformed

    private void newAccountPasswordFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_newAccountPasswordFieldKeyTyped

        Complexity.checkStrength("New");

    }//GEN-LAST:event_newAccountPasswordFieldKeyTyped

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed

        System.exit(0);

    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void accountDetailsCopyButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountDetailsCopyButton1ActionPerformed

        AccountDetails.copyPassPhrase();

    }//GEN-LAST:event_accountDetailsCopyButton1ActionPerformed

    private void accountDetailsCopyButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountDetailsCopyButton2ActionPerformed

        AccountDetails.copyPublicKey();

    }//GEN-LAST:event_accountDetailsCopyButton2ActionPerformed

    private void accountDetailsItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountDetailsItemActionPerformed

        PopUpMenu.doAccountDetails();

    }//GEN-LAST:event_accountDetailsItemActionPerformed


    private void backupMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backupMenuItemActionPerformed

        if (SystemUtils.IS_OS_UNIX) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
            Point newLocation = new Point(middle.x - (550 / 2),
                    middle.y - (470 / 2));
            this.setLocation(newLocation);
        }
        else {
            this.setLocationRelativeTo(null);
        }

        try {
            BackupWallet.backupWalletFile();
        } catch (InterruptedException ex) {
            Debug.addLoggMessage("Cast backup wallet failed.");
        }

    }//GEN-LAST:event_backupMenuItemActionPerformed

    private void newAccountPasswordFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_newAccountPasswordFieldKeyReleased

        NewAccount.phraseLength();

    }//GEN-LAST:event_newAccountPasswordFieldKeyReleased

    private void preferencesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_preferencesMenuItemActionPerformed

        MenuItem.showPreferences();

    }//GEN-LAST:event_preferencesMenuItemActionPerformed

    private void showPreferencesWarningsCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesWarningsCheckboxActionPerformed

        Preferences.checkBox("Warnings");

    }//GEN-LAST:event_showPreferencesWarningsCheckboxActionPerformed

    private void showPreferencesRandomGenerationCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesRandomGenerationCheckboxActionPerformed

        Preferences.randomGenerationCheckBox();

    }//GEN-LAST:event_showPreferencesRandomGenerationCheckboxActionPerformed

    private void showPreferencesClearClipboardSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesClearClipboardSpinnerStateChanged

        Preferences.clearClipBoard();

    }//GEN-LAST:event_showPreferencesClearClipboardSpinnerStateChanged

    private void showPreferencesPhraseBlocksSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesPhraseBlocksSpinnerStateChanged

        Preferences.phraseBlocks();

    }//GEN-LAST:event_showPreferencesPhraseBlocksSpinnerStateChanged

    private void showPreferencesLengthMinimumMinSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesLengthMinimumMinSpinnerStateChanged

        Preferences.spinnerValue("lengthMinimumMin", "Min");

    }//GEN-LAST:event_showPreferencesLengthMinimumMinSpinnerStateChanged

    private void showPreferencesLengthMinimumMaxSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesLengthMinimumMaxSpinnerStateChanged

        Preferences.spinnerValue("lengthMinimumMax", "Max");

    }//GEN-LAST:event_showPreferencesLengthMinimumMaxSpinnerStateChanged

    private void showPreferencesLengthMaximumMinSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesLengthMaximumMinSpinnerStateChanged

        Preferences.spinnerValue("lengthMaximumMin", "Min");

    }//GEN-LAST:event_showPreferencesLengthMaximumMinSpinnerStateChanged

    private void showPreferencesLengthMaximumMaxSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesLengthMaximumMaxSpinnerStateChanged

        Preferences.spinnerValue("lengthMaximumMax", "Max");

    }//GEN-LAST:event_showPreferencesLengthMaximumMaxSpinnerStateChanged

    private void showPreferencesLowerCaseMinSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesLowerCaseMinSpinnerStateChanged

        Preferences.spinnerValue("lowerCaseMin", "Min");

    }//GEN-LAST:event_showPreferencesLowerCaseMinSpinnerStateChanged

    private void showPreferencesLowerCaseMaxSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesLowerCaseMaxSpinnerStateChanged

        Preferences.spinnerValue("lowerCaseMax", "Max");

    }//GEN-LAST:event_showPreferencesLowerCaseMaxSpinnerStateChanged

    private void showPreferencesUpperCaseMinSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesUpperCaseMinSpinnerStateChanged

        Preferences.spinnerValue("upperCaseMin", "Min");

    }//GEN-LAST:event_showPreferencesUpperCaseMinSpinnerStateChanged

    private void showPreferencesUpperCaseMaxSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesUpperCaseMaxSpinnerStateChanged

        Preferences.spinnerValue("upperCaseMax", "Max");

    }//GEN-LAST:event_showPreferencesUpperCaseMaxSpinnerStateChanged

    private void showPreferencesNumeraryMinSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesNumeraryMinSpinnerStateChanged

        Preferences.spinnerValue("numeraryMin", "Min");

    }//GEN-LAST:event_showPreferencesNumeraryMinSpinnerStateChanged

    private void showPreferencesNumeraryMaxSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesNumeraryMaxSpinnerStateChanged

        Preferences.spinnerValue("numeraryMax", "Max");

    }//GEN-LAST:event_showPreferencesNumeraryMaxSpinnerStateChanged

    private void showPreferencesSpecialCharMinSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesSpecialCharMinSpinnerStateChanged

        Preferences.spinnerValue("specialCharMin", "Min");

    }//GEN-LAST:event_showPreferencesSpecialCharMinSpinnerStateChanged

    private void showPreferencesSpecialCharMaxSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesSpecialCharMaxSpinnerStateChanged

        Preferences.spinnerValue("specialCharMax", "Max");

    }//GEN-LAST:event_showPreferencesSpecialCharMaxSpinnerStateChanged

    private void showPreferencesResetSettingsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesResetSettingsButtonActionPerformed

        Reset.resetDefaultSettings();

    }//GEN-LAST:event_showPreferencesResetSettingsButtonActionPerformed

    private void showPreferencesWhitespaceCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesWhitespaceCheckboxActionPerformed

        Preferences.checkBox("Whitespace");

    }//GEN-LAST:event_showPreferencesWhitespaceCheckboxActionPerformed

    private void enableApiMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enableApiMenuItemActionPerformed

        try {
            MenuItem.enableApi();
        } catch (InterruptedException | IOException ex) {
            Debug.addLoggMessage("Start API server failed.");
        }

    }//GEN-LAST:event_enableApiMenuItemActionPerformed

    private void copyAddressItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyAddressItemActionPerformed

        PopUpMenu.doCopyAddress();

    }//GEN-LAST:event_copyAddressItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed

        About.openFrame();

    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void showPreferencesIterationSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_showPreferencesIterationSpinnerStateChanged

        Preferences.iteration();

    }//GEN-LAST:event_showPreferencesIterationSpinnerStateChanged

    private void encryptDatabasePasswordFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_encryptDatabasePasswordFieldKeyReleased

        EncryptWallet.passwordLength();

    }//GEN-LAST:event_encryptDatabasePasswordFieldKeyReleased

    private void encryptDatabasePasswordFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_encryptDatabasePasswordFieldKeyTyped

        Complexity.checkStrength("Encrypt");

    }//GEN-LAST:event_encryptDatabasePasswordFieldKeyTyped

    private void encryptDatabaseOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encryptDatabaseOKButtonActionPerformed

        try {
            EncryptWallet.doEncrypt();
        } catch (IOException ex) {
            Debug.addLoggMessage("Encrypt wallet failed.");
        }

    }//GEN-LAST:event_encryptDatabaseOKButtonActionPerformed

    private void encryptDatabaseShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encryptDatabaseShowPhraseCheckBoxActionPerformed

        EncryptWallet.showPhrase();

    }//GEN-LAST:event_encryptDatabaseShowPhraseCheckBoxActionPerformed

    private void encryptWalletFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encryptWalletFileMenuItemActionPerformed

        EncryptWallet.openFrame();
    }//GEN-LAST:event_encryptWalletFileMenuItemActionPerformed

    private void showPreferencesKeySizeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesKeySizeComboBoxActionPerformed

        Preferences.keySizeComboBox();

    }//GEN-LAST:event_showPreferencesKeySizeComboBoxActionPerformed

    private void newAccountGeneratePasswordButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newAccountGeneratePasswordButtonActionPerformed

        Memorable.generateMemorable("New");

    }//GEN-LAST:event_newAccountGeneratePasswordButtonActionPerformed

    private void encryptDatabaseGeneratePasswordButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encryptDatabaseGeneratePasswordButtonActionPerformed

        Memorable.generateMemorable("Encrypt");

    }//GEN-LAST:event_encryptDatabaseGeneratePasswordButtonActionPerformed

    private void decryptDatabaseShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decryptDatabaseShowPhraseCheckBoxActionPerformed

        DecryptWallet.showPhrase();

    }//GEN-LAST:event_decryptDatabaseShowPhraseCheckBoxActionPerformed

    private void decryptDatabaseOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decryptDatabaseOKButtonActionPerformed

        DecryptWallet.doDecrypt();
    }//GEN-LAST:event_decryptDatabaseOKButtonActionPerformed

    private void decryptDatabaseCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decryptDatabaseCancelButtonActionPerformed

        System.exit(0);

    }//GEN-LAST:event_decryptDatabaseCancelButtonActionPerformed

    private void newAccountCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newAccountCancelButtonActionPerformed

        NewAccount.closeFrame();

    }//GEN-LAST:event_newAccountCancelButtonActionPerformed

    private void confirmNewAccountCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmNewAccountCancelButtonActionPerformed

        ConfirmNewAccount.closeFrame();

    }//GEN-LAST:event_confirmNewAccountCancelButtonActionPerformed

    private void confirmNewAccountSearchAddressToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmNewAccountSearchAddressToggleButtonActionPerformed

        ConfirmNewAccount.searchAddress();

    }//GEN-LAST:event_confirmNewAccountSearchAddressToggleButtonActionPerformed

    private void confirmNewAccountGenerateNewPhraseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmNewAccountGenerateNewPhraseButtonActionPerformed

        try {
            ConfirmNewAccount.generateNewPhrase();
        } catch (IOException ex) {
            Debug.addLoggMessage("Generate new passphrase failed.");
        }

    }//GEN-LAST:event_confirmNewAccountGenerateNewPhraseButtonActionPerformed

    private void confirmNewAccountShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmNewAccountShowPhraseCheckBoxActionPerformed

        ConfirmNewAccount.showPhrase();

    }//GEN-LAST:event_confirmNewAccountShowPhraseCheckBoxActionPerformed

    private void confirmNewAccountOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmNewAccountOKButtonActionPerformed

        try {
            ConfirmNewAccount.saveAccount();
        } catch (IOException | NoSuchAlgorithmException ex) {
            Debug.addLoggMessage("Save account failed.");
        }

    }//GEN-LAST:event_confirmNewAccountOKButtonActionPerformed

    private void encryptDatabaseCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encryptDatabaseCancelButtonActionPerformed

        EncryptWallet.closeFrame();

    }//GEN-LAST:event_encryptDatabaseCancelButtonActionPerformed

    private void enterPrimaryPasswordAccountDetailsCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordAccountDetailsCancelButtonActionPerformed

        AccountDetails.closeFrame();

    }//GEN-LAST:event_enterPrimaryPasswordAccountDetailsCancelButtonActionPerformed

    private void accountDetailsOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accountDetailsOKButtonActionPerformed

        AccountDetails.closeFrameAndSave();

    }//GEN-LAST:event_accountDetailsOKButtonActionPerformed

    private void showPreferencesOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesOKButtonActionPerformed

        Preferences.closeFrame();

    }//GEN-LAST:event_showPreferencesOKButtonActionPerformed

    private void aboutOkButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutOkButtonActionPerformed

        About.closeFrame();

    }//GEN-LAST:event_aboutOkButtonActionPerformed

    private void generateTokenItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateTokenItemActionPerformed

        PopUpMenu.doGenerateTokenPrimaryPasswordFrame();

    }//GEN-LAST:event_generateTokenItemActionPerformed


    private void enterPrimaryPasswordGenerateTokenOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordGenerateTokenOKButtonActionPerformed

        try {
            GenerateToken.showGenerateToken();
        } catch (IOException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_enterPrimaryPasswordGenerateTokenOKButtonActionPerformed

    private void enterPrimaryPasswordGenerateTokenShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordGenerateTokenShowPhraseCheckBoxActionPerformed

        GenerateToken.primayPasswordTokenCheckBox();

    }//GEN-LAST:event_enterPrimaryPasswordGenerateTokenShowPhraseCheckBoxActionPerformed

    private void enterPrimaryPasswordGenerateTokenCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordGenerateTokenCancelButtonActionPerformed

        GenerateToken.primayPasswordTokenCancel();

    }//GEN-LAST:event_enterPrimaryPasswordGenerateTokenCancelButtonActionPerformed

    private void generateTokenCopyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateTokenCopyButtonActionPerformed

        ClipBoard.copyToken();

    }//GEN-LAST:event_generateTokenCopyButtonActionPerformed

    private void generateTokenOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateTokenOKButtonActionPerformed

        GenerateToken.generateTokenCloseWindow();

    }//GEN-LAST:event_generateTokenOKButtonActionPerformed

    private void showPreferencesANSICheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showPreferencesANSICheckboxActionPerformed

        Preferences.checkBox("Ansi");

    }//GEN-LAST:event_showPreferencesANSICheckboxActionPerformed

    private void generateTokenGenerateTokenButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateTokenGenerateTokenButtonActionPerformed

        GenerateToken.generateToken();

    }//GEN-LAST:event_generateTokenGenerateTokenButtonActionPerformed


    private void enterPrimaryPasswordAccountDetailsOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordAccountDetailsOKButtonActionPerformed

        try {
            AccountDetails.showAccountDetails();
        } catch (IOException ex) {
            Debug.addLoggMessage("Show account details failed.");
        }

    }//GEN-LAST:event_enterPrimaryPasswordAccountDetailsOKButtonActionPerformed

    private void offlineSigningItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offlineSigningItemActionPerformed

        PopUpMenu.doOfflineSigning();

    }//GEN-LAST:event_offlineSigningItemActionPerformed

    private void enterPrimaryPasswordOfflineSigningOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordOfflineSigningOKButtonActionPerformed

        try {
            OfflineSigning.showOfflineSigning();
        } catch (IOException ex) {
            Debug.addLoggMessage("Offline signing failed.");
        }

    }//GEN-LAST:event_enterPrimaryPasswordOfflineSigningOKButtonActionPerformed

    private void enterPrimaryPasswordOfflineSigningShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordOfflineSigningShowPhraseCheckBoxActionPerformed

        OfflineSigning.primayPasswordOfflineSigningCheckBox();

    }//GEN-LAST:event_enterPrimaryPasswordOfflineSigningShowPhraseCheckBoxActionPerformed

    private void enterPrimaryPasswordOfflineSigningCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enterPrimaryPasswordOfflineSigningCancelButtonActionPerformed

        OfflineSigning.primayPasswordOfflineSigningCancel();

    }//GEN-LAST:event_enterPrimaryPasswordOfflineSigningCancelButtonActionPerformed

    private void offlineSigningSignButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offlineSigningSignButtonActionPerformed

        if (SystemUtils.IS_OS_UNIX) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
            Point newLocation = new Point(middle.x - (550 / 2),
                    middle.y - (470 / 2));
            this.setLocation(newLocation);
        }
        else {
            this.setLocationRelativeTo(null);
        }

        OfflineSigning.signTransaction();

    }//GEN-LAST:event_offlineSigningSignButtonActionPerformed

    private void offlineSigningOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offlineSigningOKButtonActionPerformed

        OfflineSigning.offlineSigningCloseWindow();

    }//GEN-LAST:event_offlineSigningOKButtonActionPerformed

    private void importMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importMenuItemActionPerformed

        MenuItem.showImport();

    }//GEN-LAST:event_importMenuItemActionPerformed

    private void importOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importOKButtonActionPerformed

        try {
            Import.confirmPrimaryPassword();
        } catch (NoSuchProviderException ex) {
            Debug.addLoggMessage("Import passphrase failed.");
        }

    }//GEN-LAST:event_importOKButtonActionPerformed

    private void importGeneratePasswordButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importGeneratePasswordButtonActionPerformed

        Memorable.generateMemorable("Import");

    }//GEN-LAST:event_importGeneratePasswordButtonActionPerformed

    private void importCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importCancelButtonActionPerformed

        Import.closeFrame();

    }//GEN-LAST:event_importCancelButtonActionPerformed

    private void importPasswordFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_importPasswordFieldKeyReleased

        Import.phraseLength();

    }//GEN-LAST:event_importPasswordFieldKeyReleased

    private void importPasswordFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_importPasswordFieldKeyTyped

        Complexity.checkStrength("Import");

    }//GEN-LAST:event_importPasswordFieldKeyTyped

    private void importShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importShowPhraseCheckBoxActionPerformed

        Import.showPhrase();

    }//GEN-LAST:event_importShowPhraseCheckBoxActionPerformed

    private void confirmImportOKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmImportOKButtonActionPerformed

        try {
            ConfirmImport.saveAccount();
        } catch (NoSuchPaddingException | DecoderException | InvalidKeySpecException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | IOException | NoSuchAlgorithmException ex) {
            Debug.addLoggMessage("Import passphrase failed.");
        }

    }//GEN-LAST:event_confirmImportOKButtonActionPerformed

    private void confirmImportShowPhraseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmImportShowPhraseCheckBoxActionPerformed

        ConfirmImport.showPhrase();

    }//GEN-LAST:event_confirmImportShowPhraseCheckBoxActionPerformed

    private void confirmImportCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmImportCancelButtonActionPerformed

        ConfirmImport.closeFrame();

    }//GEN-LAST:event_confirmImportCancelButtonActionPerformed

    private void confirmImportPhraseFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_confirmImportPhraseFieldKeyReleased

        ConfirmImport.phraseLength();

    }//GEN-LAST:event_confirmImportPhraseFieldKeyReleased

    private void confirmImportPhraseFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_confirmImportPhraseFieldKeyTyped

        Complexity.checkStrengthLive("Confirm Import");

    }//GEN-LAST:event_confirmImportPhraseFieldKeyTyped

    private void showQRCodeItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showQRCodeItemActionPerformed

        try {
            DisplayQR.showQRCode();
        } catch (URISyntaxException | IOException ex) {
            Debug.addLoggMessage("Generate QR-Code failed.");
        }

    }//GEN-LAST:event_showQRCodeItemActionPerformed

    private void confirmNewAccountPhraseFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_confirmNewAccountPhraseFieldKeyReleased

        ConfirmNewAccount.phraseLength();

    }//GEN-LAST:event_confirmNewAccountPhraseFieldKeyReleased

    private void confirmNewAccountPhraseFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_confirmNewAccountPhraseFieldKeyTyped

        Complexity.checkStrengthLive("Confirm New");

    }//GEN-LAST:event_confirmNewAccountPhraseFieldKeyTyped

    private void welcomeOkButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_welcomeOkButtonActionPerformed

        Welcome.closeFrame();

    }//GEN-LAST:event_welcomeOkButtonActionPerformed

    private void welcomeShowOnStartupCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_welcomeShowOnStartupCheckboxActionPerformed

        Welcome.startUpCheckBox();

    }//GEN-LAST:event_welcomeShowOnStartupCheckboxActionPerformed

    private void newAccounIconBackgroundLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newAccounIconBackgroundLabelMouseReleased

        MenuItem.showNewAccount();
        welcomeFrame.setVisible(false);

    }//GEN-LAST:event_newAccounIconBackgroundLabelMouseReleased

    private void importIconBackgroundLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_importIconBackgroundLabelMouseReleased

        MenuItem.showImport();
        welcomeFrame.setVisible(false);

    }//GEN-LAST:event_importIconBackgroundLabelMouseReleased

    private void preferencesIconBackgroundLabelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_preferencesIconBackgroundLabelMouseReleased

        MenuItem.showPreferences();
        welcomeFrame.setVisible(false);

    }//GEN-LAST:event_preferencesIconBackgroundLabelMouseReleased

    private void newAccounIconBackgroundLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newAccounIconBackgroundLabelMouseEntered

        try {
            Welcome.onHover("New", 40.0f);
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_newAccounIconBackgroundLabelMouseEntered

    private void newAccounIconBackgroundLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newAccounIconBackgroundLabelMouseExited

        try {
            Welcome.onHover("New", 0.0f);
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_newAccounIconBackgroundLabelMouseExited

    private void importIconBackgroundLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_importIconBackgroundLabelMouseEntered

        try {
            Welcome.onHover("Import", 40.0f);
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_importIconBackgroundLabelMouseEntered

    private void importIconBackgroundLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_importIconBackgroundLabelMouseExited

        try {
            Welcome.onHover("Import", 0.0f);
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_importIconBackgroundLabelMouseExited

    private void preferencesIconBackgroundLabelMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_preferencesIconBackgroundLabelMouseEntered

        try {
            Welcome.onHover("Preferences", 40.0f);
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_preferencesIconBackgroundLabelMouseEntered

    private void preferencesIconBackgroundLabelMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_preferencesIconBackgroundLabelMouseExited

        try {
            Welcome.onHover("Preferences", 0.0f);
        } catch (IOException | URISyntaxException | InterruptedException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_preferencesIconBackgroundLabelMouseExited

    private void showWarningsOnStartupCheckboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showWarningsOnStartupCheckboxActionPerformed

        Welcome.warningsCheckBox();

    }//GEN-LAST:event_showWarningsOnStartupCheckboxActionPerformed

    private void startPageMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startPageMenuItemActionPerformed

        Welcome.openFrame();

    }//GEN-LAST:event_startPageMenuItemActionPerformed

    @SuppressWarnings("SleepWhileInLoop")
    public static void main(String args[])
            throws Exception {

        if (Constants.keystashd) {

            Connect.walletStructure();

            if (!Convert.stringToBoolean(Config.properties("keystash.enableDaemon"))) {

                System.out.println("Initializing...");

                if (!Connect.isDbEncrypted()) {

                    System.out.println(Options.header);
                    while (true) {
                        Interface.userInput(null);
                    }

                }
                else {

                    while (!Interface.masterPasswordInput(null)) {
                    }
                    System.out.println("Wallet decrypted successfully.");
                    System.out.println(Options.header);
                    while (true && !closeCommandLine) {
                        Interface.userInput(null);
                    }
                }

            }
            else {

                runDaemon = true;
                

                if (Convert.stringToBoolean(Config.properties("keystash.enableAPIServer"))) {

                    String parameterEncryptPassword;
                    String encryptPassword = null;

                    try {

                        ApiRefresh.refreshApi();
                        Api.startWalletServer(true, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                                Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                                Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.apiPassword"),
                                Convert.stringToBoolean(Config.properties("keystash.disableApiPassword"))
                        );
                    System.out.println("KeyStash daemon started succesfully.");    
                    } catch (Exception e) {

                        isWalletEncrypted = true;

                    }

                    if (isWalletEncrypted) {

                        try {

                            parameterEncryptPassword = args[0];
                            encryptPassword = args[1];
                            if ("-walletpassphrase".equals(parameterEncryptPassword) && encryptPassword != null) {

                                Db.connectToEncryptedWallet(encryptPassword.replace(" ", "¡"));

                            }
                        System.out.println("KeyStash daemon started succesfully.");    

                        } catch (ArrayIndexOutOfBoundsException e) {

                            if (encryptPassword == null) {

                                System.out.println("No walletpassphrase specified.");
                                Debug.addLoggMessage("No walletpassphrase specified.");

                            }
                            else {

                                System.out.println("Wrong walletpassphrase.");
                                Debug.addLoggMessage("Wrong walletpassphrase.");
                            }

                        }

                        try {

                            ApiRefresh.refreshApi();
                            Api.startWalletServer(true, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                                    Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                                    Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.apiPassword"),
                                    Convert.stringToBoolean(Config.properties("keystash.disableApiPassword"))
                            );
                        
                        } catch (Exception e) {

                            System.out.println("Cant connect with wallet. Shut down");
                            Debug.addLoggMessage("Cant connect with wallet. Shut down.");

                        }

                    }

                }
                else {

                    System.out.println("KeyStash API is not enabled. Nothing to do here. Shut down.");
                    Debug.addLoggMessage("KeyStash API is not enabled. Nothing to do here. Shut down.");

                }

            }

        }
        else {           
            
            if (Console.isSystemHeadless()) {

                System.err.println("System does not support GUI.");
                Debug.addLoggMessage("System does not support GUI.");
                System.exit(10);

            }

            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {

                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;

                    }
                }
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(KeyStashGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }

            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        new KeyStashGUI().setVisible(true);

                    } catch (SQLException | FontFormatException | IOException ex) {
                        Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            backgroundMenu = 0;
            {
                boolean backgroundMenuLoop = true;
                while (backgroundMenuLoop) {
                    switch (backgroundMenu) {
                        default:

                            Thread.sleep(1000);
                            break;

                        case 1:

                            ClipBoard.clearClipBoard();
                            backgroundMenu = 0;
                            break;

                        case 2:

                            ApiRefresh.refreshApi();
                            
                            if (Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

                                Api.startWalletServer(true, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                                        Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                                        Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.apiPassword"),
                                        Convert.stringToBoolean(Config.properties("keystash.disableApiPassword"))
                                );

                            }
                            else {

                                Api.startWalletServer(true, Constants.apiHost, Constants.apiAllowHost, Constants.apiPort,
                                        Constants.apiResourceBase, Constants.apiEnforcePost, Constants.apiAdminPassword,
                                        Constants.apiEnableAdminPassword);

                            }

                            backgroundMenu = 0;
                            break;

                        case 3:

                            if (Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

                                Api.startWalletServer(false, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                                        Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                                        Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.apiPassword"),
                                        Convert.stringToBoolean(Config.properties("keystash.disableApiPassword"))
                                );

                            }
                            else {

                                Api.startWalletServer(false, Constants.apiHost, Constants.apiAllowHost, Constants.apiPort,
                                        Constants.apiResourceBase, Constants.apiEnforcePost, Constants.apiAdminPassword,
                                        Constants.apiEnableAdminPassword);

                            }

                            backgroundMenu = 0;
                            break;

                        case 4:

                            AddressSearch.searchInAddress();
                            backgroundMenu = 0;
                            break;

                        case 99:
                            break;

                    }
                    if (backgroundMenu == 99) {
                        backgroundMenuLoop = false;
                    }
                }
            }
        }

    }

    private void clearClipBoardWhenExit() {
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                StringSelection stringSelection = new StringSelection("");
                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                clpbrd.setContents(stringSelection, null);
            }
        });
    }

    private void setIconAndFont()
            throws FontFormatException,
            IOException {

        JFrame[] guiObjects = {this, welcomeFrame, newAccountFrame, confirmNewAccountFrame, importFrame, confirmImportFrame, enterPrimaryPasswordAccountDetailsFrame,
            enterPrimaryPasswordGenerateTokenFrame, enterPrimaryPasswordOfflineSigningFrame, accountDetailsFrame, generateTokenFrame, offlineSigningFrame, showPreferencesFrame,
            aboutFrame, encryptDatabaseFrame, decryptDatabaseFrame, qrCodeFrame
        };

        for (JFrame localGuiObjectLOCAL : guiObjects) {

            localGuiObjectLOCAL.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("resources/keystashicon.png")));

        }

        InputStream is2;
        try ( InputStream is1 = this.getClass().getResourceAsStream("resources/OpenSans-Regular.ttf") ) {

            Font.createFont(Font.TRUETYPE_FONT, is1);
            is2 = this.getClass().getResourceAsStream("resources/OpenSans-Bold.ttf");
            Font.createFont(Font.TRUETYPE_FONT, is2);

        }
        is2.close();

        if (SystemUtils.IS_OS_WINDOWS) {

            setFrameSize(guiObjects[0], 550, 470);
            setFrameSize(guiObjects[1], 520, 300);
            setFrameSize(guiObjects[2], 520, 330);
            setFrameSize(guiObjects[3], 520, 330);
            setFrameSize(guiObjects[4], 520, 330);
            setFrameSize(guiObjects[5], 520, 290);
            setFrameSize(guiObjects[6], 520, 200);
            setFrameSize(guiObjects[7], 520, 200);
            setFrameSize(guiObjects[8], 520, 200);
            setFrameSize(guiObjects[9], 520, 295);
            setFrameSize(guiObjects[10], 520, 230);
            setFrameSize(guiObjects[11], 520, 310);
            setFrameSize(guiObjects[12], 520, 400);
            setFrameSize(guiObjects[13], 520, 410);
            setFrameSize(guiObjects[14], 520, 330);
            setFrameSize(guiObjects[15], 520, 200);
            setFrameSize(guiObjects[16], 360, 380);

        }
        else {

            FeelAndLook.setFrameSize(this, 550, 470);
            FeelAndLook.centerFrame(this, 550, 470);

            JFrame[] guiObjectsLinux = {welcomeFrame, newAccountFrame, confirmNewAccountFrame, importFrame, confirmImportFrame, enterPrimaryPasswordAccountDetailsFrame,
                enterPrimaryPasswordGenerateTokenFrame, enterPrimaryPasswordOfflineSigningFrame, accountDetailsFrame, generateTokenFrame, offlineSigningFrame, showPreferencesFrame,
                aboutFrame, encryptDatabaseFrame, decryptDatabaseFrame, qrCodeFrame
            };

            setFrameSize(guiObjectsLinux[11], 550, 370);

            for (JFrame localGuiObjectsLinuxLOCAL : guiObjectsLinux) {

                setFrameSizeLinux(localGuiObjectsLinuxLOCAL);

            }

        }

    }

    public void setFrameSize(JFrame type, int width, int height) {

        type.setPreferredSize(new Dimension(width, height));
        type.setMinimumSize(new Dimension(width, height));
        type.setMaximumSize(new Dimension(width, height));
        type.setLocationRelativeTo(null);
        type.pack();

    }

    public void setFrameSizeLinux(final JFrame type) {

        type.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {

                Dimension d = type.getSize();
                Dimension minD = type.getMinimumSize();
                if (d.width < minD.width) {

                    d.width = minD.width;

                }

                if (d.height < minD.height) {

                    d.height = minD.height;

                }

                type.setSize(d);
            }

        });

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel aboutBackgroundLabel;
    public static javax.swing.JFrame aboutFrame;
    private javax.swing.JLabel aboutKeyStashDevelopersLabel;
    private javax.swing.JLabel aboutKeyStashExperimentelLabel;
    private javax.swing.JLabel aboutKeyStashIncludesSoftwareLabel;
    private javax.swing.JLabel aboutKeyStashIncludesSoftwareLinkLabel;
    private javax.swing.JLabel aboutKeyStashLicenseLabel;
    private javax.swing.JLabel aboutKeyStashLicenseLinkLabel;
    private javax.swing.JMenuItem aboutMenuItem;
    public static javax.swing.JButton aboutOkButton;
    public static javax.swing.JScrollPane aboutScrollPane;
    public static javax.swing.JTable aboutTable;
    private javax.swing.JSeparator aboutjSeparator;
    private javax.swing.JLabel accountDetailsBackgroundLabel;
    private javax.swing.JLabel accountDetailsClearLabel;
    public static javax.swing.JButton accountDetailsCopyButton1;
    public static javax.swing.JButton accountDetailsCopyButton2;
    public static javax.swing.JFrame accountDetailsFrame;
    public static javax.swing.JMenuItem accountDetailsItem;
    public static javax.swing.JFormattedTextField accountDetailsLabelField;
    private javax.swing.JLabel accountDetailsLabelLabel;
    public static javax.swing.JButton accountDetailsOKButton;
    public static javax.swing.JPasswordField accountDetailsPhraseField;
    private javax.swing.JLabel accountDetailsPhraseLabel;
    public static javax.swing.JTextField accountDetailsPublicKeyField;
    private javax.swing.JLabel accountDetailsPublicKeyLabel;
    public static javax.swing.JCheckBox accountDetailsShowPhraseCheckBox;
    public static javax.swing.JProgressBar accountDetailsjProgressBar;
    private javax.swing.JSeparator accountDetailsjSeparator;
    public static javax.swing.JMenuItem accountMenuItem;
    public static javax.swing.JScrollPane accountScrollPane;
    public static javax.swing.JTable accountTable;
    public static javax.swing.JMenuItem backupMenuItem;
    private javax.swing.JSeparator confirmAccountSeparator;
    public static javax.swing.JLabel confirmImportAddressLabel;
    private javax.swing.JLabel confirmImportBackgroundLabel;
    private static javax.swing.JButton confirmImportCancelButton;
    private javax.swing.JLabel confirmImportEntropyLabel;
    public static javax.swing.JLabel confirmImportEntropyValue;
    public static javax.swing.JFrame confirmImportFrame;
    private javax.swing.JLabel confirmImportLengthLabel;
    public static javax.swing.JLabel confirmImportLengthValue;
    public static javax.swing.JButton confirmImportOKButton;
    public static javax.swing.JPasswordField confirmImportPhraseField;
    private javax.swing.JLabel confirmImportPhraseLabel;
    private javax.swing.JSeparator confirmImportSeparator;
    public static javax.swing.JLabel confirmImportShowAddressRsLabel;
    public static javax.swing.JCheckBox confirmImportShowPhraseCheckBox;
    public static javax.swing.JLabel confirmNewAccountAddressLabel;
    public static javax.swing.JLabel confirmNewAccountAttemptLabel;
    public static javax.swing.JLabel confirmNewAccountAttemptValueLabel;
    private javax.swing.JLabel confirmNewAccountBackgroundLabel;
    private static javax.swing.JButton confirmNewAccountCancelButton;
    private javax.swing.JLabel confirmNewAccountEntropyLabel;
    public static javax.swing.JLabel confirmNewAccountEntropyValue;
    public static javax.swing.JFrame confirmNewAccountFrame;
    public static javax.swing.JButton confirmNewAccountGenerateNewPhraseButton;
    public static javax.swing.JFormattedTextField confirmNewAccountIncludeLettersField;
    private javax.swing.JLabel confirmNewAccountLengthLabel;
    public static javax.swing.JLabel confirmNewAccountLengthValue;
    public static javax.swing.JButton confirmNewAccountOKButton;
    public static javax.swing.JPasswordField confirmNewAccountPhraseField;
    private javax.swing.JLabel confirmNewAccountPhraseLabel;
    public static javax.swing.JToggleButton confirmNewAccountSearchAddressToggleButton;
    public static javax.swing.JLabel confirmNewAccountShowAddressRsLabel;
    public static javax.swing.JCheckBox confirmNewAccountShowPhraseCheckBox;
    public static javax.swing.JMenuItem copyAddressItem;
    private javax.swing.JLabel decryptDatabaseBackgroundLabel;
    private javax.swing.JButton decryptDatabaseCancelButton;
    public static javax.swing.JFrame decryptDatabaseFrame;
    private javax.swing.JButton decryptDatabaseOKButton;
    public static javax.swing.JPasswordField decryptDatabasePasswordField;
    private javax.swing.JLabel decryptDatabasePasswordFieldLabel;
    private javax.swing.JSeparator decryptDatabaseSeparator;
    public static javax.swing.JCheckBox decryptDatabaseShowPhraseCheckBox;
    public static javax.swing.JCheckBoxMenuItem enableApiMenuItem;
    private javax.swing.JLabel encryptDatabaseBackgroundLabel;
    private javax.swing.JButton encryptDatabaseCancelButton;
    private javax.swing.JLabel encryptDatabaseEntropyLabel;
    public static javax.swing.JLabel encryptDatabaseEntropyValue;
    public static javax.swing.JFrame encryptDatabaseFrame;
    private javax.swing.JButton encryptDatabaseGeneratePasswordButton;
    private javax.swing.JLabel encryptDatabaseLengthLabel;
    public static javax.swing.JLabel encryptDatabaseLengthValue;
    private javax.swing.JButton encryptDatabaseOKButton;
    public static javax.swing.JPasswordField encryptDatabasePasswordConfirmField;
    private javax.swing.JLabel encryptDatabasePasswordConfirmFieldLabel;
    public static javax.swing.JPasswordField encryptDatabasePasswordField;
    private javax.swing.JLabel encryptDatabasePasswordFieldLabel;
    public static javax.swing.JLabel encryptDatabasePasswordStrengthLabel;
    private javax.swing.JSeparator encryptDatabaseSeparator;
    public static javax.swing.JCheckBox encryptDatabaseShowPhraseCheckBox;
    public static javax.swing.JProgressBar encryptDatabasejProgressBar;
    public static javax.swing.JMenuItem encryptWalletFileMenuItem;
    private javax.swing.JLabel enterPrimaryPasswordAccountDetailsBackgroundLabel;
    private javax.swing.JButton enterPrimaryPasswordAccountDetailsCancelButton;
    public static javax.swing.JFrame enterPrimaryPasswordAccountDetailsFrame;
    private javax.swing.JButton enterPrimaryPasswordAccountDetailsOKButton;
    public static javax.swing.JPasswordField enterPrimaryPasswordAccountDetailsPasswordField;
    private javax.swing.JLabel enterPrimaryPasswordAccountDetailsPasswordFieldLabel;
    private javax.swing.JSeparator enterPrimaryPasswordAccountDetailsSeparator;
    public static javax.swing.JCheckBox enterPrimaryPasswordAccountDetailsShowPhraseCheckBox;
    private javax.swing.JLabel enterPrimaryPasswordGenerateTokenBackgroundLabel;
    private javax.swing.JButton enterPrimaryPasswordGenerateTokenCancelButton;
    public static javax.swing.JFrame enterPrimaryPasswordGenerateTokenFrame;
    private javax.swing.JButton enterPrimaryPasswordGenerateTokenOKButton;
    public static javax.swing.JPasswordField enterPrimaryPasswordGenerateTokenPasswordField;
    private javax.swing.JLabel enterPrimaryPasswordGenerateTokenPasswordFieldLabel;
    private javax.swing.JSeparator enterPrimaryPasswordGenerateTokenSeparator;
    public static javax.swing.JCheckBox enterPrimaryPasswordGenerateTokenShowPhraseCheckBox;
    private javax.swing.JLabel enterPrimaryPasswordOfflineSigningBackgroundLabel;
    private javax.swing.JButton enterPrimaryPasswordOfflineSigningCancelButton;
    public static javax.swing.JFrame enterPrimaryPasswordOfflineSigningFrame;
    private javax.swing.JButton enterPrimaryPasswordOfflineSigningOKButton;
    public static javax.swing.JPasswordField enterPrimaryPasswordOfflineSigningPasswordField;
    private javax.swing.JLabel enterPrimaryPasswordOfflineSigningPasswordFieldLabel;
    private javax.swing.JSeparator enterPrimaryPasswordOfflineSigningSeparator;
    public static javax.swing.JCheckBox enterPrimaryPasswordOfflineSigningShowPhraseCheckBox;
    private javax.swing.JMenuItem exitMenuItem;
    public static javax.swing.JMenu fileMenu;
    private javax.swing.JPopupMenu.Separator fileMenuSeperator;
    private javax.swing.JLabel generateTokenBackgroundLabel;
    private javax.swing.JLabel generateTokenClearLabel;
    private static javax.swing.JButton generateTokenCopyButton;
    public static javax.swing.JFrame generateTokenFrame;
    public static javax.swing.JButton generateTokenGenerateTokenButton;
    public static javax.swing.JMenuItem generateTokenItem;
    public static javax.swing.JButton generateTokenOKButton;
    private javax.swing.JSeparator generateTokenSeparator;
    public static javax.swing.JTextField generateTokenTokenDataField;
    public static javax.swing.JTextField generateTokenTokenKeyField;
    private javax.swing.JLabel generateTokenTokenKeyLabel;
    private javax.swing.JLabel generateTokenTokenValueLabel;
    public static javax.swing.JMenu helpMenu;
    private javax.swing.JPanel iconPane;
    public static javax.swing.JLabel iconPaneAccountjLabel;
    public static javax.swing.JLabel iconPaneApijLabel;
    public static javax.swing.JLabel iconPaneLockjLabel;
    private javax.swing.JLabel importBackgroundLabel;
    private javax.swing.JButton importCancelButton;
    public static javax.swing.JLabel importEntropyLabel;
    public static javax.swing.JLabel importEntropyValue;
    public static javax.swing.JFrame importFrame;
    private javax.swing.JButton importGeneratePasswordButton;
    public static javax.swing.JLabel importIconBackgroundLabel;
    private javax.swing.JLabel importLengthLabel;
    public static javax.swing.JLabel importLengthValue;
    public static javax.swing.JMenuItem importMenuItem;
    private javax.swing.JButton importOKButton;
    public static javax.swing.JPasswordField importPasswordConfirmField;
    private javax.swing.JLabel importPasswordConfirmFieldLabel;
    public static javax.swing.JPasswordField importPasswordField;
    private javax.swing.JLabel importPasswordFieldLabel;
    public static javax.swing.JLabel importPasswordStrengthLabel;
    private javax.swing.JSeparator importSeparator;
    public static javax.swing.JCheckBox importShowPhraseCheckBox;
    public static javax.swing.JProgressBar importjProgressBar;
    public static javax.swing.JMenuBar mainMenuBar;
    public static javax.swing.JLabel newAccounIconBackgroundLabel;
    private javax.swing.JLabel newAccountBackgroundLabel;
    private javax.swing.JButton newAccountCancelButton;
    public static javax.swing.JLabel newAccountEntropyLabel;
    public static javax.swing.JLabel newAccountEntropyValue;
    public static javax.swing.JFrame newAccountFrame;
    private javax.swing.JButton newAccountGeneratePasswordButton;
    private javax.swing.JLabel newAccountLengthLabel;
    public static javax.swing.JLabel newAccountLengthValue;
    private javax.swing.JButton newAccountOKButton;
    public static javax.swing.JPasswordField newAccountPasswordConfirmField;
    public static javax.swing.JPasswordField newAccountPasswordField;
    public static javax.swing.JLabel newAccountPasswordStrengthLabel;
    private javax.swing.JSeparator newAccountSeparator;
    public static javax.swing.JCheckBox newAccountShowPhraseCheckBox;
    public static javax.swing.JProgressBar newAccountjProgressBar;
    private javax.swing.JLabel newAccountlPasswordConfirmFieldLabel;
    private javax.swing.JLabel newAccountlPasswordFieldLabel;
    private javax.swing.JLabel offlineSigningAmountLabel;
    public static javax.swing.JTextField offlineSigningAmountTextField;
    private javax.swing.JLabel offlineSigningBackgroundLabel;
    private javax.swing.JLabel offlineSigningClearLabel;
    private javax.swing.JLabel offlineSigningDeadlineLabel;
    public static javax.swing.JSpinner offlineSigningDeadlineSpinner;
    private javax.swing.JLabel offlineSigningFeeLabel;
    public static javax.swing.JSpinner offlineSigningFeeSpinner;
    public static javax.swing.JFrame offlineSigningFrame;
    public static javax.swing.JMenuItem offlineSigningItem;
    public static javax.swing.JButton offlineSigningOKButton;
    private javax.swing.JLabel offlineSigningRecepientLabel;
    public static javax.swing.JFormattedTextField offlineSigningRecepientTextField;
    private javax.swing.JSeparator offlineSigningSeparator;
    public static javax.swing.JButton offlineSigningSignButton;
    public static javax.swing.JLabel preferencesIconBackgroundLabel;
    public static javax.swing.JMenuItem preferencesMenuItem;
    public static javax.swing.JLabel qrCodeBackgroundLabel;
    public static javax.swing.JFrame qrCodeFrame;
    public static javax.swing.JMenu settingsMenu;
    public static javax.swing.JPopupMenu showDetailPopup;
    private javax.swing.JPopupMenu.Separator showDetailSeparator;
    private javax.swing.JLabel showPreferecesBackgroundLabel;
    private javax.swing.JLabel showPreferenceKeySizesLabel;
    public static javax.swing.JCheckBox showPreferencesANSICheckbox;
    private javax.swing.JLabel showPreferencesANSILabel;
    private javax.swing.JLabel showPreferencesClearClipboardLabel;
    public static javax.swing.JSpinner showPreferencesClearClipboardSpinner;
    public static javax.swing.JFrame showPreferencesFrame;
    private javax.swing.JLabel showPreferencesIterationLabel;
    public static javax.swing.JSpinner showPreferencesIterationSpinner;
    public static javax.swing.JComboBox showPreferencesKeySizeComboBox;
    private javax.swing.JLabel showPreferencesLengthMaximumLabel;
    public static javax.swing.JSpinner showPreferencesLengthMaximumMaxSpinner;
    public static javax.swing.JSpinner showPreferencesLengthMaximumMinSpinner;
    private javax.swing.JLabel showPreferencesLengthMinimumLabel;
    public static javax.swing.JSpinner showPreferencesLengthMinimumMaxSpinner;
    public static javax.swing.JSpinner showPreferencesLengthMinimumMinSpinner;
    private javax.swing.JLabel showPreferencesLowerCaseLabel;
    public static javax.swing.JSpinner showPreferencesLowerCaseMaxSpinner;
    public static javax.swing.JSpinner showPreferencesLowerCaseMinSpinner;
    private javax.swing.JLabel showPreferencesNumeraryLabel;
    public static javax.swing.JSpinner showPreferencesNumeraryMaxSpinner;
    public static javax.swing.JSpinner showPreferencesNumeraryMinSpinner;
    public static javax.swing.JButton showPreferencesOKButton;
    private javax.swing.JLabel showPreferencesPhraseBlocksLabel;
    public static javax.swing.JSpinner showPreferencesPhraseBlocksSpinner;
    public static javax.swing.JCheckBox showPreferencesRandomGenerationCheckbox;
    private javax.swing.JLabel showPreferencesRandomGenerationLabel;
    public static javax.swing.JButton showPreferencesResetSettingsButton;
    private javax.swing.JLabel showPreferencesSpecialCharLabel;
    public static javax.swing.JSpinner showPreferencesSpecialCharMaxSpinner;
    public static javax.swing.JSpinner showPreferencesSpecialCharMinSpinner;
    private javax.swing.JLabel showPreferencesUpperCaseLabel;
    public static javax.swing.JSpinner showPreferencesUpperCaseMaxSpinner;
    public static javax.swing.JSpinner showPreferencesUpperCaseMinSpinner;
    public static javax.swing.JCheckBox showPreferencesWarningsCheckbox;
    private javax.swing.JLabel showPreferencesWarningsLabel;
    public static javax.swing.JCheckBox showPreferencesWhitespaceCheckbox;
    private javax.swing.JLabel showPreferencesWhitespaceLabel;
    private javax.swing.JSeparator showPreferencesjSeparator;
    public static javax.swing.JMenuItem showQRCodeItem;
    public static javax.swing.JCheckBox showWarningsOnStartupCheckbox;
    private javax.swing.JLabel showWarningsOnStartupLabel;
    public static javax.swing.JMenuItem startPageMenuItem;
    private javax.swing.JLabel welcomeBackgroundLabel;
    public static javax.swing.JFrame welcomeFrame;
    public static javax.swing.JButton welcomeOkButton;
    public static javax.swing.JCheckBox welcomeShowOnStartupCheckbox;
    private javax.swing.JLabel welcomeShowOnStartupLabel;
    private javax.swing.JSeparator welcomejSeparator;
    // End of variables declaration//GEN-END:variables

}
