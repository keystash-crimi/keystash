package keystash.command;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import keystash.KeyStashGUI;
import keystash.util.Constants;
import keystash.util.Db;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.SystemUtils;

public class OptionBackup {

    @SuppressWarnings("SleepWhileInLoop")
    public static void backupWallet(String command)
            throws IOException,
            InterruptedException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            SQLException,
            Exception {

        Scanner consoleInput = new Scanner(System.in);
        int backupMenu = 1;
        boolean backupMenuLoop = true;
        String backupPath = null;

        while (backupMenuLoop) {
            switch (backupMenu) {
                default:

                    break;
                case 1:

                    System.out.print("Choose path to save: ");
                    backupPath = consoleInput.nextLine();
                    Path path = Paths.get(backupPath);

                    if ("cancel".equals(backupPath)) {

                        System.out.println(Options.header);
                        backupMenu = 99;
                        break;

                    }
                    else if (backupPath.length() == 0) {

                        System.out.println("Path cant be empty.");
                        backupMenu = 1;
                        break;

                    }
                    else if (!Files.exists(path)) {

                        System.out.println("Path does not exist.");
                        backupMenu = 1;
                        break;

                    }
                    else {

                        backupMenu = 2;
                        break;

                    }

                case 2:

                    System.out.println("Confirm backup [1]Yes, [2]Cancel");

                    String input = consoleInput.next();
                    int inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {

                        backupMenu = 1;
                        System.out.println("The value you have input is not a valid integer.");
                        break;

                    }

                    if ("1".equals(input)) {

                        Db.walletClose();

                        InputStream inStream;
                        OutputStream outStream;
                        File afile;

                        try {

                            if (SystemUtils.IS_OS_LINUX) {
                                
                                afile = new File(Constants.walletPathUnix);
                                
                            }
                            else {
                                
                                afile = new File(Constants.walletPath);
                                
                            }

                            File bfile = new File(backupPath + "/wallet.h2.db");
                            System.out.println(afile);
                            System.out.println(bfile);
                            inStream = new FileInputStream(afile);
                            outStream = new FileOutputStream(bfile);
                            byte[] buffer = new byte[1024];
                            int length;

                            while ((length = inStream.read(buffer)) > 0) {

                                outStream.write(buffer, 0, length);

                            }
                            
                            inStream.close();
                            outStream.close();
                            KeyStashGUI.closeCommandLine = true;
                            System.out.println("Wallet backup saved.");
                            System.out.println("Wallet connection was closed. Please restart the software now.");
                            Thread.sleep(15000);
                            System.exit(0);
                        } catch (IOException e) {
                            KeyStashGUI.closeCommandLine = true;
                            System.out.println("Wallet backup failed.");
                            System.out.println("Wallet connection was closed. Please restart the software now.");
                            Thread.sleep(15000);
                            System.exit(0);      
                            
                        }

                    }
                    else if ("2".equals(input)) {

                        System.out.println(Options.header);
                        backupMenu = 99;
                        break;

                    }

                case 99:
                    break;

            }
            if (backupMenu == 99) {
                backupMenuLoop = false;
            }
        }

    }

}
