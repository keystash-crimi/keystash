package keystash.command;

import java.util.ArrayList;
import static keystash.command.Options.countOptions;
import static keystash.command.Options.listHelp;
import static keystash.command.Options.listOption;

public class OptionHelp {

    public static void help() {

        System.out.printf("%-15s %n", "Usage:");
        System.out.printf("%-15s %n", "[help]");
        System.out.printf("%-15s %n", "[options] <command>");
        System.out.printf("%-15s %n", "[command] <cancel> <start> <ip> <stop> <allow> ");
        System.out.println("");
        System.out.printf("%-15s %n", "Options:");

        ArrayList<String> helpArray = new ArrayList<>();
        for (int i = 0; i < countOptions(); i++) {
            helpArray.add(listOption(i));
            helpArray.add(listHelp(i));
        }

        for (int i = 0; i < helpArray.size();) {
            System.out.printf("%-25s %5s %n", helpArray.get(i), helpArray.get(i + 1));
            i += 2;
        }

    }

}
