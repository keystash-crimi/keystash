package keystash.command;

import java.util.ArrayList;
import static keystash.command.Processor.isWalletEncrypted;
import keystash.util.Constants;

public class Options {

    public static final String header = "KeyStash v" + Constants.keyStashVersion + " command console. Insert -? for help.";
    public static final String option1 = "-?";
    public static final String option1_1 = "Help message.";
    public static final String option2 = "-info";
    public static final String option2_2 = "Status report.";
    public static final String option3 = "-list";
    public static final String option3_3 = "List accounts in wallet file.";
    public static final String option4 = "-new";
    public static final String option4_4 = "Create new account.";
    public static final String option5 = "-import";
    public static final String option5_5 = "Import account.";
    public static final String option6 = "-show";
    public static final String option6_6 = "Show account details";
    public static final String option7 = "-encrypt";
    public static final String option7_7 = "Encrypt wallet file.";
    public static final String option8 = "-sign";
    public static final String option8_8 = "Sign offline transaction.";
    public static final String option9 = "-backup";
    public static final String option9_9 = "Backup wallet file on a specific place.";
    public static final String option10 = "-api";
    public static final String option10_10 = "<start> <ip> <allow> <ip> <*> or <stop> api.";
    public static final String option99 = "-exit";
    public static final String option99_99 = "Stop software and exit.";

    public static String listOption(int integer) {

        ArrayList<String> optionList = new ArrayList<>();
        optionList.add(option1);
        optionList.add(option2);
        optionList.add(option3);
        optionList.add(option4);
        optionList.add(option5);
        optionList.add(option6);
        if (!isWalletEncrypted) {

            optionList.add(option7);

        }
        optionList.add(option8);
        optionList.add(option9);
        optionList.add(option10);
        optionList.add(option99);

        return optionList.get(integer);

    }

    public static int countOptions() {

        ArrayList<String> countOptionsList = new ArrayList<>();
        countOptionsList.add(option1);
        countOptionsList.add(option2);
        countOptionsList.add(option3);
        countOptionsList.add(option4);
        countOptionsList.add(option5);
        countOptionsList.add(option6);
        if (!isWalletEncrypted) {

            countOptionsList.add(option7);

        }

        countOptionsList.add(option8);
        countOptionsList.add(option9);
        countOptionsList.add(option10);
        countOptionsList.add(option99);

        return countOptionsList.size();

    }

    public static String listHelp(int integer) {

        ArrayList<String> helpList = new ArrayList<>();
        helpList.add(option1_1);
        helpList.add(option2_2);
        helpList.add(option3_3);
        helpList.add(option4_4);
        helpList.add(option5_5);
        helpList.add(option6_6);
        if (!isWalletEncrypted) {

            helpList.add(option7_7);

        }
        helpList.add(option8_8);
        helpList.add(option9_9);
        helpList.add(option10_10);
        helpList.add(option99_99);

        return helpList.get(integer);

    }

}
