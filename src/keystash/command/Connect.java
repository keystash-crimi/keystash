package keystash.command;

import java.io.IOException;
import java.sql.SQLException;
import keystash.util.Db;
import keystash.util.Structure;

public class Connect {

    static int countRows;
    static String[] id;
    static String[] cl;
    static String[] rs;
    static String[] pKey;
    static String[] lab;
    static String[] se;
    static String[] raSalt;
    static String[] raIV;
    static String[] sIt;
    static String[] sKey;
    static String[] sA;
    static String[] aT;
    static String[] timestamp;

    public static void walletStructure() {

        try {

            Structure.createWallet();

        } catch (SQLException | IOException e) {

        }

        try {
            Structure.createSettings();
        } catch (Exception e) {

        }

        try {
            Structure.createApiLog();
        } catch (Exception e) {

        }

    }

    public static boolean isDbEncrypted() {

        boolean result = false;
        try {
            Connect.queryWalletData();
        } catch (Exception e) {
            result = true;
            Db.connectToSettings();
            Db.connectToApiLog();
        }
        return result;

    }

    public static boolean connectToDb() {

        boolean result;
        try {
            Db.connectToWallet();
            result = true;
        } catch (Exception e) {
            System.out.println("Wallet file already in use or encrypted.");
            result = false;
        }

        return result;

    }

    public static boolean connectToEncryptedDb(String password) {

        boolean result = true;
        char c = '¡';
        Db.connectToEncryptedWallet(String.valueOf(password).replace(' ', c));

        try {
            Connect.queryWalletData();
        } catch (Exception e) {
            result = false;
        }

        if (result) {

            Processor.isWalletEncrypted = true;

        }

        return result;

    }

    public static void getWalletData(int rows, String[] account_id, String[] addressClassic, String[] addressRs, String[] publicKey,
            String[] label, String[] secret, String[] randomSalt, String[] randomIV, String[] secretIterations, String[] secretKeySize,
            String[] shareAccount, String[] accountType, String[] createdTimestamp) {

        countRows = rows;
        id = account_id;
        cl = addressClassic;
        rs = addressRs;
        pKey = publicKey;
        lab = label;
        se = secret;
        raSalt = randomSalt;
        raIV = randomIV;
        sIt = secretIterations;
        sKey = secretKeySize;
        sA = shareAccount;
        aT = accountType;
        timestamp = createdTimestamp;

    }

    public static void queryWalletData() {

        countRows = Db.countRows("keystash_wallet") + 1;
        String[] getAccountid = new String[countRows];
        String[] getAddressClassic = new String[countRows];
        String[] getAddressRs = new String[countRows];
        String[] getPublicKey = new String[countRows];
        String[] getLabel = new String[countRows];
        String[] getSecret = new String[countRows];
        String[] getSalt = new String[countRows];
        String[] getIV = new String[countRows];
        String[] getSecretIteration = new String[countRows];
        String[] getSecretKeySize = new String[countRows];
        String[] getShareAccount = new String[countRows];
        String[] getAccountType = new String[countRows];
        String[] getTimestamp = new String[countRows];

        for (int i = 1; i < countRows; i++) {

            getAccountid[i] = Db.selectFromWallet("account_id", "keystash_wallet", "account_id", +i);
            getAddressClassic[i] = Db.selectFromWallet("address_classic", "keystash_wallet", "account_id", +i);
            getAddressRs[i] = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +i);
            getPublicKey[i] = Db.selectFromWallet("public_key", "keystash_wallet", "account_id", +i);
            getLabel[i] = Db.selectFromWallet("label", "keystash_wallet", "account_id", +i);
            getSecret[i] = Db.selectFromWallet("secret", "keystash_wallet", "account_id", +i);
            getSalt[i] = Db.selectFromWallet("randomsalt", "keystash_wallet", "account_id", +i);
            getIV[i] = Db.selectFromWallet("randomiv", "keystash_wallet", "account_id", +i);
            getSecretIteration[i] = Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", +i);
            getSecretKeySize[i] = Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", +i);
            getShareAccount[i] = Db.selectFromWallet("share_account", "keystash_wallet", "account_id", +i);
            getAccountType[i] = Db.selectFromWallet("account_type", "keystash_wallet", "account_id", +i);
            getTimestamp[i] = Db.selectFromWallet("created_timestamp", "keystash_wallet", "account_id", +i);

            getWalletData(countRows, getAccountid, getAddressClassic, getAddressRs, getPublicKey, getLabel, getSecret,
                    getSalt, getIV, getSecretIteration, getSecretKeySize, getShareAccount, getAccountType, getTimestamp);

        }

    }

}
