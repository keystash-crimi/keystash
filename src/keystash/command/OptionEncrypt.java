package keystash.command;

import java.io.Console;
import java.sql.SQLException;
import java.util.Scanner;
import static keystash.command.Processor.isWalletEncrypted;
import keystash.util.Constants;
import keystash.util.Db;
import keystash.util.Memory;
import org.h2.tools.ChangeFileEncryption;

public class OptionEncrypt {

    public static void encryptWallet(String command)
            throws InterruptedException,
            SQLException {

        char[] password = null;
        char[] passwordConfirm = null;
        Console consolePassword = System.console();
        Scanner consoleInput = new Scanner(System.in);
        int encryptWalletMenu = 1;
        boolean encryptWalletMenuLoop = true;

        while (encryptWalletMenuLoop) {
            switch (encryptWalletMenu) {
                default:

                    break;
                case 1:

                    System.out.print("Enter new master password: ");
                    password = consolePassword.readPassword();

                    if ("cancel".equals(new String(password))) {

                        System.out.println(Options.header);
                        encryptWalletMenu = 99;
                        break;

                    }

                    if (password.length == 0) {

                        System.out.println("Password cant be empty.");
                        encryptWalletMenu = 1;
                        break;

                    }
                    else if (password.length < Constants.minPasswordChars) {

                        System.out.println("Password must contain at least " + Constants.minPasswordChars + " characters.");
                        encryptWalletMenu = 1;
                        break;

                    }
                    else {

                        encryptWalletMenu = 2;
                        break;

                    }

                case 2:

                    System.out.print("Confirm your master password: ");
                    passwordConfirm = consolePassword.readPassword();

                    if ("cancel".equals(new String(passwordConfirm))) {

                        System.out.println(Options.header);
                        encryptWalletMenu = 99;
                        break;

                    }

                    if (!String.valueOf(passwordConfirm).equals(String.valueOf(password))) {
                        
                        encryptWalletMenu = 2;
                        System.out.println("Password does not match.");
                        break;
                        
                    }
                    else {
                        
                        encryptWalletMenu = 3;
                        break;
                        
                    }
                case 3:

                    System.out.println("Confirm encryption with [1]Yes, [2]Cancel");

                    String input = consoleInput.next();
                    int inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {
                        encryptWalletMenu = 3;
                        System.out.println("The value you have input is not a valid integer.");
                        break;
                    }

                    if ("1".equals(input)) {
                        
                        System.out.println("Processing...");
                        char c = '¡';
                        try {

                            Db.walletClose();
                            ChangeFileEncryption.main("-dir", Constants.walletPathOnly, "-db", "wallet", "-cipher", "AES", "-encrypt", new String(password).replace(' ', c));
                            isWalletEncrypted = true;
                            Db.connectToEncryptedWallet(String.valueOf(password).replace(' ', c));
                            System.out.println("Wallet encypted successfully.");
                        } catch (SQLException ex) {

                            Db.connectToWallet();
                            System.out.println("Wallet encyption failed.");

                        }

                        Memory.clearPasswordMemory(password);
                        Memory.clearPasswordMemory(passwordConfirm);

                        System.out.println(Options.header);
                        encryptWalletMenu = 99;
                        break;

                    }
                    else if ("2".equals(input)) {

                        Memory.clearPasswordMemory(password);
                        Memory.clearPasswordMemory(passwordConfirm);

                        System.out.println(Options.header);
                        encryptWalletMenu = 99;
                        break;

                    }

                case 99:
                    break;

            }
            if (encryptWalletMenu == 99) {
                encryptWalletMenuLoop = false;
            }
        }

    }

}
