package keystash.command;

import static keystash.command.Connect.countRows;
import static keystash.command.Connect.id;
import static keystash.command.Connect.pKey;
import static keystash.command.Connect.queryWalletData;
import static keystash.command.Connect.rs;

public class OptionList {

    public static void list(String command) {

        queryWalletData();
        System.out.printf("%-15s %n", "{");

        if (countRows == 1) {

            System.out.printf("%-15s %n", "\"AccountQuantity\" : 0");

        }
        else {
            for (int i = 1; i < countRows; i++) {
                System.out.printf("%-15s %n", "\"AccountId\" : " + id[i] + ",");
                System.out.printf("%-15s %n", "\"AddressRs\" : " + rs[i] + ",");
                
                if (i == countRows - 1) {

                    System.out.printf("%-15s %n", "\"PublicKey\" : " + pKey[i]);

                }
                else {

                    System.out.printf("%-15s %n", "\"PublicKey\" : " + pKey[i] + ",");

                }
            }

        }
        System.out.printf("%-15s %n", "}");

    }

}
