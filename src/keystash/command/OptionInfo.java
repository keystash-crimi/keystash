package keystash.command;

import java.security.NoSuchAlgorithmException;
import static keystash.command.Processor.isWalletEncrypted;
import keystash.crypto.KeyLength;
import keystash.util.Constants;
import org.apache.commons.lang.SystemUtils;

public class OptionInfo {

    public static void getInfo(String command)
            throws NoSuchAlgorithmException {

        String walletHome = Constants.walletPathOnly;
        String walletHomeUnix = Constants.walletPathOnlyUnix;
        boolean keyLength = KeyLength.isUnlimitedKeyStrength();
        String apiUrl = Constants.apiAddress;

        System.out.printf("%-15s %n", "{");
        
        if (SystemUtils.IS_OS_LINUX){
            
        System.out.printf("%-15s %n", "\"WalletHomeFolder\" : " + walletHomeUnix + ",");    
        
        }else{
            
        System.out.printf("%-15s %n", "\"WalletHomeFolder\" : " + walletHome + ",");  
        
        } 
        
        System.out.printf("%-15s %n", "\"UnlimitedKeyLength\" : " + keyLength + ",");
        System.out.printf("%-15s %n", "\"ApiUrl\" : " + "http://" + apiUrl + ",");
        System.out.printf("%-15s %n", "\"WalletEncrypted\" : " + isWalletEncrypted);
        System.out.printf("%-15s %n", "}");

    }

}
