package keystash.command;

import java.io.Console;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import static keystash.command.Options.*;
import keystash.util.Debug;
import org.apache.commons.codec.DecoderException;

public class Interface {

    public static boolean masterPasswordInput(String password) throws IOException {

        Console console = System.console();

        if (console == null) {

            System.out.println("Couldn't get console instance.");
            Debug.addLoggMessage("Couldn't get console instance.");
            System.exit(0);

        }

        char passwordArray[] = console.readPassword("Enter master password: ");

        return Connect.connectToEncryptedDb(new String(passwordArray));

    }

    public static void userInput(String option)
            throws NoSuchAlgorithmException,
            InterruptedException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            SQLException,
            Exception {

        Scanner console = new Scanner(System.in);
        option = console.nextLine();
        processInput(option);

    }

    public static void processInput(String option)
            throws NoSuchAlgorithmException,
            InterruptedException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            SQLException,
            Exception {

        String[] splitInput = option.split("\\s+");
        option = splitInput[0];
        option = option.replaceAll("\\s+", "");
        String command = null;
        String ip = null;
        String value = null;
        String allow = null;
        String allowIp = null;

        if (splitInput.length > 1) {

            command = splitInput[1];
            command = command.replaceAll("\\s+", "");

            if (splitInput.length > 2) {

                ip = splitInput[2];
                ip = ip.replaceAll("\\s+", "");

            }
            if (splitInput.length > 3) {

                value = splitInput[3];
                value = value.replaceAll("\\s+", "");

            }
            if (splitInput.length > 4) {

               allow = splitInput[4];
               allow = allow.replaceAll("\\s+", "");

            }
            if (splitInput.length > 5) {

                allowIp = splitInput[5];
                allowIp = allowIp.replaceAll("\\s+", "");

            }
        }

        boolean validOption = Processor.validateInput(option, command, ip, value, allow, allowIp);

        if ("".equals(splitInput[0])) {

        }
        else if (splitInput.length == 1 && !listOption(0).equals(option)) {

            if (!validOption) {

                System.out.println(option + " option not exists.");

            }
            else if ("-exit".equals(option)) {
                
                System.out.println("Exiting...");
                System.exit(0);

            }
            else if (validOption && splitInput.length == 0) {

                System.out.println(option + " no value given.");

            }
        }
        else if (!validOption && splitInput.length >= 1) {

            System.out.println(option + " option or command not exists.");

        }
        else if (listOption(0).equals(option)) {

            OptionHelp.help();

        }

    }

}
