package keystash.command;

import java.io.Console;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.crypto.KeyLength;
import keystash.crypto.PhraseGenerator;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;
import keystash.util.Memory;
import keystash.util.PasswordStrength;
import org.apache.commons.codec.DecoderException;

public class OptionNew {

    public static void newAccount(String command)
            throws InterruptedException,
            NoSuchAlgorithmException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            IOException {

        char[] password = null;
        char[] passwordConfirm = null;
        char[] secret = null;
        String letters = null;
        Console consolePassword = System.console();
        Scanner consoleInput = new Scanner(System.in);
        String input = null;
        int newAccountMenu = 1;
        boolean newAccountMenuLoop = true;
        boolean addressSearchLoop = false;

        while (newAccountMenuLoop) {
            switch (newAccountMenu) {
                default:

                    break;
                case 1:

                    System.out.print("Enter new primary password: ");
                    password = consolePassword.readPassword();

                    if ("cancel".equals(new String(password))) {

                        System.out.println(Options.header);
                        newAccountMenu = 99;
                        break;

                    }

                    if (password.length == 0) {

                        System.out.println("Password cant be empty.");
                        newAccountMenu = 1;
                        break;

                    }
                    else if (password.length < Constants.minPasswordChars) {

                        System.out.println("Password must contain at least " + Constants.minPasswordChars + " characters.");
                        newAccountMenu = 1;
                        break;

                    }
                    else {

                        newAccountMenu = 2;
                        break;

                    }

                case 2:

                    System.out.print("Confirm your primary password: ");
                    passwordConfirm = consolePassword.readPassword();

                    if ("cancel".equals(new String(passwordConfirm))) {

                        System.out.println(Options.header);
                        newAccountMenu = 99;
                        break;

                    }

                    if (!String.valueOf(passwordConfirm).equals(String.valueOf(password))) {

                        newAccountMenu = 2;
                        System.out.println("Password does not match.");
                        break;

                    }
                    else {

                        System.out.println("Processing...");
                        newAccountMenu = 3;
                        break;

                    }

                case 3:

                    secret = PhraseGenerator.generatePhrase();
                    byte[] secretPhrase = Crypto.getPublicKey(new String(secret));
                    Long secretPhraseConvert = Convert.getId(secretPhrase);
                    String addressRs = Convert.rsAccount(secretPhraseConvert);
                    String publicKey = Convert.toHexString(secretPhrase);
                    int passLength = secret.length;
                    String passLengthString = String.valueOf(passLength);
                    double shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
                    String shannonEntropyString = String.valueOf(shannonEntropy);

                    System.out.printf("%-15s %n", "{");
                    System.out.printf("%-15s %n", "\"AddressRs\" : " + addressRs + ",");
                    System.out.printf("%-15s %n", "\"PubKey\" : " + publicKey + ",");
                    System.out.printf("%-15s %n", "\"PhraseEntropy\" : " + shannonEntropyString + ",");
                    System.out.printf("%-15s %n", "\"PhraseLength\" : " + passLengthString + ",");
                    System.out.printf("%-15s %n", "}");

                    System.out.println("Confirm with [1]Save, [2]Generate new account [3]Search address [4]Cancel");

                    input = consoleInput.next();
                    int inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {
                        newAccountMenu = 3;
                        System.out.println("The value you have input is not a valid integer.");
                        break;
                    }

                    if ("1".equals(input)) {

                        saveAccountToWallet(secret, new String(password));
                        Memory.clearPasswordMemory(password);
                        Memory.clearPasswordMemory(passwordConfirm);
                        Memory.clearPasswordMemory(secret);
                        System.out.println(Options.header);
                        newAccountMenu = 99;
                        break;

                    }
                    else if ("2".equals(input)) {

                        newAccountMenu = 3;
                        break;

                    }
                    else if ("3".equals(input)) {

                        newAccountMenu = 4;
                        break;

                    }
                    else if ("4".equals(input)) {

                        System.out.println(Options.header);
                        newAccountMenu = 99;
                        break;

                    }

                case 4:

                    if ("3".equals(input)) {

                        System.out.println("Use maximal 5 characters and only letters A-Z and numbers 2-9.");
                        Scanner lettersInput = new Scanner(System.in);
                        String regex = ".*[*01$-+#?@_&=!%{}/]+.*";

                        while ((letters == null) || (letters.trim().isEmpty() || letters.length() >= 5 || letters.matches(regex))) {

                            System.out.print("Enter characters: ");
                            letters = lettersInput.nextLine();
                            if (letters.length() >= 5) {
                                System.out.println("Maximal 5 characters are valid.");
                            }

                            if ((letters == null) || (letters.trim().isEmpty())) {
                                System.out.println("Not any character is given.");
                            }

                            if (letters.matches(regex)) {
                                System.out.println("Not valid character.");
                            }

                        }
                        addressSearchLoop = true;
                        newAccountMenu = 5;
                        break;
                    }

                case 5:

                    if ("3".equals(input)) {

                        String findValue = letters;
                        findValue = findValue.toUpperCase();
                        int countAttempts = 0;

                        do {

                            char[] myPhrase = PhraseGenerator.generatePhrase();
                            secret = myPhrase;
                            secretPhrase = Crypto.getPublicKey(new String(secret));
                            secretPhraseConvert = Convert.getId(secretPhrase);
                            addressRs = Convert.rsAccount(secretPhraseConvert);
                            publicKey = Convert.toHexString(secretPhrase);
                            passLength = secret.length;
                            passLengthString = String.valueOf(passLength);
                            shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
                            shannonEntropyString = String.valueOf(shannonEntropy);
                            String[] splitAddress = addressRs.split("\\-", -1);

                            for (int i = 1; i < 5; i++) {
                                String word = splitAddress[i].replaceAll("\\s+$", "");
                                String searchingWord = findValue.replaceAll("\\s+$", "");
                                if (word.contains(searchingWord) && word.length() >= searchingWord.length()) {

                                    System.out.println("word found " + letters);
                                    System.out.printf("%-15s %n", "{");
                                    System.out.printf("%-15s %n", "\"AddressRs\" : " + addressRs + ",");
                                    System.out.printf("%-15s %n", "\"PubKey\" : " + publicKey + ",");
                                    System.out.printf("%-15s %n", "\"PhraseEntropy\" : " + shannonEntropyString + ",");
                                    System.out.printf("%-15s %n", "\"PhraseLength\" : " + passLengthString + ",");
                                    System.out.printf("%-15s %n", "}");
                                    letters = null;
                                    addressSearchLoop = false;
                                    newAccountMenu = 6;
                                    break;

                                }

                            }

                            countAttempts++;

                            System.out.print("Attempts: " + countAttempts + "\r");

                        } while (addressSearchLoop);
                    }
                case 6:
                    if ("3".equals(input)) {

                        System.out.println("Confirm with [1]Save, [2]Generate new account [3]Search address [4]Cancel");
                        input = consoleInput.next();
                        inputInt = 0;
                        try {
                            inputInt = Integer.parseInt(input);
                        } catch (Exception e) {
                            newAccountMenu = 3;
                            System.out.println("The value you have input is not a valid integer.");
                            break;
                        }

                        if ("1".equals(input)) {

                            saveAccountToWallet(secret, new String(password));
                            Memory.clearPasswordMemory(password);
                            Memory.clearPasswordMemory(passwordConfirm);
                            Memory.clearPasswordMemory(secret);
                            System.out.println(Options.header);
                            newAccountMenu = 99;
                            break;

                        }
                        else if ("2".equals(input)) {

                            newAccountMenu = 3;
                            break;

                        }
                        else if ("3".equals(input)) {

                            newAccountMenu = 4;
                            break;

                        }
                        else if ("4".equals(input)) {

                            System.out.println(Options.header);
                            newAccountMenu = 99;
                            break;

                        }
                        else {

                            newAccountMenu = 3;
                            break;

                        }

                    }
                case 99:
                    break;

            }
            if (newAccountMenu == 99) {

                newAccountMenuLoop = false;

            }
        }

    }

    public static void saveAccountToWallet(char[] secret, String password)
            throws NoSuchAlgorithmException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            IOException {

        int accountId = Db.countRows("keystash_wallet") + 1;
        char[] genSalt;
        genSalt = Aes.generateSaltOrIv(Constants.saltAndIvSize[0]);
        char[] tmpSalt = genSalt;
        char[] genIV;
        genIV = Aes.generateSaltOrIv(Constants.saltAndIvSize[1]);
        char[] tmpIV = genIV;
        char[] cipherPassPhrase;
        cipherPassPhrase = Aes.encrypt(new String(secret), password, new String(tmpSalt), new String(tmpIV));
        byte[] secretPhrase = Crypto.getPublicKey(String.valueOf(secret));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String publicKey = Convert.toHexString(secretPhrase);
        String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        String label = Constants.accountLabel;
        int secretIteration;
        int secretKeySize;
        String encryptionStandard;

        if (Convert.stringToBoolean(Config.properties("keystash.enableExpertEncryption")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

            secretIteration = Convert.stringToInt(Config.properties("keystash.PBKDF2Iterations"));
            secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

            if (KeyLength.isUnlimitedKeyStrength() && secretKeySize == Constants.secretKeySize[1]) {

                secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

            }
            else {

                secretKeySize = Constants.secretKeySize[0];

            }
            
            encryptionStandard = Config.properties("keystash.keyGeneratorAlgorithm");

        }
        else {

            secretIteration = Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1));
            secretKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
            encryptionStandard = Db.selectFromSettings("advanced_encryption_standard", "keystash_settings", "settings_id", 1);

        }

        int shareAccount = (Convert.stringToInt(Db.selectFromSettings("share_account", "keystash_settings", "settings_id", 1)));
        String accountType = Db.selectFromSettings("account_type", "keystash_settings", "settings_id", 1);
        String createdTimestamp = Date.getDate(false);
        Db.walletInsertNewAccount(accountId, addressClassic, addressRs, publicKey, label, new String(cipherPassPhrase), new String(tmpSalt),
                new String(tmpIV), secretIteration, secretKeySize, encryptionStandard, shareAccount, accountType, createdTimestamp);
        System.out.println("Account " + addressRs + " saved to wallet file");

    }

}
