package keystash.command;

import java.io.Console;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import javax.crypto.NoSuchPaddingException;
import static keystash.command.Connect.countRows;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Memory;
import org.apache.commons.codec.DecoderException;

public class OptionShow {

    public static void showKey(String command)
            throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            UnsupportedEncodingException,
            DecoderException,
            IOException {

        char[] password = null;
        char[] decipherSecret;
        String publicKey;
        String addressRs;
        Console consolePassword = System.console();
        Scanner consoleInput = new Scanner(System.in);
        String input = null;
        int showKeyMenu = 1;
        boolean showKeyMenuLoop = true;

        while (showKeyMenuLoop) {
            switch (showKeyMenu) {
                default:
                    break;

                case 1:
                    int accountId = countRows - 1;
                    int accountIdCancel = countRows;

                    if (accountId >= 1) {
                        System.out.println("Choose accountId [1-" + accountId + "]Start, [" + accountIdCancel + "]Cancel");
                    }
                    else {
                        System.out.println("Choose accountId [account quantity 0], [1]Cancel");
                    }

                    input = consoleInput.next();
                    int inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {

                        showKeyMenu = 1;
                        System.out.println("The value you have input is not a valid integer.");
                        break;

                    }

                    if (Convert.stringToInt(input) <= accountId && Convert.stringToInt(input) != 0) {
                        showKeyMenu = 2;
                        break;
                    }
                    else if (accountIdCancel == Convert.stringToInt(input)) {

                        System.out.println(Options.header);
                        showKeyMenu = 99;
                        break;

                    }
                    else {
                        showKeyMenu = 1;
                        break;
                    }

                case 2:

                    System.out.print("Enter primary password: ");
                    password = consolePassword.readPassword();

                    if ("cancel".equals(new String(password))) {

                        System.out.println(Options.header);
                        showKeyMenu = 99;
                        break;

                    }

                    if (password.length == 0) {

                        System.out.println("Password cant be empty.");
                        showKeyMenu = 2;
                        break;

                    }
                    else if (password.length < Constants.minPasswordChars) {

                        System.out.println("Password must contain at least " + Constants.minPasswordChars + " characters.");
                        showKeyMenu = 2;
                        break;

                    }
                    else {
                        
                        System.out.println("Processing...");
                        showKeyMenu = 3;
                        break;

                    }

                case 3:

                    String getAccountRs = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", + Convert.stringToInt(input));
                    String getSecret = Db.selectFromWallet("secret", "keystash_wallet", "account_id", + Convert.stringToInt(input));
                    String getSalt = Db.selectFromWallet("randomSalt", "keystash_wallet", "account_id", + Convert.stringToInt(input));
                    String getIV = Db.selectFromWallet("randomIV", "keystash_wallet", "account_id", + Convert.stringToInt(input));
                    String getLabel = Db.selectFromWallet("label", "keystash_wallet", "account_id", + Convert.stringToInt(input));
                    int getIteration = Convert.stringToInt(Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", + Convert.stringToInt(input)));
                    int getKeySize = Convert.stringToInt(Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", + Convert.stringToInt(input)));
                    String getEncryptionStandard = Db.selectFromWallet("advanced_encryption_standard", "keystash_wallet", "account_id", + Convert.stringToInt(input));

                    decipherSecret = Aes.decrypt(getSecret, new String(password), getSalt, getIV, getIteration, getKeySize, getEncryptionStandard);

                    if (decipherSecret == null) {
                        System.out.println("Password does not match with encryption.");
                        showKeyMenu = 2;
                        break;
                    }
                    else {
                        byte[] secretPhrase = Crypto.getPublicKey(new String(decipherSecret));
                        Long secretPhraseConvert = Convert.getId(secretPhrase);
                        publicKey = Convert.toHexString(secretPhrase);
                        String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
                        addressRs = Convert.rsAccount(secretPhraseConvert);

                        if (getAccountRs.equals(addressRs)) {

                            System.out.printf("%-15s %n", "{");
                            System.out.printf("%-15s %n", "\"AccountId\" : " + input + ",");
                            System.out.printf("%-15s %n", "\"AddressRs\" : " + addressRs + ",");
                            System.out.printf("%-15s %n", "\"PrivateKey\" : " + new String(decipherSecret) + ",");
                            System.out.printf("%-15s %n", "\"PublicKey\" : " + publicKey + "");
                            System.out.printf("%-15s %n", "}");

                            showKeyMenu = 4;
                            break;

                        }
                        else {
                            System.out.println("Password does not match with encryption.");
                        }

                    }

                case 4:

                    System.out.println("Choose integer [1]Cancel");

                    input = consoleInput.next();
                    inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {
                        showKeyMenu = 4;
                        System.out.println("The value you have input is not a valid integer.");
                        break;
                    }

                    if ("1".equals(input)) {

                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        showKeyMenu = 99;
                        break;

                    }

                case 99:
                    break;

            }
            if (showKeyMenu == 99) {
                showKeyMenuLoop = false;
            }
        }

    }

}
