package keystash.command;

import java.util.Scanner;
import static keystash.command.Processor.isApiEnabled;
import keystash.http.Api;
import keystash.http.ApiRefresh;
import keystash.util.Config;
import keystash.util.Convert;

public class OptionApi {

    @SuppressWarnings("SleepWhileInLoop")
    public static void runApi(String command)
            throws Exception {

        Scanner consoleInput = new Scanner(System.in);
        int apiMenu = 1;
        boolean apiMenuLoop = true;

        while (apiMenuLoop) {
            switch (apiMenu) {
                default:

                    break;
                case 1:

                    System.out.println("Api server [1]Start, [2]Stop, [3]Cancel");

                    String input = consoleInput.next();
                    int inputInt = 0;

                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {

                        apiMenu = 1;
                        System.out.println("The value you have input is not a valid integer.");
                        break;

                    }

                    if ("1".equals(input)) {

                        ApiRefresh.refreshApi();
                        Api.startWalletServer(true, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                                        Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                                        Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.apiPassword"),
                                        Convert.stringToBoolean(Config.properties("keystash.disableApiPassword"))
                                );
                        Thread.sleep(2000);
                        System.out.println(Options.header);
                        isApiEnabled = true;
                        apiMenu = 99;
                        break;

                    }
                    else if ("2".equals(input)) {

                        if (!isApiEnabled) {

                            System.out.println("Api server is not running.");

                        }
                        else {

                            Api.startWalletServer(false, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                                        Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                                        Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.apiPassword"),
                                        Convert.stringToBoolean(Config.properties("keystash.disableApiPassword"))
                                );
                            Thread.sleep(2000);
                            System.out.println("Api server successfully stopped.");
                            System.out.println(Options.header);
                            isApiEnabled = false;
                            apiMenu = 99;
                            break;

                        }

                    }
                    else if ("3".equals(input)) {

                        System.out.println(Options.header);
                        apiMenu = 99;
                        break;

                    }

                case 99:
                    break;

            }
            if (apiMenu == 99) {

                apiMenuLoop = false;

            }
        }

    }

}
