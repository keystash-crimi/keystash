package keystash.command;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import static keystash.command.Options.countOptions;
import static keystash.command.Options.listOption;
import static keystash.command.Options.option2;
import static keystash.command.Options.option3;
import static keystash.command.Options.option4;
import static keystash.command.Options.option5;
import static keystash.command.Options.option6;
import static keystash.command.Options.option7;
import static keystash.command.Options.option8;
import static keystash.command.Options.option9;
import static keystash.command.Options.option10;
import keystash.http.Api;
import keystash.http.ApiRefresh;
import keystash.util.Config;
import keystash.util.Convert;
import keystash.util.Debug;
import keystash.util.Validate;
import org.apache.commons.codec.DecoderException;

public class Processor {

    public static boolean isApiEnabled = false;
    public static boolean isWalletEncrypted = false;

    public static boolean validateInput(String option, String command, String ip, String value, String allow, String allowIp)
            throws NoSuchAlgorithmException,
            InterruptedException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            SQLException,
            Exception {

        ArrayList<String> optionArray = new ArrayList<>();
        for (int i = 0; i < countOptions(); i++) {

            optionArray.add(listOption(i));

        }

        boolean isValidOption = false;
        if (optionArray.contains(option)) {

            isValidOption = true;
            filterInput(option, command, ip, value, allow, allowIp);

        }
        return isValidOption;

    }

    public static void filterInput(String option, String command, String ip, String value, String allow, String allowIp)
            throws NoSuchAlgorithmException,
            InterruptedException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            SQLException,
            Exception {

        if (option.equals(option2)) {

            OptionInfo.getInfo(command);

        }
        if (option.equals(option3)) {

            OptionList.list(command);

        }
        if (option.equals(option4)) {

            OptionNew.newAccount(command);

        }

        if (option.equals(option5)) {

            OptionImport.importAccount(command);

        }

        if (option.equals(option6)) {

            OptionShow.showKey(command);

        }
        if (option.equals(option7) && !isWalletEncrypted) {

            OptionEncrypt.encryptWallet(command);

        }
        else if (option.equals(option7) && isWalletEncrypted) {

            System.out.println("Wallet seems already encrypted");
            Debug.addLoggMessage("Wallet seems already encrypted.");

        }
        if (option.equals(option8)) {

            OptionSign.offlineSigning(command);

        }
        if (option.equals(option9)) {

            OptionBackup.backupWallet(command);

        }

        if (option.equals(option10) && !isApiEnabled) {

            if ("start".equals(command) && !isApiEnabled && value == null) {

                ApiRefresh.refreshApi();
                Api.startWalletServer(true, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                        Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                        Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.adminPassword"),
                        Convert.stringToBoolean(Config.properties("keystash.disableAdminPassword"))
                );

                Thread.sleep(2000);
                System.out.println(Options.header);

            }
            else if (!isApiEnabled && value == null && !"stop".equals(command)) {

                OptionApi.runApi(command);

            }
            else if (!isApiEnabled && value == null && "stop".equals(command)) {

                System.out.println("API server is not running. Cant stop it.");
                Debug.addLoggMessage("API server is not running. Cant stop it.");

            }
        }
        else if (option.equals(option10) && isApiEnabled && value == null) {
            if ("stop".equals(command)) {

                Api.startWalletServer(false, Config.properties("keystash.apiServerHost"), Config.properties("keystash.allowedBotHosts"),
                        Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                        Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.adminPassword"),
                        Convert.stringToBoolean(Config.properties("keystash.disableAdminPassword"))
                );

                Thread.sleep(2000);
                System.out.println("API server successfully stopped.");
                System.out.println(Options.header);
                isApiEnabled = false;

            }
            else {

                System.out.println("API server seems already running. You may want to use <stop>.");
                Debug.addLoggMessage("API server seems already running. You may want to use <stop>.");
                
            }

        }

        if (Validate.validIP(value) && !isApiEnabled && allow == null && !Validate.validIP(allowIp)) {

            if ("start".equals(command) && "ip".equals(ip) && Validate.validIP(value) && !isApiEnabled) {

                ApiRefresh.refreshApi();
                try {

                    Api.startWalletServer(true, value, Config.properties("keystash.allowedBotHosts"),
                            Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                            Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.adminPassword"),
                            Convert.stringToBoolean(Config.properties("keystash.disableAdminPassword"))
                    );

                } catch (Exception e) {

                    System.out.println("API invalid ip address.");
                    Debug.addLoggMessage("API invalid ip address.");

                }
                Thread.sleep(2000);
                System.out.println(Options.header);
            }
            else {

                System.out.println("API server seems already running. You may want to use <stop>.");
                Debug.addLoggMessage("API server seems already running. You may want to use <stop>.");

            }
        }

        if (Validate.validIP(value) && !isApiEnabled && "allow".equals(allow) && Validate.validIP(allowIp)) {

            if ("start".equals(command) && "ip".equals(ip) && Validate.validIP(value) && "allow".equals(allow) && Validate.validIP(allowIp) && !isApiEnabled) {

                ApiRefresh.refreshApi();
                try {

                    Api.startWalletServer(true, value, allowIp,
                            Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                            Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.adminPassword"),
                            Convert.stringToBoolean(Config.properties("keystash.disableAdminPassword"))
                    );

                } catch (Exception e) {

                    System.out.println("API invalid ip address.");
                    Debug.addLoggMessage("API invalid ip address.");
                    
                }

                Thread.sleep(2000);
                System.out.println(Options.header);

            }
            else {

                System.out.println("API server seems already running. You may want to use <stop>.");
                Debug.addLoggMessage("API server seems already running. You may want to use <stop>.");

            }
        }

        if (Validate.validIP(value) && !isApiEnabled && "allow".equals(allow) && "*".equals(allowIp)) {

            if ("start".equals(command) && "ip".equals(ip) && Validate.validIP(value) && "allow".equals(allow) && "*".equals(allowIp) && !isApiEnabled) {

                ApiRefresh.refreshApi();
                try {

                    Api.startWalletServer(true, value, allowIp,
                            Convert.stringToInt(Config.properties("keystash.apiServerPort")), Config.properties("keystash.apiResourceBase"),
                            Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")), Config.properties("keystash.adminPassword"),
                            Convert.stringToBoolean(Config.properties("keystash.disableAdminPassword"))
                    );

                } catch (Exception e) {

                    System.out.println("API invalid ip address.");
                    Debug.addLoggMessage("API invalid ip address.");

                }
                Thread.sleep(2000);
                System.out.println(Options.header);

            }
            else {

                System.out.println("AAPI server seems already running. You may want to use <stop>.");
                Debug.addLoggMessage("API server seems already running. You may want to use <stop>.");

            }
        }

        else if (value != null && !isApiEnabled) {

            System.out.println("API invalid ip address.");
            Debug.addLoggMessage("API invalid ip address.");

        }

    }

}
