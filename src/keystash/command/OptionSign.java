package keystash.command;

import java.io.BufferedWriter;
import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Scanner;
import javax.crypto.NoSuchPaddingException;
import static keystash.command.Connect.countRows;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.sign.Transaction;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;
import keystash.util.Memory;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.SystemUtils;

public class OptionSign {

    public static void offlineSigning(String command)
            throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            UnsupportedEncodingException,
            DecoderException,
            IOException {

        char[] password = null;
        char[] decipherSecret = null;
        String publicKey;
        String addressRs;
        String recipient = null;
        String amount = null;
        String fee = null;
        String deadline = null;
        String backupPath = null;
        byte[] signedFinalBytes = null;
        Console consolePassword = System.console();
        Scanner consoleInput = new Scanner(System.in);
        String input = null;
        int inputInt = 0;
        int signMenu = 1;
        boolean signMenuLoop = true;

        while (signMenuLoop) {
            switch (signMenu) {
                default:
                    break;

                case 1:
                    int accountId = countRows - 1;
                    int accountIdCancel = countRows;

                    if (accountId >= 1) {

                        System.out.println("Choose accountId [1-" + accountId + "]Start, [" + accountIdCancel + "]Cancel");

                    }
                    else {

                        System.out.println("Choose accountId [account quantity 0], [1]Cancel");

                    }

                    input = consoleInput.next();

                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {

                        signMenu = 1;
                        System.out.println("The value you have input is not a valid integer.");
                        break;

                    }

                    if (Convert.stringToInt(input) <= accountId && Convert.stringToInt(input) != 0) {
                        signMenu = 2;
                        break;
                    }
                    else if (accountIdCancel == Convert.stringToInt(input)) {

                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }
                    else {

                        signMenu = 1;
                        break;

                    }

                case 2:

                    System.out.print("Enter primary password: ");
                    password = consolePassword.readPassword();

                    if ("cancel".equals(new String(password))) {

                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                    if (password.length == 0) {

                        System.out.println("Password cant be empty.");
                        signMenu = 2;
                        break;

                    }
                    else if (password.length < Constants.minPasswordChars) {

                        System.out.println("Password must contain at least " + Constants.minPasswordChars + " characters.");
                        signMenu = 2;
                        break;

                    }
                    else {

                        System.out.println("Processing...");
                        signMenu = 3;
                        break;

                    }

                case 3:

                    String getAccountRs = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +Convert.stringToInt(input));
                    String getSecret = Db.selectFromWallet("secret", "keystash_wallet", "account_id", +Convert.stringToInt(input));
                    String getSalt = Db.selectFromWallet("randomSalt", "keystash_wallet", "account_id", +Convert.stringToInt(input));
                    String getIV = Db.selectFromWallet("randomIV", "keystash_wallet", "account_id", +Convert.stringToInt(input));
                    int getIteration = Convert.stringToInt(Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", +Convert.stringToInt(input)));
                    int getKeySize = Convert.stringToInt(Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", +Convert.stringToInt(input)));
                    String getEncryptionStandard = Db.selectFromWallet("advanced_encryption_standard", "keystash_wallet", "account_id", +Convert.stringToInt(input));

                    decipherSecret = Aes.decrypt(getSecret, new String(password), getSalt, getIV, getIteration, getKeySize, getEncryptionStandard);

                    if (decipherSecret == null) {

                        System.out.println("Password does not match with encryption.");
                        signMenu = 2;
                        break;

                    }
                    else {
                        byte[] secretPhrase = Crypto.getPublicKey(new String(decipherSecret));
                        Long secretPhraseConvert = Convert.getId(secretPhrase);
                        publicKey = Convert.toHexString(secretPhrase);
                        addressRs = Convert.rsAccount(secretPhraseConvert);

                        if (getAccountRs.equals(addressRs)) {

                            System.out.printf("%-15s %n", "{");
                            System.out.printf("%-15s %n", "\"AccountId\" : " + input + ",");
                            System.out.printf("%-15s %n", "\"AddressRs\" : " + addressRs + ",");
                            System.out.printf("%-15s %n", "\"PublicKey\" : " + publicKey + "");
                            System.out.printf("%-15s %n", "}");

                            signMenu = 4;
                            break;

                        }
                        else {
                            System.out.println("Password does not match with encryption.");
                        }

                    }

                case 4:

                    System.out.println("Choose integer [1]Sign new transaction, [2]Cancel");

                    input = consoleInput.next();
                    inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {
                        signMenu = 4;
                        System.out.println("The value you have input is not a valid integer.");
                        break;
                    }

                    if ("1".equals(input)) {

                        signMenu = 5;
                        break;

                    }

                    if ("2".equals(input)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                case 5:

                    if ("1".equals(input)) {

                        Scanner lettersInput = new Scanner(System.in);
                        String regex = ".*[*01$-+#?@_&=!%{}/]+.*";

                        while ((recipient == null) || (recipient.trim().isEmpty() || recipient.length() != 24 || recipient.matches(regex))) {

                            System.out.print("Enter recipient address: ");
                            recipient = lettersInput.nextLine();

                            if ("cancel".equals(recipient)) {

                                break;

                            }

                            if (recipient.length() != 24) {

                                System.out.println("Only addresses with 24 characters are valid.");

                            }

                            if ((recipient == null) || (recipient.trim().isEmpty())) {

                                System.out.println("Not any character is given.");

                            }

                            if (recipient.matches(regex)) {

                                System.out.println("Not valid characters.");

                            }

                        }

                        if ("cancel".equals(recipient)) {

                            Memory.clearPasswordMemory(decipherSecret);
                            Memory.clearPasswordMemory(password);
                            System.out.println(Options.header);
                            signMenu = 99;
                            break;

                        }

                        signMenuLoop = true;
                        signMenu = 6;
                        break;
                    }

                case 6:

                    Scanner amountInput = new Scanner(System.in);
                    String regex = ".*[0123456789]+.*";

                    while ((amount == null) || (amount.trim().isEmpty() || !amount.matches(regex))) {

                        System.out.print("Enter amount: ");
                        amount = amountInput.nextLine();

                        if ("cancel".equals(amount)) {

                            break;

                        }

                        if ((amount == null) || (amount.trim().isEmpty())) {

                            System.out.println("Not any character is given.");

                        }
                        else if (!amount.matches(regex)) {

                            System.out.println("Not valid characters.");

                        }

                    }

                    if ("cancel".equals(amount)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                    signMenuLoop = true;
                    signMenu = 7;
                    break;

                case 7:

                    Scanner feeInput = new Scanner(System.in);
                    regex = ".*[0123456789]+.*";

                    while ((fee == null) || (fee.trim().isEmpty() || !fee.matches(regex))) {

                        System.out.print("Enter fee(1): ");
                        fee = feeInput.nextLine();

                        if ("cancel".equals(fee)) {

                            break;

                        }

                        if ((fee == null) || (fee.trim().isEmpty())) {

                            System.out.println("Not any character is given.");

                        }
                        else if (!fee.matches(regex)) {

                            System.out.println("Not valid characters.");

                        }
                        else if (Convert.stringToInt(fee) > 999) {

                            System.out.println("Fee not valid.");

                        }

                    }

                    if ("cancel".equals(fee)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                    signMenuLoop = true;
                    signMenu = 8;
                    break;

                case 8:

                    Scanner deadlineInput = new Scanner(System.in);
                    regex = ".*[0123456789]+.*";

                    while ((deadline == null) || (deadline.trim().isEmpty() || !deadline.matches(regex))) {

                        System.out.print("Enter deadline(500): ");
                        deadline = deadlineInput.nextLine();

                        if ("cancel".equals(deadline)) {

                            break;

                        }

                        if ((deadline == null) || (deadline.trim().isEmpty())) {

                            System.out.println("Not any character is given.");

                        }
                        else if (!deadline.matches(regex)) {

                            System.out.println("Not valid characters.");

                        }
                        else if (Convert.stringToInt(deadline) > 1440) {

                            System.out.println("Deadline not valid.");

                        }

                    }

                    if ("cancel".equals(deadline)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                    signMenuLoop = true;
                    signMenu = 9;
                    break;

                case 9:

                    try {
                        byte[] pubKey = Crypto.getPublicKey(new String(decipherSecret));
                        byte[] senderPublicKey = pubKey;
                        long recipientId = Convert.parseAccountId(recipient);
                        long amountNQT = Transaction.convertToNQT(amount);
                        long feeNQT = Transaction.convertToNQT(fee);
                        int deadlInt = Convert.stringToInt(deadline);
                        short deadlineShort = (short) deadlInt;
                        byte[] signature = null;
                        byte[] unsignedBytes = Transaction.getBytes(deadlineShort, senderPublicKey, recipientId, amountNQT, feeNQT, signature);
                        byte[] signatureBytes = Crypto.sign(unsignedBytes, new String(decipherSecret));
                        byte[] signedBytes = Transaction.getBytes(deadlineShort, senderPublicKey, recipientId, amountNQT, feeNQT, signatureBytes);
                        signedFinalBytes = signedBytes;

                        System.out.printf("%-15s %n", "{");
                        System.out.printf("%-15s %n", "\"Recipient\" : " + recipient + ",");
                        System.out.printf("%-15s %n", "\"Amount\" : " + amount + ",");
                        System.out.printf("%-15s %n", "\"Fee\" : " + fee + ",");
                        System.out.printf("%-15s %n", "\"Deadline\" : " + deadline + ",");
                        System.out.printf("%-15s %n", "\"Signature\" : " + Convert.toHexString(signatureBytes) + ",");
                        System.out.printf("%-15s %n", "\"Signed bytes\" : " + Convert.toHexString(signedBytes) + "");
                        System.out.printf("%-15s %n", "}");

                    } catch (Exception e) {
                        System.out.println("Failed to sign, input syntax is wrong.");
                        System.out.println(Options.header);
                        signMenuLoop = false;
                        break;
                    }
                    signMenu = 10;
                    break;

                case 10:

                    System.out.println("Choose integer [1]Save to file, [2]Cancel");

                    input = consoleInput.next();
                    inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {
                        signMenu = 10;
                        System.out.println("The value you have input is not a valid integer.");
                        break;
                    }

                    if ("1".equals(input)) {

                        signMenu = 11;
                        break;

                    }
                    if ("2".equals(input)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                case 11:

                    Scanner backupInput = new Scanner(System.in);

                    while ((backupPath == null) || (backupPath.trim().isEmpty())) {

                        System.out.print("Enter path to save: ");
                        backupPath = backupInput.nextLine();
                        Path path = Paths.get(backupPath);

                        if ("cancel".equals(backupPath)) {

                            break;

                        }

                        if ((backupPath == null) || (backupPath.trim().isEmpty())) {

                            System.out.println("Not any character is given.");

                        }
                        else if (!Files.exists(path)) {

                            backupPath = null;
                            System.out.println("Path does not exist.");

                        }

                    }

                    if ("cancel".equals(backupPath)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                    signMenuLoop = true;
                    signMenu = 12;
                    break;

                case 12:

                    System.out.println("Save file [1]Yes, [2]Cancel");

                    input = consoleInput.next();
                    inputInt = 0;
                    try {
                        inputInt = Integer.parseInt(input);
                    } catch (Exception e) {

                        signMenu = 12;
                        System.out.println("The value you have input is not a valid integer.");
                        break;

                    }

                    if ("1".equals(input)) {

                        if (SystemUtils.IS_OS_LINUX) {

                            backupPath = backupPath + "/";

                        }
                        else {

                            backupPath = backupPath + "\\";

                        }

                        try ( BufferedWriter out = new BufferedWriter(new FileWriter(backupPath + Date.getDate(true) + "_" + "recipient_" + recipient + "_"
                                + "amount_" + amount + Constants.fileType)) ) {

                            out.write(Convert.toHexString(signedFinalBytes));
                            out.flush();
                            out.close();
                            System.out.println("Transaction file successfully saved.");
                            Memory.clearPasswordMemory(decipherSecret);
                            Memory.clearPasswordMemory(password);
                            System.out.println(Options.header);
                            signMenu = 99;
                            break;
                        } catch (IOException e) {
                            Memory.clearPasswordMemory(decipherSecret);
                            Memory.clearPasswordMemory(password);
                            System.out.println("Failed to write transaction file.");
                            System.out.println(Options.header);
                            signMenu = 99;
                            break;
                        }
                    }
                    else if ("2".equals(input)) {

                        Memory.clearPasswordMemory(decipherSecret);
                        Memory.clearPasswordMemory(password);
                        System.out.println(Options.header);
                        signMenu = 99;
                        break;

                    }

                case 99:
                    break;
            }
            if (signMenu == 99) {
                signMenuLoop = false;
            }
        }
    }

}
