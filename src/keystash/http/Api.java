package keystash.http;

import java.io.IOException;
import java.net.InetAddress;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.KeyStashGUI;
import static keystash.command.Processor.isApiEnabled;
import keystash.gui.Icon;
import keystash.util.Config;
import keystash.util.Constants;
import static keystash.util.Constants.apiResourceBaseHome;
import static keystash.util.Constants.apiResourceBaseTest;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Debug;
import keystash.util.Validate;
import org.apache.commons.codec.DecoderException;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class Api {

    public static Server server;

    static int countRows = Db.countRows("keystash_wallet");
    static String[] id;
    static String[] cl;
    static String[] rs;
    static String[] pKey;
    static String[] lab;
    static String[] se;
    static String[] raSalt;
    static String[] raIV;
    static String[] sIt;
    static String[] sKey;
    static String[] eS;
    static String[] sA;
    static String[] aT;
    static String[] timestamp;
    static String aIp;
    static Set<String> allowedBotHosts;
    static boolean allowAll = false;
    static String requestUrl = Constants.apiResourceBase;
    static int logCountRows = Db.countRowsApiLog("keystash_apilog");
    static String[] logId;
    static String[] logReqT;
    static String[] logRes;
    static String[] logMsg;
    static String[] logTS;
    static String apiIp;

    public static void getWalletData(int rows, String[] account_id, String[] addressClassic, String[] addressRs, String[] publicKey, String[] label,
            String[] secret, String[] randomSalt, String[] randomIV, String[] secretIterations, String[] secretKeySize, String[] encryptionStandard, String[] shareAccount,
            String[] accountType, String[] createdTimestamp) {

        countRows = rows;
        id = account_id;
        cl = addressClassic;
        rs = addressRs;
        pKey = publicKey;
        lab = label;
        se = secret;
        raSalt = randomSalt;
        raIV = randomIV;
        sIt = secretIterations;
        sKey = secretKeySize;
        eS = encryptionStandard;
        sA = shareAccount;
        aT = accountType;
        timestamp = createdTimestamp;

    }

    public static void getApiLog(int rows, String[] log_Id, String[] requestType, String[] response, String[] logMessage, String[] logTime) {

        logCountRows = rows;
        logId = log_Id;
        logReqT = requestType;
        logRes = response;
        logMsg = logMessage;
        logTS = logTime;

    }

    public static void startWalletServer(boolean statusServer, String apiHost, String apiAllowHost, int apiPort, String apiResourceBase, boolean apiEnforcePost,
            String apiAdminPassword, boolean apiEnableAdminPassword)
            throws IOException,
            Exception {

        aIp = apiAllowHost;
        apiIp = apiHost;
        boolean isRunning = statusServer;

        if (isRunning && InetAddress.getByName(apiHost).isReachable(5000) && Validate.validIP(apiHost)) {

            try {

                Logger logger = Logger.getLogger("org.eclipse.jetty");
                logger.setLevel(Level.WARNING);
                server = new Server();
                ServerConnector connector;

                boolean enableSSL = Convert.stringToBoolean(Config.properties("keystash.apiSSL"));
                if (enableSSL) {

                    System.out.println("Using SSL (https) for the API server");
                    HttpConfiguration https_config = new HttpConfiguration();
                    https_config.setSecureScheme("https");
                    https_config.setSecurePort(apiPort);
                    https_config.addCustomizer(new SecureRequestCustomizer());
                    SslContextFactory sslContextFactory = new SslContextFactory();
                    sslContextFactory.setKeyStorePath(Config.properties("keystash.keyStorePath"));
                    sslContextFactory.setKeyStorePassword(Config.properties("keystash.keyStorePassword"));
                    sslContextFactory.setExcludeCipherSuites("SSL_RSA_WITH_DES_CBC_SHA", "SSL_DHE_RSA_WITH_DES_CBC_SHA",
                            "SSL_DHE_DSS_WITH_DES_CBC_SHA", "SSL_RSA_EXPORT_WITH_RC4_40_MD5", "SSL_RSA_EXPORT_WITH_DES40_CBC_SHA",
                            "SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA", "SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA");
                    sslContextFactory.setExcludeProtocols("SSLv3");
                    connector = new ServerConnector(server, new SslConnectionFactory(sslContextFactory, "http/1.1"),
                            new HttpConnectionFactory(https_config));

                }
                else {

                    connector = new ServerConnector(server);

                }

                connector.setPort(apiPort);
                connector.setHost(apiHost);
                connector.setIdleTimeout(Long.valueOf(Config.properties("keystash.apiServerIdleTimeout")));
                connector.setReuseAddress(true);
                server.addConnector(connector);
                ServletContextHandler apiHandler = new ServletContextHandler();
                String apiResourceFolder = Config.properties("keystash.apiResourceFolder");

                if (apiResourceFolder != null) {

                    ServletHolder defaultServletHolder = new ServletHolder(new DefaultServlet());
                    defaultServletHolder.setInitParameter("dirAllowed", "false");
                    defaultServletHolder.setInitParameter("resourceBase", apiResourceFolder);
                    defaultServletHolder.setInitParameter("gzip", "true");
                    apiHandler.addServlet(defaultServletHolder, "/*");

                }

                if (Convert.stringToBoolean(Config.properties("keystash.apiServerCORS"))) {

                    FilterHolder filterHolder = apiHandler.addFilter(CrossOriginFilter.class, "/*", null);
                    filterHolder.setInitParameter("allowedHeaders", "*");
                    filterHolder.setAsyncSupported(true);

                }

                server.setHandler(apiHandler);
                server.setStopAtShutdown(true);
                apiHandler.addServlet(ApiServlet.class, apiResourceBase);
                apiHandler.addServlet(ApiTestServlet.class, apiResourceBaseTest);
                apiHandler.addServlet(ApiHomeServlet.class, apiResourceBaseHome);
                server.start();

                if (Constants.keystashd) {

                    isApiEnabled = true;

                }
                else {

                    Icon.initializeIcons(false);

                }

            } catch (Exception e) {

                System.out.println("Failed to start API server.");
                Debug.addLoggMessage("Failed to start API server.");
                ApiLog.logMessage("server", "error", "failed to start API server.", "");

                if (Constants.keystashd) {

                    isApiEnabled = false;

                }
                else {

                    Icon.initializeIcons(true);

                }

            }

            System.out.println("Started API server at " + apiHost + ":" + apiPort);
            ApiLog.logMessage("server", "success", "started API server at " + apiHost + ":" + apiPort, "");

        }
        else if (isRunning) {

            System.out.println("Ip address seems not valid. Failed to start API server.");
            Debug.addLoggMessage("Ip address seems not valid. Failed to start API server.");
            ApiLog.logMessage("server", "error", "ip address seems not valid. failed to start API server.", "");

            if (Constants.keystashd) {

                isApiEnabled = false;

            }
            else {

                Icon.initializeIcons(true);

            }
        }
        if (!isRunning) {

            try {

                System.out.println("stop API server");
                ApiLog.logMessage("server", "success", "stop API server.", "");
                server.setStopTimeout(10000L);
                server.stop();

            } catch (Exception e) {

                System.out.println("Failed to stop API server.");
                Debug.addLoggMessage("Failed to stop API server.");
                ApiLog.logMessage("server", "error", "failed to stop API server.", "");

            }

        }

    }

    public static class ApiTestServlet extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException,
                IOException {

            List<String> allowedBotHostsList = getStringListProperty(aIp);

            if (!allowedBotHostsList.contains("*")) {

                allowedBotHosts = Collections.unmodifiableSet(new HashSet<>(allowedBotHostsList));

            }
            else {

                allowedBotHosts = null;
                allowAll = true;

            }

            if (allowedBotHosts != null && allowedBotHosts.contains(request.getRemoteHost()) || allowAll) {

                ApiTest.apiTestPage(request, response);

            }
            else {

                response.sendError(HttpServletResponse.SC_FORBIDDEN);

            }

        }
    }

    public static class ApiHomeServlet extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException,
                IOException {

            List<String> allowedBotHostsList = getStringListProperty(aIp);

            if (!allowedBotHostsList.contains("*")) {

                allowedBotHosts = Collections.unmodifiableSet(new HashSet<>(allowedBotHostsList));

            }
            else {

                allowedBotHosts = null;
                allowAll = true;

            }

            if (allowedBotHosts != null && allowedBotHosts.contains(request.getRemoteHost()) || allowAll) {

                try {
                    ApiHome.apiHomePage(request, response);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(Api.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            else {

                response.sendError(HttpServletResponse.SC_FORBIDDEN);

            }

        }
    }

    public static class ApiServlet extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
                throws ServletException,
                IOException {

            JSONObject json = new JSONObject();

            List<String> allowedBotHostsList = getStringListProperty(aIp);

            if (!allowedBotHostsList.contains("*")) {

                allowedBotHosts = Collections.unmodifiableSet(new HashSet<>(allowedBotHostsList));

            }
            else {

                allowedBotHosts = null;
                allowAll = true;

            }

            String apiPassword = null;
            String apiRequestType = null;

            try {

                apiPassword = request.getParameter("password");
                apiRequestType = request.getParameter("requestType");

            } catch (Exception e) {

                response.getWriter().println("{\"error\": [");
                json.put("reason", "requestType not specified");
                response.getWriter().println(json);

            }

            response.setContentType("text/javascript");
            response.setStatus(HttpServletResponse.SC_OK);

            if (allowedBotHosts != null && allowedBotHosts.contains(request.getRemoteHost()) || allowAll) {

                if ("jsonp".equals(Config.properties("keystash.apiFormat"))) {

                    response.getWriter().println("keystash(");
                }

                if (ApiProcessor.apiValidateInput(apiRequestType) && Convert.stringToBoolean(Config.properties("keystash.enableAPIServer")) || KeyStashGUI.runDaemon == false
                        || Convert.stringToBoolean(Config.properties("keystash.enableApiPassword")) && Config.properties("keystash.apiPassword").equals(apiPassword)) {

                    if (apiRequestType.equals(ApiOptions.option1)) {

                        response.getWriter().println("{\"" + ApiOptions.option1_1 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option2)) {

                        response.getWriter().println("{\"" + ApiOptions.option2_2 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option3)) {

                        response.getWriter().println("{\"" + ApiOptions.option3_3 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option4)) {

                        response.getWriter().println("{\"" + ApiOptions.option4_4 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option5)) {

                        response.getWriter().println("{\"" + ApiOptions.option5_5 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option6)) {

                        response.getWriter().println("{\"" + ApiOptions.option6_6 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option7)) {

                        response.getWriter().println("{\"" + ApiOptions.option7_7 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option8)) {

                        response.getWriter().println("{\"" + ApiOptions.option8_8 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option9)) {

                        response.getWriter().println("{\"" + ApiOptions.option9_9 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option10)) {

                        response.getWriter().println("{\"" + ApiOptions.option10_10 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option11)) {

                        response.getWriter().println("{\"" + ApiOptions.option11_11 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option12)) {

                        response.getWriter().println("{\"" + ApiOptions.option12_12 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option13)) {

                        response.getWriter().println("{\"" + ApiOptions.option13_13 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option14)) {

                        response.getWriter().println("{\"" + ApiOptions.option14_14 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option15)) {

                        response.getWriter().println("{\"" + ApiOptions.option15_15 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option16)) {

                        response.getWriter().println("{\"" + ApiOptions.option16_16 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option17)) {

                        response.getWriter().println("{\"" + ApiOptions.option17_17 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option18)) {

                        response.getWriter().println("{\"" + ApiOptions.option18_18 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option19)) {

                        response.getWriter().println("{\"" + ApiOptions.option19_19 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option20)) {

                        response.getWriter().println("{\"" + ApiOptions.option20_20 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option21)) {

                        response.getWriter().println("{\"" + ApiOptions.option21_21 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option22)) {

                        response.getWriter().println("{\"" + ApiOptions.option22_22 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option23)) {

                        response.getWriter().println("{\"" + ApiOptions.option23_23 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option24)) {

                        response.getWriter().println("{\"" + ApiOptions.option24_24 + "\": [");

                    }
                    else if (apiRequestType.equals(ApiOptions.option99)) {

                        response.getWriter().println("{\"" + ApiOptions.option99_99 + "\": [");

                    }

                }

                if (!Convert.stringToBoolean(Config.properties("keystash.enableAPIServer")) && KeyStashGUI.runDaemon == true) {

                    showError(1, request, response);

                }
                else if (Convert.stringToBoolean(Config.properties("keystash.enableAPIServer")) && !ApiProcessor.apiValidateInput(apiRequestType)) {

                    showError(2, request, response);

                }

                else {

                    if (!Convert.stringToBoolean(Config.properties("keystash.enableApiPassword")) || Convert.stringToBoolean(Config.properties("keystash.enableApiPassword"))
                            && Config.properties("keystash.apiPassword").equals(apiPassword)) {

                        try {

                            ApiProcessor.ApiServlet.filterInput(apiRequestType, request, response);

                        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | DecoderException | InvalidKeySpecException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException | ParseException | SQLException | InterruptedException ex) {

                            Debug.addLoggMessage("APIProcessor cant handle request.");
                            ApiLog.logMessage("server", "error", "APIProcessor cant handle request.", "");

                        }

                    }
                    else {

                        showError(3, request, response);

                    }

                }

                if ("jsonp".equals(Config.properties("keystash.apiFormat"))) {

                    response.getWriter().println("]});");

                }
                else if ("json".equals(Config.properties("keystash.apiFormat"))) {

                    response.getWriter().println("]}");

                }

            }
            else {

                showError(4, request, response);

            }
        }
    }

    public static void showError(int type, HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        JSONObject json = new JSONObject();

        if (type == 1) {

            json.put("error", "API not enabled");
            ApiLog.logMessage("server", "error", "API not enabled.", "");

        }
        else if (type == 2) {

            json.put("error", "requestType not specified");
            ApiLog.logMessage("server", "error", "requestType not specified.", "");

        }
        else if (type == 3) {

            json.put("error", "password invalid, protection for API requests enabled");
            ApiLog.logMessage("server", "error", "password invalid, protection for API requests enabled.", "");

        }
        else if (type == 4) {

            response.sendError(HttpServletResponse.SC_FORBIDDEN);

        }

        if (type != 4) {

            response.getWriter().println(json);

        }

    }

    public static List<String> getStringListProperty(String name) {
        String value = name;
        if (value == null || value.length() == 0) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<>();
        for (String s : value.split(";")) {
            s = s.trim();
            if (s.length() > 0) {
                result.add(s);
            }
        }
        return result;
    }

}
