package keystash.http;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import static keystash.http.Api.cl;
import static keystash.http.Api.countRows;
import static keystash.http.Api.eS;
import static keystash.http.Api.pKey;
import static keystash.http.Api.raIV;
import static keystash.http.Api.raSalt;
import static keystash.http.Api.rs;
import static keystash.http.Api.sIt;
import static keystash.http.Api.sKey;
import static keystash.http.Api.se;
import keystash.sign.Transaction;
import keystash.util.Config;
import keystash.util.Convert;
import keystash.util.Memory;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionSign {

    public static class ApiServlet extends HttpServlet {

        public static void signOffline(HttpServletRequest request, HttpServletResponse response)
                throws IOException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiAssignment = null;
            char[] apiDecryptPassword = null;
            char[] decipherSecret;
            String publicKey;
            String apiSenderAccount = null;
            String apiRecipient = null;
            String apiAmountNQT = null;
            String apiFeeNQT = null;
            String apiDeadline = null;
            String secret = null;
            String salt = null;
            String IV = null;
            int iteration = 0;
            int keySize = 0;
            String encryptionStandard = null;

            try {

                apiAssignment = request.getParameter("assignment");
                apiSenderAccount = request.getParameter("senderAccount");
                apiRecipient = request.getParameter("recipient");
                apiAmountNQT = request.getParameter("amountNQT");
                apiFeeNQT = request.getParameter("feeNQT");
                apiDeadline = request.getParameter("deadline");
                apiDecryptPassword = request.getParameter("decryptPassword").toCharArray();

            } catch (Exception e) {

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("signTransaction", "error", "apiServerEnforcePOST is disabled.", apiAssignment);

            }
            else if (apiSenderAccount == null && apiRecipient == null && apiAmountNQT == null && apiFeeNQT == null && apiDeadline == null && apiDecryptPassword == null) {

                json.put("error", "senderAccount, recipient, amountNQT, feeNQT, deadline, decryptPassword not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("signTransaction", "error", "senderAccount, recipient, amountNQT, feeNQT, deadline, decryptPassword not specified.", apiAssignment);

            }

            if (countRows == 0) {

                json.put("error", "accountQuantity = 0");
                response.getWriter().println(json);
                ApiLog.logMessage("signTransaction", "error", "accountQuantity = 0.", apiAssignment);

            }
            else if (apiSenderAccount != null) {

                int countAccounts = 0;
                for (int i = 1; i < countRows; i++) {

                    if (apiSenderAccount.equals(rs[i]) || apiSenderAccount.equals(cl[i])) {

                        publicKey = pKey[i];
                        secret = se[i];
                        iteration = Convert.stringToInt(sIt[i]);
                        keySize = Convert.stringToInt(sKey[i]);
                        encryptionStandard = eS[i];
                        IV = raIV[i];
                        salt = raSalt[i];
                        countAccounts++;

                    }

                }

                if (countAccounts == 0) {

                    json.put("error", "senderAccount not in wallet file");
                    response.getWriter().println(json);
                    ApiLog.logMessage("signTransaction", "error", "senderAccount not in wallet file.", apiAssignment);

                }
                else {

                    try {
                        decipherSecret = Aes.decrypt(secret, new String(apiDecryptPassword), salt, IV, iteration, keySize, encryptionStandard);
                        byte[] pubKey = Crypto.getPublicKey(new String(decipherSecret));
                        byte[] senderPublicKey = pubKey;
                        long recipientId = Convert.parseAccountId(apiRecipient);
                        int amountInt = Convert.stringToInt(apiAmountNQT);
                        long amountNQT = (long) amountInt;
                        int feeInt = Convert.stringToInt(apiFeeNQT);
                        long feeNQT = (long) feeInt;
                        int deadlInt = Convert.stringToInt(apiDeadline);
                        short deadlineShort = (short) deadlInt;
                        byte[] signature = null;
                        byte[] unsignedBytes = Transaction.getBytes(deadlineShort, senderPublicKey, recipientId, amountNQT, feeNQT, signature);
                        byte[] signatureBytes = Crypto.sign(unsignedBytes, new String(decipherSecret));
                        byte[] signedBytes = Transaction.getBytes(deadlineShort, senderPublicKey, recipientId, amountNQT, feeNQT, signatureBytes);

                        jsonOrderedMap.put("recipient", apiRecipient);
                        jsonOrderedMap.put("amountNQT", apiAmountNQT);
                        jsonOrderedMap.put("feeNQT", apiFeeNQT);
                        jsonOrderedMap.put("deadline", apiDeadline);
                        jsonOrderedMap.put("signature", Convert.toHexString(signatureBytes));
                        jsonOrderedMap.put("signedBytes", Convert.toHexString(signedBytes));
                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        ApiLog.logMessage("signTransaction", "success", "amount " + apiAmountNQT + "NQT, sender: " + apiSenderAccount + ", recipient: " + apiRecipient, apiAssignment);

                        Memory.clearPasswordMemory(apiDecryptPassword);

                    } catch (Exception e) {

                        if (apiSenderAccount == null) {

                            json.put("error", "senderAccount not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "senderAccount not specified.", apiAssignment);

                        }
                        else if (apiRecipient == null) {

                            json.put("error", "recipient not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "recipient not specified.", apiAssignment);

                        }
                        else if (apiAmountNQT == null) {

                            json.put("error", "amountNQT not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "amountNQT not specified.", apiAssignment);

                        }
                        else if (apiFeeNQT == null) {

                            json.put("error", "feeNQT not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "feeNQT not specified.", apiAssignment);

                        }
                        else if (apiDeadline == null) {

                            json.put("error", "deadline not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "deadline not specified.", apiAssignment);

                        }
                        else if (apiDecryptPassword == null) {

                            json.put("error", "decryptPassword not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "decryptPassword not specified.", apiAssignment);

                        }
                        else {

                            json.put("error", "syntax or decryptPassword wrong, offline signing failed");
                            response.getWriter().println(json);
                            ApiLog.logMessage("signTransaction", "error", "syntax or decryptPassword wrong, offline signing failed", apiAssignment);

                        }

                    }

                }
            }
        }
    }
}
