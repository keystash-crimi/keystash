package keystash.http;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.util.Config;
import keystash.util.Convert;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionBroadcast {

    public static class ApiServlet extends HttpServlet {

        public static void broadcastBytes(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiAssignment = null;
            String apiSignedBytes = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";

            try {

                apiAssignment = request.getParameter("assignment");
                apiSignedBytes = request.getParameter("signedBytes");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "signedBytes, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("broadcastTransaction", "error", "signedBytes, server, serverPort must be specified.", "");

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("broadcastTransaction", "error", "apiServerEnforcePOST is disabled.", "");

            }
            else if (apiSignedBytes == null) {

                json.put("error", "signedBytes not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("broadcastTransaction", "error", "signedBytes not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("broadcastTransaction", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("broadcastTransaction", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("broadcastTransaction", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=broadcastTransaction&transactionBytes=" + apiSignedBytes;

                try {

                    String responseRequest = IOUtils.toString(new URL(url));
                    JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);
                    jsonOrderedMap.put("fullHash", responseJsonObject.get("fullHash"));
                    jsonOrderedMap.put("transaction", responseJsonObject.get("transaction"));

                    if (responseJsonObject.get("fullHash") == null || responseJsonObject.get("transaction") == null) {

                        json.put("error", "signedBytes not valid, transaction not broadcasted!");
                        response.getWriter().println(json);
                        ApiLog.logMessage("broadcastTransaction", "error", "signedBytes not valid, transaction not broadcasted!", "");

                    }
                    else {

                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        ApiLog.logMessage("broadcastTransaction", "success", "transaction: " + responseJsonObject.get("transaction").toString(), "");

                    }

                } catch (IOException | ParseException e) {

                    json.put("error", "broadcastTransaction failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("broadcastTransaction", "error", "broadcastTransaction failed", "");

                }

            }
        }
    }
}
