package keystash.http;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.ApiOptions.apiCountOptions;
import static keystash.http.ApiOptions.apiListOption;
import keystash.util.Config;
import keystash.util.Convert;
import org.apache.commons.codec.DecoderException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiProcessor {

    public static boolean apiValidateInput(String option) {

        ArrayList<String> apiOptionArray = new ArrayList<>();
        for (int i = 0; i < apiCountOptions(); i++) {

            apiOptionArray.add(apiListOption(i));

        }

        boolean isValidOption = false;
        if (apiOptionArray.contains(option)) {

            isValidOption = true;

        }

        return isValidOption;

    }

    public static class ApiServlet extends HttpServlet {

        public static void filterInput(String option, HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException,
                NoSuchProviderException,
                NoSuchPaddingException,
                DecoderException,
                InvalidKeySpecException,
                InvalidKeyException,
                InvalidAlgorithmParameterException,
                IllegalBlockSizeException,
                BadPaddingException,
                ParseException,
                SQLException,
                InterruptedException {

            JSONObject json = new JSONObject();

            if (option != null) {

                if (option.equals(ApiOptions.option1)) {

                    ApiOptionInfo.ApiServlet.getInfo(request, response);

                }

                if (option.equals(ApiOptions.option2)) {

                    ApiOptionAccount.ApiServlet.getAccount(request, response);

                }

                if (option.equals(ApiOptions.option3)) {

                    ApiOptionAccountLabel.ApiServlet.getAccountByLabel(request, response);

                }

                if (option.equals(ApiOptions.option4)) {

                    ApiOptionNewAccount.ApiServlet.getNewAddress(request, response);

                }

                if (option.equals(ApiOptions.option5)) {

                    ApiOptionAccounts.ApiServlet.getAccounts(request, response);

                }

                if (option.equals(ApiOptions.option6)) {

                    ApiOptionImport.ApiServlet.importAccount(request, response);

                }

                if (option.equals(ApiOptions.option7)) {

                    ApiOptionDecrypt.ApiServlet.decryptSecret(request, response);

                }

                if (option.equals(ApiOptions.option8)) {

                    ApiOptionLabel.ApiServlet.changeLabel(request, response);

                }

                if (option.equals(ApiOptions.option9)) {

                    ApiOptionSign.ApiServlet.signOffline(request, response);

                }

                if (option.equals(ApiOptions.option10)) {

                    ApiOptionParse.ApiServlet.parseBytes(request, response);

                }

                if (option.equals(ApiOptions.option11)) {

                    ApiOptionBroadcast.ApiServlet.broadcastBytes(request, response);

                }

                if (option.equals(ApiOptions.option12)) {

                    ApiOptionSinceBlock.ApiServlet.getSinceBlock(request, response);

                }

                if (option.equals(ApiOptions.option13)) {

                    ApiOptionReceivedAccount.ApiServlet.receivedTransactionsAccount(request, response);

                }

                if (option.equals(ApiOptions.option14)) {

                    ApiOptionReceivedLabel.ApiServlet.receivedTransactionsLabel(request, response);

                }

                if (option.equals(ApiOptions.option15)) {

                    ApiOptionUnconfirmed.ApiServlet.unconfirmedTransactions(request, response);

                }

                if (option.equals(ApiOptions.option16)) {

                    ApiOptionUnconfirmedAccount.ApiServlet.unconfirmedTransactionsAccount(request, response);

                }

                if (option.equals(ApiOptions.option17)) {

                    ApiOptionUnconfirmedLabel.ApiServlet.unconfirmedTransactionsLabel(request, response);

                }

                if (option.equals(ApiOptions.option18)) {

                    ApiOptionTransactionInfo.ApiServlet.transactionInfo(request, response);

                }

                if (option.equals(ApiOptions.option19)) {

                    ApiOptionBalanceAccount.ApiServlet.balanceAccount(request, response);

                }

                if (option.equals(ApiOptions.option20)) {

                    ApiOptionBalanceLabel.ApiServlet.balancLabel(request, response);

                }

                if (option.equals(ApiOptions.option21)) {

                    ApiOptionBackup.ApiServlet.saveBackup(request, response);

                }

                if (option.equals(ApiOptions.option22)) {

                    ApiOptionEncrypt.ApiServlet.encryptWallet(request, response);

                }

                if (option.equals(ApiOptions.option23)) {
                    
                    ApiOptionToken.ApiServlet.generateToken(request, response);

                }
                
                if (option.equals(ApiOptions.option24)) {
                    
                   ApiOptionDecodeToken.ApiServlet.decodeToken(request, response);

                }

                if (option.equals(ApiOptions.option99)) {

                    ApiOptionMonitor.ApiServlet.logMonitor(request, response);

                }

            }
            else {

                if (!Convert.stringToBoolean(Config.properties("keystash.enableAPIServer"))) {

                    response.getWriter().println("{\"error\": [");
                    json.put("reason", "API not enabled");
                    response.getWriter().println(json);

                }
                else {

                    response.getWriter().println("{\"error\": [");
                    json.put("reason", "requestType not specified");
                    response.getWriter().println(json);

                }
            }
        }
    }

}
