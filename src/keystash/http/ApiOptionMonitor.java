package keystash.http;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.Api.logCountRows;
import static keystash.http.Api.logId;
import static keystash.http.Api.logMsg;
import static keystash.http.Api.logReqT;
import static keystash.http.Api.logRes;
import static keystash.http.Api.logTS;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionMonitor {

    public static class ApiServlet extends HttpServlet {

        public static void logMonitor(HttpServletRequest request, HttpServletResponse response)
                throws IOException {

            JSONObject json = new JSONObject();
            ApiRefresh.refreshApiLog();

            if (logCountRows == 0) {

                json.put("error", "no API logs found.");
                response.getWriter().println(json);

            }
            else {

                Map jsonOrderedMap = new LinkedHashMap();

                for (int i = 1; i < logCountRows; i++) {

                    jsonOrderedMap.put("id", logId[i]);
                    jsonOrderedMap.put("requestType", logReqT[i]);
                    jsonOrderedMap.put("response", logRes[i]);
                    jsonOrderedMap.put("message", logMsg[i]);
                    jsonOrderedMap.put("createdDate", logTS[i]);

                    response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));

                    if (i != logCountRows - 1) {
                        response.getWriter().println(", ");

                    }
                }
            }
        }
    }
}
