package keystash.http;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.Api.aT;
import static keystash.http.Api.cl;
import static keystash.http.Api.countRows;
import static keystash.http.Api.eS;
import static keystash.http.Api.id;
import static keystash.http.Api.lab;
import static keystash.http.Api.pKey;
import static keystash.http.Api.raIV;
import static keystash.http.Api.raSalt;
import static keystash.http.Api.rs;
import static keystash.http.Api.sIt;
import static keystash.http.Api.sKey;
import static keystash.http.Api.se;
import static keystash.http.Api.timestamp;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionAccounts {

    public static class ApiServlet extends HttpServlet {

        public static void getAccounts(HttpServletRequest request, HttpServletResponse response)
                throws IOException {

            JSONObject json = new JSONObject();

            if (countRows == 0) {

                json.put("error", "accountQuantity = 0");
                response.getWriter().println(json);
                ApiLog.logMessage("getAccounts", "error", "accountQuantity = 0.", "");

            }
            else {

                Map jsonOrderedMap = new LinkedHashMap();

                for (int i = 1; i < countRows; i++) {

                    jsonOrderedMap.put("accountId", id[i]);
                    jsonOrderedMap.put("accountType", aT[i]);
                    jsonOrderedMap.put("address", cl[i]);
                    jsonOrderedMap.put("addressRS", rs[i]);
                    jsonOrderedMap.put("publicKey", pKey[i]);
                    jsonOrderedMap.put("label", lab[i]);
                    jsonOrderedMap.put("secret", se[i]);
                    jsonOrderedMap.put("randomSalt", raSalt[i]);
                    jsonOrderedMap.put("randomIV", raIV[i]);
                    jsonOrderedMap.put("secretIteration", sIt[i]);
                    jsonOrderedMap.put("secretKeySize", sKey[i]);
                    jsonOrderedMap.put("secretGeneratorAlgorithm", eS[i]);
                    jsonOrderedMap.put("createdDate", timestamp[i]);
                    response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));

                    if (i != countRows - 1) {
                        response.getWriter().println(", ");

                    }
                }

                ApiLog.logMessage("getAccounts", "success", "", "");

            }
        }
    }
}
