package keystash.http;

import java.io.IOException;
import java.net.URL;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionUnconfirmedAccount {

    public static class ApiServlet extends HttpServlet {

        public static void unconfirmedTransactionsAccount(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            String apiAddress = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";
            try {

                apiAddress = request.getParameter("address");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "address, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmedByAccount", "error", "address, server, serverPort must be specified.", "");

            }

            if (apiAddress == null) {

                json.put("error", "address not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmedByAccount", "error", "address not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmedByAccount", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmedByAccount", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmedByAccount", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getUnconfirmedTransactions&account="
                        + apiAddress;

                try {

                    String responseRequest = IOUtils.toString(new URL(url));
                    JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);

                    if (responseJsonObject.get("unconfirmedTransactions") == null) {

                        json.put("error", "no unconfirmed transactions found");
                        response.getWriter().println(json);
                        ApiLog.logMessage("getUnconfirmedByAccount", "error", "no unconfirmed transactions found.", "");

                    }
                    else {
                        json.put("unconfirmedTransactions", responseJsonObject.get("unconfirmedTransactions"));
                        response.getWriter().println(json);
                        ApiLog.logMessage("getUnconfirmedByAccount", "success", "", "");

                    }

                } catch (IOException | ParseException e) {

                    json.put("error", "getUnconfirmedByAccount failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getUnconfirmedByAccount", "error", "getUnconfirmedByAccount failed.", "");

                }

            }
        }
    }
}
