package keystash.http;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.crypto.Aes;
import static keystash.http.Api.cl;
import static keystash.http.Api.countRows;
import static keystash.http.Api.eS;
import static keystash.http.Api.id;
import static keystash.http.Api.raIV;
import static keystash.http.Api.raSalt;
import static keystash.http.Api.rs;
import static keystash.http.Api.sIt;
import static keystash.http.Api.sKey;
import static keystash.http.Api.se;
import keystash.util.Convert;
import keystash.util.Memory;
import keystash.util.Token;
import org.apache.commons.codec.DecoderException;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class ApiOptionToken {

    public static class ApiServlet extends HttpServlet {

        public static void generateToken(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException,
                NoSuchProviderException,
                NoSuchPaddingException,
                DecoderException,
                InvalidKeySpecException,
                InvalidKeyException,
                InvalidAlgorithmParameterException,
                IllegalBlockSizeException,
                BadPaddingException {

            JSONObject json = new JSONObject();
            String apiAssignment = null;
            String apiAddress = null;
            String apiData = null;
            char[] apiDecryptPassword = null;
            String addressRS = null;
            String secret = null;
            String accountId = null;
            String salt = null;
            String IV = null;
            int iteration = 0;
            int keySize = 0;
            String encryptionStandard = null;
            char[] decipherSecret = null;

            try {

                apiAssignment = request.getParameter("assignment");
                apiAddress = request.getParameter("address");
                apiData = request.getParameter("data");
                apiDecryptPassword = request.getParameter("decryptPassword").toCharArray();

            } catch (Exception e) {

            }

            if (apiAddress == null && apiDecryptPassword == null && apiData == null) {

                json.put("error", "address, data, decryptPassword must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("generateToken", "error", "address, label, decryptPassword must be specified.", apiAssignment);

            }
            else if (countRows == 0) {

                json.put("error", "accountQuantity = 0");
                response.getWriter().println(json);
                ApiLog.logMessage("generateToken", "error", "accountQuantity = 0.", apiAssignment);

            }
            else if (apiAddress != null) {

                int countAccounts = 0;

                for (int i = 1; i < countRows; i++) {

                    if (apiAddress.equals(rs[i]) || apiAddress.equals(cl[i])) {

                        addressRS = rs[i];
                        accountId = id[i];
                        secret = se[i];
                        iteration = Convert.stringToInt(sIt[i]);
                        keySize = Convert.stringToInt(sKey[i]);
                        encryptionStandard = eS[i];
                        IV = raIV[i];
                        salt = raSalt[i];
                        countAccounts++;

                    }

                }

                if (countAccounts == 0) {

                    json.put("error", "address not in wallet file");
                    response.getWriter().println(json);
                    ApiLog.logMessage("generateToken", "error", apiAddress + " not in wallet file", apiAssignment);

                }
                else {

                    try {

                        decipherSecret = Aes.decrypt(secret, new String(apiDecryptPassword), salt, IV, iteration, keySize, encryptionStandard);

                        String tokenString = Token.generateToken(String.valueOf(decipherSecret), apiData.trim());

                        json.put("token", tokenString);
                        response.getWriter().println(json);
                        ApiLog.logMessage("generateToken", "success", "", apiAssignment);
                        Memory.clearPasswordMemory(decipherSecret);

                    } catch (Exception e) {

                        if (apiAddress == null) {

                            json.put("error", "address not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("generateToken", "error", "address not specified.", apiAssignment);

                        }
                        else if (apiData == null) {

                            json.put("error", "data not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("generateToken", "error", "data not specified.", apiAssignment);

                        }
                        else {

                            json.put("error", "decryptPassword is invalid");
                            response.getWriter().println(json);
                            ApiLog.logMessage("generateToken", "error", "decryptPassword is invalid.", apiAssignment);

                        }

                        Memory.clearPasswordMemory(apiDecryptPassword);
                       
                    }
                }
            }
        }
    }
}
