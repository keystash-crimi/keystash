package keystash.http;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.Api.countRows;
import static keystash.http.Api.lab;
import static keystash.http.Api.rs;
import keystash.util.Convert;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionBalanceLabel {

    public static class ApiServlet extends HttpServlet {

        public static void balancLabel(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            JSONArray jsonArrayBalance = new JSONArray();
            String apiLabel = null;
            String apiNumberOfConfirmations = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";

            try {

                apiLabel = request.getParameter("label");
                apiNumberOfConfirmations = request.getParameter("numberOfConfirmations");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "label, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByLabel", "error", "label, server, serverPort must be specified.", "");

            }

            if (apiLabel == null) {

                json.put("error", "label not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByLabel", "error", "label not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByLabel", "error", "label not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByLabel", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByLabel", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url;

                String responseRequest;
                JSONObject responseJsonObject = null;
                ArrayList<Double> sumNQT = new ArrayList<>();
                double totalNQT = 0;

                for (int i = 1; i < countRows; i++) {

                    if (apiLabel.equals(lab[i])) {

                        if (apiNumberOfConfirmations != null) {

                            url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getGuaranteedBalance&account="
                                    + rs[i] + "&numberOfConfirmations=" + apiNumberOfConfirmations;

                        }
                        else {

                            url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getGuaranteedBalance&account="
                                    + rs[i];

                        }

                        try {

                            responseRequest = IOUtils.toString(new URL(url));
                            responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);

                            if (responseJsonObject.get("guaranteedBalanceNQT") != null) {

                                jsonArrayBalance.add(responseJsonObject.get("guaranteedBalanceNQT"));

                                String bla = responseJsonObject.get("guaranteedBalanceNQT").toString();
                                double bla2 = Double.parseDouble(bla) / 100000000;

                                sumNQT.add((double) bla2);

                            }

                        } catch (IOException | ParseException e) {

                            json.put("error", "getBalanceByLabel failed");
                            response.getWriter().println(json);
                            ApiLog.logMessage("getBalanceByLabel", "error", "getBalanceByLabel failed.", "");

                        }

                    }

                }

                if (responseJsonObject == null) {

                    json.put("error", "no account found with this label");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getBalanceByLabel", "error", "no account found with this label.", "");

                }
                else {

                    for (Double localSumNQTLOCAL : sumNQT) {

                        totalNQT = totalNQT + localSumNQTLOCAL;

                    }

                    String sumString = String.valueOf(totalNQT);
                    long totalAmountNQT = Convert.parseNXT(sumString);
                    json.put("guaranteedBalanceNQT", totalAmountNQT);
                    response.getWriter().println(json);
                    ApiLog.logMessage("getBalanceByLabel", "success", apiLabel, "");

                }

            }
        }
    }
}
