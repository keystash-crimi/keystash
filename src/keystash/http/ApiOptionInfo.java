package keystash.http;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.KeyStashGUI;
import keystash.crypto.KeyLength;
import static keystash.http.Api.apiIp;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Db;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionInfo {

    public static class ApiServlet extends HttpServlet {

        public static void getInfo(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException {

            int countRows = Db.countRows("keystash_wallet");
            boolean keyLength = KeyLength.isUnlimitedKeyStrength();
            Map jsonOrderedMap = new LinkedHashMap();
            jsonOrderedMap.put("keystashVersion", Constants.keyStashVersion);
            jsonOrderedMap.put("hostname", apiIp);
            jsonOrderedMap.put("walletEncrypted", KeyStashGUI.isWalletEncrypted);
            jsonOrderedMap.put("numberOfAccounts", countRows);
            jsonOrderedMap.put("unlimitedKeyStrengthAES", keyLength);
            jsonOrderedMap.put("enableProperties", Config.properties("keystash.enableProperties"));
            jsonOrderedMap.put("apiServerEnforcePOST", Config.properties("keystash.apiServerEnforcePOST"));
            jsonOrderedMap.put("apiFormat", Config.properties("keystash.apiFormat"));
            jsonOrderedMap.put("enableApiPassword", Config.properties("keystash.enableApiPassword"));
            response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
            ApiLog.logMessage("getInfo", "success", "", "");

        }
    }
}
