package keystash.http;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.ApiTest.ApiTestServlet.apiOption1;
import static keystash.http.ApiTest.ApiTestServlet.apiOption10;
import static keystash.http.ApiTest.ApiTestServlet.apiOption11;
import static keystash.http.ApiTest.ApiTestServlet.apiOption12;
import static keystash.http.ApiTest.ApiTestServlet.apiOption13;
import static keystash.http.ApiTest.ApiTestServlet.apiOption14;
import static keystash.http.ApiTest.ApiTestServlet.apiOption15;
import static keystash.http.ApiTest.ApiTestServlet.apiOption16;
import static keystash.http.ApiTest.ApiTestServlet.apiOption17;
import static keystash.http.ApiTest.ApiTestServlet.apiOption18;
import static keystash.http.ApiTest.ApiTestServlet.apiOption19;
import static keystash.http.ApiTest.ApiTestServlet.apiOption2;
import static keystash.http.ApiTest.ApiTestServlet.apiOption20;
import static keystash.http.ApiTest.ApiTestServlet.apiOption21;
import static keystash.http.ApiTest.ApiTestServlet.apiOption22;
import static keystash.http.ApiTest.ApiTestServlet.apiOption23;
import static keystash.http.ApiTest.ApiTestServlet.apiOption24;
import static keystash.http.ApiTest.ApiTestServlet.apiOption3;
import static keystash.http.ApiTest.ApiTestServlet.apiOption4;
import static keystash.http.ApiTest.ApiTestServlet.apiOption5;
import static keystash.http.ApiTest.ApiTestServlet.apiOption6;
import static keystash.http.ApiTest.ApiTestServlet.apiOption7;
import static keystash.http.ApiTest.ApiTestServlet.apiOption8;
import static keystash.http.ApiTest.ApiTestServlet.apiOption9;
import static keystash.http.ApiTest.ApiTestServlet.footer1;
import static keystash.http.ApiTest.ApiTestServlet.footer1_2;
import static keystash.http.ApiTest.ApiTestServlet.footer2;
import static keystash.http.ApiTest.ApiTestServlet.header1;
import static keystash.http.ApiTest.ApiTestServlet.header2;
import static keystash.http.ApiTest.ApiTestServlet.headerAccount;
import static keystash.http.ApiTest.ApiTestServlet.headerTransaction;
import static keystash.http.ApiTest.ApiTestServlet.headerUtil;
import static keystash.http.ApiTest.ApiTestServlet.headerWallet;
import keystash.util.Config;

public class ApiTest {

    public static class ApiTestServlet extends HttpServlet {

        static final int countAccount = 7;
        static final int countTransaction = 10;
        static final int countUtil = 4;
        static final int countWallet = 3;

        static final String header1
                = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<meta charset=\"UTF-8\"/>\n"
                + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
                + "<title>KeyStash http API</title>\n"
                + "<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />"
                + "<link href=\"css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />"
                + "<link href=\"css/highlight.style.css\" rel=\"stylesheet\" type=\"text/css\" />"
                + "<style type=\"text/css\">\n"
                + "table {border-collapse: collapse;}\n"
                + "td {padding: 10px;}\n"
                + ".result {white-space: pre; font-family: monospace; overflow: auto;}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body>\n"
                + "<div class=\"navbar navbar-default\" role=\"navigation\">"
                + "<div class=\"container\" style=\"min-width: 90%;\">"
                + "<div class=\"navbar-header\">"
                + "<a class=\"navbar-brand\" href=\"/test\">KeyStash http API - Test</a>"
                + "</div>"
                + "<div class=\"navbar-collapse collapse\">"
                + "<ul class=\"nav navbar-nav navbar-right\">"
                + "<li><a href=\"http://wikiapi.keystash.org\" target=\"_blank\" style=\"margin-left:20px;\">Wiki Docs</a></li></ul>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"container\" style=\"min-width: 90%;\">"
                + "<div class=\"row\">"
                + "<div class=\"col-xs-12\" style=\"margin-bottom:15px;\">"
                + "</div>"
                + "<div class=\"row\" style=\"margin-bottom:15px;\">"
                + "<div class=\"col-xs-4 col-sm-3 col-md-2\">"
                + "<ul class=\"nav nav-pills nav-stacked\">";
        static final String headerAccount
                = "<li class=\"active\"><a href=\"/test?requestTag=ACCOUNT&requestType=getAccount\">Account<span class=\"badge\">" + countAccount + "</span></a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=getAccount\">getAccount</a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=getAccountByLabel\">getAccountByLabel</a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=getAccounts\">getAccounts</a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=getNewAccount\">getNewAccount</a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=importAccount\">importAccount</a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=getBalanceByAccount\">getBalanceByAccount</a></li>"
                + "<li><a href=\"/test?requestTag=ACCOUNT&requestType=getBalanceByLabel\">getBalanceByLabel</a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=TRANSACTION&requestType=signTransaction\">Transaction<span class=\"badge\">" + countTransaction + "</span></a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=UTIL&requestType=decryptSecret\">Util<span class=\"badge\">" + countUtil + "</span></a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=WALLET&requestType=backupWallet\">Wallet<span class=\"badge\">" + countWallet + "</span></a></li>";
        static final String headerTransaction
                = "<li class=\"inactive\"><a href=\"/test?requestTag=ACCOUNT&requestType=getAccount\">Account<span class=\"badge\">" + countAccount + "</span></a></li>"
                + "<li class=\"active\"><a href=\"/test?requestTag=TRANSACTION&requestType=signTransaction\">Transaction<span class=\"badge\">" + countTransaction + "</span></a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=signTransaction\">signTransaction</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=parseTransaction\">parseTransaction</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=broadcastTransaction\">broadcastTransaction</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getSinceBlock\">getSinceBlock</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getTransactionsByAccount\">getTransactionsByAccount</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getTransactionsByLabel\">getTransactionsByLabel</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getUnconfirmed\">getUnconfirmed</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getUnconfirmedByAccount\">getUnconfirmedByAccount</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getUnconfirmedByLabel\">getUnconfirmedByLabel</a></li>"
                + "<li><a href=\"/test?requestTag=TRANSACTION&requestType=getTransactionInfo\">getTransactionInfo</a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=UTIL&requestType=decryptSecret\">Util<span class=\"badge\">" + countUtil + "</span></a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=WALLET&requestType=backupWallet\">Wallet<span class=\"badge\">" + countWallet + "</span></a></li>";
        static final String headerUtil
                = "<li class=\"inactive\"><a href=\"/test?requestTag=ACCOUNT&requestType=getAccount\">Account<span class=\"badge\">" + countAccount + "</span></a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=TRANSACTION&requestType=signTransaction\">Transaction<span class=\"badge\">" + countTransaction + "</span></a></li>"
                + "<li class=\"active\"><a href=\"/test?requestTag=UTIL&requestType=decryptSecret\">Util<span class=\"badge\">" + countUtil + "</span></a></li>"
                + "<li><a href=\"/test?requestTag=UTIL&requestType=decryptSecret\">decryptSecret</a></li>"
                + "<li><a href=\"/test?requestTag=UTIL&requestType=changeLabel\">changeLabel</a></li>"
                + "<li><a href=\"/test?requestTag=UTIL&requestType=generateToken\">generateToken</a></li>"
                + "<li><a href=\"/test?requestTag=UTIL&requestType=decodeToken\">decodeToken</a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=WALLET&requestType=backupWallet\">Wallet<span class=\"badge\">" + countWallet + "</span></a></li>";
        static final String headerWallet
                = "<li class=\"inactive\"><a href=\"/test?requestTag=ACCOUNT&requestType=getAccount\">Account<span class=\"badge\">" + countAccount + "</span></a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=TRANSACTION&requestType=signTransaction\">Transaction<span class=\"badge\">" + countTransaction + "</span></a></li>"
                + "<li class=\"inactive\"><a href=\"/test?requestTag=UTIL&requestType=decryptSecret\">Util<span class=\"badge\">" + countUtil + "</span></a></li>"
                + "<li class=\"active\"><a href=\"/test?requestTag=WALLET&requestType=backupWallet\">Wallet<span class=\"badge\">" + countWallet + "</span></a></li>"
                + "<li><a href=\"/test?requestTag=WALLET&requestType=backupWallet\">backupWallet</a></li>"
                + "<li><a href=\"/test?requestTag=WALLET&requestType=encryptWallet\">encryptWallet</a></li>"
                + "<li><a href=\"/test?requestTag=WALLET&requestType=getInfo\">getInfo</a></li>";
        static final String header2
                = "</ul>"
                + "</div> <!-- col -->"
                + "<div class=\"col-xs-8 col-sm-9 col-md-10\">"
                + "<div class=\"panel-group\" id=\"accordion\">";
        static final String apiOption1
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getInfo\"><div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetInfo\" href=\"#\">getInfo</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\">"
                + "<a href=\"/test?requestType=getInfo\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetInfo\" class=\"panel-collapse collapse in\"><div class=\"panel-body\">"
                + "<form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getInfo\"/>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\">"
                + "<table class=\"table\"><tr><td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\"><span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div></form></div> "
                + "<!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption2
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getAccount\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetAccount\" href=\"#\">getAccount</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getAccount\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetAccount\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getAccount\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr><td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption3
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getAccountByLabel\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetAccountByLabel\" href=\"#\">getAccountByLabel</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getAccountByLabel\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetAccountByLabel\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getAccountByLabel\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr><td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption4
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getNewAccount\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetNewAccount\" href=\"#\">getNewAccount</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getNewAccount\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetNewAccount\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getNewAccount\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>encryptPassword:</td><td><input type=\"password\" name=\"encryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption5
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getAccounts\"><div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetAccounts\" href=\"#\">getAccounts</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\">"
                + "<a href=\"/test?requestType=getAccounts\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetAccounts\" class=\"panel-collapse collapse in\"><div class=\"panel-body\">"
                + "<form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getAccounts\"/>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\">"
                + "<table class=\"table\"><tr><td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\"><span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div></form></div> "
                + "<!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption6
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-importAccount\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapseimportAccount\" href=\"#\">importAccount</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=importAccount\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapseimportAccount\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"importAccount\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>encryptPassword:</td><td><input type=\"password\" name=\"encryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>importPhrase:</td><td><input type=\"password\" name=\"importPhrase\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption7
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-decryptSecret\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsedecryptSecret\" href=\"#\">decryptSecret</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=decryptSecret\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsedecryptSecret\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"decryptSecret\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>decryptPassword:</td><td><input type=\"password\" name=\"decryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption8
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-changeLabel\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsechangeLabel\" href=\"#\">changeLabel</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=changeLabel\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsechangeLabel\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"changeLabel\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>decryptPassword:</td><td><input type=\"password\" name=\"decryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption9
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-signTransaction\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsesignTransaction\" href=\"#\">signTransaction</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=signTransaction\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsesignTransaction\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"signTransaction\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>senderAccount:</td><td><input type=\"text\" name=\"senderAccount\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>recipient:</td><td><input type=\"text\" name=\"recipient\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>amountNQT:</td><td><input type=\"text\" name=\"amountNQT\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>feeNQT:</td><td><input type=\"text\" name=\"feeNQT\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>deadline:</td><td><input type=\"text\" name=\"deadline\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>decryptPassword:</td><td><input type=\"password\" name=\"decryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption10
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-parseTransaction\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapseparseTransaction\" href=\"#\">parseTransaction</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=parseTransaction\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapseparseTransaction\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"parseTransaction\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>signedBytes:</td><td><input type=\"text\" name=\"signedBytes\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption11
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-broadcastTransaction\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsebroadcastTransaction\" href=\"#\">broadcastTransaction</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=broadcastTransaction\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsebroadcastTransaction\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"broadcastTransaction\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>signedBytes:</td><td><input type=\"text\" name=\"signedBytes\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption12
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getSinceBlock\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetSinceBlock\" href=\"#\">getSinceBlock</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getSinceBlock\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetSinceBlock\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getSinceBlock\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>firstIndex:</td><td><input type=\"text\" name=\"firstIndex\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>lastIndex:</td><td><input type=\"text\" name=\"lastIndex\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption13
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getTransactionsByAccount\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetTransactionsByAccount\" href=\"#\">getTransactionsByAccount</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getTransactionsByAccount\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetTransactionsByAccount\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getTransactionsByAccount\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>numberOfConfirmations:</td><td><input type=\"text\" name=\"numberOfConfirmations\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption14
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getTransactionsByLabel\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetTransactionsByLabel\" href=\"#\">getTransactionsByLabel</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getTransactionsByLabel\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetTransactionsByLabel\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getTransactionsByLabel\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>numberOfConfirmations:</td><td><input type=\"text\" name=\"numberOfConfirmations\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption15
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getUnconfirmed\"><div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetUnconfirmed\" href=\"#\">getUnconfirmed</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\">"
                + "<a href=\"/test?requestType=getUnconfirmed\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetUnconfirmed\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getUnconfirmed\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\"><span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div></form></div> "
                + "<!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption16
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getUnconfirmedByAccount\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetUnconfirmedByAccount\" href=\"#\">getUnconfirmedByAccount</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getUnconfirmedByAccount\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetUnconfirmedByAccount\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getUnconfirmedByAccount\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption17
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getUnconfirmedByLabel\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetUnconfirmedByLabel\" href=\"#\">getUnconfirmedByLabel</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getUnconfirmedByLabel\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetUnconfirmedByLabel\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getUnconfirmedByLabel\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption18
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getTransactionInfo\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetTransactionInfo\" href=\"#\">getTransactionInfo</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getTransactionInfo\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetTransactionInfo\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getTransactionInfo\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>transaction:</td><td><input type=\"text\" name=\"transaction\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption19
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getBalanceByAccount\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetBalanceByAccount\" href=\"#\">getBalanceByAccount</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getBalanceByAccount\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetBalanceByAccount\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getBalanceByAccount\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>numberOfConfirmations:</td><td><input type=\"text\" name=\"numberOfConfirmations\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption20
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-getBalanceByLabel\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegetBalanceByLabel\" href=\"#\">getBalanceByLabel</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=getBalanceByLabel\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegetBalanceByLabel\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"getBalanceByLabel\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>label:</td><td><input type=\"text\" name=\"label\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>numberOfConfirmations:</td><td><input type=\"text\" name=\"numberOfConfirmations\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>server:</td><td><input type=\"text\" name=\"server\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverPort:</td><td><input type=\"text\" name=\"serverPort\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>serverSSL:</td><td><input type=\"text\" name=\"serverSSL\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption21
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-backupWallet\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsebackupWallet\" href=\"#\">backupWallet</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=backupWallet\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsebackupWallet\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"backupWallet\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>save:</td><td><input type=\"text\" name=\"save\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption22
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-encryptWallet\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapseencryptWallet\" href=\"#\">encryptWallet</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=encryptWallet\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapseencryptWallet\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"encryptWallet\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>encryptPassword:</td><td><input type=\"password\" name=\"encryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"><span style=\"float:right;font-size:12px;font-weight:normal;\">POST only</span></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption23
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-generateToken\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsegenerateToken\" href=\"#\">generateToken</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=generateToken\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsegenerateToken\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"generateToken\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>address:</td><td><input type=\"text\" name=\"address\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>data:</td><td><input type=\"text\" name=\"data\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<tr class=\"api-call-input-tr\"><td>decryptPassword:</td><td><input type=\"password\" name=\"decryptPassword\" style=\"width:100%;min-width:200px;\"/></td></tr><tr>"
                + "<td colspan=\"2\">"
                + "<input type=\"hidden\" name=\"assignment\" value=\"test\"/>"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String apiOption24
                = "<div class=\"panel panel-default api-call-All\" id=\"api-call-decodeToken\">"
                + "<div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsedecodeToken\" href=\"#\">decodeToken</a>"
                + "<span style=\"float:right;font-weight:normal;font-size:14px;\"><a href=\"/test?requestType=decodeToken\" target=\"_blank\" style=\"font-weight:normal;font-size:14px;color:#777;\">"
                + "</a> &nbsp;&nbsp;"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsedecodeToken\" class=\"panel-collapse collapse in\">"
                + "<div class=\"panel-body\"><form action=\"/keystash\" method=\"POST\" onsubmit=\"return ATS.submitForm(this);\">"
                + "<input type=\"hidden\" name=\"requestType\" value=\"decodeToken\"/><div class=\"col-xs-12 col-lg-6\" style=\"min-width: 40%;\"><table class=\"table\">"
                + "<tr class=\"api-call-input-tr\"><td>data:</td><td><input type=\"text\" name=\"data\" style=\"width:100%;min-width:200px;\"/></td></tr><tr><td colspan=\"2\">"
                + "<tr class=\"api-call-input-tr\"><td>token:</td><td><input type=\"text\" name=\"token\" style=\"width:100%;min-width:200px;\"/></td></tr><tr><td colspan=\"2\">"
                + "<input type=\"submit\" class=\"btn btn-default\" value=\"submit\"/></td></tr></table></div>"
                + "<div class=\"col-xs-12 col-lg-6\" style=\"min-width: 50%;\"><h5 style=\"margin-top:0px;\">"
                + "<span style=\"float:right;\" class=\"uri-link\"></span>Response</h5><pre class=\"hljs json\"><code class=\"result\">JSON response</code></pre></div>"
                + "</form></div> <!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String footer1
                = "</div> <!-- panel-group -->"
                + "</div> <!-- col -->"
                + "</div> <!-- row -->"
                + "</div> <!-- container -->"
                + "<script src=\"js/3rdparty/jquery.js\"></script>"
                + "<script src=\"js/3rdparty/bootstrap.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/3rdparty/highlight.pack.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/ats.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/ats.util.js\" type=\"text/javascript\"></script>";
        static final String footer1_2
                = "</div> <!-- panel-group -->"
                + "</div> <!-- col -->"
                + "</div> <!-- row -->"
                + "</div> <!-- container -->"
                + "<script src=\"js/3rdparty/jquery.js\"></script>"
                + "<script src=\"js/3rdparty/bootstrap.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/3rdparty/highlight.pack.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/atsJSON.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/ats.util.js\" type=\"text/javascript\"></script>";
        static final String footer2
                = "</body>\n"
                + "</html>\n";
    }

    public static void apiTestPage(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate, private");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("text/html; charset=UTF-8");

        String apiRequestTag = null;
        String apiRequestType = null;

        try {

            apiRequestTag = request.getParameter("requestTag");
            apiRequestType = request.getParameter("requestType");

        } catch (Exception e) {

        }

        try ( PrintWriter writer = response.getWriter() ) {

            writer.print(header1);

            if ("ACCOUNT".equals(apiRequestTag) || apiRequestTag == null) {

                writer.print(headerAccount);

            }
            else if ("TRANSACTION".equals(apiRequestTag)) {

                writer.print(headerTransaction);

            }
            else if ("UTIL".equals(apiRequestTag)) {

                writer.print(headerUtil);

            }
            else if ("WALLET".equals(apiRequestTag)) {

                writer.print(headerWallet);

            }
            writer.print(header2);

            if ("getInfo".equals(apiRequestType)) {

                writer.print(apiOption1);

            }
            else if ("getAccount".equals(apiRequestType)) {

                writer.print(apiOption2);

            }
            else if ("getAccountByLabel".equals(apiRequestType)) {

                writer.print(apiOption3);

            }
            else if ("getNewAccount".equals(apiRequestType)) {

                writer.print(apiOption4);

            }
            else if ("getAccounts".equals(apiRequestType)) {

                writer.print(apiOption5);

            }
            else if ("importAccount".equals(apiRequestType)) {

                writer.print(apiOption6);

            }
            else if ("decryptSecret".equals(apiRequestType)) {

                writer.print(apiOption7);

            }
            else if ("changeLabel".equals(apiRequestType)) {

                writer.print(apiOption8);

            }
            else if ("generateToken".equals(apiRequestType)) {

                writer.print(apiOption23);

            }
            else if ("decodeToken".equals(apiRequestType)) {

                writer.print(apiOption24);

            }
            else if ("signTransaction".equals(apiRequestType)) {

                writer.print(apiOption9);

            }
            else if ("parseTransaction".equals(apiRequestType)) {

                writer.print(apiOption10);

            }
            else if ("broadcastTransaction".equals(apiRequestType)) {

                writer.print(apiOption11);

            }
            else if ("getSinceBlock".equals(apiRequestType)) {

                writer.print(apiOption12);

            }
            else if ("getTransactionsByAccount".equals(apiRequestType)) {

                writer.print(apiOption13);

            }
            else if ("getTransactionsByLabel".equals(apiRequestType)) {

                writer.print(apiOption14);

            }
            else if ("getUnconfirmed".equals(apiRequestType)) {

                writer.print(apiOption15);

            }
            else if ("getUnconfirmedByAccount".equals(apiRequestType)) {

                writer.print(apiOption16);

            }
            else if ("getUnconfirmedByLabel".equals(apiRequestType)) {

                writer.print(apiOption17);

            }
            else if ("getTransactionInfo".equals(apiRequestType)) {

                writer.print(apiOption18);

            }
            else if ("getBalanceByAccount".equals(apiRequestType)) {

                writer.print(apiOption19);

            }
            else if ("getBalanceByLabel".equals(apiRequestType)) {

                writer.print(apiOption20);

            }
            else if ("backupWallet".equals(apiRequestType)) {

                writer.print(apiOption21);

            }
            else if ("encryptWallet".equals(apiRequestType)) {

                writer.print(apiOption22);

            }
            else {

                writer.print(apiOption2);

            }

            if ("jsonp".equals(Config.properties("keystash.apiFormat"))) {

                writer.print(footer1);

            }
            else {

                writer.print(footer1_2);

            }

            writer.print(footer2);

        }

    }

}
