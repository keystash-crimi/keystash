package keystash.http;

import java.io.IOException;
import java.net.URL;
import java.util.regex.PatternSyntaxException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.gui.TableContent;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionSinceBlock {

    public static class ApiServlet extends HttpServlet {

        public static void getSinceBlock(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            JSONObject jsonTempTx = new JSONObject();
            JSONObject jsonTx = new JSONObject();
            JSONObject allTx;
            String apiFirstIndex = null;
            String apiLastIndex = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";
            JSONArray jsonArray = new JSONArray();

            try {

                apiFirstIndex = request.getParameter("firstIndex");
                apiLastIndex = request.getParameter("lastIndex");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getSinceBlock", "error", "server, serverPort must be specified.", "");

            }

            if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getSinceBlock", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getSinceBlock", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getSinceBlock", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url;

                if (apiFirstIndex == null) {
                    url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getBlocks" + "&lastIndex="
                            + apiLastIndex + "&includeTransactions=false";

                }
                else {

                    url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getBlocks&firstIndex=" + apiFirstIndex + "&lastIndex="
                            + apiLastIndex + "&includeTransactions=false";

                }

                try {

                    String responseRequest = IOUtils.toString(new URL(url));
                    JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);
                    JSONArray responseArray = (JSONArray) responseJsonObject.get("blocks");
                    JSONObject getResponse;

                    for (Object localResponseArrayLOCAL : responseArray) {

                        getResponse = (JSONObject) localResponseArrayLOCAL;

                        if (!"[]".equals(getResponse.get("transactions").toString())) {

                            if (!"0".equals(getResponse.get("totalAmountNQT").toString())) {

                                jsonTempTx.put("transactions", jsonArray);
                                jsonArray.add(getResponse.get("transactions"));

                            }

                        }
                    }

                    allTx = jsonTempTx;
                    String myTx = null;

                    try {
                        myTx = allTx.get("transactions").toString();
                        myTx = myTx.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");
                    } catch (Exception e) {
                    }

                    if (myTx != null) {

                        String[] splitArray = null;

                        try {

                            splitArray = myTx.split(",");

                        } catch (PatternSyntaxException ex) {
                        }

                        JSONArray jsonArrayTx = new JSONArray();

                        for (String localSplitArrayLOCAL : splitArray) {

                            url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getTransaction&transaction=" + localSplitArrayLOCAL;
                            String responseRequestTx = IOUtils.toString(new URL(url));
                            JSONObject responseJsonObjectTx = (JSONObject) JSONValue.parseWithException(responseRequestTx);

                            if (!"0".equals(responseJsonObjectTx.get("amountNQT").toString())) {

                                if (TableContent.isAccountInTable(responseJsonObjectTx.get("recipientRS").toString())) {

                                    jsonArrayTx.add(responseJsonObjectTx);
                                    jsonTx.put("transactions", jsonArrayTx);

                                }
                            }
                        }
                    }

                    if ("{}".equals(jsonTx.toString())) {

                        json.put("error", "no transactions found associated with accounts in wallet");
                        response.getWriter().println(json);
                        ApiLog.logMessage("getSinceBlock", "error", "no transactions found associated with accounts in wallet.", "");

                    }
                    else {

                        response.getWriter().println(jsonTx);
                        ApiLog.logMessage("getSinceBlock", "success", "", "");

                    }

                } catch (IOException | ParseException e) {

                    json.put("error", "getSinceBlock failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getSinceBlock", "error", "getSinceBlock failed.", "");

                }

            }

        }
    }
}
