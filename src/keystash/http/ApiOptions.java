package keystash.http;

import java.util.ArrayList;

public class ApiOptions {

    public static final String option1 = "getInfo";
    public static final String option1_1 = "wallet";
    public static final String option2 = "getAccount";
    public static final String option2_2 = "account";
    public static final String option3 = "getAccountByLabel";
    public static final String option3_3 = "account";
    public static final String option4 = "getNewAccount";
    public static final String option4_4 = "account";
    public static final String option5 = "getAccounts";
    public static final String option5_5 = "account";
    public static final String option6 = "importAccount";
    public static final String option6_6 = "account";
    public static final String option7 = "decryptSecret";
    public static final String option7_7 = "util";
    public static final String option8 = "changeLabel";
    public static final String option8_8 = "util";
    public static final String option9 = "signTransaction";
    public static final String option9_9 = "transaction";
    public static final String option10 = "parseTransaction";
    public static final String option10_10 = "transaction";
    public static final String option11 = "broadcastTransaction";
    public static final String option11_11 = "transaction";
    public static final String option12 = "getSinceBlock";
    public static final String option12_12 = "transaction";
    public static final String option13 = "getTransactionsByAccount";
    public static final String option13_13 = "transaction";
    public static final String option14 = "getTransactionsByLabel";
    public static final String option14_14 = "transaction";
    public static final String option15 = "getUnconfirmed";
    public static final String option15_15 = "transaction";   
    public static final String option16 = "getUnconfirmedByAccount";
    public static final String option16_16 = "transaction";
    public static final String option17 = "getUnconfirmedByLabel";
    public static final String option17_17 = "transaction";
    public static final String option18 = "getTransactionInfo";
    public static final String option18_18 = "transaction";
    public static final String option19 = "getBalanceByAccount";
    public static final String option19_19 = "account";
    public static final String option20 = "getBalanceByLabel";
    public static final String option20_20 = "account";
    public static final String option21 = "backupWallet";
    public static final String option21_21 = "wallet";
    public static final String option22 = "encryptWallet";
    public static final String option22_22 = "wallet";
    public static final String option23 = "generateToken";
    public static final String option23_23 = "util";
    public static final String option24 = "decodeToken";
    public static final String option24_24 = "util";
    public static final String option99 = "logMonitor";
    public static final String option99_99 = "log";

    public static String apiListOption(int integer) {

        ArrayList<String> apiOptionList = new ArrayList<>();
        apiOptionList.add(option1);
        apiOptionList.add(option2);
        apiOptionList.add(option3);
        apiOptionList.add(option4);
        apiOptionList.add(option5);
        apiOptionList.add(option6);
        apiOptionList.add(option7);
        apiOptionList.add(option8);
        apiOptionList.add(option9);
        apiOptionList.add(option10);
        apiOptionList.add(option11);
        apiOptionList.add(option12);
        apiOptionList.add(option13);
        apiOptionList.add(option14);
        apiOptionList.add(option15);
        apiOptionList.add(option16);
        apiOptionList.add(option17);
        apiOptionList.add(option18);
        apiOptionList.add(option19);
        apiOptionList.add(option20);
        apiOptionList.add(option21);
        apiOptionList.add(option22);
        apiOptionList.add(option23);
        apiOptionList.add(option24);
        apiOptionList.add(option99);
        
        return apiOptionList.get(integer);

    }

    public static int apiCountOptions() {

        ArrayList<String> apiCountOptionsList = new ArrayList<>();
        apiCountOptionsList.add(option1);
        apiCountOptionsList.add(option2);
        apiCountOptionsList.add(option3);
        apiCountOptionsList.add(option4);
        apiCountOptionsList.add(option5);
        apiCountOptionsList.add(option6);
        apiCountOptionsList.add(option7);
        apiCountOptionsList.add(option8);
        apiCountOptionsList.add(option9);
        apiCountOptionsList.add(option10);
        apiCountOptionsList.add(option11);
        apiCountOptionsList.add(option12);
        apiCountOptionsList.add(option13);
        apiCountOptionsList.add(option14);
        apiCountOptionsList.add(option15);
        apiCountOptionsList.add(option16);
        apiCountOptionsList.add(option17);
        apiCountOptionsList.add(option18);
        apiCountOptionsList.add(option19);
        apiCountOptionsList.add(option20);
        apiCountOptionsList.add(option21);
        apiCountOptionsList.add(option22);
        apiCountOptionsList.add(option23);
        apiCountOptionsList.add(option24);
        apiCountOptionsList.add(option99);

        return apiCountOptionsList.size();

    }

}
