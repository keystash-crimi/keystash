package keystash.http;

import keystash.util.Db;

public class ApiValidate {

    public static boolean validateWalletInsert(String addressRS) {

        boolean result = false;
        int countRows = Db.countRows("keystash_wallet") + 1;
        String[] getAddressRS = new String[countRows];

        for (int i = 1; i < countRows; i++) {

            getAddressRS[i] = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +i);

            if (getAddressRS[i].equals(addressRS)) {

                result = true;

            }

        }

        return result;

    }

}
