package keystash.http;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.Api.countRows;
import static keystash.http.Api.lab;
import static keystash.http.Api.rs;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionReceivedLabel {

    public static class ApiServlet extends HttpServlet {

        public static void receivedTransactionsLabel(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            JSONArray jsonArrayTx = new JSONArray();
            String apiLabel = null;
            String apiNumberOfConfirmations = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";

            try {
                
                apiLabel = request.getParameter("label");
                apiNumberOfConfirmations = request.getParameter("numberOfConfirmations");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");
                
            } catch (Exception e) {

                json.put("error", "label, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionsByLabel", "error", "label, server, serverPort must be specified.", "");

            }

            if (apiLabel == null) {

                json.put("error", "label not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionsByLabel", "error", "label not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionsByLabel", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionsByLabel", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionsByLabel", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                if (apiNumberOfConfirmations == null) {

                    apiNumberOfConfirmations = "";

                }
                String url;
                String responseRequest;
                JSONObject responseJsonObject = null;

                for (int i = 1; i < countRows; i++) {

                    if (apiLabel.equals(lab[i])) {

                        url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getAccountTransactions&account="
                                + rs[i] + "&numberOfConfirmations=" + apiNumberOfConfirmations;

                        try {

                            responseRequest = IOUtils.toString(new URL(url));
                            responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);

                            if (responseJsonObject.get("transactions") != null) {

                                jsonArrayTx.add(responseJsonObject.get("transactions"));

                            }

                        } catch (IOException | ParseException e) {

                            json.put("error", "getAccountTransactions failed");
                            response.getWriter().println(json);
                            ApiLog.logMessage("getTransactionsByLabel", "error", "getAccountTransactions failed.", "");

                        }

                    }

                }

                if (responseJsonObject == null) {

                    json.put("error", "no account found with label");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getTransactionsByLabel", "error", apiLabel + "no account found with label.", "");

                }
                else {

                    json.put("transactions", jsonArrayTx);
                    response.getWriter().println(json);
                    ApiLog.logMessage("getTransactionsByLabel", "success", apiLabel, "");

                }

            }
        }
    }
}
