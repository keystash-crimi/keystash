package keystash.http;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.crypto.Aes;
import static keystash.http.Api.cl;
import static keystash.http.Api.countRows;
import static keystash.http.Api.eS;
import static keystash.http.Api.id;
import static keystash.http.Api.lab;
import static keystash.http.Api.pKey;
import static keystash.http.Api.raIV;
import static keystash.http.Api.raSalt;
import static keystash.http.Api.rs;
import static keystash.http.Api.sIt;
import static keystash.http.Api.sKey;
import static keystash.http.Api.se;
import keystash.util.Config;
import keystash.util.Convert;
import keystash.util.Memory;
import org.apache.commons.codec.DecoderException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionDecrypt {

    public static class ApiServlet extends HttpServlet {

        public static void decryptSecret(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException,
                NoSuchProviderException,
                NoSuchPaddingException,
                DecoderException,
                InvalidKeySpecException,
                InvalidKeyException,
                InvalidAlgorithmParameterException,
                IllegalBlockSizeException,
                BadPaddingException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiAssignment = null;
            char[] apiDecryptPassword = null;
            String apiAddress = null;
            String accountId = null;
            String address = null;
            String addressRS = null;
            String secret = null;
            String salt = null;
            String publicKey = null;
            String label = null;
            String IV = null;
            int iteration = 0;
            int keySize = 0;
            String encryptionStandard = null;
            char[] decipherSecret = null;

            try {

                apiAssignment = request.getParameter("assignment");
                apiDecryptPassword = request.getParameter("decryptPassword").toCharArray();
                apiAddress = request.getParameter("address");

            } catch (Exception e) {

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("decryptSecret", "error", "apiServerEnforcePOST is disabled.", apiAssignment);

            }
            else if (apiAddress == null && apiDecryptPassword == null) {

                json.put("error", "address, decryptPassword not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("decryptSecret", "error", "address, decryptPassword not specified.", apiAssignment);

            }
            else if (countRows == 0) {

                json.put("error", "accountQuantity = 0");
                response.getWriter().println(json);
                ApiLog.logMessage("decryptSecret", "error", "accountQuantity = 0.", apiAssignment);

            }
            else if (apiAddress != null) {

                int countAccounts = 0;
                for (int i = 1; i < countRows; i++) {

                    if (apiAddress.equals(rs[i]) || apiAddress.equals(cl[i])) {

                        accountId = id[i];
                        address = cl[i];
                        publicKey = pKey[i];
                        label = lab[i];
                        secret = se[i];
                        addressRS = rs[i];
                        iteration = Convert.stringToInt(sIt[i]);
                        keySize = Convert.stringToInt(sKey[i]);
                        encryptionStandard = eS[i];
                        IV = raIV[i];
                        salt = raSalt[i];
                        countAccounts++;

                    }

                }

                if (countAccounts == 0) {

                    json.put("error", "address not in wallet file");
                    response.getWriter().println(json);
                    ApiLog.logMessage("decryptSecret", "error", "address not in wallet file.", apiAssignment);

                }
                else {

                    try {

                        decipherSecret = Aes.decrypt(secret, new String(apiDecryptPassword), salt, IV, iteration, keySize, encryptionStandard);
                        jsonOrderedMap.put("accountId", accountId);
                        jsonOrderedMap.put("address", address);
                        jsonOrderedMap.put("addressRS", addressRS);
                        jsonOrderedMap.put("publicKey", publicKey);
                        jsonOrderedMap.put("label", label);
                        jsonOrderedMap.put("secretPhrase", new String(decipherSecret));
                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));                       
                        ApiLog.logMessage("decryptSecret", "success", "", apiAssignment);                       
                        Memory.clearPasswordMemory(decipherSecret);

                    } catch (Exception e) {

                        if (apiAddress == null) {

                            json.put("error", "address not specified");
                            response.getWriter().println(json);
                            ApiLog.logMessage("decryptSecret", "error", "address not specified.", apiAssignment);

                        }
                        else {

                            json.put("error", "decryptPassword is invalid");
                            response.getWriter().println(json);
                            ApiLog.logMessage("decryptSecret", "error", "decryptPassword is invalid.", apiAssignment);

                        }
                    }
                    
                    Memory.clearPasswordMemory(apiDecryptPassword);                  
                    
                }
            }
        }
    }
}
