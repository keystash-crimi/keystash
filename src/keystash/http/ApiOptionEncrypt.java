package keystash.http;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.KeyStashGUI;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import org.apache.commons.codec.DecoderException;
import org.h2.tools.ChangeFileEncryption;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class ApiOptionEncrypt {

    public static class ApiServlet extends HttpServlet {

        public static void encryptWallet(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException,
                NoSuchProviderException,
                NoSuchPaddingException,
                DecoderException,
                InvalidKeySpecException,
                InvalidKeyException,
                InvalidAlgorithmParameterException,
                IllegalBlockSizeException,
                BadPaddingException,
                SQLException {

            JSONObject json = new JSONObject();
            String apiAssignment = null;
            String c = "¡";
            String apiEncryptPassword = null;

            try {

                apiAssignment = request.getParameter("assignment");
                apiEncryptPassword = request.getParameter("encryptPassword");

            } catch (Exception e) {

                json.put("error", "request not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "request not specified.", apiAssignment);

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "apiServerEnforcePOST is disabled.", apiAssignment);

            }
            else if (KeyStashGUI.isWalletEncrypted) {

                json.put("error", "wallet file is already encrypted");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "wallet file is already encrypted.", apiAssignment);

            }
            else if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST"))) {

                json.put("error", "cant encrypt wallet file, apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "cant encrypt wallet file, apiServerEnforcePOST is disabled.", apiAssignment);

            }
            else if (apiEncryptPassword == null) {

                json.put("error", "encryptPassword is empty");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "encryptPassword is empty.", apiAssignment);

            }
            else if (apiEncryptPassword.length() < Constants.minPasswordChars) {

                json.put("error", "encryptPassword must contain at least " + Constants.minPasswordChars + " characters");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "encryptPassword must contain at least " + Constants.minPasswordChars + " characters.", apiAssignment);

            }
            else if (apiEncryptPassword.contains(c)) {

                json.put("error", "no ¡ character allowed in encryptPassword.");
                response.getWriter().println(json);
                ApiLog.logMessage("encryptWallet", "error", "no ¡ character allowed in encryptPassword.", apiAssignment);

            }

            else {

                if (encryptWalletFile(apiEncryptPassword, apiAssignment)) {

                    json.put("success", "wallet file encryption successful.");
                    response.getWriter().println(json);
                    ApiLog.logMessage("encryptWallet", "success", "wallet file encryption successful.", apiAssignment);

                }
                else {

                    json.put("error", "wallet file encryption failed.");
                    response.getWriter().println(json);
                    ApiLog.logMessage("encryptWallet", "error", "wallet file encryption failed.", apiAssignment);

                }

            }

        }

    }

    public static boolean encryptWalletFile(String password, String apiAssignment)
            throws NoSuchAlgorithmException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            SQLException {

        boolean result;

        try {

            if (!"test".equals(apiAssignment)) {
                
                Db.walletClose();
                ChangeFileEncryption.main("-dir", Constants.walletPathOnly, "-db", "wallet", "-cipher", "AES", "-encrypt", password.replace(" ", "¡"));
                KeyStashGUI.isWalletEncrypted = true;
                Db.connectToEncryptedWallet(password.replace(" ", "¡"));

            }
            result = true;

        } catch (SQLException ex) {

            Db.connectToWallet();
            result = false;

        }

        return result;

    }

}
