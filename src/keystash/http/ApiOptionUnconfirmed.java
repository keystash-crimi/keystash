package keystash.http;

import java.io.IOException;
import java.net.URL;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.Api.countRows;
import static keystash.http.Api.rs;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionUnconfirmed {

    public static class ApiServlet extends HttpServlet {

        public static void unconfirmedTransactions(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            JSONArray jsonArrayTx = new JSONArray();

            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";

            try {

                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmed", "error", "server, serverPort must be specified.", "");

            }

            if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmed", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmed", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getUnconfirmed", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url;
                String responseRequest;
                JSONObject responseJsonObject = null;

                for (int i = 1; i < countRows; i++) {

                    url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getUnconfirmedTransactions&account="
                            + rs[i];

                    try {

                        responseRequest = IOUtils.toString(new URL(url));
                        responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);

                        if (!"[]".equals(responseJsonObject.get("unconfirmedTransactions").toString())) {

                            jsonArrayTx.add(responseJsonObject.get("unconfirmedTransactions"));

                        }

                    } catch (IOException | ParseException e) {

                        json.put("error", "getUnconfirmed failed");
                        response.getWriter().println(json);
                        ApiLog.logMessage("getUnconfirmed", "error", "getUnconfirmed failed.", "");

                    }

                }

                if (responseJsonObject == null) {

                    json.put("error", "no unconfirmed transactions found");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getUnconfirmed", "error", "no unconfirmed transactions found.", "");

                }
                else {

                    json.put("unconfirmedTransactions", jsonArrayTx);
                    response.getWriter().println(json);
                    ApiLog.logMessage("getUnconfirmed", "success", "", "");

                }

            }
        }
    }
}
