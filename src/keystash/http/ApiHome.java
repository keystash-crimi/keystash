package keystash.http;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.KeyStashGUI;
import keystash.crypto.KeyLength;
import static keystash.http.Api.apiIp;
import static keystash.http.ApiHome.ApiHomeServlet.apiMonitorBody;
import static keystash.http.ApiHome.ApiHomeServlet.apiMonitorFooter;
import static keystash.http.ApiHome.ApiHomeServlet.apiMonitorHeader;
import static keystash.http.ApiHome.ApiHomeServlet.footer1;
import static keystash.http.ApiHome.ApiHomeServlet.footer2;
import static keystash.http.ApiHome.ApiHomeServlet.header1;
import static keystash.http.ApiTest.ApiTestServlet.header2;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Db;

public class ApiHome {

    public static class ApiHomeServlet extends HttpServlet {

        static final String header1
                = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "<meta charset=\"UTF-8\"/>\n"
                + "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
                + "<title>KeyStash http API</title>\n"
                + "<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\" />"
                + "<link href=\"css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />"
                + "<link href=\"css/highlight.style.css\" rel=\"stylesheet\" type=\"text/css\" />"
                + "<style type=\"text/css\">\n"
                + "table {border-collapse: collapse;}\n"
                + "td {padding: 10px;}\n"
                + ".result {white-space: pre; font-family: monospace; overflow: auto;}\n"
                + ".center {float: none;margin-left: auto;margin-right: auto;}\n"
                + ".panel-height {height: 600px; overflow-y: scroll;}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body>\n"
                + "<div class=\"navbar navbar-default\" role=\"navigation\">"
                + "<div class=\"container\" style=\"min-width: 90%;\">"
                + "<div class=\"navbar-header\">"
                + "<a class=\"navbar-brand\" href=\"/\">KeyStash http API</a>"
                + "</div>"
                + "<div class=\"navbar-collapse collapse\">"
                + "<ul class=\"nav navbar-nav navbar-right\">"
                + "<li><a href=\"http://wikiapi.keystash.org\" target=\"_blank\" style=\"margin-left:20px;\">Wiki Docs</a></li></ul>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "<div class=\"container\" style=\"min-width: 90%;\">"
                + "<div class=\"row\">"
                + "<div class=\"col-xs-12\" style=\"margin-bottom:15px;\">"
                + "</div>"
                + "<div class=\"row\" style=\"margin-bottom:15px;\">"
                + "<div class=\"col-xs-4 col-sm-3 col-md-2\">"
                + "<ul class=\"nav nav-pills nav-stacked\">";
        static final String header2
                = "</div> <!-- col -->"
                + "<div class=\"col-xs-8 col-sm-9 col-md-10\">"
                + "<div class=\"panel-group\" id=\"accordion\">";
        static final String apiMonitorHeader
                = "<div class=\"panel panel-default\" id=\"api-call-getInfo\"><div class=\"panel-heading\"><h4 class=\"panel-title\">"
                + "<a data-toggle=\"collapse\" class=\"collapse-link\" data-target=\"#collapsemonitor\" href=\"#\">API Monitor</a> <span style=\"float:right;font-weight:normal;font-size:14px;\"><a style=\"font-weight:normal;font-size:11px;color:#FFF;\" href=\"/keystash?requestType=logMonitor\" target=\"_blank\">Open GET URL</a></span>"
                + "</span></h4></div> "
                + "<!-- panel-heading --><div id=\"collapsemonitor\" class=\"panel-collapse collapse in\"><div id=\"monitor\" class=\"panel-body panel-height\">";
        static String apiMonitorBody = "";
        static final String apiMonitorFooter
                = "<!-- panel-body --></div> <!-- panel-collapse --></div>";
        static final String footer1
                = "</div> <!-- panel-group -->"
                + "</div> <!-- col -->"
                + "</div> <!-- row -->"
                + "</div> <!-- container -->"
                + "<script src=\"js/3rdparty/jquery.js\"></script>"
                + "<script src=\"js/3rdparty/bootstrap.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/3rdparty/highlight.pack.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/ats.js\" type=\"text/javascript\"></script>"
                + "<script src=\"js/ats.util.js\" type=\"text/javascript\"></script>";
        static final String footer2
                = "</body>\n"
                + "</html>\n";

    }

    public static void apiHomePage(HttpServletRequest request, HttpServletResponse response)
            throws IOException,
            NoSuchAlgorithmException {

        queryApiLog();
        int countRows = Db.countRows("keystash_wallet");

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate, private");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("text/html; charset=UTF-8");

        String headerStatus
                = "<li class=\"active\"><a>Info:</a></li></ul>"
                + "<div class=\"list-group\">"
                + "<a class=\"list-group-item\">keystashVersion: " + Constants.keyStashVersion + "</a>"
                + "<a class=\"list-group-item\">hostname: " + apiIp + "</a>"
                + "<a class=\"list-group-item\">walletEncrypted: " + KeyStashGUI.isWalletEncrypted + "</a>"
                + "<a class=\"list-group-item\">numberOfAccounts: " + countRows + "</a>"
                + "<a class=\"list-group-item\">unlimitedKeyStrengthAES: " + KeyLength.isUnlimitedKeyStrength() + "</a>"
                + "<a class=\"list-group-item\">enableProperties: " + Config.properties("keystash.enableProperties") + "</a>"
                + "<a class=\"list-group-item\">apiServerEnforcePOST: " + Config.properties("keystash.apiServerEnforcePOST") + "</a>"
                + "<a class=\"list-group-item\">apiFormat: " + Config.properties("keystash.apiFormat") + "</a>"
                + "<a class=\"list-group-item\">enableApiPassword: " + Config.properties("keystash.enableApiPassword") + "</a>"
                + "</div>";

        try ( PrintWriter writer = response.getWriter() ) {

            writer.print(header1);
            writer.print(headerStatus);
            writer.print(header2);
            writer.print(apiMonitorHeader);
            writer.print(apiMonitorBody);
            writer.print(apiMonitorFooter);
            writer.print(footer1);
            writer.print(footer2);

        }

        apiMonitorBody = "";

    }

    public static void queryApiLog() {

        int logCountRows = Db.countRowsApiLog("keystash_apilog");

        for (int i = logCountRows; i > 0; i--) {

            String getlogReqT = Db.selectFromApiLog("request_type", "keystash_apilog", "log_id", +i);
            String getLogRes = Db.selectFromApiLog("response", "keystash_apilog", "log_id", +i);
            String getLogMsg = Db.selectFromApiLog("log_message", "keystash_apilog", "log_id", +i);
            String getLogTS = Db.selectFromApiLog("log_timestamp", "keystash_apilog", "log_id", +i);

            if ("".equals(getLogMsg)) {

                apiMonitorBody = apiMonitorBody + "<p>" + getLogTS + " -" + getlogReqT + ", " + getLogRes + "</p>";

            }
            else {

                apiMonitorBody = apiMonitorBody + "<p>" + getLogTS + " -" + getlogReqT + ", " + getLogRes + ", " + getLogMsg + "</p>";

            }

        }

    }

}
