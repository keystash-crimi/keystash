package keystash.http;

import java.io.IOException;
import keystash.util.Config;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;

public class ApiLog {

    public static void logMessage(String requestType, String response, String logMessage, String apiAssignment)
            throws IOException {
        
        int logId = Db.countRowsApiLog("keystash_apilog") + 1;

        if (Convert.stringToBoolean(Config.properties("keystash.apiLog")) && !"test".equals(apiAssignment)) {
           
            String logTime = Date.getDate(false);
            Db.insertApiLog(logId, requestType, response, logMessage, "[" + logTime + "]");
            
        }
        
    }
    
}
