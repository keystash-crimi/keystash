package keystash.http;

import keystash.util.Db;

public class ApiRefresh {

    public static void refreshApi() {

        int countRows = Db.countRows("keystash_wallet") + 1;
        String[] getAccountid = new String[countRows];
        String[] getAddressClassic = new String[countRows];
        String[] getAddressRs = new String[countRows];
        String[] getPublicKey = new String[countRows];
        String[] getLabel = new String[countRows];
        String[] getSecret = new String[countRows];
        String[] getSalt = new String[countRows];
        String[] getIV = new String[countRows];
        String[] getSecretIteration = new String[countRows];
        String[] getSecretKeySize = new String[countRows];
        String[] getSecretEncryptionStandard = new String[countRows];
        String[] getShareAccount = new String[countRows];
        String[] getAccountType = new String[countRows];
        String[] getTimestamp = new String[countRows];

        for (int i = 1; i < countRows; i++) {

            getAccountid[i] = Db.selectFromWallet("account_id", "keystash_wallet", "account_id", +i);
            getAddressClassic[i] = Db.selectFromWallet("address_classic", "keystash_wallet", "account_id", +i);
            getAddressRs[i] = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +i);
            getPublicKey[i] = Db.selectFromWallet("public_key", "keystash_wallet", "account_id", +i);
            getLabel[i] = Db.selectFromWallet("label", "keystash_wallet", "account_id", +i);
            getSecret[i] = Db.selectFromWallet("secret", "keystash_wallet", "account_id", +i);
            getSalt[i] = Db.selectFromWallet("randomsalt", "keystash_wallet", "account_id", +i);
            getIV[i] = Db.selectFromWallet("randomiv", "keystash_wallet", "account_id", +i);
            getSecretIteration[i] = Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", +i);
            getSecretKeySize[i] = Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", +i);
            getSecretEncryptionStandard[i] = Db.selectFromWallet("advanced_encryption_standard", "keystash_wallet", "account_id", +i);
            getShareAccount[i] = Db.selectFromWallet("share_account", "keystash_wallet", "account_id", +i);
            getAccountType[i] = Db.selectFromWallet("account_type", "keystash_wallet", "account_id", +i);
            getTimestamp[i] = Db.selectFromWallet("created_timestamp", "keystash_wallet", "account_id", +i);

            Api.getWalletData(countRows, getAccountid, getAddressClassic, getAddressRs, getPublicKey, getLabel, getSecret, getSalt,
                    getIV, getSecretIteration, getSecretKeySize, getSecretEncryptionStandard, getShareAccount, getAccountType, getTimestamp);

        }
    }

    public static void refreshApiLog() {

        int logCountRows = Db.countRowsApiLog("keystash_apilog") + 1;
        String[] getLogId = new String[logCountRows];
        String[] getlogReqT = new String[logCountRows];
        String[] getLogRes = new String[logCountRows];
        String[] getLogMsg = new String[logCountRows];
        String[] getLogTS = new String[logCountRows];

        for (int i = 1; i < logCountRows; i++) {

            getLogId[i] = Db.selectFromApiLog("log_id", "keystash_apilog", "log_id", +i);
            getlogReqT[i] = Db.selectFromApiLog("request_type", "keystash_apilog", "log_id", +i);
            getLogRes[i] = Db.selectFromApiLog("response", "keystash_apilog", "log_id", +i);
            getLogMsg[i] = Db.selectFromApiLog("log_message", "keystash_apilog", "log_id", +i);
            getLogTS[i] = Db.selectFromApiLog("log_timestamp", "keystash_apilog", "log_id", +i);

            Api.getApiLog(logCountRows, getLogId, getlogReqT, getLogRes, getLogMsg, getLogTS);

        }

    }

}
