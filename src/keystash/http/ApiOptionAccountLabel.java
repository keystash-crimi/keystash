package keystash.http;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static keystash.http.Api.aT;
import static keystash.http.Api.cl;
import static keystash.http.Api.id;
import static keystash.http.Api.lab;
import static keystash.http.Api.pKey;
import static keystash.http.Api.raIV;
import static keystash.http.Api.raSalt;
import static keystash.http.Api.rs;
import static keystash.http.Api.sIt;
import static keystash.http.Api.sKey;
import static keystash.http.Api.se;
import static keystash.http.Api.timestamp;
import static keystash.http.Api.countRows;
import static keystash.http.Api.eS;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionAccountLabel {

    public static class ApiServlet extends HttpServlet {

        public static void getAccountByLabel(HttpServletRequest request, HttpServletResponse response)
                throws IOException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiLabel = null;
            int countAccounts = 0;

            try {

                apiLabel = request.getParameter("label");

            } catch (Exception e) {

                json.put("error", "label not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getAccountByLabel", "error", "label not specified.", "");

            }

            if (apiLabel == null) {

                json.put("error", "label not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getAccountByLabel", "error", "label not specified.", "");

            }
            else if (countRows == 0) {

                json.put("error", "accountQuantity = 0");
                response.getWriter().println(json);
                ApiLog.logMessage("getAccountByLabel", "error", "accountQuantity = 0.", "");

            }
            else if (apiLabel != null) {

                for (int i = 1; i < countRows; i++) {

                    jsonOrderedMap.put("accountId", id[i]);
                    jsonOrderedMap.put("accountType", aT[i]);
                    jsonOrderedMap.put("address", cl[i]);
                    jsonOrderedMap.put("addressRS", rs[i]);
                    jsonOrderedMap.put("publicKey", pKey[i]);
                    jsonOrderedMap.put("label", lab[i]);
                    jsonOrderedMap.put("secret", se[i]);
                    jsonOrderedMap.put("randomSalt", raSalt[i]);
                    jsonOrderedMap.put("randomIV", raIV[i]);
                    jsonOrderedMap.put("secretIteration", sIt[i]);
                    jsonOrderedMap.put("secretKeySize", sKey[i]);
                    jsonOrderedMap.put("secretGeneratorAlgorithm", eS[i]);
                    jsonOrderedMap.put("createdDate", timestamp[i]);

                    if (apiLabel.equals(lab[i])) {

                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        countAccounts++;

                        if (i != countRows - 1) {

                            response.getWriter().println(", ");

                        }

                    }

                }
            }

            if (countAccounts == 0 && apiLabel != null) {

                json.put("error", "label not in wallet file");
                response.getWriter().println(json);
                ApiLog.logMessage("getAccountByLabel", "error", apiLabel + " label not in wallet file.", "");

            }else{
                
                ApiLog.logMessage("getAccountByLabel", "success", "", "");
                
            }

        }
    }
}
