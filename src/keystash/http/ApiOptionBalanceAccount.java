package keystash.http;

import java.io.IOException;
import java.net.URL;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionBalanceAccount {

    public static class ApiServlet extends HttpServlet {

        public static void balanceAccount(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            String apiAddress = null;
            String apiNumberOfConfirmations = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";

            try {

                apiAddress = request.getParameter("address");
                apiNumberOfConfirmations = request.getParameter("numberOfConfirmations");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "address, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByAccount", "error", "address, server, serverPort must be specified.", "");

            }

            if (apiAddress == null) {

                json.put("error", "address not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByAccount", "error", "address not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByAccount", "error", "address not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByAccount", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getBalanceByAccount", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url;

                if (apiNumberOfConfirmations != null) {

                    url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getGuaranteedBalance&account="
                            + apiAddress + "&numberOfConfirmations=" + apiNumberOfConfirmations;

                }
                else {

                    url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getGuaranteedBalance&account="
                            + apiAddress;

                }

                try {

                    String responseRequest = IOUtils.toString(new URL(url));
                    JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);

                    if (responseJsonObject.get("guaranteedBalanceNQT") == null) {

                        json.put("error", "account not found");
                        response.getWriter().println(json);
                        ApiLog.logMessage("getBalanceByAccount", "error", "account not found.", "");

                    }
                    else {

                        json.put("guaranteedBalanceNQT", responseJsonObject.get("guaranteedBalanceNQT"));
                        response.getWriter().println(json);
                        ApiLog.logMessage("getBalanceByAccount", "success", apiAddress, "");

                    }

                } catch (IOException | ParseException e) {

                    json.put("error", "getBalanceByAccount failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getBalanceByAccount", "success", "getBalanceByAccount failed", "");

                }

            }
        }
    }
}
