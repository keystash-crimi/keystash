package keystash.http;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.KeyStashGUI;
import keystash.crypto.Crypto;
import keystash.crypto.KeyLength;
import keystash.gui.TableContent;
import static keystash.http.ApiOptionNewAccount.saveAccountToWallet;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Memory;
import keystash.util.PasswordStrength;
import org.apache.commons.codec.DecoderException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionImport {

    public static class ApiServlet extends HttpServlet {

        public static void importAccount(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException,
                NoSuchProviderException,
                NoSuchPaddingException,
                DecoderException,
                InvalidKeySpecException,
                InvalidKeyException,
                InvalidAlgorithmParameterException,
                IllegalBlockSizeException,
                BadPaddingException,
                InterruptedException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            int accountId = Db.countRows("keystash_wallet") + 1;
            char[] secret;
            String apiAssignment = null;
            char[] apiEncryptPassword = null;
            char[] apiImportPhrase = null;
            String apiLabel = null;
            int secretIteration;
            int secretKeySize;
            String encryptionStandard;

            try {

                apiAssignment = request.getParameter("assignment");
                apiEncryptPassword = request.getParameter("encryptPassword").toCharArray();
                apiImportPhrase = request.getParameter("importPhrase").toCharArray();
                apiLabel = request.getParameter("label");

            } catch (Exception e) {

            }

            byte[] secretPhrase;
            Long secretPhraseConvert;
            String addressRS = null;
            String addressClassic = null;
            String publicKey = null;
            double shannonEntropy;
            long shannonLong = 0;
            int passLength;
            String passLengthString = null;
            String shannonEntropyString = null;

            try {

                secretPhrase = Crypto.getPublicKey(new String(apiImportPhrase));
                secretPhraseConvert = Convert.getId(secretPhrase);
                addressRS = Convert.rsAccount(secretPhraseConvert);
                addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
                publicKey = Convert.toHexString(secretPhrase);
                shannonEntropy = PasswordStrength.getShannonEntropy(new String(apiImportPhrase));
                shannonLong = (long) shannonEntropy;
                passLength = apiImportPhrase.length;
                passLengthString = String.valueOf(passLength);
                shannonEntropyString = String.valueOf(shannonEntropy);

            } catch (Exception e) {

            }

            if (apiLabel == null) {

                apiLabel = Constants.accountLabel;

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "apiServerEnforcePOST is disabled.", apiAssignment);
                
            }
            else if (apiEncryptPassword == null) {

                json.put("error", "encryptPassword is empty");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "encryptPassword is empty.", apiAssignment);

            }
            else if (apiImportPhrase == null) {

                json.put("error", "importPhrase is empty");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "importPhrase is empty.", apiAssignment);

            }
            else if (apiImportPhrase.length < Constants.minPrivateKeyLength) {

                json.put("error", "importPhrase length must be at least " + Constants.minPrivateKeyLength + " characters");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "importPhrase length must be at least " + Constants.minPrivateKeyLength + " characters", apiAssignment);

            }
            else if (shannonLong <= 2) {

                json.put("error", "entropy is not high enough");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "entropy is not high enough.", apiAssignment);

            }
            else if (apiEncryptPassword.length < Constants.minPasswordChars) {

                json.put("error", "password must contain at least " + Constants.minPasswordChars + " characters");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "password must contain at least " + Constants.minPasswordChars + " characters", apiAssignment);

            }
            else if (apiLabel.length() > Constants.maxCharAccountLabel) {

                json.put("error", "label maximum length is " + Constants.maxCharAccountLabel + " characters");
                response.getWriter().println(json);
                ApiLog.logMessage("importAccount", "error", "label maximum length is " + Constants.maxCharAccountLabel + " characters", apiAssignment);

            }

            else {

                secret = Convert.stringToChar(new String(apiImportPhrase));

                if (TableContent.isAccountInTable(addressRS)) {

                    json.put("error", "account " + addressRS + " already in wallet file. import failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("importAccount", "error", "account " + addressRS + " already in wallet file. import failed", apiAssignment);

                }
                else {

                    if (!"test".equals(apiAssignment)) {

                        saveAccountToWallet(secret, apiEncryptPassword, apiLabel, apiAssignment);

                        if (!KeyStashGUI.runDaemon) {

                            TableContent.updateGuiTables();

                        }

                        ApiRefresh.refreshApi();

                    }

                    if (Convert.stringToBoolean(Config.properties("keystash.enableExpertEncryption")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

                        secretIteration = Convert.stringToInt(Config.properties("keystash.PBKDF2Iterations"));

                        secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

                        if (KeyLength.isUnlimitedKeyStrength() && secretKeySize == Constants.secretKeySize[1]) {

                            secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

                        }
                        else {

                            secretKeySize = Constants.secretKeySize[0];

                        }

                        encryptionStandard = Config.properties("keystash.keyGeneratorAlgorithm");

                    }
                    else {

                        secretIteration = Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1));
                        secretKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
                        encryptionStandard = Db.selectFromSettings("advanced_encryption_standard", "keystash_settings", "settings_id", 1);

                    }

                    if (!ApiValidate.validateWalletInsert(addressRS) && !"test".equals(apiAssignment)) {

                        json.put("error", "wallet insert was to fast, cant import account");
                        response.getWriter().println(json);
                        ApiLog.logMessage("importAccount", "error", "wallet insert was to fast, cant import account.", apiAssignment);

                    }
                    else {

                        jsonOrderedMap.put("accountId", accountId);
                        jsonOrderedMap.put("address", addressClassic);
                        jsonOrderedMap.put("addressRS", addressRS);
                        jsonOrderedMap.put("publicKey", publicKey);

                        if (apiLabel != null) {

                            jsonOrderedMap.put("label", apiLabel);

                        }
                        else {

                            jsonOrderedMap.put("label", Constants.accountLabel);

                        }

                        jsonOrderedMap.put("phraseEntropy", shannonEntropyString);
                        jsonOrderedMap.put("phraseLength", passLengthString);
                        jsonOrderedMap.put("secretIteration", secretIteration);
                        jsonOrderedMap.put("secretKeySize", secretKeySize);
                        jsonOrderedMap.put("secretGeneratorAlgorithm", encryptionStandard);
                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        ApiLog.logMessage("importAccount", "success", addressRS, apiAssignment);

                    }

                    Memory.clearPasswordMemory(apiEncryptPassword);
                    Memory.clearPasswordMemory(secret);
                    Memory.clearPasswordMemory(apiImportPhrase);
                    
                }
            }
        }
    }
}
