package keystash.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import org.apache.commons.lang.SystemUtils;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class ApiOptionBackup {

    public static class ApiServlet extends HttpServlet {

        public static void saveBackup(HttpServletRequest request, HttpServletResponse response)
                throws IOException {

            JSONObject json = new JSONObject();
            String apiAssignment = null;
            String apiSave = null;
            String backupPath = Config.properties("keystash.apiWalletBackupPath");

            if ("".equals(Config.properties("keystash.apiWalletBackupPath"))) {

                if (SystemUtils.IS_OS_LINUX) {

                    backupPath = Constants.walletPathOnlyUnix;

                }
                else {

                    backupPath = Constants.walletPathOnly;

                }

            }

            Path path = Paths.get(backupPath);

            try {

                apiAssignment = request.getParameter("assignment");
                apiSave = request.getParameter("save");

            } catch (Exception e) {

                json.put("error", "nothing specified");
                response.getWriter().println(json);
                ApiLog.logMessage("backupWallet", "error", "nothing specified.", apiAssignment);

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);
                ApiLog.logMessage("backupWallet", "error", "apiServerEnforcePOST is disabled.", apiAssignment);

            }
            else if (apiSave == null) {

                json.put("error", "save not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("backupWallet", "error", "save not specified.", apiAssignment);

            }
            else if (!Files.exists(path)) {

                json.put("error", "file path does not exist");
                response.getWriter().println(json);
                ApiLog.logMessage("backupWallet", "error", "file path does not exist", apiAssignment);

            }
            else if (Convert.stringToBoolean(apiSave) == true) {

                InputStream inStream;
                OutputStream outStream;
                File afile;

                try {

                    if (SystemUtils.IS_OS_LINUX) {

                        afile = new File(Constants.walletPathUnix);

                    }
                    else {

                        afile = new File(Constants.walletPath);

                    }

                    if (!"test".equals(apiAssignment)) {

                        File bfile = new File(backupPath + "/wallet_" + Date.getDate(true) + ".h2.db");
                        inStream = new FileInputStream(afile);
                        outStream = new FileOutputStream(bfile);
                        byte[] buffer = new byte[1024];
                        int length;

                        while ((length = inStream.read(buffer)) > 0) {

                            outStream.write(buffer, 0, length);

                        }

                        inStream.close();
                        outStream.close();
                    }
                    
                    json.put("success", "wallet backup saved with filename wallet_" + Date.getDate(true));
                    response.getWriter().println(json);
                    ApiLog.logMessage("backupWallet", "success", "wallet backup saved with filename wallet_" + Date.getDate(true), apiAssignment);

                } catch (IOException e) {

                    json.put("error", "wallet file not saved");
                    response.getWriter().println(json);
                    ApiLog.logMessage("backupWallet", "error", "wallet file not saved.", apiAssignment);

                }
            }

            else {

                json.put("error", "wallet file not saved");
                response.getWriter().println(json);
                ApiLog.logMessage("backupWallet", "error", "wallet file not saved.", apiAssignment);

            }
        }
    }
}
