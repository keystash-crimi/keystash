package keystash.http;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.KeyStashGUI;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.crypto.KeyLength;
import keystash.crypto.PhraseGenerator;
import keystash.gui.TableContent;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;
import keystash.util.Memory;
import keystash.util.PasswordStrength;
import org.apache.commons.codec.DecoderException;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionNewAccount {

    public static int secretIteration;
    public static int secretKeySize;
    public static String encryptionStandard;

    public static class ApiServlet extends HttpServlet {

        public static void getNewAddress(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                NoSuchAlgorithmException,
                NoSuchProviderException,
                NoSuchPaddingException,
                DecoderException,
                InvalidKeySpecException,
                InvalidKeyException,
                InvalidAlgorithmParameterException,
                IllegalBlockSizeException,
                BadPaddingException,
                InterruptedException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            int accountId = Db.countRows("keystash_wallet") + 1;
            char[] secret = PhraseGenerator.generatePhrase();
            byte[] secretPhrase = Crypto.getPublicKey(new String(secret));
            Long secretPhraseConvert = Convert.getId(secretPhrase);
            String addressRS = Convert.rsAccount(secretPhraseConvert);
            String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
            String publicKey = Convert.toHexString(secretPhrase);
            int passLength = secret.length;
            double shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
            String passLengthString = String.valueOf(passLength);
            shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
            String shannonEntropyString = String.valueOf(shannonEntropy);
            String apiAssignment = null;
            char[] apiEncryptPassword = null;
            String apiLabel = null;

            try {

                apiAssignment = request.getParameter("assignment");
                apiEncryptPassword = request.getParameter("encryptPassword").toCharArray();
                apiLabel = request.getParameter("label");

            } catch (Exception e) {

            }

            if (apiLabel == null) {

                apiLabel = Constants.accountLabel;

            }

            if (Convert.stringToBoolean(Config.properties("keystash.apiServerEnforcePOST")) && !"test".equals(apiAssignment)) {

                json.put("error", "apiServerEnforcePOST is disabled");
                response.getWriter().println(json);

            }
            else if (apiEncryptPassword == null) {

                json.put("error", "encryptPassword is empty");
                response.getWriter().println(json);

            }
            else if (apiEncryptPassword.length < Constants.minPasswordChars) {

                json.put("error", "encryptPassword must contain at least " + Constants.minPasswordChars + " characters");
                response.getWriter().println(json);

            }
            else if (apiLabel.length() > Constants.maxCharAccountLabel) {

                json.put("error", "label maximum length is " + Constants.maxCharAccountLabel + " characters");
                response.getWriter().println(json);

            }

            else {

                if (TableContent.isAccountInTable(addressRS)) {

                    json.put("error", "account " + addressRS + " already in wallet file. create new account failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getNewAccount", "error", addressRS + " already in wallet file.", apiAssignment);

                }
                else {

                    saveAccountToWallet(secret, apiEncryptPassword, apiLabel, apiAssignment);

                    if (!KeyStashGUI.runDaemon) {

                        TableContent.updateGuiTables();

                    }

                    ApiRefresh.refreshApi();

                    if (!ApiValidate.validateWalletInsert(addressRS) && !"test".equals(apiAssignment)) {

                        json.put("error", "wallet insert was to fast, cant generate account");
                        response.getWriter().println(json);
                        ApiLog.logMessage("getNewAccount", "error", "wallet insert was to fast.", apiAssignment);
                        
                    }
                    else {

                        jsonOrderedMap.put("accountId", accountId);
                        jsonOrderedMap.put("address", addressClassic);
                        jsonOrderedMap.put("addressRS", addressRS);
                        jsonOrderedMap.put("publicKey", publicKey);

                        if (apiLabel != null) {

                            jsonOrderedMap.put("label", apiLabel);

                        }
                        else {

                            jsonOrderedMap.put("label", Constants.accountLabel);

                        }

                        jsonOrderedMap.put("phraseEntropy", shannonEntropyString);
                        jsonOrderedMap.put("phraseLength", passLengthString);
                        jsonOrderedMap.put("secretIteration", secretIteration);
                        jsonOrderedMap.put("secretKeySize", secretKeySize);
                        jsonOrderedMap.put("secretGeneratorAlgorithm", encryptionStandard);
                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        ApiLog.logMessage("getNewAccount", "success", addressRS + " saved to wallet file.", apiAssignment);

                    }
                    
                    Memory.clearPasswordMemory(apiEncryptPassword);
                    Memory.clearPasswordMemory(secret);
                    
                }
            }
        }
    }

    public static void saveAccountToWallet(char[] secret, char[] password, String label, String apiAssignment)
            throws NoSuchAlgorithmException,
            NoSuchProviderException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            IOException,
            InterruptedException {

        int accountId = Db.countRows("keystash_wallet") + 1;
        char[] genSalt;
        genSalt = Aes.generateSaltOrIv(Constants.saltAndIvSize[0]);
        char[] tmpSalt = genSalt;
        char[] genIV;
        genIV = Aes.generateSaltOrIv(Constants.saltAndIvSize[1]);
        char[] tmpIV = genIV;
        char[] cipherPassPhrase;

        if (Convert.stringToBoolean(Config.properties("keystash.enableExpertEncryption")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

            secretIteration = Convert.stringToInt(Config.properties("keystash.PBKDF2Iterations"));

            if (KeyLength.isUnlimitedKeyStrength() && secretKeySize == Constants.secretKeySize[1]) {

                secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

            }
            else {

                secretKeySize = Constants.secretKeySize[0];

            }

            encryptionStandard = Config.properties("keystash.keyGeneratorAlgorithm");

        }
        else {

            secretIteration = Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1));
            secretKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
            encryptionStandard = Db.selectFromSettings("advanced_encryption_standard", "keystash_settings", "settings_id", 1);

        }

        cipherPassPhrase = Aes.encrypt(new String(secret), new String(password), new String(tmpSalt), new String(tmpIV));
        byte[] secretPhrase = Crypto.getPublicKey(String.valueOf(secret));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String publicKey = Convert.toHexString(secretPhrase);
        String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        int shareAccount = (Convert.stringToInt(Db.selectFromSettings("share_account", "keystash_settings", "settings_id", 1)));
        String accountType = Db.selectFromSettings("account_type", "keystash_settings", "settings_id", 1);
        String createdTimestamp = Date.getDate(false);

        if (!"test".equals(apiAssignment)) {

            Db.walletInsertNewAccount(accountId, addressClassic, addressRs, publicKey, label, new String(cipherPassPhrase), new String(tmpSalt),
                    new String(tmpIV), secretIteration, secretKeySize, encryptionStandard, shareAccount, accountType, createdTimestamp);

        }

    }

}
