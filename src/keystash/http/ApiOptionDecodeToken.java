package keystash.http;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.crypto.Crypto;
import keystash.util.Convert;
import keystash.util.Token;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

@SuppressWarnings("unchecked")
public class ApiOptionDecodeToken {

    public static class ApiServlet extends HttpServlet {

        public static void decodeToken(HttpServletRequest request, HttpServletResponse response)
                throws IOException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiData = null;
            String apiToken = null;

            try {

                apiData = request.getParameter("data");
                apiToken = request.getParameter("token");

            } catch (Exception e) {

                json.put("error", "data, token not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("decodeToken", "error", "data, token not specified.", "");

            }

            if (apiData == null) {

                json.put("error", "data not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("decodeToken", "error", "data not specified.", "");

            }
            else if (apiToken == null) {

                json.put("error", "data not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("decodeToken", "error", "token not specified.", "");

            }

            else {

                Token tok = Token.parseToken(apiToken, apiData.trim());
                byte[] publicKeyHash = Crypto.sha256().digest(tok.getPublicKey());

                jsonOrderedMap.put("timestamp", tok.getTimestamp());
                jsonOrderedMap.put("valid", tok.isValid());
                jsonOrderedMap.put("addressRS", Convert.rsAccount(Convert.fullHashToId(publicKeyHash)));
                jsonOrderedMap.put("address", Convert.toUnsignedLong(Convert.fullHashToId(publicKeyHash)));
                response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                ApiLog.logMessage("decodeToken", "success", Convert.rsAccount(Convert.fullHashToId(publicKeyHash)) + ", valid = " + tok.isValid() + ".", "");

            }
        }
    }
}
