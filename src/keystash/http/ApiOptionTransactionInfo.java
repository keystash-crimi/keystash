package keystash.http;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionTransactionInfo {

    public static class ApiServlet extends HttpServlet {

        public static void transactionInfo(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiTransaction = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";
            try {

                apiTransaction = request.getParameter("transaction");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "transaction, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionInfo", "error", "transaction, server, serverPort must be specified.", "");

            }

            if (apiTransaction == null) {

                json.put("error", "transaction not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionInfo", "error", "transaction not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionInfo", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionInfo", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("getTransactionInfo", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=getTransaction&transaction="
                        + apiTransaction;

                try {

                    String responseRequest = IOUtils.toString(new URL(url));
                    JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);

                    if (responseJsonObject.get("transaction") == null) {

                        json.put("error", "no transaction found");
                        response.getWriter().println(json);
                        ApiLog.logMessage("getTransactionInfo", "error", "no transaction found.", "");

                    }
                    else {

                        jsonOrderedMap.put("fullHash", responseJsonObject.get("fullHash"));
                        jsonOrderedMap.put("confirmations", responseJsonObject.get("confirmations"));
                        jsonOrderedMap.put("signatureHash", responseJsonObject.get("signatureHash"));
                        jsonOrderedMap.put("transaction", responseJsonObject.get("transaction"));
                        jsonOrderedMap.put("amountNQT", responseJsonObject.get("amountNQT"));
                        jsonOrderedMap.put("ecBlockHeight", responseJsonObject.get("ecBlockHeight"));
                        jsonOrderedMap.put("transactionIndex", responseJsonObject.get("transactionIndex"));
                        jsonOrderedMap.put("block", responseJsonObject.get("block"));
                        jsonOrderedMap.put("recipientRS", responseJsonObject.get("recipientRS"));
                        jsonOrderedMap.put("feeNQT", responseJsonObject.get("feeNQT"));
                        jsonOrderedMap.put("type", responseJsonObject.get("type"));
                        jsonOrderedMap.put("recipient", responseJsonObject.get("recipient"));
                        jsonOrderedMap.put("version", responseJsonObject.get("version"));
                        jsonOrderedMap.put("timestamp", responseJsonObject.get("timestamp"));
                        jsonOrderedMap.put("sender", responseJsonObject.get("sender"));
                        jsonOrderedMap.put("ecBlockId", responseJsonObject.get("ecBlockId"));
                        jsonOrderedMap.put("height", responseJsonObject.get("height"));
                        jsonOrderedMap.put("subtype", responseJsonObject.get("subtype"));
                        jsonOrderedMap.put("senderPublicKey", responseJsonObject.get("senderPublicKey"));
                        jsonOrderedMap.put("deadline", responseJsonObject.get("deadline"));
                        jsonOrderedMap.put("blockTimestamp", responseJsonObject.get("blockTimestamp"));
                        jsonOrderedMap.put("signature", responseJsonObject.get("signature"));
                        jsonOrderedMap.put("senderRS", responseJsonObject.get("senderRS"));
                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        ApiLog.logMessage("getTransactionInfo", "success", "", "");

                    }

                } catch (IOException | ParseException e) {

                    json.put("error", "getTransactionInfo failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("getTransactionInfo", "error", "getTransactionInfo failed.", "");

                }

            }
        }
    }
}
