package keystash.http;

import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import keystash.util.Validate;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;

@SuppressWarnings("unchecked")
public class ApiOptionParse {

    public static class ApiServlet extends HttpServlet {

        public static void parseBytes(HttpServletRequest request, HttpServletResponse response)
                throws IOException,
                ParseException {

            JSONObject json = new JSONObject();
            Map jsonOrderedMap = new LinkedHashMap();
            String apiSignedBytes = null;
            String apiServer = null;
            String apiServerPort = null;
            String apiServerSSL = "false";

            try {

                apiSignedBytes = request.getParameter("signedBytes");
                apiServer = request.getParameter("server");
                apiServerPort = request.getParameter("serverPort");
                apiServerSSL = request.getParameter("serverSSL");

            } catch (Exception e) {

                json.put("error", "signedBytes, server, serverPort must be specified");
                response.getWriter().println(json);
                ApiLog.logMessage("parseTransaction", "error", "signedBytes, server, serverPort must be specified.", "");

            }

            if (apiSignedBytes == null) {

                json.put("error", "signedBytes not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("parseTransaction", "error", "signedBytes not specified.", "");

            }
            else if (apiServer == null) {

                json.put("error", "server not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("parseTransaction", "error", "server not specified.", "");

            }
            else if (!Validate.validIP(apiServer) && !"localhost".equals(apiServer)) {

                json.put("error", "server not a valid ip address");
                response.getWriter().println(json);
                ApiLog.logMessage("parseTransaction", "error", "server not a valid ip address.", "");

            }
            else if (apiServerPort == null) {

                json.put("error", "serverPort not specified");
                response.getWriter().println(json);
                ApiLog.logMessage("parseTransaction", "error", "serverPort not specified.", "");

            }
            else {

                String isEnabledSSL = "http://";
                if ("true".equals(apiServerSSL)) {
                    isEnabledSSL = "https://";
                }

                String url = isEnabledSSL + apiServer + ":" + apiServerPort + "/nxt?requestType=parseTransaction&transactionBytes=" + apiSignedBytes;

                try {

                    String responseRequest = IOUtils.toString(new URL(url));
                    JSONObject responseJsonObject = (JSONObject) JSONValue.parseWithException(responseRequest);
                    jsonOrderedMap.put("senderPublicKey", responseJsonObject.get("senderPublicKey"));
                    jsonOrderedMap.put("feeNQT", responseJsonObject.get("feeNQT"));
                    jsonOrderedMap.put("type", responseJsonObject.get("type"));
                    jsonOrderedMap.put("fullHash", responseJsonObject.get("fullHash"));
                    jsonOrderedMap.put("version", responseJsonObject.get("version"));
                    jsonOrderedMap.put("ecBlockId", responseJsonObject.get("ecBlockId"));
                    jsonOrderedMap.put("signature", responseJsonObject.get("signature"));
                    jsonOrderedMap.put("signatureHash", responseJsonObject.get("signatureHash"));
                    jsonOrderedMap.put("senderRS", responseJsonObject.get("senderRS"));
                    jsonOrderedMap.put("subtype", responseJsonObject.get("subtype"));
                    jsonOrderedMap.put("amountNQT", responseJsonObject.get("amountNQT"));
                    jsonOrderedMap.put("sender", responseJsonObject.get("sender"));
                    jsonOrderedMap.put("recipientRS", responseJsonObject.get("recipientRS"));
                    jsonOrderedMap.put("recipient", responseJsonObject.get("recipient"));
                    jsonOrderedMap.put("ecBlockHeight", responseJsonObject.get("ecBlockHeight"));
                    jsonOrderedMap.put("verify", responseJsonObject.get("verify"));
                    jsonOrderedMap.put("deadline", responseJsonObject.get("deadline"));
                    jsonOrderedMap.put("transaction", responseJsonObject.get("transaction"));
                    jsonOrderedMap.put("timestamp", responseJsonObject.get("timestamp"));
                    jsonOrderedMap.put("height", responseJsonObject.get("height"));

                    if (responseJsonObject.get("fullHash") == null || responseJsonObject.get("transaction") == null) {

                        json.put("error", "signedBytes not valid or this request is only accepted using POST!");
                        response.getWriter().println(json);
                        ApiLog.logMessage("parseTransaction", "error", "signedBytes not valid or this request is only accepted using POST!.", "");

                    }
                    else {

                        response.getWriter().println(JSONValue.toJSONString(jsonOrderedMap));
                        ApiLog.logMessage("parseTransaction", "success", responseJsonObject.get(" transaction.") + " transaction.", "");

                    }

                } catch (IOException | ParseException e) {

                    json.put("error", "broadcastTransaction failed");
                    response.getWriter().println(json);
                    ApiLog.logMessage("parseTransaction", "error", "broadcastTransaction failed.", "");

                }

            }
        }
    }
}
