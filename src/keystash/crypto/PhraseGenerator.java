package keystash.crypto;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;

public class PhraseGenerator {

    public static char[] generateRandomChar(int minLength, int maxLength, int minLCaseCount, int minUCaseCount, int minNumCount, int minSpecialCount)
            throws NoSuchAlgorithmException,
            IOException {

        char[] randomString;
        String LCaseChars = Constants.lCaseChars;
        String UCaseChars = Constants.uCaseChars;
        String NumericChars = Constants.numericChars;
        String SpecialChars = Constants.specialChars;
        String AnsiChars = Constants.ansiChars;

        if (Convert.stringToBoolean(Config.properties("keystash.enableExpertGeneration")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

            try {
                if (Convert.stringToBoolean(Config.properties("keystash.blockEnableANSI"))) {

                    SpecialChars = SpecialChars + AnsiChars;

                }

                if (Convert.stringToInt(Config.properties("keystash.blockUpperCaseMin")) == 0 && Convert.stringToInt(Config.properties("keystash.blockUpperCaseMax")) == 0) {

                    UCaseChars = "";

                }

                if (Convert.stringToInt(Config.properties("keystash.blockNumeraryMin")) == 0 && Convert.stringToInt(Config.properties("keystash.blockNumeraryMax")) == 0) {

                    NumericChars = "";

                }

                if (Convert.stringToInt(Config.properties("keystash.blockSpecialANSICharMin")) == 0 && Convert.stringToInt(Config.properties("keystash.blockSpecialANSICharMax")) == 0) {

                    SpecialChars = "";

                }
            } catch (Exception e) {

                System.out.println("Cant load preferences from keystash.properties");

            }

        }
        else {

            try {
                if (Convert.stringToBoolean(Db.selectFromSettings("enable_ansi", "keystash_settings", "settings_id", 1))) {

                    SpecialChars = SpecialChars + AnsiChars;

                }

                if (Convert.stringToInt(Db.selectFromSettings("upper_case_min", "keystash_settings", "settings_id", 1)) == 0 && Convert.stringToInt(Db.selectFromSettings("upper_case_max", "keystash_settings", "settings_id", 1)) == 0) {

                    UCaseChars = "";

                }

                if (Convert.stringToInt(Db.selectFromSettings("numerary_min", "keystash_settings", "settings_id", 1)) == 0 && Convert.stringToInt(Db.selectFromSettings("numerary_max", "keystash_settings", "settings_id", 1)) == 0) {

                    NumericChars = "";

                }

                if (Convert.stringToInt(Db.selectFromSettings("special_char_min", "keystash_settings", "settings_id", 1)) == 0 && Convert.stringToInt(Db.selectFromSettings("special_char_max", "keystash_settings", "settings_id", 1)) == 0) {

                    SpecialChars = "";

                }
            } catch (Exception e) {

                System.out.println("Cant load preferences from settings db");

            }

        }

        Map<String, Integer> charGroupsUsed = new HashMap<>();
        charGroupsUsed.put("lcase", minLCaseCount);
        charGroupsUsed.put("ucase", minUCaseCount);
        charGroupsUsed.put("num", minNumCount);
        charGroupsUsed.put("special", minSpecialCount);

        int seed = generateSecureRandomSeed();
        Random random = new Random(seed);
        int randomIndex;
        if (minLength < maxLength) {

            randomIndex = random.nextInt((maxLength - minLength) + 1) + minLength;
            randomString = new char[randomIndex];

        }
        else {

            randomString = new char[minLength];

        }

        int requiredCharactersLeft = minLCaseCount + minUCaseCount + minNumCount + minSpecialCount;

        for (int i = 0; i < randomString.length; i++) {

            String selectableChars = "";

            if (requiredCharactersLeft < randomString.length - i) {

                selectableChars = LCaseChars + UCaseChars + NumericChars + SpecialChars;

            }
            else {

                for (Map.Entry<String, Integer> charGroup : charGroupsUsed.entrySet()) {

                    if ((int) charGroup.getValue() > 0) {

                        if (null != charGroup.getKey()) {

                            switch (charGroup.getKey()) {

                                case "lcase":

                                    selectableChars += LCaseChars;
                                    break;

                                case "ucase":

                                    selectableChars += UCaseChars;
                                    break;

                                case "num":

                                    selectableChars += NumericChars;
                                    break;

                                case "special":

                                    selectableChars += SpecialChars;
                                    break;

                            }
                        }
                    }
                }

            }

            randomIndex = random.nextInt((selectableChars.length()) - 1);
            char nextChar = selectableChars.charAt(randomIndex);

            randomString[i] = nextChar;

            if (LCaseChars.indexOf(nextChar) > -1) {

                charGroupsUsed.put("lcase", charGroupsUsed.get("lcase") - 1);

                if (charGroupsUsed.get("lcase") >= 0) {

                    requiredCharactersLeft--;

                }
            }
            else if (UCaseChars.indexOf(nextChar) > -1) {

                charGroupsUsed.put("ucase", charGroupsUsed.get("ucase") - 1);

                if (charGroupsUsed.get("ucase") >= 0) {

                    requiredCharactersLeft--;

                }
            }
            else if (NumericChars.indexOf(nextChar) > -1) {

                charGroupsUsed.put("num", charGroupsUsed.get("num") - 1);

                if (charGroupsUsed.get("num") >= 0) {

                    requiredCharactersLeft--;

                }
            }
            else if (SpecialChars.indexOf(nextChar) > -1) {

                charGroupsUsed.put("special", charGroupsUsed.get("special") - 1);

                if (charGroupsUsed.get("special") >= 0) {

                    requiredCharactersLeft--;

                }
            }
        }
        return randomString;
    }

    public static int randInt(int min, int max)
            throws NoSuchAlgorithmException,
            NoSuchProviderException {

        int seed = generateSecureRandomSeed();
        Random random = new Random(seed);
        int randomNum = random.nextInt((max - min) + 1) + min;
        return randomNum;

    }

    public static char[] generatePhrase()
            throws NoSuchAlgorithmException,
            NoSuchProviderException,
            IOException {

        int phraseBlocks;
        int lengthMinimumMin;
        int lengthMinimumMax;
        int lengthMaximumMin;
        int lengthMaximumMax;
        int lowerCaseMin;
        int lowerCaseMax;
        int upperCaseMin;
        int upperCaseMax;
        int numeraryMin;
        int numeraryMax;
        int specialCharMin;
        int specialCharMax;
        boolean whitespace = false;
        StringBuilder sbOne = new StringBuilder("");

        if (Convert.stringToBoolean(Config.properties("keystash.enableExpertGeneration")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

            phraseBlocks = Convert.stringToInt(Config.properties("keystash.phraseBlocks"));
            lengthMinimumMin = Convert.stringToInt(Config.properties("keystash.blockLengthMinimumMin"));
            lengthMinimumMax = Convert.stringToInt(Config.properties("keystash.blockLengthMinimumMax"));
            lengthMaximumMin = Convert.stringToInt(Config.properties("keystash.blockLengthMaximumMin"));
            lengthMaximumMax = Convert.stringToInt(Config.properties("keystash.blockLengthMaximumMax"));
            lowerCaseMin = Convert.stringToInt(Config.properties("keystash.blockLowerCaseMin"));
            lowerCaseMax = Convert.stringToInt(Config.properties("keystash.blockLowerCaseMax"));
            upperCaseMin = Convert.stringToInt(Config.properties("keystash.blockUpperCaseMin"));
            upperCaseMax = Convert.stringToInt(Config.properties("keystash.blockUpperCaseMax"));
            numeraryMin = Convert.stringToInt(Config.properties("keystash.blockNumeraryMin"));
            numeraryMax = Convert.stringToInt(Config.properties("keystash.blockNumeraryMax"));
            specialCharMin = Convert.stringToInt(Config.properties("keystash.blockSpecialANSICharMin"));
            specialCharMax = Convert.stringToInt(Config.properties("keystash.blockSpecialANSICharMax"));
            whitespace = Convert.stringToBoolean(Config.properties("keystash.blockEnableWhitespace"));

            for (int i = 0; i < phraseBlocks; i++) {

                char[] sbTwo = generateRandomChar(randInt(lengthMinimumMin, lengthMinimumMax), randInt(lengthMaximumMin, lengthMaximumMax),
                        randInt(lowerCaseMin, lowerCaseMax), randInt(upperCaseMin, upperCaseMax), randInt(numeraryMin, numeraryMax), randInt(specialCharMin, specialCharMax));
                StringBuilder combineBuilderOne = sbOne.append(sbTwo);

                if (i == phraseBlocks - 1) {
                }
                else if (whitespace) {

                    String sbThree = " ";
                    StringBuilder combineBuilderTwo = sbOne.append(sbThree);

                }
            }

            return Convert.stringToChar(new String(sbOne));

        }
        else if (Convert.stringToBoolean(Db.selectFromSettings("expert_generation", "keystash_settings", "settings_id", 1)) && !Convert.stringToBoolean(Config.properties("keystash.enableExpertGeneration")) || !Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

            phraseBlocks = Convert.stringToInt(Db.selectFromSettings("phrase_blocks", "keystash_settings", "settings_id", 1));
            lengthMinimumMin = Convert.stringToInt(Db.selectFromSettings("length_minimum_min", "keystash_settings", "settings_id", 1));
            lengthMinimumMax = Convert.stringToInt(Db.selectFromSettings("length_minimum_max", "keystash_settings", "settings_id", 1));
            lengthMaximumMin = Convert.stringToInt(Db.selectFromSettings("length_maximum_min", "keystash_settings", "settings_id", 1));
            lengthMaximumMax = Convert.stringToInt(Db.selectFromSettings("length_maximum_max", "keystash_settings", "settings_id", 1));
            lowerCaseMin = Convert.stringToInt(Db.selectFromSettings("lower_case_min", "keystash_settings", "settings_id", 1));
            lowerCaseMax = Convert.stringToInt(Db.selectFromSettings("lower_case_max", "keystash_settings", "settings_id", 1));
            upperCaseMin = Convert.stringToInt(Db.selectFromSettings("upper_case_min", "keystash_settings", "settings_id", 1));
            upperCaseMax = Convert.stringToInt(Db.selectFromSettings("upper_case_max", "keystash_settings", "settings_id", 1));
            numeraryMin = Convert.stringToInt(Db.selectFromSettings("numerary_min", "keystash_settings", "settings_id", 1));
            numeraryMax = Convert.stringToInt(Db.selectFromSettings("numerary_max", "keystash_settings", "settings_id", 1));
            specialCharMin = Convert.stringToInt(Db.selectFromSettings("special_char_min", "keystash_settings", "settings_id", 1));
            specialCharMax = Convert.stringToInt(Db.selectFromSettings("special_char_max", "keystash_settings", "settings_id", 1));
            whitespace = Convert.stringToBoolean(Db.selectFromSettings("enable_whitespace", "keystash_settings", "settings_id", 1));

            for (int i = 0; i < phraseBlocks; i++) {

                char[] sbTwo = generateRandomChar(randInt(lengthMinimumMin, lengthMinimumMax), randInt(lengthMaximumMin, lengthMaximumMax),
                        randInt(lowerCaseMin, lowerCaseMax), randInt(upperCaseMin, upperCaseMax), randInt(numeraryMin, numeraryMax), randInt(specialCharMin, specialCharMax));
                StringBuilder combineBuilderOne = sbOne.append(sbTwo);

                if (i == phraseBlocks - 1) {
                }
                else if (whitespace) {

                    String sbThree = " ";
                    StringBuilder combineBuilderTwo = sbOne.append(sbThree);

                }
            }

            return Convert.stringToChar(new String(sbOne));

        }
        else {
            phraseBlocks = Constants.phraseBlocks;
            for (int i = 0; i < phraseBlocks; i++) {

                char[] sbTwo = generateRandomChar(randInt(Constants.lengthMinimumMin, Constants.lengthMinimumMax), randInt(Constants.lengthMaximumMin, Constants.lengthMaximumMax), randInt(Constants.lowerCaseMin, Constants.lowerCaseMax), randInt(Constants.upperCaseMin, Constants.upperCaseMax), randInt(Constants.numeraryMin, Constants.numeraryMax), randInt(Constants.specialCharMin, Constants.specialCharMax));
                StringBuilder builderOne = sbOne.append(sbTwo);

                if (i == phraseBlocks - 1) {
                }
                else if (whitespace) {
                    String sbThree = " ";
                    StringBuilder builderTwo = sbOne.append(sbThree);

                }
            }

            return Convert.stringToChar(new String(sbOne));

        }

    }

    public static final int generateSecureRandomSeed()
            throws NoSuchAlgorithmException {

        SecureRandom secureRandomGenerator = SecureRandom.getInstance("SHA1PRNG");
        byte[] randomBytes = new byte[4];
        secureRandomGenerator.nextBytes(randomBytes);
        int seed = (randomBytes[0]) << 24
                | randomBytes[1] << 16
                | randomBytes[2] << 8
                | randomBytes[3];
        return seed;

    }

}
