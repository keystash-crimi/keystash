package keystash.crypto;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;

public class KeyLength {

    public static boolean isUnlimitedKeyStrength()
            throws NoSuchAlgorithmException {

        return Cipher.getMaxAllowedKeyLength("AES") == Integer.MAX_VALUE;

    }

}
