package keystash.crypto;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.security.SecureRandom;


public final class Crypto {

    private static final ThreadLocal<SecureRandom> secureRandom = new ThreadLocal<SecureRandom>() {
        @Override
        protected SecureRandom initialValue() {
            return new SecureRandom();
        }
    };

    public static void sign(String paraSecretPhrasePARA) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Crypto() {
    } 

    public static MessageDigest getMessageDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            
            System.err.println("Missing message digest algorithm: " + algorithm);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static MessageDigest sha256() {
        return getMessageDigest("SHA-256");
    }

    public static byte[] getPublicKey(String secretPhrase) {
        try {
            byte[] publicKey = new byte[32];
            Curve25519.keygen(publicKey, null, Crypto.sha256().digest(secretPhrase.getBytes("UTF-8")));
         
            return publicKey;
        } catch (UnsupportedEncodingException e) {            
            System.err.println("Error getting public key");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static byte[] getPrivateKey(String secretPhrase) {
        try {
            byte[] s = Crypto.sha256().digest(secretPhrase.getBytes("UTF-8"));
            Curve25519.clamp(s);
            return s;
        } catch (UnsupportedEncodingException e) {        
            System.err.println("Error getting private key");
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static String rsEncode(long id) {
        return ReedSolomon.encode(id);
    }

    public static long rsDecode(String rsString) {
        rsString = rsString.toUpperCase();
        try {
            long id = ReedSolomon.decode(rsString);
            if (!rsString.equals(ReedSolomon.encode(id))) {
                throw new RuntimeException("ERROR: Reed-Solomon decoding of " + rsString
                        + " not reversible, decoded to " + id);
            }
            return id;
        } catch (ReedSolomon.DecodeException e) {
            
            throw new RuntimeException(e.toString(), e);
        }
    }

    public static byte[] sign(byte[] message, String secretPhrase) {

        try {

            byte[] P = new byte[32];
            byte[] s = new byte[32];
            MessageDigest digest = Crypto.sha256();
            Curve25519.keygen(P, s, digest.digest(secretPhrase.getBytes("UTF-8")));

            byte[] m = digest.digest(message);

            digest.update(m);
            byte[] x = digest.digest(s);

            byte[] Y = new byte[32];
            Curve25519.keygen(Y, null, x);

            digest.update(m);
            byte[] h = digest.digest(Y);

            byte[] v = new byte[32];
            Curve25519.sign(v, h, x, s);

            byte[] signature = new byte[64];
            System.arraycopy(v, 0, signature, 0, 32);
            System.arraycopy(h, 0, signature, 32, 32);

            return signature;

        } catch (UnsupportedEncodingException e) {

            throw new RuntimeException(e.getMessage(), e);
        }

    }

    public static boolean verify(byte[] signature, byte[] message, byte[] publicKey, boolean enforceCanonical) {

        if (enforceCanonical && !Curve25519.isCanonicalSignature(signature)) {

            return false;
        }

        if (enforceCanonical && !Curve25519.isCanonicalPublicKey(publicKey)) {

            return false;
        }

        byte[] Y = new byte[32];
        byte[] v = new byte[32];
        System.arraycopy(signature, 0, v, 0, 32);
        byte[] h = new byte[32];
        System.arraycopy(signature, 32, h, 0, 32);
        Curve25519.verify(Y, v, h, publicKey);

        MessageDigest digest = Crypto.sha256();
        byte[] m = digest.digest(message);
        digest.update(m);
        byte[] h2 = digest.digest(Y);

        return Arrays.equals(h, h2);
    }

}
