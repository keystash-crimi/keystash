package keystash.crypto;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Debug;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import static org.apache.commons.lang.SystemUtils.IS_JAVA_1_7;

public class Aes {

    public static int pswdIterations = Constants.secretIteration;
    public static int keySize = Constants.secretKeySize[0];
    public static String encryptionStandard = Constants.encryptionStandard[0];

    public static char[] encrypt(String plainText, String password, String salt, String initializationVector)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            IOException {

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeyFactory factory = null;

        if (Convert.stringToBoolean(Config.properties("keystash.enableExpertEncryption")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

            keySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

            if (KeyLength.isUnlimitedKeyStrength() && keySize == Constants.secretKeySize[1]) {

                keySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

            }
            else {

                keySize = Constants.secretKeySize[0];

            }

            pswdIterations = Convert.stringToInt(Config.properties("keystash.PBKDF2Iterations"));

            encryptionStandard = Config.properties("keystash.keyGeneratorAlgorithm");

            if (!"PBKDF2WithHmacSHA224".equals(encryptionStandard) || !"PBKDF2WithHmacSHA256".equals(encryptionStandard)
                    || !"PBKDF2WithHmacSHA384".equals(encryptionStandard) || !"PBKDF2WithHmacSHA512".equals(encryptionStandard)) {

                factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

            }
            else if (IS_JAVA_1_7) {

                factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

            }
            else {

                factory = SecretKeyFactory.getInstance(Config.properties("keystash.keyGeneratorAlgorithm"));

            }

        }
        else {

            keySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
            pswdIterations = Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1));
            factory = SecretKeyFactory.getInstance(Db.selectFromSettings("advanced_encryption_standard", "keystash_settings", "settings_id", 1));

        }

        KeySpec spec = new PBEKeySpec(password.toCharArray(), Hex.decodeHex(salt.toCharArray()), pswdIterations, keySize);
        SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(Hex.decodeHex(initializationVector.toCharArray())));
        byte[] encryptedTextBytes = cipher.doFinal(plainText.getBytes());
        char[] charEncryptedTextBytes = new Base64().encodeAsString(encryptedTextBytes).toCharArray();
        return charEncryptedTextBytes;

    }

    public static char[] decrypt(String encryptedText, String password, String salt, String initializationVector, int pswdIt, int kSize, String eStandard)
            throws
            NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            UnsupportedEncodingException,
            DecoderException,
            IOException {

        pswdIterations = pswdIt;
        keySize = kSize;
        encryptionStandard = eStandard;
        SecretKeyFactory factory;
        byte[] encryptedTextBytes = Base64.decodeBase64(encryptedText);
        try {
            factory = SecretKeyFactory.getInstance(encryptionStandard);
        } catch (Exception e) {
            Debug.addLoggMessage("SecretKeyFactory not available");
        }

        if (!"PBKDF2WithHmacSHA224".equals(encryptionStandard) || !"PBKDF2WithHmacSHA256".equals(encryptionStandard)
                || !"PBKDF2WithHmacSHA384".equals(encryptionStandard) || !"PBKDF2WithHmacSHA512".equals(encryptionStandard)) {

            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

        }
        else if (IS_JAVA_1_7) {

            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

        }
        else {

            factory = SecretKeyFactory.getInstance(Config.properties("keystash.keyGeneratorAlgorithm"));

        }

        PBEKeySpec spec = new PBEKeySpec(
                password.toCharArray(),
                Hex.decodeHex(salt.toCharArray()),
                pswdIterations,
                keySize
        );

        SecretKey secretKey = factory.generateSecret(spec);
        SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(Hex.decodeHex(initializationVector.toCharArray())));
        byte[] decryptedTextBytes;

        try {

            decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
            char[] charDecryptedTextBytes = new String(decryptedTextBytes).toCharArray();
            return charDecryptedTextBytes;

        } catch (IllegalBlockSizeException | BadPaddingException e) {

            Debug.addLoggMessage("Decryption error, invalid syntax(keySize, secretKeyFactory).");

        }

        return null;

    }

    public static char[] generateSaltOrIv(int length)
            throws NoSuchAlgorithmException,
            NoSuchProviderException {

        int l = length;
        String chars = Constants.hexAlphabet;
        int seed = PhraseGenerator.generateSecureRandomSeed();
        Random random = new Random(seed);
        StringBuilder buf = new StringBuilder();

        for (int i = 0; i < l; i++) {

            buf.append(chars.charAt(random.nextInt(chars.length())));

        }

        char[] result = new String(buf).toCharArray();

        return result;

    }

}
