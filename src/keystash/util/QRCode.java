package keystash.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;
import javax.imageio.ImageIO;
import org.apache.commons.lang.SystemUtils;

public class QRCode {

    public static void createCode(String codeText)
            throws URISyntaxException {

        String myCodeText = codeText;
        File f;
        String path;

        if (SystemUtils.IS_OS_LINUX) {

            f = new File(Constants.qrCodeImagePathUnix);
            path = Constants.qrCodeImagePathUnix;

        }
        else {

            f = new File(Constants.qrCodeImagePath);
            path = Constants.qrCodeImagePath;

        }

        boolean createQRDirs = (new File(path)).mkdirs();
        int size = 360;
        String fileType = "png";
        File QRFile = f;
        try {
            @SuppressWarnings("UseOfObsoleteCollectionType")
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
            int width = byteMatrix.getWidth();
            BufferedImage image = new BufferedImage(width, width,
                    BufferedImage.TYPE_INT_RGB);
            image.createGraphics();

            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, width, width);
            graphics.setColor(Color.BLACK);

            for (int i = 0; i < width; i++) {
                for (int j = 0; j < width; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            ImageIO.write(image, fileType, QRFile);
        } catch (WriterException | IOException e) {
            System.err.println("Cant write to QR code template.");
        }
    }

}
