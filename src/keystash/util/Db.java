package keystash.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import keystash.gui.Dialog;
import org.apache.commons.lang.SystemUtils;
import org.h2.jdbcx.JdbcDataSource;

public class Db {

    static Connection wallet;
    static Connection settings;
    static Connection apiLog;

    public static void connectToWallet() {

        JdbcDataSource ds = new JdbcDataSource();
        if (SystemUtils.IS_OS_LINUX) {
            ds.setURL(Constants.walletDbNameUnix);
        }
        else {
            ds.setURL(Constants.walletDbName);
        }
        ds.setUser(Constants.dbUsernameWallet);
        ds.setPassword(Constants.dbPasswordWallet);

        try {

            wallet = ds.getConnection();

        } catch (SQLException e) {

            if (Constants.keystashd) {

                System.out.println("Wallet probably encrypted or already in use.");

            }
            else {

                Debug.addLoggMessage("Wallet probably encrypted or already in use.");

            }

        } finally {

        }

    }

    public static void connectToEncryptedWallet(String password) {

        String p = password + " " + Constants.dbPasswordWallet;
        JdbcDataSource ds = new JdbcDataSource();

        if (SystemUtils.IS_OS_LINUX) {
            ds.setURL(Constants.walletDbNameEncryptUnix);
        }
        else {
            ds.setURL(Constants.walletDbNameEncrypt);
        }

        ds.setUser(Constants.dbUsernameWallet);
        ds.setPassword(p);

        try {

            wallet = ds.getConnection();
        } catch (SQLException e) {

            if (Constants.keystashd) {

                System.out.println("Wrong master password or wallet file already in use.");

            }

        } finally {

            DbVersion.applyChanges();

        }

    }

    public static void connectToSettings() {

        JdbcDataSource ds = new JdbcDataSource();
        if (SystemUtils.IS_OS_LINUX) {

            ds.setURL(Constants.settingsDbNameUnix);

        }
        else {

            ds.setURL(Constants.settingsDbName);

        }
        ds.setUser(Constants.dbUsernameSettings);
        ds.setPassword(Constants.dbPasswordSettings);

        try {

            settings = ds.getConnection();
        } catch (SQLException e) {

            if (Constants.keystashd) {

                System.out.println("Wallet settings took to long to load or KeyStash is already running.");

            }
            else {

                Dialog.showDialog("Wallet settings took to long to load or KeyStash is already running.", "Note");

            }
            System.exit(0);
        } finally {
        }

    }

    public static void connectToApiLog() {

        JdbcDataSource ds = new JdbcDataSource();
        if (SystemUtils.IS_OS_LINUX) {

            ds.setURL(Constants.apiLogDbNameUnix);

        }
        else {

            ds.setURL(Constants.apiLogDbName);

        }
        ds.setUser(Constants.dbUsernameApiLog);
        ds.setPassword(Constants.dbPasswordApiLog);

        try {

            apiLog = ds.getConnection();
        } catch (SQLException e) {

            if (Constants.keystashd) {

                System.out.println("API log took to long to load or KeyStash is already running.");

            }
            else {

                Dialog.showDialog("API log took to long to load or KeyStash is already running.", "Note");

            }
            System.exit(0);
        } finally {
        }

    }

    public static void walletInsert(String sqlstmt) {

        Statement stmt;

        try {

            stmt = wallet.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("Wallet insert not possible.");

        }

    }

    public static void settingsInsert(String sqlstmt) {

        Statement stmt;

        try {

            stmt = settings.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("Settings insert not possible.");

        }

    }

    public static void apiLogInsert(String sqlstmt) {

        Statement stmt;

        try {

            stmt = apiLog.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("API insert not possible.");

        }

    }

    public static void walletInsertNewAccount(int accountId, String addressClassic, String addressRS, String publicKey,
            String label, String secret, String randomSalt, String randomIV, int secretIteration, int secretKeySize, String encryptionStandard,
            int shareAccount, String accountType, String createdTimestamp) {

        String insertstmt = "insert into keystash_wallet values(";
        String insertstmtclose = ")";
        String decimal = ", ";
        String invertDecimal = "'";
        String sqlstmt = insertstmt + accountId + decimal + invertDecimal + addressClassic + invertDecimal + decimal
                + invertDecimal + addressRS + invertDecimal + decimal + invertDecimal + publicKey + invertDecimal + decimal
                + invertDecimal + label + invertDecimal + decimal + invertDecimal + secret + invertDecimal + decimal
                + invertDecimal + randomSalt + invertDecimal + decimal + invertDecimal + randomIV + invertDecimal + decimal
                + invertDecimal + secretIteration + invertDecimal + decimal + invertDecimal + secretKeySize + invertDecimal + decimal
                + invertDecimal + encryptionStandard + invertDecimal + decimal + invertDecimal + shareAccount + invertDecimal + decimal
                + invertDecimal + accountType + invertDecimal + decimal + invertDecimal + createdTimestamp + invertDecimal + insertstmtclose;

        Statement stmt;
        try {

            stmt = wallet.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("Wallet insert not possible.");

        }

    }

    public static void walletInsertSettings(int settingsId, String lazyWalletVersion, boolean loadFromDevice, boolean welcomeScreen, boolean showWarnings,
            int clearClipboard, int phraseBlocks, int secretIteration, int secretKeySize, String encryptionStandard, int shareAccount, String accountType, boolean enableWhitespace,
            boolean enableAnsi, boolean randomGeneration, int lengthMinimumMin, int lengthMinimumMax, int lengthMaximumMin, int lengthMaximumMax,
            int lowerCaseMin, int lowerCaseMax, int upperCaseMin, int upperCaseMax, int numeraryMin, int numeraryMax, int specialCharMin, int specialCharMax) {

        String insertstmt = "insert into keystash_settings values(";
        String insertstmtclose = ")";
        String decimal = ", ";
        String invertDecimal = "'";
        String sqlstmt = insertstmt + settingsId + decimal + invertDecimal + lazyWalletVersion + invertDecimal + decimal
                + invertDecimal + loadFromDevice + invertDecimal + decimal + invertDecimal + welcomeScreen + invertDecimal + decimal
                + invertDecimal + showWarnings + invertDecimal + decimal + invertDecimal + clearClipboard + invertDecimal + decimal
                + invertDecimal + phraseBlocks + invertDecimal + decimal + invertDecimal + secretIteration + invertDecimal + decimal
                + invertDecimal + secretKeySize + invertDecimal + decimal + invertDecimal + encryptionStandard + invertDecimal + decimal
                + invertDecimal + shareAccount + invertDecimal + decimal + invertDecimal + accountType + invertDecimal
                + decimal + invertDecimal + enableWhitespace + invertDecimal + decimal + invertDecimal + enableAnsi + invertDecimal
                + decimal + invertDecimal + randomGeneration + invertDecimal + decimal + invertDecimal + lengthMinimumMin + invertDecimal + decimal
                + invertDecimal + lengthMinimumMax + invertDecimal + decimal + invertDecimal + lengthMaximumMin + invertDecimal + decimal
                + invertDecimal + lengthMaximumMax + invertDecimal + decimal + invertDecimal + lowerCaseMin + invertDecimal + decimal
                + invertDecimal + lowerCaseMax + invertDecimal + decimal + invertDecimal + upperCaseMin + invertDecimal + decimal
                + invertDecimal + upperCaseMax + invertDecimal + decimal + invertDecimal + numeraryMin + invertDecimal + decimal
                + invertDecimal + numeraryMax + invertDecimal + decimal + invertDecimal + specialCharMin + invertDecimal + decimal
                + invertDecimal + specialCharMax + invertDecimal + insertstmtclose;

        Statement stmt;
        try {

            stmt = settings.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            System.err.println("Wallet settings insert not possible.");

        }

    }

    public static void insertApiLog(int logId, String requestType, String response, String logMessage, String logTime) {

        String insertstmt = "insert into keystash_apilog values(";
        String insertstmtclose = ")";
        String decimal = ", ";
        String invertDecimal = "'";

        String sqlstmt = insertstmt + logId + decimal + invertDecimal + requestType + invertDecimal + decimal + invertDecimal + response + invertDecimal + decimal
                + invertDecimal + logMessage + invertDecimal + decimal + invertDecimal + logTime + invertDecimal + insertstmtclose;

        Statement stmt;
        try {

            stmt = apiLog.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            System.err.println("API log insert not possible.");

        }

    }

    public static void walletUpdateAccount(int accountId, String column, String value) {

        String insertstmt = "UPDATE keystash_wallet SET ";
        String where = " WHERE account_id = ";
        String equal = " = ";
        String invertDecimal = "'";
        String sqlstmt = insertstmt + column + equal + invertDecimal + value + invertDecimal + where + accountId;
        Statement stmt;

        try {

            stmt = wallet.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("Wallet update account not possible.");

        }

    }

    public static void walletUpdateSettings(int settingsId, String column, String value) {

        String insertstmt = "UPDATE keystash_settings SET ";
        String where = " WHERE settings_id = ";
        String equal = " = ";
        String invertDecimal = "'";
        String sqlstmt = insertstmt + column + equal + invertDecimal + value + invertDecimal + where + settingsId;

        Statement stmt;
        try {

            stmt = settings.createStatement();
            stmt.executeUpdate(sqlstmt);
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("Settings update not possible.");

        }

    }

    public static void walletQuery(String sqlstmt) {

        try {
            Statement select = wallet.createStatement();
            ResultSet result = select.executeQuery(sqlstmt);
            ResultSetMetaData resultMetaData = result.getMetaData();
            int numberOfColumns = resultMetaData.getColumnCount();
            int rowNum = 0;

            while (result.next()) {
                rowNum++;

                for (int i = 1; i <= numberOfColumns; i++) {

                    if (i < numberOfColumns) {

                    }
                }

            }
        } catch (Exception e) {

            Debug.addLoggMessage("Wallet query not possible.");

        }

    }

    public static String selectFromWallet(String value, String tableName, String where, int whereEquals) {

        try {
            Statement select = wallet.createStatement();

            String v;
            try ( ResultSet r = select.executeQuery("SELECT " + value + " from " + tableName + " where " + where + " = " + whereEquals) ) {

                r.next();
                v = r.getString(1);
            }

            return v;

        } catch (SQLException e) {

            Debug.addLoggMessage("Select from wallet not possible.");

        }
        return null;

    }

    public static String selectFromSettings(String value, String tableName, String where, int whereEquals) {

        try {
            Statement select = settings.createStatement();
            String v;

            try ( ResultSet r = select.executeQuery("SELECT " + value + " from " + tableName + " where " + where + " = " + whereEquals) ) {
                r.next();
                v = r.getString(1);
            }
            return v;
        } catch (SQLException e) {

            Debug.addLoggMessage("Select from settings not possible.");

        }
        return null;

    }

    public static String selectFromApiLog(String value, String tableName, String where, int whereEquals) {

        try {
            Statement select = apiLog.createStatement();
            String v;

            try ( ResultSet r = select.executeQuery("SELECT " + value + " from " + tableName + " where " + where + " = " + whereEquals) ) {
                r.next();
                v = r.getString(1);
            }
            return v;
        } catch (SQLException e) {

            Debug.addLoggMessage("Select from API log not possible.");

        }
        return null;

    }

    public static int countRows(String tableName) {

        try {

            Statement select = wallet.createStatement();

            int count;
            try ( ResultSet r = select.executeQuery("SELECT COUNT(*) AS rowcount FROM " + tableName) ) {

                r.next();
                count = r.getInt("rowcount");

            }

            return count;

        } catch (SQLException e) {

            Debug.addLoggMessage("Count not possible.");

        }

        return 0;

    }

    public static int countRowsApiLog(String tableName) {

        try {

            Statement select = apiLog.createStatement();

            int count;
            try ( ResultSet r = select.executeQuery("SELECT COUNT(*) AS rowcount FROM " + tableName) ) {

                r.next();
                count = r.getInt("rowcount");

            }

            return count;

        } catch (SQLException e) {

            Debug.addLoggMessage("Count not possible.");

        }

        return 0;

    }

    public static void walletClose() {

        Statement stmt;
        try {

            stmt = wallet.createStatement();
            stmt.executeUpdate("SHUTDOWN");
            stmt.close();

        } catch (SQLException ex) {

            Debug.addLoggMessage("Close wallet connection failed.");

        }
    }

}
