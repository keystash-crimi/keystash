package keystash.util;

import java.text.SimpleDateFormat;

public class Date {

    public static String getDate(boolean OfflineSigning) {
        java.util.Date dNow = new java.util.Date();
        SimpleDateFormat ft;
        if (OfflineSigning) {
            ft = new SimpleDateFormat("yyyy.MM.dd_hh.mm.ss_a_zzz");
        }
        else {
            ft = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss a zzz");
        }
        String showDate = ft.format(dNow);
        return showDate;
    }

}
