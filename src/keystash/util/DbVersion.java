package keystash.util;

public class DbVersion {

    public static void applyChanges() {

        try {

            Db.walletInsert("ALTER TABLE keystash_wallet ADD COLUMN IF NOT EXISTS advanced_encryption_standard VARCHAR(50) DEFAULT 'PBKDF2WithHmacSHA1' after secret_key_size");

        } catch (Exception e) {            
            
        }

    }

}
