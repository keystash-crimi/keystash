package keystash.util;

import java.io.File;

public class FileChecker {

    public static boolean checkExists(String pathUrl) {
        
    File file = new File(pathUrl);
    boolean walletExists = file.exists();
    return walletExists;
    
  }

    public static boolean checkIsFile(String pathUrl) {
        
    File file = new File(pathUrl);
    boolean walletIsFile = file.isFile();
    return walletIsFile;
    
  }

}
