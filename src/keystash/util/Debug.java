package keystash.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Debug {

    public static void addLoggMessage(String debugMessage) {

        try ( BufferedWriter out = new BufferedWriter(new FileWriter(Constants.debugFile, true)) ) {

            out.write("[" + Date.getDate(false) + "] " + debugMessage);
            out.write(System.lineSeparator());
            out.flush();
            out.close();

        } catch (IOException e) {

            System.err.println("Failed to write in debug.log");

        }

    }
}
