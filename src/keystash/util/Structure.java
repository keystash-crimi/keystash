package keystash.util;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import org.apache.commons.lang.SystemUtils;

public class Structure {

    public static void createWallet()
            throws SQLException,
            IOException {

        String walletPath;
        String walletDir;

        if (SystemUtils.IS_OS_LINUX) {

            walletPath = Constants.walletPathUnix;
            walletDir = "/keystash/wallet";

        }
        else {

            walletPath = Constants.walletPath;
            walletDir = "\\keystash\\wallet";

        }

        if (!FileChecker.checkExists(walletPath) && !FileChecker.checkIsFile(walletPath)) {

            boolean createWalletDirs = (new File(Constants.walletHomePath + walletDir)).mkdirs();
            Db.connectToWallet();
            Db.walletInsert("CREATE TABLE IF NOT EXISTS keystash_wallet(account_id int primary key, address_classic VARCHAR(30),"
                    + "address_rs VARCHAR(30), public_key VARCHAR(64), label VARCHAR(30), secret VARCHAR(1000000), randomSalt VARCHAR(256),"
                    + "randomIV VARCHAR(32), secret_iteration int(10), secret_key_size int(3), advanced_encryption_standard VARCHAR(50),"
                    + "share_account int(1), account_type VARCHAR(50), created_timestamp VARCHAR(50))");

        }

        if (FileChecker.checkExists(walletPath) && FileChecker.checkIsFile(walletPath)) {

            Db.connectToWallet();
            try {
                DbVersion.applyChanges();
            } catch (Exception e) {
            }

        }

    }

    public static void createSettings()
            throws IOException {

        String settingsPath;
        String settingsDir;

        if (SystemUtils.IS_OS_LINUX) {

            settingsPath = Constants.settingsPathUnix;
            settingsDir = "/settings";

        }
        else {

            settingsPath = Constants.settingsPath;
            settingsDir = "\\settings";

        }

        if (!FileChecker.checkExists(settingsPath) && !FileChecker.checkIsFile(settingsPath)) {

            boolean createSettingsDirs = (new File(settingsDir)).mkdirs();
            Db.connectToSettings();
            Db.settingsInsert("CREATE TABLE IF NOT EXISTS keystash_settings(settings_id int primary key, keystash_version VARCHAR(50),"
                    + "load_from_device boolean(5), welcome_screen boolean(5), show_warnings boolean(5), clear_clipboard int(4), phrase_blocks int(4),"
                    + "secret_iteration int(10), secret_key_size int(3), advanced_encryption_standard VARCHAR(50), share_account int(1), account_type VARCHAR(50), enable_whitespace boolean(5),"
                    + "enable_ansi boolean(5), expert_generation boolean(5), length_minimum_min int(4), length_minimum_max int(4), length_maximum_min int(4),"
                    + "length_maximum_max int(4), lower_case_min int(4), lower_case_max int(4), upper_case_min int(4), upper_case_max int(4), numerary_min int(4),"
                    + "numerary_max int(4), special_char_min int(4), special_char_max int(4))");

            Db.walletInsertSettings(Constants.settingsId, Constants.keyStashVersion, Constants.loadFromDevice, Constants.welcomeScreen, Constants.showWarnings,
                    Constants.clearClipboard, Constants.phraseBlocks, Constants.secretIteration, Constants.secretKeySize[0], Constants.encryptionStandard[0], Constants.shareAccount,
                    Constants.accountType[0], Constants.enableWhitespace, Constants.enableAnsi, Constants.expertGeneration, Constants.lengthMinimumMin,
                    Constants.lengthMinimumMax, Constants.lengthMaximumMin, Constants.lengthMaximumMax, Constants.lowerCaseMin, Constants.lowerCaseMax,
                    Constants.upperCaseMin, Constants.upperCaseMax, Constants.numeraryMin, Constants.numeraryMax, Constants.specialCharMin, Constants.specialCharMax);

        }

        if (FileChecker.checkExists(settingsPath) && FileChecker.checkIsFile(settingsPath)) {

            Db.connectToSettings();

            if ((!Db.selectFromSettings("keystash_version", "keystash_settings", "settings_id", 1).equals(Constants.keyStashVersion))) {

                Db.settingsInsert("DROP TABLE keystash_settings");
                Db.settingsInsert("CREATE TABLE IF NOT EXISTS keystash_settings(settings_id int primary key, keystash_version VARCHAR(50), load_from_device boolean(5),"
                        + "welcome_screen boolean(5), show_warnings boolean(5), clear_clipboard int(4), phrase_blocks int(4), secret_iteration int(10),"
                        + "secret_key_size int(3), advanced_encryption_standard VARCHAR(50), share_account int(1), account_type VARCHAR(50), enable_whitespace boolean(5), enable_ansi boolean(5),"
                        + "expert_generation boolean(5), length_minimum_min int(4), length_minimum_max int(4), length_maximum_min int(4), length_maximum_max int(4),"
                        + "lower_case_min int(4), lower_case_max int(4), upper_case_min int(4), upper_case_max int(4), numerary_min int(4), numerary_max int(4),"
                        + "special_char_min int(4), special_char_max int(4))");

                Db.walletInsertSettings(Constants.settingsId, Constants.keyStashVersion, Constants.loadFromDevice, Constants.welcomeScreen,
                        Constants.showWarnings, Constants.clearClipboard, Constants.phraseBlocks, Constants.secretIteration, Constants.secretKeySize[0],
                        Constants.encryptionStandard[0], Constants.shareAccount, Constants.accountType[0], Constants.enableWhitespace, Constants.enableAnsi, Constants.expertGeneration,
                        Constants.lengthMinimumMin, Constants.lengthMinimumMax, Constants.lengthMaximumMin, Constants.lengthMaximumMax, Constants.lowerCaseMin,
                        Constants.lowerCaseMax, Constants.upperCaseMin, Constants.upperCaseMax, Constants.numeraryMin, Constants.numeraryMax, Constants.specialCharMin,
                        Constants.specialCharMax);

            }
        }
    }

    public static void createApiLog()
            throws IOException {

        String apiLogPath;
        String apiLogDir;

        if (SystemUtils.IS_OS_LINUX) {

            apiLogPath = Constants.apiLogPathUnix;
            apiLogDir = "/apiLog";

        }
        else {

            apiLogPath = Constants.apiLogPath;
            apiLogDir = "\\apiLog";

        }

        if (!FileChecker.checkExists(apiLogPath) && !FileChecker.checkIsFile(apiLogPath)) {

            boolean createApiLogDirs = (new File(apiLogDir)).mkdirs();
            Db.connectToApiLog();
            Db.apiLogInsert("CREATE TABLE IF NOT EXISTS keystash_apilog(log_id int primary key, request_type VARCHAR(100),"
                    + "response VARCHAR(100), log_message VARCHAR(300), log_timestamp VARCHAR(100))");

        }
        else {

            Db.connectToApiLog();

        }
    }

}
