package keystash.util;

import java.util.Arrays;

public class Memory {

    public static void clearPasswordMemory(char[] checkPassword) {

        Arrays.fill(checkPassword, '\0');

    }

}
