package keystash.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    public static String properties(String value)
            throws IOException {

        Properties prop = new Properties();

        InputStream inputStream = new FileInputStream(Constants.propertiesFilePath + Constants.propertiesFile);

        if (inputStream != null) {
            
            prop.load(inputStream);
            
        }
        else {
            
            throw new FileNotFoundException("Property file '" + Constants.propertiesFile + "' not found in the conf folder.");
            
        }

        String result = prop.getProperty(value);

        return result;

    }

}
