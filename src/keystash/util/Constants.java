package keystash.util;

import java.util.Calendar;
import java.util.TimeZone;

public final class Constants {

    public static final String keyStashVersion = "0.5.0e";

    public static final String propertiesFile = "keystash.properties";
    public static final String propertiesFilePath = "conf/";
    public static final String debugFile = "debug.log";
    public static final String debugFilePath = "/";
    
    public static final boolean keystashd = false;
    public static final boolean loadFromDevice = false;
    public static final boolean welcomeScreen = true;

    public static final String walletHomePath = System.getProperty("user.home");
    public static final String walletPath = walletHomePath + "\\" + "keystash" + "\\" + "wallet" + "\\" + "wallet.h2.db";
    public static final String walletDirPath = walletHomePath + "\\" + "keystash" + "\\" + "wallet" + "\\" + "wallet";
    public static final String walletPathOnly = walletHomePath + "\\" + "keystash" + "\\" + "wallet";
    public static final String walletDbName = "jdbc:h2:" + walletDirPath + ";DB_CLOSE_ON_EXIT=TRUE";
    public static final String walletDbNameEncrypt = "jdbc:h2:" + walletDirPath + ";DB_CLOSE_ON_EXIT=TRUE;CIPHER=AES";
    public static final String settingsPath = "settings\\settings.h2.db";
    public static final String settingsDbName = "jdbc:h2:settings\\settings;DB_CLOSE_ON_EXIT=TRUE";
    public static final String apiLogPath = "log\\log.h2.db";
    public static final String apiLogDbName = "jdbc:h2:log\\log;DB_CLOSE_ON_EXIT=TRUE";

    public static final String dbUsernameWallet = "Key";
    public static final String dbPasswordWallet = "Stash";
    public static final String dbUsernameSettings = "Key";
    public static final String dbPasswordSettings = "Stash";
    public static final String dbUsernameApiLog = "Key";
    public static final String dbPasswordApiLog = "Stash";

    public static final int apiPort = 7801;
    public static final String apiHost = "127.0.0.1";
    public static final String apiAllowHost = "127.0.0.1";
    public static final String apiResourceBase = "/keystash";
    public static final String apiResourceBaseHome = "";
    public static final String apiResourceBaseTest = "/test";
    public static final boolean apiEnforcePost = false;
    public static final String apiAdminPassword = null;
    public static final boolean apiEnableAdminPassword = false;
    public static final String apiAddress = Constants.apiHost + ":" + apiPort + "/keystash";

    public static final String accountLabel = "No Label";
    public static final int maxCharAccountLabel = 30;

    public static final int minPasswordChars = 8;
    public static final int minPrivateKeyLength = 35;
    public static final int closeWindows = 480;

    public static final int settingsId = 1;
    public static final boolean showWarnings = false;
    public static final int secretIteration = 2000;
    public static final int[] secretKeySize = {128, 256};
    public static final String[] encryptionStandard = {"PBKDF2WithHmacSHA1", "PBKDF2WithHmacSHA224", "PBKDF2WithHmacSHA256", "PBKDF2WithHmacSHA384", "PBKDF2WithHmacSHA512"};
    public static final int shareAccount = 0;
    public static final String[] accountType = {"standard", "custom data", "multisignature"};
    public static final int clearClipboard = 10;
    public static final int phraseBlocks = 20;
    public static final boolean enableWhitespace = true;
    public static final boolean enableAnsi = false;
    public static final boolean expertGeneration = false;
    public static final int lengthMinimumMin = 5;
    public static final int lengthMinimumMax = 10;
    public static final int lengthMaximumMin = 15;
    public static final int lengthMaximumMax = 20;
    public static final int lowerCaseMin = 0;
    public static final int lowerCaseMax = 20;
    public static final int upperCaseMin = 0;
    public static final int upperCaseMax = 20;
    public static final int numeraryMin = 0;
    public static final int numeraryMax = 20;
    public static final int specialCharMin = 0;
    public static final int specialCharMax = 20;

    public static final int[] saltAndIvSize = {128, 32};

    public static final long genesisAccount = 1739068987193023818L;
    public static final long MAX_BALANCE_NXT = 1000000000;
    public static final long ONE_NXT = 100000000;
    public static final long MAX_BALANCE_NQT = MAX_BALANCE_NXT * ONE_NXT;
    public static final byte version = 16;
    public static final int payloadLength = 176;
    public static final int type = 0;
    public static final int deadline = 500;
    public static final String fileType = ".tx";

    public static final String alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
    public static final String hexAlphabet = "abcdef0123456789";
    public static final String lCaseChars = "abcdefgijkmnopqrstwxyz";
    public static final String uCaseChars = "ABCDEFGHJKLMNPQRSTWXYZ";
    public static final String numericChars = "0123456789";
    public static final String specialChars = "$@^`,|%;.~():?[]=-+_#!*'&";
    public static final String ansiChars = "ÆØ£¦Þä¢©óðÔæÙ»ü¸øûË½¡þÅÀÑ¿°ÎÕÐùÂòîá¾õÿÍ¤ô«¹×ñ±çÈ¬àïÇ·Áµºß¶ëÌÛ¯Úìâê¨ýÄéèö´ªÊÏ¥Ö³®§úÝíÒ²Ãå÷Óã¼ÜÉ";

    public static final String qrCodeImagePath = "images\\QR\\template.png";
    public static final String iconAccountsPath = "images\\icon\\accountsIconBG.png";
    public static final String iconLockEncryptedPath = "images\\icon\\lockEncryptedIconBG.png";
    public static final String iconLockDecryptedPath = "images\\icon\\lockDecryptedIconBG.png";
    public static final String iconApiOnPath = "images\\icon\\apiOnIconBG.png";
    public static final String iconApiOffPath = "images\\icon\\apiOffIconBG.png";
    public static final String iconApiFailPath = "images\\icon\\apiFailIconBG.png";

    public static final long EPOCH_BEGINNING;

    static {
        
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.YEAR, 2013);
        calendar.set(Calendar.MONTH, Calendar.NOVEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, 24);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        EPOCH_BEGINNING = calendar.getTimeInMillis();
        
    }

    //Unix
    public static final String walletPathUnix = walletHomePath + "/" + "keystash" + "/" + "wallet" + "/" + "wallet.h2.db";
    public static final String walletDirPathUnix = walletHomePath + "/" + "keystash" + "/" + "wallet" + "/" + "wallet";
    public static final String walletPathOnlyUnix = walletHomePath + "/" + "keystash" + "/" + "wallet";
    public static final String walletDbNameUnix = "jdbc:h2:" + walletDirPathUnix + ";DB_CLOSE_ON_EXIT=TRUE";
    public static final String walletDbNameEncryptUnix = "jdbc:h2:" + walletDirPathUnix + ";DB_CLOSE_ON_EXIT=TRUE;CIPHER=AES";
    public static final String settingsPathUnix = "settings/settings.h2.db";
    public static final String settingsDbNameUnix = "jdbc:h2:settings/settings;DB_CLOSE_ON_EXIT=TRUE";
    public static final String apiLogPathUnix = "log/log.h2.db";
    public static final String apiLogDbNameUnix = "jdbc:h2:log/log;DB_CLOSE_ON_EXIT=TRUE";

    public static final String qrCodeImagePathUnix = "images/QR/template.png";
    public static final String iconAccountsPathUnix = "images/icon/accountsIconBG.png";
    public static final String iconLockEncryptedPathUnix = "images/icon/lockEncryptedIconBG.png";
    public static final String iconLockDecryptedPathUnix = "images/icon/lockDecryptedIconBG.png";
    public static final String iconApiOnPathUnix = "images/icon/apiOnIconBG.png";
    public static final String iconApiOffPathUnix = "images/icon/apiOffIconBG.png";
    public static final String iconApiFailPathUnix = "images/icon/apiFailIconBG.png";

}
