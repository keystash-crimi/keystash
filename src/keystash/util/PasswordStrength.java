package keystash.util;

import java.util.HashMap;
import java.util.Map;

public class PasswordStrength {


    public static int checkPasswordStrength(String password) {
        int strengthPercentage = 0;

        String[] partialRegexChecks = {
            ".*[a-z]+.*", 
            ".*[A-Z]+.*", 
            ".*[\\d]+.*", 
            ".*[*$-+#?_&=!%{}/]+.*",          
        };

        if (password.matches(partialRegexChecks[0])) {
            strengthPercentage += 25;
        }
        if (password.matches(partialRegexChecks[1])) {
            strengthPercentage += 25;
        }
        if (password.matches(partialRegexChecks[2])) {
            strengthPercentage += 25;
        }
        if (password.matches(partialRegexChecks[3])) {
            strengthPercentage += 25;
        }
      
        return strengthPercentage;
        
    }

    @SuppressWarnings("boxing")
    public static double getShannonEntropy(String s) {
 
        Map<Character, Integer> occ = new HashMap<>();

        for (int c_ = 0; c_ < s.length(); ++c_) {
            char cx = s.charAt(c_);
            if (occ.containsKey(cx)) {
                occ.put(cx, occ.get(cx) + 1);
            }
            else {
                occ.put(cx, 1);
            }
           
        }

        double e = 0.0;
        for (Map.Entry<Character, Integer> entry : occ.entrySet()) {
            char cx = entry.getKey();
            double p = (double) entry.getValue() / s.length();
            e += p * log2(p);
        }
        if (-e < 1.0) {
            return 0.0;
        }
        return -e;
    }

    private static double log2(double a) {
        return Math.log(a) / Math.log(2);
    }

}