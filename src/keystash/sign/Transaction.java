package keystash.sign;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import keystash.util.Convert;
import keystash.util.Time;
import keystash.util.Constants;
import keystash.util.Debug;

public class Transaction {

    static final long genesisAccount = Constants.genesisAccount;
    static volatile Time time;
    static int timestamp;
    static final byte version = Constants.version;

    public static byte[] getBytes(short deadline, byte[] senderPublicKey, long recipientId, long amountNQT, long feeNQT, byte[] signature) {
        try {

            time = new Time.EpochTime();
            timestamp = Convert.getEpochTime();
            ByteBuffer buffer = ByteBuffer.allocate(Constants.payloadLength);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.put((byte) Constants.type);
            buffer.put((byte) version);
            buffer.putInt(timestamp);
            buffer.putShort(deadline);
            buffer.put(senderPublicKey);
            buffer.putLong(true ? recipientId : genesisAccount);
            buffer.putLong(amountNQT);
            buffer.putLong(feeNQT);
            buffer.put(new byte[32]);
            buffer.put(signature != null ? signature : new byte[64]);
            return buffer.array();

        } catch (RuntimeException e) {

            System.err.println("Failed to get transaction bytes for transaction.");
            Debug.addLoggMessage("Failed to get transaction bytes for transaction.");
            throw e;

        }

    }

    public static int getEpochTime() {
        return time.getTime();
    }

    public static long convertToNQT(String amountNXT) {

        long amountNQT = 0;

        if (amountNXT.contains(".")) {

            String fraction = amountNXT;
            String[] parts = fraction.split("\\.");
            String part1 = parts[0];
            String part2 = parts[1];

            for (int i = part2.length(); i <= 7; i++) {

                part2 = part2 + "0";

            }

            String totalNXT = part1 + part2;
            amountNQT = Long.parseLong(totalNXT);

        }
        else {

            int amountInt = Convert.stringToInt(amountNXT);
            amountNQT = (long) amountInt * 100000000;

        }

        return amountNQT;

    }
}
