package keystash.gui;

import java.awt.GraphicsEnvironment;

public class Console {

    public static boolean isSystemHeadless() {

        boolean headless = false;
        if (GraphicsEnvironment.isHeadless()) {
            headless = true;
        }
        return headless;

    }

}
