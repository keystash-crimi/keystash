package keystash.gui;

import static keystash.KeyStashGUI.decryptDatabaseFrame;
import static keystash.KeyStashGUI.decryptDatabasePasswordField;
import static keystash.KeyStashGUI.decryptDatabaseShowPhraseCheckBox;
import static keystash.KeyStashGUI.encryptWalletFileMenuItem;
import static keystash.KeyStashGUI.iconPaneAccountjLabel;
import keystash.util.Constants;
import keystash.util.Db;
import keystash.util.Memory;

public class DecryptWallet {

    public static void doDecrypt() {

        char[] checkPassword = decryptDatabasePasswordField.getPassword();
        char c = '¡';

        if (checkPassword.length < Constants.minPasswordChars) {
            Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");
        }
        else if (new String(checkPassword).contains(Character.toString(c))) {
            Dialog.showDialog("No ¡ character allowed in password.", "Note");
        }
        else {
            try {
                decryptDatabasePasswordField.setText("");
                decryptDatabaseFrame.setVisible(false);
                Loader.decryptFrameActive = false;
                Db.connectToEncryptedWallet(String.valueOf(checkPassword).replace(' ', c));
                TableContent.getTable();
                encryptWalletFileMenuItem.setEnabled(false);
                MenuItem.encryptedEnableMenu(true);
                Loader.showWelcomeScreen();
                Icon.initializeIcons(false);
                int countRows = Db.countRows("keystash_wallet");
                iconPaneAccountjLabel.setToolTipText("Accounts: " + countRows);
            } catch (Exception e) {
                Dialog.showDialog("Wrong password, decryption failed.", "Note");
                decryptDatabaseFrame.setVisible(true);
            }

        }

        Memory.clearPasswordMemory(checkPassword);

    }

    public static void showPhrase() {

        boolean showPhrase = decryptDatabaseShowPhraseCheckBox.isSelected();

        if (showPhrase) {
            decryptDatabasePasswordField.setEchoChar((char) 0);
        }
        else {
            decryptDatabasePasswordField.setEchoChar('*');
        }

    }

}
