package keystash.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

public class FeelAndLook {

    public static void loadCustom() {

        ColorUIResource colorResource = new ColorUIResource(Color.CYAN.darker().darker());
        UIManager.put("nimbusOrange", colorResource);

    }

    public static void centerFrame(JFrame type, int width, int height) {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (width / 2),
                middle.y - (height / 2));
        type.setLocation(newLocation);

    }

    public static void setFrameSize(JFrame type, int width, int height) {

        type.setPreferredSize(new Dimension(width, height));
        type.setMinimumSize(new Dimension(width, height));
        type.setMaximumSize(new Dimension(width, height));
        type.setLocationRelativeTo(null);
        type.pack();

    }

    public static void setFrameSizeLinux(final JFrame type) {

        type.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                Dimension d = type.getSize();
                Dimension minD = type.getMinimumSize();
                if (d.width < minD.width) {
                    d.width = minD.width;
                }
                if (d.height < minD.height) {
                    d.height = minD.height;
                }
                type.setSize(d);
            }
        });

    }

}
