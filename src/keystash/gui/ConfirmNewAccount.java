package keystash.gui;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.accountMenuItem;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.confirmNewAccountAttemptLabel;
import static keystash.KeyStashGUI.confirmNewAccountAttemptValueLabel;
import static keystash.KeyStashGUI.confirmNewAccountEntropyValue;
import static keystash.KeyStashGUI.confirmNewAccountFrame;
import static keystash.KeyStashGUI.confirmNewAccountGenerateNewPhraseButton;
import static keystash.KeyStashGUI.confirmNewAccountIncludeLettersField;
import static keystash.KeyStashGUI.confirmNewAccountLengthValue;
import static keystash.KeyStashGUI.confirmNewAccountOKButton;
import static keystash.KeyStashGUI.confirmNewAccountPhraseField;
import static keystash.KeyStashGUI.confirmNewAccountSearchAddressToggleButton;
import static keystash.KeyStashGUI.confirmNewAccountShowAddressRsLabel;
import static keystash.KeyStashGUI.confirmNewAccountShowPhraseCheckBox;
import static keystash.KeyStashGUI.enableApiMenuItem;
import static keystash.KeyStashGUI.globalPassword;
import static keystash.KeyStashGUI.guiShowWarnings;
import static keystash.KeyStashGUI.iconPaneAccountjLabel;
import static keystash.KeyStashGUI.secret;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.crypto.KeyLength;
import keystash.crypto.PhraseGenerator;
import keystash.http.ApiRefresh;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;
import keystash.util.Memory;
import keystash.util.PasswordStrength;
import org.apache.commons.codec.DecoderException;

public class ConfirmNewAccount {

    public static void saveAccount()
            throws IOException,
            NoSuchAlgorithmException {

        char[] checkPassword = confirmNewAccountPhraseField.getPassword();
        int passLength = checkPassword.length;
        byte[] secretPhrase = Crypto.getPublicKey(String.valueOf(checkPassword));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String publicKey = Convert.toHexString(secretPhrase);
        String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        double shannonEntropy = PasswordStrength.getShannonEntropy(new String(checkPassword));
        long shannonLong = (long) shannonEntropy;
        secret = confirmNewAccountPhraseField.getPassword();

        if (passLength < 35) {

            Dialog.showDialog("Minimum Length of phrase must be 35 characters.", "Note");

        }
        else if (TableContent.isAccountInTable(addressRs)) {

            Dialog.showDialog("Account already in wallet.", "Note");

        }
        else if (shannonLong <= 2) {

            Dialog.showDialog("Entropy is not high enough.", "Note");

        }
        else {
            if (enableApiMenuItem.isSelected()) {

                backgroundMenu = 3;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            int accountId = Db.countRows("keystash_wallet") + 1;
            char[] genSalt = null;
            try {

                genSalt = Aes.generateSaltOrIv(Constants.saltAndIvSize[0]);

            } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {

                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);

            }
            char[] tmpSalt = genSalt;
            char[] genIV = null;
            try {

                genIV = Aes.generateSaltOrIv(Constants.saltAndIvSize[1]);

            } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {

                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);

            }
            char[] tmpIV = genIV;
            char[] cipherPassPhrase = null;
            try {

                cipherPassPhrase = Aes.encrypt(String.valueOf(secret), String.valueOf(globalPassword), new String(tmpSalt), new String(tmpIV));

            } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | DecoderException ex) {

                System.err.println("Illegal key size check for AES unlimited key lenght true");

            }

            String label = Constants.accountLabel;

            int secretIteration;
            int secretKeySize = 0;
            String encryptionStandard;

            if (Convert.stringToBoolean(Config.properties("keystash.enableExpertEncryption")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

                secretIteration = Convert.stringToInt(Config.properties("keystash.PBKDF2Iterations"));

                secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

                if (KeyLength.isUnlimitedKeyStrength() && secretKeySize == Constants.secretKeySize[1]) {

                    secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

                }
                else {

                    secretKeySize = Constants.secretKeySize[0];

                }

                encryptionStandard = Config.properties("keystash.keyGeneratorAlgorithm");

            }
            else {

                secretIteration = Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1));
                secretKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
                encryptionStandard = Db.selectFromSettings("advanced_encryption_standard", "keystash_settings", "settings_id", 1);

            }

            int shareAccount = (Convert.stringToInt(Db.selectFromSettings("share_account", "keystash_settings", "settings_id", 1)));
            String accountType = Db.selectFromSettings("account_type", "keystash_settings", "settings_id", 1);

            String createdTimestamp = Date.getDate(false);

            Db.walletInsertNewAccount(accountId, addressClassic, addressRs, publicKey, label, new String(cipherPassPhrase), new String(tmpSalt),
                    new String(tmpIV), secretIteration, secretKeySize, encryptionStandard, shareAccount, accountType, createdTimestamp);

            TableContent.deleteGuiTables();
            TableContent.getTable();

            ApiRefresh.refreshApi();
            guiShowWarnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));

            if (guiShowWarnings) {

                Dialog.showDialog("Always backup your wallet after changes on a secure place!", "Note");

            }

            int countRows = Db.countRows("keystash_wallet");
            iconPaneAccountjLabel.setToolTipText("Accounts: " + countRows);
            confirmNewAccountFrame.setVisible(false);
            accountMenuItem.setEnabled(true);
            MenuItem.enableMenu(true);

        }

        if (enableApiMenuItem.isSelected()) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            backgroundMenu = 2;

        }

        Memory.clearPasswordMemory(checkPassword);
        Memory.clearPasswordMemory(secret);
        Memory.clearPasswordMemory(globalPassword);
    }

    public static void showPhrase() {

        boolean showPhrase = confirmNewAccountShowPhraseCheckBox.isSelected();

        if (showPhrase) {

            confirmNewAccountPhraseField.setEchoChar((char) 0);

        }
        else {

            confirmNewAccountPhraseField.setEchoChar('*');

        }

    }

    public static void generateNewPhrase()
            throws IOException {

        char[] myPhrase = null;
        try {
            myPhrase = PhraseGenerator.generatePhrase();
        } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        secret = myPhrase;
        byte[] secretPhrase = Crypto.getPublicKey(new String(secret));
        Long secretPhraseConvert = Convert.getId(secretPhrase);

        String addressRs = Convert.rsAccount(secretPhraseConvert);
        confirmNewAccountShowAddressRsLabel.setText(addressRs);
        confirmNewAccountPhraseField.setText(new String(myPhrase));
        int passLength = secret.length;
        String passLengthString = String.valueOf(passLength);
        double shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
        String shannonEntropyString = String.valueOf(shannonEntropy);
        confirmNewAccountEntropyValue.setText(shannonEntropyString);
        confirmNewAccountLengthValue.setText(passLengthString);

    }

    public static void searchAddress() {

        boolean isToogled = confirmNewAccountSearchAddressToggleButton.isSelected();
        String r = ".*[*$-+.,10#?_&=!%{}/]+.*";
        Matcher m = Pattern.compile(r).matcher(confirmNewAccountIncludeLettersField.getText());

        if (isToogled) {

            if (confirmNewAccountIncludeLettersField.getText().equals("NXT-     ")) {
                Dialog.showDialog("Searchfield is empty!", "Note");
                confirmNewAccountSearchAddressToggleButton.setSelected(false);

            }
            else if (m.find()) {

                Dialog.showDialog("Wrong characters!", "Note");
                confirmNewAccountSearchAddressToggleButton.setSelected(false);

            }
            else {

                confirmNewAccountAttemptLabel.setEnabled(true);
                confirmNewAccountAttemptValueLabel.setEnabled(true);
                confirmNewAccountGenerateNewPhraseButton.setEnabled(false);
                confirmNewAccountOKButton.setEnabled(false);
                confirmNewAccountIncludeLettersField.setEnabled(false);
                backgroundMenu = 4;

            }
        }
        else {

            confirmNewAccountAttemptLabel.setEnabled(false);
            confirmNewAccountAttemptValueLabel.setEnabled(false);
            confirmNewAccountGenerateNewPhraseButton.setEnabled(true);
            confirmNewAccountOKButton.setEnabled(true);
            confirmNewAccountIncludeLettersField.setEnabled(true);

        }

    }

    public static void phraseLength() {

        char[] checkPassword = confirmNewAccountPhraseField.getPassword();
        confirmNewAccountLengthValue.setText(Convert.intToString(checkPassword.length));
        byte[] secretPhrase = Crypto.getPublicKey(new String(checkPassword));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        if (checkPassword.length == 0) {
            confirmNewAccountShowAddressRsLabel.setText("");
        }
        else {
            confirmNewAccountShowAddressRsLabel.setText(addressRs);
        }

    }

    public static void closeFrame() {

        confirmNewAccountAttemptLabel.setEnabled(false);
        confirmNewAccountAttemptValueLabel.setEnabled(false);
        confirmNewAccountGenerateNewPhraseButton.setEnabled(true);
        confirmNewAccountOKButton.setEnabled(true);
        confirmNewAccountIncludeLettersField.setEnabled(true);
        confirmNewAccountSearchAddressToggleButton.setSelected(false);
        backgroundMenu = 0;
        confirmNewAccountFrame.setVisible(false);
        accountMenuItem.setEnabled(true);
        confirmNewAccountFrame.dispose();
        MenuItem.enableMenu(true);

    }

}
