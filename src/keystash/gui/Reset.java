package keystash.gui;

import static keystash.KeyStashGUI.showPreferencesANSICheckbox;
import static keystash.KeyStashGUI.showPreferencesIterationSpinner;
import static keystash.KeyStashGUI.showPreferencesKeySizeComboBox;
import static keystash.KeyStashGUI.showPreferencesLengthMaximumMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMaximumMinSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMinimumMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMinimumMinSpinner;
import static keystash.KeyStashGUI.showPreferencesLowerCaseMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLowerCaseMinSpinner;
import static keystash.KeyStashGUI.showPreferencesNumeraryMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesNumeraryMinSpinner;
import static keystash.KeyStashGUI.showPreferencesPhraseBlocksSpinner;
import static keystash.KeyStashGUI.showPreferencesSpecialCharMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesSpecialCharMinSpinner;
import static keystash.KeyStashGUI.showPreferencesUpperCaseMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesUpperCaseMinSpinner;
import static keystash.KeyStashGUI.showPreferencesWhitespaceCheckbox;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;

public class Reset {

    public static void resetDefaultSettings() {

        Db.walletUpdateSettings(1, "phrase_blocks", Convert.intToString(Constants.phraseBlocks));
        Db.walletUpdateSettings(1, "secret_iteration", Convert.intToString(Constants.secretIteration));
        Db.walletUpdateSettings(1, "enable_whitespace", Convert.booleanToString(Constants.enableWhitespace));
        Db.walletUpdateSettings(1, "enable_ansi", Convert.booleanToString(Constants.enableAnsi));
        Db.walletUpdateSettings(1, "length_minimum_min", Convert.intToString(Constants.lengthMinimumMin));
        Db.walletUpdateSettings(1, "length_minimum_max", Convert.intToString(Constants.lengthMinimumMax));
        Db.walletUpdateSettings(1, "length_maximum_min", Convert.intToString(Constants.lengthMaximumMin));
        Db.walletUpdateSettings(1, "length_maximum_max", Convert.intToString(Constants.lengthMaximumMax));
        Db.walletUpdateSettings(1, "lower_case_min", Convert.intToString(Constants.lowerCaseMin));
        Db.walletUpdateSettings(1, "lower_case_max", Convert.intToString(Constants.lowerCaseMax));
        Db.walletUpdateSettings(1, "upper_case_min", Convert.intToString(Constants.upperCaseMin));
        Db.walletUpdateSettings(1, "upper_case_max", Convert.intToString(Constants.upperCaseMax));
        Db.walletUpdateSettings(1, "numerary_min", Convert.intToString(Constants.numeraryMin));
        Db.walletUpdateSettings(1, "numerary_max", Convert.intToString(Constants.numeraryMax));
        Db.walletUpdateSettings(1, "special_char_min", Convert.intToString(Constants.specialCharMin));
        Db.walletUpdateSettings(1, "special_char_max", Convert.intToString(Constants.specialCharMax));
        Db.walletUpdateSettings(1, "secret_key_size", Convert.intToString(Constants.secretKeySize[0]));

        showPreferencesPhraseBlocksSpinner.setValue(Constants.phraseBlocks);
        showPreferencesIterationSpinner.setValue(Constants.secretIteration);
        showPreferencesWhitespaceCheckbox.setSelected(Constants.enableWhitespace);
        showPreferencesANSICheckbox.setSelected(Constants.enableAnsi);
        showPreferencesLengthMinimumMinSpinner.setValue(Constants.lengthMinimumMin);
        showPreferencesLengthMinimumMaxSpinner.setValue(Constants.lengthMinimumMax);
        showPreferencesLengthMaximumMinSpinner.setValue(Constants.lengthMaximumMin);
        showPreferencesLengthMaximumMaxSpinner.setValue(Constants.lengthMaximumMax);
        showPreferencesLowerCaseMinSpinner.setValue(Constants.lowerCaseMin);
        showPreferencesLowerCaseMaxSpinner.setValue(Constants.lowerCaseMax);
        showPreferencesUpperCaseMinSpinner.setValue(Constants.upperCaseMin);
        showPreferencesUpperCaseMaxSpinner.setValue(Constants.upperCaseMax);
        showPreferencesNumeraryMinSpinner.setValue(Constants.numeraryMin);
        showPreferencesNumeraryMaxSpinner.setValue(Constants.numeraryMax);
        showPreferencesSpecialCharMinSpinner.setValue(Constants.specialCharMin);
        showPreferencesSpecialCharMaxSpinner.setValue(Constants.specialCharMax);
        showPreferencesKeySizeComboBox.setSelectedIndex(0);

    }

}
