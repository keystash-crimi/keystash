package keystash.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.accountMenuItem;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.confirmNewAccountAttemptLabel;
import static keystash.KeyStashGUI.confirmNewAccountAttemptValueLabel;
import static keystash.KeyStashGUI.confirmNewAccountEntropyValue;
import static keystash.KeyStashGUI.confirmNewAccountFrame;
import static keystash.KeyStashGUI.confirmNewAccountGenerateNewPhraseButton;
import static keystash.KeyStashGUI.confirmNewAccountIncludeLettersField;
import static keystash.KeyStashGUI.confirmNewAccountLengthValue;
import static keystash.KeyStashGUI.confirmNewAccountOKButton;
import static keystash.KeyStashGUI.confirmNewAccountPhraseField;
import static keystash.KeyStashGUI.confirmNewAccountSearchAddressToggleButton;
import static keystash.KeyStashGUI.confirmNewAccountShowAddressRsLabel;
import static keystash.KeyStashGUI.confirmNewAccountShowPhraseCheckBox;
import static keystash.KeyStashGUI.globalPassword;
import static keystash.KeyStashGUI.newAccountFrame;
import static keystash.KeyStashGUI.newAccountLengthValue;
import static keystash.KeyStashGUI.newAccountPasswordConfirmField;
import static keystash.KeyStashGUI.newAccountPasswordField;
import static keystash.KeyStashGUI.newAccountShowPhraseCheckBox;
import static keystash.KeyStashGUI.secret;
import keystash.crypto.Crypto;
import keystash.crypto.PhraseGenerator;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.PasswordStrength;

public class NewAccount {

    public static void confirmPrimaryPassword() throws IOException {

        char[] checkPassword1 = newAccountPasswordField.getPassword();
        char[] checkPassword2 = newAccountPasswordConfirmField.getPassword();

        if (!Arrays.equals(checkPassword1, checkPassword2)) {
            Dialog.showDialog("Passwords do not match!", "Note");
        }
        else {
            if (checkPassword1.length < Constants.minPasswordChars) {
                Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");
            }
            else {

                globalPassword = checkPassword1;
                char[] myPhrase = null;
                try {
                    myPhrase = PhraseGenerator.generatePhrase();
                } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
                    Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                secret = myPhrase;
                byte[] secretPhrase = Crypto.getPublicKey(new String(secret));
                Long secretPhraseConvert = Convert.getId(secretPhrase);
                String addressRs = Convert.rsAccount(secretPhraseConvert);
                confirmNewAccountShowAddressRsLabel.setText(addressRs);
                confirmNewAccountPhraseField.setText(new String(myPhrase));
                int passLength = secret.length;
                String passLengthString = String.valueOf(passLength);
                double shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
                String shannonEntropyString = String.valueOf(shannonEntropy);
                confirmNewAccountEntropyValue.setText(shannonEntropyString);
                confirmNewAccountLengthValue.setText(passLengthString);
                newAccountFrame.setVisible(false);
                FeelAndLook.centerFrame(confirmNewAccountFrame, 520, 330);
                confirmNewAccountFrame.setVisible(true);
                confirmNewAccountShowPhraseCheckBox.setSelected(false);
                confirmNewAccountPhraseField.setEchoChar('*');
                confirmNewAccountPhraseField.putClientProperty("JPasswordField.cutCopyAllowed", true);
                confirmNewAccountAttemptValueLabel.setText("0");
                confirmNewAccountIncludeLettersField.setText("NXT-     ");
                confirmNewAccountAttemptLabel.setEnabled(false);
                confirmNewAccountAttemptValueLabel.setEnabled(false);
                confirmNewAccountGenerateNewPhraseButton.setEnabled(true);
                confirmNewAccountOKButton.setEnabled(true);
                confirmNewAccountIncludeLettersField.setEnabled(true);
                confirmNewAccountOKButton.requestFocus();
                confirmNewAccountFrame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent we) {
                        confirmNewAccountAttemptLabel.setEnabled(false);
                        confirmNewAccountAttemptValueLabel.setEnabled(false);
                        confirmNewAccountGenerateNewPhraseButton.setEnabled(true);
                        confirmNewAccountOKButton.setEnabled(true);
                        confirmNewAccountIncludeLettersField.setEnabled(true);
                        confirmNewAccountSearchAddressToggleButton.setSelected(false);
                        backgroundMenu = 0;
                        accountMenuItem.setEnabled(true);
                        MenuItem.enableMenu(true);
                    }
                });
            }
        }

    }

    public static void phraseLength() {

        char[] checkPassword = newAccountPasswordField.getPassword();
        newAccountLengthValue.setText(Convert.intToString(checkPassword.length));

    }

    public static void showPhrase() {

        boolean showPhrase = newAccountShowPhraseCheckBox.isSelected();

        if (showPhrase) {
            newAccountPasswordField.setEchoChar((char) 0);
            newAccountPasswordConfirmField.setEchoChar((char) 0);
        }
        else {
            newAccountPasswordField.setEchoChar('*');
            newAccountPasswordConfirmField.setEchoChar('*');
        }

    }

    public static void closeFrame() {

        newAccountFrame.setVisible(false);
        newAccountFrame.dispose();
        accountMenuItem.setEnabled(true);
        MenuItem.enableMenu(true);

    }

}
