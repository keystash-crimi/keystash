package keystash.gui;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.accountTable;
import static keystash.KeyStashGUI.backupMenuItem;
import static keystash.KeyStashGUI.encryptWalletFileMenuItem;
import static keystash.KeyStashGUI.fileMenu;
import static keystash.KeyStashGUI.isWalletEncrypted;
import static keystash.KeyStashGUI.settingsMenu;
import static keystash.KeyStashGUI.accountDetailsItem;
import static keystash.KeyStashGUI.copyAddressItem;
import static keystash.KeyStashGUI.generateTokenItem;
import static keystash.KeyStashGUI.accountMenuItem;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.enableApi;
import static keystash.KeyStashGUI.enableApiMenuItem;
import static keystash.KeyStashGUI.guiShowWarnings;
import static keystash.KeyStashGUI.importEntropyValue;
import static keystash.KeyStashGUI.importFrame;
import static keystash.KeyStashGUI.importLengthValue;
import static keystash.KeyStashGUI.importMenuItem;
import static keystash.KeyStashGUI.importPasswordConfirmField;
import static keystash.KeyStashGUI.importPasswordField;
import static keystash.KeyStashGUI.importShowPhraseCheckBox;
import static keystash.KeyStashGUI.importjProgressBar;
import static keystash.KeyStashGUI.newAccountEntropyValue;
import static keystash.KeyStashGUI.newAccountFrame;
import static keystash.KeyStashGUI.newAccountLengthValue;
import static keystash.KeyStashGUI.newAccountPasswordConfirmField;
import static keystash.KeyStashGUI.newAccountPasswordField;
import static keystash.KeyStashGUI.newAccountShowPhraseCheckBox;
import static keystash.KeyStashGUI.newAccountjProgressBar;
import static keystash.KeyStashGUI.offlineSigningItem;
import static keystash.KeyStashGUI.preferencesMenuItem;
import static keystash.KeyStashGUI.showPreferencesANSICheckbox;
import static keystash.KeyStashGUI.showPreferencesClearClipboardSpinner;
import static keystash.KeyStashGUI.showPreferencesFrame;
import static keystash.KeyStashGUI.showPreferencesIterationSpinner;
import static keystash.KeyStashGUI.showPreferencesKeySizeComboBox;
import static keystash.KeyStashGUI.showPreferencesLengthMaximumMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMaximumMinSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMinimumMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMinimumMinSpinner;
import static keystash.KeyStashGUI.showPreferencesLowerCaseMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLowerCaseMinSpinner;
import static keystash.KeyStashGUI.showPreferencesNumeraryMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesNumeraryMinSpinner;
import static keystash.KeyStashGUI.showPreferencesOKButton;
import static keystash.KeyStashGUI.showPreferencesPhraseBlocksSpinner;
import static keystash.KeyStashGUI.showPreferencesRandomGenerationCheckbox;
import static keystash.KeyStashGUI.showPreferencesResetSettingsButton;
import static keystash.KeyStashGUI.showPreferencesSpecialCharMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesSpecialCharMinSpinner;
import static keystash.KeyStashGUI.showPreferencesUpperCaseMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesUpperCaseMinSpinner;
import static keystash.KeyStashGUI.showPreferencesWarningsCheckbox;
import static keystash.KeyStashGUI.showPreferencesWhitespaceCheckbox;
import static keystash.KeyStashGUI.showQRCodeItem;
import static keystash.KeyStashGUI.startPageMenuItem;
import keystash.crypto.KeyLength;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import org.apache.commons.lang.SystemUtils;

public class MenuItem {

    public static void enableMenu(boolean isEnabled) {

        if (isWalletEncrypted) {
            
            encryptWalletFileMenuItem.setEnabled(false);
            
        }
        else {
            
            encryptWalletFileMenuItem.setEnabled(isEnabled);
            
        }

        backupMenuItem.setEnabled(isEnabled);
        startPageMenuItem.setEnabled(isEnabled);
        accountDetailsItem.setEnabled(isEnabled);
        generateTokenItem.setEnabled(isEnabled);
        copyAddressItem.setEnabled(isEnabled);
        offlineSigningItem.setEnabled(isEnabled);
        showQRCodeItem.setEnabled(isEnabled);
        accountTable.setEnabled(isEnabled);
        accountMenuItem.setEnabled(isEnabled);
        importMenuItem.setEnabled(isEnabled);
        preferencesMenuItem.setEnabled(isEnabled);

    }

    public static void encryptedEnableMenu(boolean isEnabled) {

        fileMenu.setEnabled(isEnabled);
        startPageMenuItem.setEnabled(isEnabled);
        settingsMenu.setEnabled(isEnabled);
        accountDetailsItem.setEnabled(isEnabled);
        generateTokenItem.setEnabled(isEnabled);
        offlineSigningItem.setEnabled(isEnabled);
        showQRCodeItem.setEnabled(isEnabled);
        copyAddressItem.setEnabled(isEnabled);
        accountTable.setEnabled(isEnabled);

    }

    public static void enableApi()
            throws InterruptedException, IOException {

        boolean checkBox = enableApiMenuItem.isSelected();

        if (checkBox) {

            enableApi = true;
            backgroundMenu = 2;
            Thread.sleep(300);
            Icon.initializeIcons(true);

        }
        else {

            enableApi = false;
            backgroundMenu = 3;
            Thread.sleep(300);
            Icon.initializeIcons(false);

        }

    }

    public static void showNewAccount() {

        FeelAndLook.centerFrame(newAccountFrame, 520, 330);
        newAccountFrame.setVisible(true);
        newAccountShowPhraseCheckBox.setSelected(false);
        accountMenuItem.setEnabled(false);
        newAccountPasswordField.setEchoChar('*');
        newAccountPasswordConfirmField.setEchoChar('*');
        newAccountPasswordField.setText("");
        newAccountPasswordConfirmField.setText("");
        newAccountjProgressBar.setValue(0);
        newAccountEntropyValue.setText("0.0");
        newAccountLengthValue.setText("0");
        newAccountPasswordField.requestFocus();
        newAccountjProgressBar.setString("");
        newAccountPasswordField.putClientProperty("JPasswordField.cutCopyAllowed", true);
        newAccountPasswordConfirmField.putClientProperty("JPasswordField.cutCopyAllowed", true);
        MenuItem.enableMenu(false);
        newAccountFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                MenuItem.enableMenu(true);
                accountMenuItem.setEnabled(true);
            }
        });
        guiShowWarnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));
        if (guiShowWarnings) {
            
            Dialog.showDialog("You will lose all access if you forget the primary password!", "Note");
            
        }

    }

    public static void showImport() {

        FeelAndLook.centerFrame(importFrame, 520, 330);
        importFrame.setVisible(true);
        importShowPhraseCheckBox.setSelected(false);
        importMenuItem.setEnabled(false);
        importPasswordField.setEchoChar('*');
        importPasswordConfirmField.setEchoChar('*');
        importPasswordField.setText("");
        importPasswordConfirmField.setText("");
        importjProgressBar.setValue(0);
        importEntropyValue.setText("0.0");
        importLengthValue.setText("0");
        importPasswordField.requestFocus();
        importjProgressBar.setString("");
        importPasswordField.putClientProperty("JPasswordField.cutCopyAllowed", true);
        importPasswordConfirmField.putClientProperty("JPasswordField.cutCopyAllowed", true);
        MenuItem.enableMenu(false);
        importFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                MenuItem.enableMenu(true);
                importMenuItem.setEnabled(true);
            }
        });
        guiShowWarnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));
        if (guiShowWarnings) {
            
            Dialog.showDialog("You will lose all access if you forget the primary password!", "Note");
            
        }

    }

    public static void showPreferences() {

        preferencesMenuItem.setEnabled(false);
        try {

            int getKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
            int keySizeComboBox = 0;
            boolean isMaxKeyLength = false;
            try {
                isMaxKeyLength = KeyLength.isUnlimitedKeyStrength();
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (getKeySize == 128) {
                keySizeComboBox = 0;
            }
            else if (getKeySize == 256 && isMaxKeyLength) {
                keySizeComboBox = 1;
            }

            if (!isMaxKeyLength) {
                Db.walletUpdateSettings(1, "secret_key_size", Convert.intToString(Constants.secretKeySize[0]));
                showPreferencesKeySizeComboBox.removeItemAt(1);
            }

            FeelAndLook.centerFrame(showPreferencesFrame, 520, 400);
            
            if (SystemUtils.IS_OS_UNIX) {
                
                showPreferencesFrame.setPreferredSize(new Dimension(520, 370));
                showPreferencesFrame.setMinimumSize(new Dimension(520, 370));
                showPreferencesFrame.setMaximumSize(new Dimension(520, 370));
                showPreferencesFrame.pack();
                
            }
            showPreferencesFrame.setVisible(true);
            showPreferencesOKButton.requestFocus();

            MenuItem.enableMenu(false);
            showPreferencesFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we) {
                    MenuItem.enableMenu(true);
                }
            });
            showPreferencesWarningsCheckbox.setSelected(Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1)));
            showPreferencesClearClipboardSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("clear_clipboard", "keystash_settings", "settings_id", 1)));
            showPreferencesRandomGenerationCheckbox.setSelected(Convert.stringToBoolean(Db.selectFromSettings("expert_generation", "keystash_settings", "settings_id", 1)));
            if (Convert.stringToBoolean(Db.selectFromSettings("expert_generation", "keystash_settings", "settings_id", 1))) {
                
                showPreferencesPhraseBlocksSpinner.setEnabled(true);
                showPreferencesIterationSpinner.setEnabled(true);
                showPreferencesWhitespaceCheckbox.setEnabled(true);
                showPreferencesANSICheckbox.setEnabled(true);
                showPreferencesLengthMinimumMinSpinner.setEnabled(true);
                showPreferencesLengthMinimumMaxSpinner.setEnabled(true);
                showPreferencesLengthMaximumMinSpinner.setEnabled(true);
                showPreferencesLengthMaximumMaxSpinner.setEnabled(true);
                showPreferencesLowerCaseMinSpinner.setEnabled(true);
                showPreferencesLowerCaseMaxSpinner.setEnabled(true);
                showPreferencesUpperCaseMinSpinner.setEnabled(true);
                showPreferencesUpperCaseMaxSpinner.setEnabled(true);
                showPreferencesNumeraryMinSpinner.setEnabled(true);
                showPreferencesNumeraryMaxSpinner.setEnabled(true);
                showPreferencesSpecialCharMinSpinner.setEnabled(true);
                showPreferencesSpecialCharMaxSpinner.setEnabled(true);
                showPreferencesResetSettingsButton.setEnabled(true);
                showPreferencesKeySizeComboBox.setEnabled(true);
                
            }
            else {
                
                showPreferencesPhraseBlocksSpinner.setEnabled(false);
                showPreferencesWhitespaceCheckbox.setEnabled(false);
                showPreferencesANSICheckbox.setEnabled(false);
                showPreferencesIterationSpinner.setEnabled(false);
                showPreferencesLengthMinimumMinSpinner.setEnabled(false);
                showPreferencesLengthMinimumMaxSpinner.setEnabled(false);
                showPreferencesLengthMaximumMinSpinner.setEnabled(false);
                showPreferencesLengthMaximumMaxSpinner.setEnabled(false);
                showPreferencesLowerCaseMinSpinner.setEnabled(false);
                showPreferencesLowerCaseMaxSpinner.setEnabled(false);
                showPreferencesUpperCaseMinSpinner.setEnabled(false);
                showPreferencesUpperCaseMaxSpinner.setEnabled(false);
                showPreferencesNumeraryMinSpinner.setEnabled(false);
                showPreferencesNumeraryMaxSpinner.setEnabled(false);
                showPreferencesSpecialCharMinSpinner.setEnabled(false);
                showPreferencesSpecialCharMaxSpinner.setEnabled(false);
                showPreferencesResetSettingsButton.setEnabled(false);
                showPreferencesKeySizeComboBox.setEnabled(false);
                
            }

            showPreferencesWhitespaceCheckbox.setSelected(Convert.stringToBoolean(Db.selectFromSettings("enable_whitespace", "keystash_settings", "settings_id", 1)));
            showPreferencesANSICheckbox.setSelected(Convert.stringToBoolean(Db.selectFromSettings("enable_ansi", "keystash_settings", "settings_id", 1)));
            showPreferencesPhraseBlocksSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("phrase_blocks", "keystash_settings", "settings_id", 1)));
            showPreferencesIterationSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMinimumMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_minimum_min", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMinimumMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_minimum_max", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMaximumMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_maximum_min", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMaximumMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_maximum_max", "keystash_settings", "settings_id", 1)));
            showPreferencesLowerCaseMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("lower_case_min", "keystash_settings", "settings_id", 1)));
            showPreferencesLowerCaseMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("lower_case_max", "keystash_settings", "settings_id", 1)));
            showPreferencesUpperCaseMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("upper_case_min", "keystash_settings", "settings_id", 1)));
            showPreferencesUpperCaseMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("upper_case_max", "keystash_settings", "settings_id", 1)));
            showPreferencesNumeraryMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("numerary_min", "keystash_settings", "settings_id", 1)));
            showPreferencesNumeraryMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("numerary_max", "keystash_settings", "settings_id", 1)));
            showPreferencesSpecialCharMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("special_char_min", "keystash_settings", "settings_id", 1)));
            showPreferencesSpecialCharMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("special_char_max", "keystash_settings", "settings_id", 1)));
            showPreferencesKeySizeComboBox.setSelectedIndex(keySizeComboBox);

        } catch (Exception e) {
            System.err.println("Wallet settings already in use or corrupted.");
        }

    }

}