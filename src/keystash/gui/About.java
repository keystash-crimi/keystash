package keystash.gui;

import static keystash.KeyStashGUI.aboutFrame;
import static keystash.KeyStashGUI.aboutOkButton;
import static keystash.KeyStashGUI.aboutScrollPane;

public class About {

    public static void openFrame() {

        FeelAndLook.centerFrame(aboutFrame, 520, 410);      
        aboutOkButton.requestFocus();
        aboutScrollPane.putClientProperty("JTable.cutCopyAllowed", true);
        aboutFrame.setVisible(true);

    }

    public static void closeFrame() {

        aboutFrame.setVisible(false);
        aboutFrame.dispose();

    }

}
