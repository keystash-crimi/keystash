package keystash.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static keystash.KeyStashGUI.enableApi;
import static keystash.KeyStashGUI.iconPaneAccountjLabel;
import static keystash.KeyStashGUI.iconPaneApijLabel;
import static keystash.KeyStashGUI.iconPaneLockjLabel;
import static keystash.KeyStashGUI.isWalletEncrypted;
import keystash.util.Constants;
import keystash.util.Db;
import org.apache.commons.lang.SystemUtils;

public class Icon {

    public static void initializeIcons(boolean apiFail)
            throws IOException {

        BufferedImage bufferedImage;
        String path;
        JLabel imageLabel;
        int countRows = 0;
        try {
            countRows = Db.countRows("keystash_wallet");
        } catch (Exception e) {
        }

        if (SystemUtils.IS_OS_LINUX) {

            path = Constants.iconAccountsPathUnix;

        }
        else {

            path = Constants.iconAccountsPath;

        }

        imageLabel = iconPaneAccountjLabel;
        ImageIcon icon;
        File file = new File(path);
        bufferedImage = ImageIO.read(file);
        icon = new ImageIcon(bufferedImage);
        icon.getImage().flush();
        imageLabel.setIcon(icon);

        iconPaneAccountjLabel.setToolTipText("Accounts: " + countRows);

        if (isWalletEncrypted) {

            iconPaneAccountjLabel.setToolTipText("Accounts: " + countRows);

            iconPaneLockjLabel.setToolTipText("Wallet File Encrypted");

            if (SystemUtils.IS_OS_LINUX) {

                path = Constants.iconLockEncryptedPathUnix;

            }
            else {

                path = Constants.iconLockEncryptedPath;

            }

        }
        else {

            iconPaneLockjLabel.setToolTipText("Wallet File Not Encrypted");

            if (SystemUtils.IS_OS_LINUX) {

                path = Constants.iconLockDecryptedPathUnix;

            }
            else {

                path = Constants.iconLockDecryptedPath;

            }

        }

        imageLabel = iconPaneLockjLabel;
        file = new File(path);
        bufferedImage = ImageIO.read(file);
        icon = new ImageIcon(bufferedImage);
        icon.getImage().flush();
        imageLabel.setIcon(icon);

        if (enableApi && !apiFail) {

            iconPaneApijLabel.setToolTipText("Api Enabled");

            if (SystemUtils.IS_OS_LINUX) {

                path = Constants.iconApiOnPathUnix;

            }
            else {

                path = Constants.iconApiOnPath;

            }

        }

        else if (apiFail) {

            iconPaneApijLabel.setToolTipText("Api Failed");

            if (SystemUtils.IS_OS_LINUX) {

                path = Constants.iconApiFailPathUnix;

            }
            else {

                path = Constants.iconApiFailPath;

            }

        }

        else {

            iconPaneApijLabel.setToolTipText("Api Disabled");

            if (SystemUtils.IS_OS_LINUX) {

                path = Constants.iconApiOffPathUnix;

            }
            else {

                path = Constants.iconApiOffPath;

            }

        }

        imageLabel = iconPaneApijLabel;
        file = new File(path);
        bufferedImage = ImageIO.read(file);
        icon = new ImageIcon(bufferedImage);

        icon.getImage().flush();
        imageLabel.setIcon(icon);

    }

}
