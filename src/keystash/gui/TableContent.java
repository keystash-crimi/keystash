package keystash.gui;

import java.security.NoSuchAlgorithmException;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import static keystash.KeyStashGUI.aboutTable;
import static keystash.KeyStashGUI.accountTable;
import static keystash.KeyStashGUI.iconPaneAccountjLabel;
import keystash.util.Constants;
import keystash.util.Db;
import static keystash.crypto.KeyLength.isUnlimitedKeyStrength;
import org.apache.commons.lang.SystemUtils;

public class TableContent {

    public static void getTable() {

        int countRows = Db.countRows("keystash_wallet") + 1;

        String[] getAccountid = new String[countRows];
        String[] getAddressRs = new String[countRows];
        String[] getLabel = new String[countRows];
        String[] getTimestamp = new String[countRows];

        for (int i = 1; i < countRows; i++) {
            getAccountid[i] = Db.selectFromWallet("account_id", "keystash_wallet", "account_id", +i);
            getAddressRs[i] = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +i);
            getLabel[i] = Db.selectFromWallet("label", "keystash_wallet", "account_id", +i);
            getTimestamp[i] = Db.selectFromWallet("created_timestamp", "keystash_wallet", "account_id", +i);
            Object[] row = {getAccountid[i] + ".", getAddressRs[i], getLabel[i], getTimestamp[i]};
            DefaultTableModel model = (DefaultTableModel) accountTable.getModel();
            model.addRow(row);
        }

    }

    public static void insertAboutComponents()
            throws NoSuchAlgorithmException {

        DefaultTableModel model = (DefaultTableModel) aboutTable.getModel();
        Object[] rowVersion = {"KeyStash", Constants.keyStashVersion};
        model.addRow(rowVersion);

        boolean isMaxKeyLength = isUnlimitedKeyStrength();
        Object[] rowKeySize = {"Unlimited Key Length(\"AES\")", isMaxKeyLength};
        model.addRow(rowKeySize);

        Object[] rowApi = {"Api URL", Constants.apiAddress};
        model.addRow(rowApi);

        if (SystemUtils.IS_OS_LINUX) {
            Object[] rowHomeFolder = {"Wallet File Home Folder", Constants.walletPathOnlyUnix};
            model.addRow(rowHomeFolder);
        }
        else {
            Object[] rowHomeFolder = {"Wallet File Home Folder", Constants.walletPathOnly};
            model.addRow(rowHomeFolder);
        }

    }

    public static void deleteGuiTables() {

        DefaultTableModel model = (DefaultTableModel) accountTable.getModel();
        int rows = model.getRowCount();

        for (int i = rows - 1; i >= 0; i--) {
            model.removeRow(i);
        }

    }

    public static void updateGuiTables() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {

                TableContent.deleteGuiTables();
                TableContent.getTable();
                int countRows = Db.countRows("keystash_wallet");
                iconPaneAccountjLabel.setToolTipText("Accounts: " + countRows);

            }
        });
    }

    public static boolean isAccountInTable(String addressRS) {

        boolean result = false;
        int countRows = Db.countRows("keystash_wallet") + 1;
        String[] getAddressRS = new String[countRows];

        for (int i = 1; i < countRows; i++) {

            getAddressRS[i] = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +i);

            if (getAddressRS[i].equals(addressRS)) {

                result = true;

            }

        }

        return result;

    }

}
