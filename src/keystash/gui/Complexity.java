package keystash.gui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import static keystash.KeyStashGUI.confirmImportEntropyValue;
import static keystash.KeyStashGUI.confirmImportLengthValue;
import static keystash.KeyStashGUI.confirmImportPhraseField;
import static keystash.KeyStashGUI.confirmImportShowAddressRsLabel;
import static keystash.KeyStashGUI.confirmNewAccountEntropyValue;
import static keystash.KeyStashGUI.confirmNewAccountLengthValue;
import static keystash.KeyStashGUI.confirmNewAccountPhraseField;
import static keystash.KeyStashGUI.confirmNewAccountShowAddressRsLabel;
import static keystash.KeyStashGUI.encryptDatabaseEntropyValue;
import static keystash.KeyStashGUI.encryptDatabaseLengthValue;
import static keystash.KeyStashGUI.encryptDatabasePasswordField;
import static keystash.KeyStashGUI.encryptDatabasejProgressBar;
import static keystash.KeyStashGUI.importEntropyValue;
import static keystash.KeyStashGUI.importLengthValue;
import static keystash.KeyStashGUI.importPasswordField;
import static keystash.KeyStashGUI.importjProgressBar;
import static keystash.KeyStashGUI.newAccountEntropyValue;
import static keystash.KeyStashGUI.newAccountLengthValue;
import static keystash.KeyStashGUI.newAccountPasswordField;
import static keystash.KeyStashGUI.newAccountjProgressBar;
import keystash.crypto.Crypto;
import keystash.util.Convert;
import keystash.util.Memory;
import keystash.util.PasswordStrength;

public class Complexity {

    public static void checkStrength(String type) {

        JPasswordField passwordField = null;
        JLabel lengthValue = null;
        JLabel entropyValue = null;
        JProgressBar progressBar = null;

        if ("New".equals(type)) {

            passwordField = newAccountPasswordField;
            lengthValue = newAccountLengthValue;
            entropyValue = newAccountEntropyValue;
            progressBar = newAccountjProgressBar;

        }
        else if ("Import".equals(type)) {

            passwordField = importPasswordField;
            lengthValue = importLengthValue;
            entropyValue = importEntropyValue;
            progressBar = importjProgressBar;

        }
        else if ("Encrypt".equals(type)) {

            passwordField = encryptDatabasePasswordField;
            lengthValue = encryptDatabaseLengthValue;
            entropyValue = encryptDatabaseEntropyValue;
            progressBar = encryptDatabasejProgressBar;

        }

        progressBar.setStringPainted(true);
        char[] checkPassword = passwordField.getPassword();
        lengthValue.setText(Convert.intToString(checkPassword.length));
        double shannonEntropy = PasswordStrength.getShannonEntropy(new String(checkPassword));
        entropyValue.setText(String.valueOf(shannonEntropy));
        int showPasswordStrenght = PasswordStrength.checkPasswordStrength(new String(checkPassword));
        long shannonLong = (long) shannonEntropy;

        if (showPasswordStrenght == 0 && shannonLong <= 1) {

            progressBar.setValue(0);
            progressBar.setString("");

        }

        if (showPasswordStrenght >= 25 && shannonLong == 2) {

            progressBar.setValue(25);
            progressBar.setString("Weak");
            progressBar.setForeground(Color.red.darker());

        }

        if (showPasswordStrenght >= 50 && shannonLong == 3) {

            progressBar.setValue(50);
            progressBar.setString("Good");
            progressBar.setForeground(Color.blue.brighter());

        }

        if (showPasswordStrenght >= 75 && shannonLong == 4) {

            progressBar.setValue(75);
            progressBar.setString("Very Good");
            progressBar.setForeground(Color.cyan.brighter());

        }

        if (showPasswordStrenght == 100 && shannonLong == 5) {

            progressBar.setValue(100);
            progressBar.setString("Strong");
            progressBar.setForeground(Color.white);

        }

        if (checkPassword.length <= 5) {

            progressBar.setValue(0);
            progressBar.setString("Very Weak");
            progressBar.setForeground(Color.red);

        }

        if (checkPassword.length == 0) {

            progressBar.setValue(0);
            progressBar.setString("");

        }

        Memory.clearPasswordMemory(checkPassword);

    }

    public static void checkStrengthLive(String type) {

        JPasswordField passwordField = null;
        JLabel lengthValue = null;
        JLabel entropyValue = null;
        JLabel strengthLabel = null;

        if ("Confirm New".equals(type)) {

            passwordField = confirmNewAccountPhraseField;
            lengthValue = confirmNewAccountLengthValue;
            entropyValue = confirmNewAccountEntropyValue;
            strengthLabel = confirmNewAccountShowAddressRsLabel;

        }
        else if ("Confirm Import".equals(type)) {

            passwordField = confirmImportPhraseField;
            lengthValue = confirmImportLengthValue;
            entropyValue = confirmImportEntropyValue;
            strengthLabel = confirmImportShowAddressRsLabel;

        }

        char[] checkPassword = passwordField.getPassword();
        lengthValue.setText(Convert.intToString(checkPassword.length));
        double shannonEntropy = PasswordStrength.getShannonEntropy(new String(checkPassword));
        entropyValue.setText(String.valueOf(shannonEntropy));
        byte[] secretPhrase = Crypto.getPublicKey(new String(checkPassword));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        if (checkPassword.length == 0) {
            strengthLabel.setText("");
        }
        else {
            strengthLabel.setText(addressRs);
        }

        Memory.clearPasswordMemory(checkPassword);

    }

}
