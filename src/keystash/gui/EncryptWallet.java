package keystash.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.accountMenuItem;
import static keystash.KeyStashGUI.encryptDatabaseEntropyValue;
import static keystash.KeyStashGUI.encryptDatabaseFrame;
import static keystash.KeyStashGUI.encryptDatabaseLengthValue;
import static keystash.KeyStashGUI.encryptWalletFileMenuItem;
import static keystash.KeyStashGUI.encryptDatabasePasswordConfirmField;
import static keystash.KeyStashGUI.encryptDatabasePasswordField;
import static keystash.KeyStashGUI.encryptDatabaseShowPhraseCheckBox;
import static keystash.KeyStashGUI.encryptDatabasejProgressBar;
import static keystash.KeyStashGUI.guiShowWarnings;
import static keystash.KeyStashGUI.isWalletEncrypted;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Memory;
import org.h2.tools.ChangeFileEncryption;

public class EncryptWallet {

    public static void openFrame() {

        FeelAndLook.centerFrame(encryptDatabaseFrame, 520, 330);
        encryptDatabaseFrame.setVisible(true);
        encryptDatabaseShowPhraseCheckBox.setSelected(false);
        encryptDatabasePasswordField.requestFocus();
        encryptDatabasePasswordField.setEchoChar('*');
        encryptDatabasePasswordConfirmField.setEchoChar('*');
        encryptDatabasePasswordField.setText("");
        encryptDatabasePasswordConfirmField.setText("");
        encryptDatabasejProgressBar.setValue(0);
        encryptDatabaseEntropyValue.setText("0.0");
        encryptDatabaseLengthValue.setText("0");
        encryptDatabasejProgressBar.setString("");
        encryptDatabasePasswordField.putClientProperty("JPasswordField.cutCopyAllowed", true);
        encryptDatabasePasswordConfirmField.putClientProperty("JPasswordField.cutCopyAllowed", true);
        accountMenuItem.setEnabled(false);
        MenuItem.enableMenu(false);

        encryptDatabaseFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                accountMenuItem.setEnabled(true);
                MenuItem.enableMenu(true);
            }
        });

        guiShowWarnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));

        if (guiShowWarnings) {
            Dialog.showDialog("You will lose all access if you forget the master password!", "Note");
        }

    }

    public static void closeFrame() {

        encryptDatabaseFrame.setVisible(false);
        encryptDatabaseFrame.dispose();
        accountMenuItem.setEnabled(true);
        MenuItem.enableMenu(true);

    }

    public static void doEncrypt() throws IOException {

        char[] checkPassword1 = encryptDatabasePasswordField.getPassword();
        char[] checkPassword2 = encryptDatabasePasswordConfirmField.getPassword();

        char c = '¡';

        if (!Arrays.equals(checkPassword1, checkPassword2)) {
            Dialog.showDialog("Passwords do not match!", "Note");
        }
        else if (checkPassword1.length < Constants.minPasswordChars) {
            Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");
        }
        else if (new String(checkPassword1).contains(Character.toString(c))) {
            Dialog.showDialog("No ¡ character allowed in password.", "Note");
        }
        else {
            try {

                Db.walletClose();
                ChangeFileEncryption.main("-dir", Constants.walletPathOnly, "-db", "wallet", "-cipher", "AES", "-encrypt", new String(checkPassword1).replace(' ', c));
                isWalletEncrypted = true;
                encryptDatabaseFrame.setVisible(false);
                encryptWalletFileMenuItem.setEnabled(false);
                Db.connectToEncryptedWallet(String.valueOf(checkPassword1).replace(' ', c));
                Icon.initializeIcons(false);
                Dialog.showDialog("Wallet file encryption successful.", "Note");
            } catch (SQLException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
                Db.connectToWallet();
                encryptDatabaseFrame.setVisible(false);
                Dialog.showDialog("Wallet file encryption failed.", "Note");
            }
            accountMenuItem.setEnabled(true);
            MenuItem.enableMenu(true);
            encryptWalletFileMenuItem.setEnabled(false);
            Memory.clearPasswordMemory(checkPassword1);
            Memory.clearPasswordMemory(checkPassword2);
        }

    }

    public static void passwordLength() {

        char[] checkPassword = encryptDatabasePasswordField.getPassword();
        encryptDatabaseLengthValue.setText(Convert.intToString(checkPassword.length));

    }

    public static void showPhrase() {

        boolean showPhrase = encryptDatabaseShowPhraseCheckBox.isSelected();

        if (showPhrase) {
            encryptDatabasePasswordField.setEchoChar((char) 0);
            encryptDatabasePasswordConfirmField.setEchoChar((char) 0);
        }
        else {
            encryptDatabasePasswordField.setEchoChar('*');
            encryptDatabasePasswordConfirmField.setEchoChar('*');
        }

    }

}
