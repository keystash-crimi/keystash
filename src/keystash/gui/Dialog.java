package keystash.gui;

import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Dialog {

    public static void showDialog(String text, String title) {

        JOptionPane op = new JOptionPane(text, JOptionPane.INFORMATION_MESSAGE);
        JDialog dialog = op.createDialog(title);
        Image icon = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB_PRE);
        dialog.setIconImage(icon);
        dialog.setAlwaysOnTop(true);
        dialog.setModal(true);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setFocusableWindowState(true);
        dialog.setVisible(true);

    }

}