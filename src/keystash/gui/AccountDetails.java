package keystash.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.Timer;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.accountDetailsCopyButton2;
import static keystash.KeyStashGUI.accountDetailsFrame;
import static keystash.KeyStashGUI.accountDetailsLabelField;
import static keystash.KeyStashGUI.accountDetailsOKButton;
import static keystash.KeyStashGUI.accountDetailsPhraseField;
import static keystash.KeyStashGUI.accountDetailsPublicKeyField;
import static keystash.KeyStashGUI.accountDetailsShowPhraseCheckBox;
import static keystash.KeyStashGUI.accountDetailsjProgressBar;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.enableApiMenuItem;
import static keystash.KeyStashGUI.enterPrimaryPasswordAccountDetailsFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordAccountDetailsPasswordField;
import static keystash.KeyStashGUI.enterPrimaryPasswordAccountDetailsShowPhraseCheckBox;
import static keystash.KeyStashGUI.lockClearClipboardButton;
import static keystash.KeyStashGUI.row;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.http.ApiRefresh;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Memory;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.SystemUtils;

public class AccountDetails {

    public static void showAccountDetails() throws IOException {

        String getAccountRs = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +row + 1);
        String getSecret = Db.selectFromWallet("secret", "keystash_wallet", "account_id", +row + 1);
        String getSalt = Db.selectFromWallet("randomSalt", "keystash_wallet", "account_id", +row + 1);
        String getIV = Db.selectFromWallet("randomIV", "keystash_wallet", "account_id", +row + 1);
        String getLabel = Db.selectFromWallet("label", "keystash_wallet", "account_id", +row + 1);
        int getIteration = Convert.stringToInt(Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", +row + 1));
        int getKeySize = Convert.stringToInt(Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", +row + 1));
        String getEncryptionStandard = Db.selectFromWallet("advanced_encryption_standard", "keystash_wallet", "account_id", +row + 1);
        char[] checkPassword = enterPrimaryPasswordAccountDetailsPasswordField.getPassword();

        if (checkPassword.length < Constants.minPasswordChars) {

            Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");

        }
        else {

            char[] decipherSecret = null;
            try {
                
                decipherSecret = Aes.decrypt(getSecret, new String(checkPassword), getSalt, getIV, getIteration, getKeySize, getEncryptionStandard);
                
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | UnsupportedEncodingException | DecoderException ex) {
                
            }

            if (decipherSecret == null) {

                Dialog.showDialog("Password does not match with encryption!", "Note");

            }
            else {

                byte[] secretPhrase = Crypto.getPublicKey(new String(decipherSecret));
                Long secretPhraseConvert = Convert.getId(secretPhrase);
                String publicKey = Convert.toHexString(secretPhrase);
                String addressRs = Convert.rsAccount(secretPhraseConvert);

                if (getAccountRs.equals(addressRs)) {

                    enterPrimaryPasswordAccountDetailsFrame.setVisible(false);
                    FeelAndLook.centerFrame(accountDetailsFrame, 520, 380);

                    if (SystemUtils.IS_OS_UNIX) {

                        accountDetailsFrame.setPreferredSize(new Dimension(520, 270));
                        accountDetailsFrame.setMinimumSize(new Dimension(520, 270));
                        accountDetailsFrame.setMaximumSize(new Dimension(520, 270));
                        accountDetailsFrame.pack();

                    }

                    accountDetailsFrame.setVisible(true);
                    accountDetailsFrame.setTitle("Account Details: " + addressRs);
                    accountDetailsShowPhraseCheckBox.setSelected(false);
                    accountDetailsPhraseField.setText("");
                    accountDetailsOKButton.requestFocus();
                    accountDetailsFrame.addWindowListener(new WindowAdapter() {

                        @Override
                        public void windowClosing(WindowEvent we) {

                            StringSelection stringSelection = new StringSelection("");
                            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                            clpbrd.setContents(stringSelection, null);
                            accountDetailsCopyButton2.setEnabled(true);
                            accountDetailsjProgressBar.setString("");
                            accountDetailsjProgressBar.setValue(0);
                            lockClearClipboardButton = false;
                            backgroundMenu = 0;
                            MenuItem.enableMenu(true);

                        }
                    });

                    Timer timer;
                    timer = new Timer(Constants.closeWindows * 1000, new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {

                            accountDetailsFrame.setVisible(false);
                            accountDetailsFrame.dispose();
                            MenuItem.enableMenu(true);

                        }
                    });
                    timer.setRepeats(false);
                    timer.start();
                    accountDetailsjProgressBar.setValue(0);
                    accountDetailsShowPhraseCheckBox.setSelected(false);
                    accountDetailsPhraseField.setEchoChar('*');
                    accountDetailsPhraseField.setText(new String(decipherSecret));
                    accountDetailsLabelField.setText(getLabel);
                    accountDetailsPhraseField.putClientProperty("JPasswordField.cutCopyAllowed", true);
                    accountDetailsPublicKeyField.setText(publicKey);
                    accountDetailsPublicKeyField.putClientProperty("JPasswordField.cutCopyAllowed", true);

                }
                if (!getAccountRs.equals(addressRs)) {

                    Dialog.showDialog("Password does not match with encryption!", "Note");

                }
            }
            Memory.clearPasswordMemory(checkPassword);
        }
    }

    public static void closeFrameAndSave() {

        if (enableApiMenuItem.isSelected()) {

            backgroundMenu = 3;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        String labelValue = accountDetailsLabelField.getText().replaceAll("\\s+$", "");
        Db.walletUpdateAccount(row + 1, "label", labelValue);

        TableContent.deleteGuiTables();
        TableContent.getTable();
        ApiRefresh.refreshApi();
        StringSelection stringSelection = new StringSelection("");
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);

        accountDetailsCopyButton2.setEnabled(true);
        accountDetailsjProgressBar.setString("");
        accountDetailsjProgressBar.setValue(0);
        lockClearClipboardButton = false;
        backgroundMenu = 0;
        accountDetailsFrame.setVisible(false);
        accountDetailsFrame.dispose();
        MenuItem.enableMenu(true);

        if (enableApiMenuItem.isSelected()) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            backgroundMenu = 2;

        }

    }

    public static void primayPasswordAccountDetailCheckBox() {

        boolean showPhrase = enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.isSelected();

        if (showPhrase) {

            enterPrimaryPasswordAccountDetailsPasswordField.setEchoChar((char) 0);

        }
        else {

            enterPrimaryPasswordAccountDetailsPasswordField.setEchoChar('*');

        }

    }

    public static void showPhrase() {

        boolean showPhrase = accountDetailsShowPhraseCheckBox.isSelected();

        if (showPhrase) {

            accountDetailsPhraseField.setEchoChar((char) 0);

        }
        else {

            accountDetailsPhraseField.setEchoChar('*');

        }

    }

    public static void copyPublicKey() {

        StringSelection stringSelection = new StringSelection(accountDetailsPublicKeyField.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);

    }

    public static void copyPassPhrase() {

        if (!lockClearClipboardButton) {

            lockClearClipboardButton = true;
            accountDetailsjProgressBar.setValue(100);
            accountDetailsCopyButton2.setEnabled(false);
            char[] copyValue = accountDetailsPhraseField.getPassword();
            String passwordString = new String(copyValue);
            StringSelection stringSelection = new StringSelection(passwordString);
            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
            clpbrd.setContents(stringSelection, null);
            backgroundMenu = 1;
            accountDetailsjProgressBar.setString("Clear Private Phrase From Clipboard");

        }

    }

    public static void closeFrame() {

        enterPrimaryPasswordAccountDetailsFrame.setVisible(false);
        enterPrimaryPasswordAccountDetailsFrame.dispose();
        MenuItem.enableMenu(true);

    }

}
