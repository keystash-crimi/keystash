package keystash.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.Timer;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.enterPrimaryPasswordGenerateTokenFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordGenerateTokenPasswordField;
import static keystash.KeyStashGUI.enterPrimaryPasswordGenerateTokenShowPhraseCheckBox;
import static keystash.KeyStashGUI.generateTokenFrame;
import static keystash.KeyStashGUI.generateTokenGenerateTokenButton;
import static keystash.KeyStashGUI.generateTokenTokenDataField;
import static keystash.KeyStashGUI.generateTokenTokenKeyField;
import static keystash.KeyStashGUI.row;
import static keystash.KeyStashGUI.secretToken;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Memory;
import keystash.util.Token;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.SystemUtils;

public class GenerateToken {

    public static void showGenerateToken() throws IOException {

        String getAccountRs = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +row + 1);
        String getSecret = Db.selectFromWallet("secret", "keystash_wallet", "account_id", +row + 1);
        String getSalt = Db.selectFromWallet("randomSalt", "keystash_wallet", "account_id", +row + 1);
        String getIV = Db.selectFromWallet("randomIV", "keystash_wallet", "account_id", +row + 1);
        int getIteration = Convert.stringToInt(Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", +row + 1));
        int getKeySize = Convert.stringToInt(Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", +row + 1));
        String getEncryptionStandard = Db.selectFromWallet("advanced_encryption_standard", "keystash_wallet", "account_id", +row + 1);
        
        char[] checkPassword = enterPrimaryPasswordGenerateTokenPasswordField.getPassword();

        if (checkPassword.length < Constants.minPasswordChars) {

            Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");

        }
        else {

            char[] decipherSecret = null;
            
            try {
                decipherSecret = Aes.decrypt(getSecret, new String(checkPassword), getSalt, getIV, getIteration, getKeySize, getEncryptionStandard);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | UnsupportedEncodingException | DecoderException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (decipherSecret == null) {
                Dialog.showDialog("Password does not match with encryption!", "Note");
            }
            else {

                byte[] secretPhrase = Crypto.getPublicKey(new String(decipherSecret));
                Long secretPhraseConvert = Convert.getId(secretPhrase);
                String publicKey = Convert.toHexString(secretPhrase);
                String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
                String addressRs = Convert.rsAccount(secretPhraseConvert);

                if (getAccountRs.equals(addressRs)) {

                    KeyStashGUI.secretToken = Convert.stringToChar(new String(decipherSecret));
                    enterPrimaryPasswordGenerateTokenFrame.setVisible(false);
                    FeelAndLook.centerFrame(generateTokenFrame, 520, 230);

                    if (SystemUtils.IS_OS_UNIX) {

                        generateTokenFrame.setPreferredSize(new Dimension(520, 210));
                        generateTokenFrame.setMinimumSize(new Dimension(520, 210));
                        generateTokenFrame.setMaximumSize(new Dimension(520, 212));
                        generateTokenFrame.pack();

                    }

                    generateTokenFrame.setVisible(true);
                    generateTokenFrame.setTitle("Generate Token: " + addressRs);
                    generateTokenGenerateTokenButton.requestFocus();
                    generateTokenFrame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent we) {
                            MenuItem.enableMenu(true);
                            StringSelection stringSelection = new StringSelection("");
                            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                            clpbrd.setContents(stringSelection, null);

                        }
                    });

                    Timer timer;
                    timer = new Timer(Constants.closeWindows * 1000, new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            generateTokenFrame.setVisible(false);
                            generateTokenFrame.dispose();
                            MenuItem.enableMenu(true);
                        }
                    });
                    timer.setRepeats(false);
                    timer.start();

                }
                if (!getAccountRs.equals(addressRs)) {
                    Dialog.showDialog("Password does not match with encryption!", "Note");
                }

            }
            Memory.clearPasswordMemory(checkPassword);
        }

    }

    public static void generateToken() {

        String website = generateTokenTokenDataField.getText();
        String tokenString = Token.generateToken(String.valueOf(secretToken), website.trim());
        generateTokenTokenKeyField.setText(tokenString);

    }

    public static void generateTokenCloseWindow() {

        Memory.clearPasswordMemory(secretToken);
        generateTokenFrame.setVisible(false);
        generateTokenFrame.dispose();
        MenuItem.enableMenu(true);

    }

    public static void primayPasswordTokenCancel() {

        enterPrimaryPasswordGenerateTokenFrame.setVisible(false);
        enterPrimaryPasswordGenerateTokenFrame.dispose();
        MenuItem.enableMenu(true);

    }

    public static void primayPasswordTokenCheckBox() {

        boolean showPhrase = enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.isSelected();

        if (showPhrase) {
            enterPrimaryPasswordGenerateTokenPasswordField.setEchoChar((char) 0);
        }
        else {
            enterPrimaryPasswordGenerateTokenPasswordField.setEchoChar('*');
        }

    }

}
