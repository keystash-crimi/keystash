package keystash.gui;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.confirmNewAccountAttemptLabel;
import static keystash.KeyStashGUI.confirmNewAccountAttemptValueLabel;
import static keystash.KeyStashGUI.confirmNewAccountEntropyValue;
import static keystash.KeyStashGUI.confirmNewAccountGenerateNewPhraseButton;
import static keystash.KeyStashGUI.confirmNewAccountIncludeLettersField;
import static keystash.KeyStashGUI.confirmNewAccountLengthValue;
import static keystash.KeyStashGUI.confirmNewAccountOKButton;
import static keystash.KeyStashGUI.confirmNewAccountPhraseField;
import static keystash.KeyStashGUI.confirmNewAccountSearchAddressToggleButton;
import static keystash.KeyStashGUI.confirmNewAccountShowAddressRsLabel;
import static keystash.KeyStashGUI.secret;
import keystash.crypto.Crypto;
import keystash.crypto.PhraseGenerator;
import keystash.util.Convert;
import keystash.util.PasswordStrength;

public class AddressSearch {

    public static void searchInAddress()
            throws NoSuchAlgorithmException,
            NoSuchProviderException,
            IOException {

        String findValue = confirmNewAccountIncludeLettersField.getText();
        findValue = findValue.toUpperCase();
        String[] getSearchValue = findValue.split("\\-", -1);

        int countAttempts = 0;
        do {
            char[] myPhrase = PhraseGenerator.generatePhrase();
            secret = myPhrase;
            byte[] secretPhrase = Crypto.getPublicKey(new String(secret));
            Long secretPhraseConvert = Convert.getId(secretPhrase);
            String addressRs = Convert.rsAccount(secretPhraseConvert);
            confirmNewAccountShowAddressRsLabel.setText(addressRs);
            String[] splitAddress = addressRs.split("\\-", -1);

            for (int i = 1; i < 5; i++) {
                String word = splitAddress[i].replaceAll("\\s+$", "");
                String searchingWord = getSearchValue[1].replaceAll("\\s+$", "");
                if (word.contains(searchingWord) && word.length() >= searchingWord.length()) {
                    confirmNewAccountAttemptLabel.setEnabled(false);
                    confirmNewAccountAttemptValueLabel.setEnabled(false);
                    confirmNewAccountGenerateNewPhraseButton.setEnabled(true);
                    confirmNewAccountIncludeLettersField.setEnabled(true);
                    confirmNewAccountSearchAddressToggleButton.setSelected(false);
                    backgroundMenu = 0;
                }

            }

            confirmNewAccountPhraseField.setText(new String(myPhrase));
            int passLength = secret.length;
            String passLengthString = String.valueOf(passLength);
            double shannonEntropy = PasswordStrength.getShannonEntropy(new String(secret));
            String shannonEntropyString = String.valueOf(shannonEntropy);
            confirmNewAccountEntropyValue.setText(shannonEntropyString);
            confirmNewAccountLengthValue.setText(passLengthString);
            countAttempts++;
            confirmNewAccountAttemptValueLabel.setText(Convert.intToString(countAttempts));

        } while (confirmNewAccountSearchAddressToggleButton.isSelected() == true);
        confirmNewAccountOKButton.setEnabled(true);
    }

}