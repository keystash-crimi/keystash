package keystash.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static keystash.KeyStashGUI.decryptDatabaseFrame;
import static keystash.KeyStashGUI.iconPaneLockjLabel;
import static keystash.KeyStashGUI.isWalletEncrypted;
import static keystash.KeyStashGUI.showWarningsOnStartupCheckbox;
import static keystash.KeyStashGUI.welcomeFrame;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Db;
import keystash.util.Debug;
import keystash.util.Structure;
import org.apache.commons.lang.SystemUtils;

public class Loader {

    public static boolean decryptFrameActive = false;

    public static void walletStructure()
            throws IOException {

        try {
            
            Structure.createWallet();
            
        } catch (Exception e) {
            
            Debug.addLoggMessage("Creating wallet structure and connection failed.");
            
        }

        try {
            
            Structure.createSettings();
            
        } catch (Exception e) {
            
            Debug.addLoggMessage("Creating settings structure and connection failed.");
            
        }
        
         try {
            
            Structure.createApiLog();
            
        } catch (Exception e) {
            
            Debug.addLoggMessage("Creating apiLog structure and connection failed.");
            
        }

        try {
            
            TableContent.insertAboutComponents();
            TableContent.getTable();
            
        } catch (Exception e) {
            
            Debug.addLoggMessage("Wallet file looks encrypted. Enter master password.");
            isWalletEncrypted = true;
            decryptFrameActive = true;
            Db.connectToSettings();
            Db.connectToApiLog();
            MenuItem.encryptedEnableMenu(false);
            FeelAndLook.centerFrame(decryptDatabaseFrame, 520, 200);
            decryptDatabaseFrame.setVisible(true);
            iconPaneLockjLabel.setToolTipText("Wallet File Encrypted");
            BufferedImage bufferedImage;
            String path;
            JLabel imageLabel;
            ImageIcon icon;        
           

            if (SystemUtils.IS_OS_LINUX) {

                path = Constants.iconLockEncryptedPathUnix;

            }
            else {

                path = Constants.iconLockEncryptedPath;

            }

            imageLabel = iconPaneLockjLabel;
            File file = new File(path);
            bufferedImage = ImageIO.read(file);
            icon = new ImageIcon(bufferedImage);
            icon.getImage().flush();
            imageLabel.setIcon(icon);

        }

        if (!isWalletEncrypted) {

            showWelcomeScreen();
           

        }
        
         

    }

    public static void showWelcomeScreen() {

        if (Convert.stringToBoolean(Db.selectFromSettings("welcome_screen", "keystash_settings", "settings_id", 1)) && !decryptFrameActive) {

            boolean warnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));
            FeelAndLook.centerFrame(welcomeFrame, 520, 300);
            MenuItem.enableMenu(false);
            welcomeFrame.setVisible(true);
            showWarningsOnStartupCheckbox.setSelected(warnings);
            welcomeFrame.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent we) {

                    MenuItem.enableMenu(true);

                }
            });

        }

    }

}
