package keystash.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JFileChooser;
import javax.swing.Timer;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.aboutFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordOfflineSigningFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordOfflineSigningPasswordField;
import static keystash.KeyStashGUI.enterPrimaryPasswordOfflineSigningShowPhraseCheckBox;
import static keystash.KeyStashGUI.offlineSigningAmountTextField;
import static keystash.KeyStashGUI.offlineSigningDeadlineSpinner;
import static keystash.KeyStashGUI.offlineSigningFeeSpinner;
import static keystash.KeyStashGUI.offlineSigningFrame;
import static keystash.KeyStashGUI.offlineSigningOKButton;
import static keystash.KeyStashGUI.offlineSigningRecepientTextField;
import static keystash.KeyStashGUI.row;
import static keystash.KeyStashGUI.secretSigning;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.sign.Transaction;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;
import keystash.util.Memory;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.lang.SystemUtils;

public class OfflineSigning {

    public static void showOfflineSigning()
            throws IOException {

        String getAccountRs = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +row + 1);
        String getSecret = Db.selectFromWallet("secret", "keystash_wallet", "account_id", +row + 1);
        String getSalt = Db.selectFromWallet("randomSalt", "keystash_wallet", "account_id", +row + 1);
        String getIV = Db.selectFromWallet("randomIV", "keystash_wallet", "account_id", +row + 1);
        int getIteration = Convert.stringToInt(Db.selectFromWallet("secret_iteration", "keystash_wallet", "account_id", +row + 1));
        int getKeySize = Convert.stringToInt(Db.selectFromWallet("secret_key_size", "keystash_wallet", "account_id", +row + 1));
        String getEncryptionStandard = Db.selectFromWallet("advanced_encryption_standard", "keystash_wallet", "account_id", +row + 1);
        char[] checkPassword = enterPrimaryPasswordOfflineSigningPasswordField.getPassword();

        if (checkPassword.length < Constants.minPasswordChars) {

            Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");

        }
        else {

            char[] decipherSecret = null;

            try {
                decipherSecret = Aes.decrypt(getSecret, new String(checkPassword), getSalt, getIV, getIteration, getKeySize, getEncryptionStandard);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | UnsupportedEncodingException | DecoderException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (decipherSecret == null) {
                Dialog.showDialog("Password does not match with encryption!", "Note");
            }
            else {

                byte[] secretPhrase = Crypto.getPublicKey(new String(decipherSecret));
                Long secretPhraseConvert = Convert.getId(secretPhrase);
                String publicKey = Convert.toHexString(secretPhrase);
                String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
                String addressRs = Convert.rsAccount(secretPhraseConvert);

                if (getAccountRs.equals(addressRs)) {

                    KeyStashGUI.secretSigning = Convert.stringToChar(new String(decipherSecret));
                    enterPrimaryPasswordOfflineSigningFrame.setVisible(false);
                    FeelAndLook.centerFrame(offlineSigningFrame, 520, 310);

                    if (SystemUtils.IS_OS_UNIX) {

                        offlineSigningFrame.setPreferredSize(new Dimension(520, 280));
                        offlineSigningFrame.setMinimumSize(new Dimension(520, 280));
                        offlineSigningFrame.setMaximumSize(new Dimension(520, 280));
                        offlineSigningFrame.pack();

                    }

                    offlineSigningFrame.setVisible(true);
                    offlineSigningFrame.setTitle("Offline Signing: " + addressRs);
                    offlineSigningOKButton.requestFocus();
                    offlineSigningFrame.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent we) {

                            MenuItem.enableMenu(true);
                            Memory.clearPasswordMemory(secretSigning);
                            offlineSigningRecepientTextField.setText("");
                            offlineSigningDeadlineSpinner.setValue(Constants.deadline);
                            offlineSigningAmountTextField.setText("");

                        }
                    });

                    Timer timer;
                    timer = new Timer(Constants.closeWindows * 1000, new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            offlineSigningFrame.setVisible(false);
                            offlineSigningFrame.dispose();
                            MenuItem.enableMenu(true);
                        }
                    });
                    timer.setRepeats(false);
                    timer.start();

                }
                if (!getAccountRs.equals(addressRs)) {

                    Dialog.showDialog("Password does not match with encryption!", "Note");

                }

            }
            Memory.clearPasswordMemory(checkPassword);
        }

    }

    @SuppressWarnings("UseSpecificCatch")
    public static void signTransaction() {

        char[] secretPhrase = secretSigning;
        byte[] publicKey = Crypto.getPublicKey(new String(secretPhrase));
        byte[] senderPublicKey = publicKey;
        String receiver = offlineSigningRecepientTextField.getText();
        String validReceiver = receiver.replaceAll("\\s+$", "");
        String amountString = offlineSigningAmountTextField.getText();
        Object fee = offlineSigningFeeSpinner.getValue();
        String feeString = String.valueOf(fee);
        Object deadl = offlineSigningDeadlineSpinner.getValue();
        String deadlString = String.valueOf(deadl);
        int deadlInt = Convert.stringToInt(deadlString);
        short deadline = (short) deadlInt;
        byte[] signature = null;
        String regex = ".*[*01$+#?@_&=!%{}/]+.*";
        String fraction = amountString;
        String[] parts;
        String part2 = null;
        try {
            parts = parts = fraction.split("\\.");
            part2 = parts[1];
        } catch (Exception e) {
        }

        if ((validReceiver == null) || (validReceiver.trim().isEmpty())) {

            Dialog.showDialog("Recipient cant be empty.", "Note");

        }
        else if (validReceiver.length() < 24) {

            Dialog.showDialog("Recipient address length not valid.", "Note");

        }
        else if (!validReceiver.contains("NXT")) {

            Dialog.showDialog("Recipient address not valid.", "Note");

        }
        else if (validReceiver.matches(regex)) {

            Dialog.showDialog("Recipient syntax error.", "Note");

        }
        else if ((amountString == null) || (amountString.trim().isEmpty())) {

            Dialog.showDialog("Amount cant be empty.", "Note");

        }
        else if (amountString.contains(".") && part2.length() >= 9) {

            Dialog.showDialog("Amount decimals limited to 8.", "Note");

        }
        else {

            long amountNQT = Transaction.convertToNQT(amountString);
            long feeNQT = Transaction.convertToNQT(feeString);

            try {
                long recipientId = Convert.parseAccountId(receiver);
                byte[] unsignedBytes = Transaction.getBytes(deadline, senderPublicKey, recipientId, amountNQT, feeNQT, signature);
                byte[] signatureBytes = Crypto.sign(unsignedBytes, new String(secretPhrase));
                byte[] signedBytes = Transaction.getBytes(deadline, senderPublicKey, recipientId, amountNQT, feeNQT, signatureBytes);
                String sb = Convert.toHexString(signedBytes);
                JFileChooser chooser = new JFileChooser();
                chooser.setSelectedFile(new File(Date.getDate(true) + "_" + "recipient_" + receiver + "_" + "amount_" + amountString + Constants.fileType));
                chooser.setDialogTitle("Specify a directory to save");
                offlineSigningFrame.setAlwaysOnTop(false);
                chooser.setCurrentDirectory(new File(Constants.walletHomePath));
                int userSelection = chooser.showSaveDialog(aboutFrame);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    try ( FileWriter fw = new FileWriter(chooser.getSelectedFile()) ) {
                        fw.write(sb);
                        fw.close();
                        Dialog.showDialog("Transaction signed and saved successfully.", "Note");
                        offlineSigningFrame.setAlwaysOnTop(true);
                    } catch (Exception ex) {
                        Dialog.showDialog("Save transaction failed.", "Note");
                    }

                }
            } catch (Exception e) {
                Dialog.showDialog("Transaction signing failed, syntax input wrong.", "Note");
            }

        }

    }

    public static void primayPasswordOfflineSigningCheckBox() {

        boolean showPhrase = enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.isSelected();

        if (showPhrase) {

            enterPrimaryPasswordOfflineSigningPasswordField.setEchoChar((char) 0);

        }
        else {

            enterPrimaryPasswordOfflineSigningPasswordField.setEchoChar('*');

        }

    }

    public static void offlineSigningCloseWindow() {

        Memory.clearPasswordMemory(secretSigning);
        offlineSigningRecepientTextField.setText("");
        offlineSigningDeadlineSpinner.setValue(Constants.deadline);
        offlineSigningAmountTextField.setText("");
        offlineSigningFrame.setVisible(false);
        offlineSigningFrame.dispose();
        MenuItem.enableMenu(true);

    }

    public static void primayPasswordOfflineSigningCancel() {

        enterPrimaryPasswordOfflineSigningFrame.setVisible(false);
        enterPrimaryPasswordOfflineSigningFrame.dispose();
        MenuItem.enableMenu(true);

    }
}
