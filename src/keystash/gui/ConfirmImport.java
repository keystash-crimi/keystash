package keystash.gui;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.accountMenuItem;
import static keystash.KeyStashGUI.confirmImportFrame;
import static keystash.KeyStashGUI.confirmImportOKButton;
import static keystash.KeyStashGUI.confirmImportPhraseField;
import static keystash.KeyStashGUI.confirmImportShowPhraseCheckBox;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.confirmImportLengthValue;
import static keystash.KeyStashGUI.confirmImportShowAddressRsLabel;
import static keystash.KeyStashGUI.enableApiMenuItem;
import static keystash.KeyStashGUI.globalPassword;
import static keystash.KeyStashGUI.guiShowWarnings;
import static keystash.KeyStashGUI.iconPaneAccountjLabel;
import keystash.crypto.Aes;
import keystash.crypto.Crypto;
import keystash.crypto.KeyLength;
import keystash.http.ApiRefresh;
import keystash.util.Config;
import keystash.util.Constants;
import keystash.util.Convert;
import keystash.util.Date;
import keystash.util.Db;
import keystash.util.Memory;
import keystash.util.PasswordStrength;
import org.apache.commons.codec.DecoderException;

public class ConfirmImport {

    public static void saveAccount()
            throws NoSuchPaddingException,
            DecoderException,
            InvalidKeySpecException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            BadPaddingException,
            IOException,
            NoSuchAlgorithmException {

        char[] checkPassword = confirmImportPhraseField.getPassword();
        int passLength = checkPassword.length;
        byte[] secretPhrase = Crypto.getPublicKey(String.valueOf(checkPassword));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String publicKey = Convert.toHexString(secretPhrase);
        String addressClassic = Convert.toUnsignedLong(secretPhraseConvert);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        int accountId = Db.countRows("keystash_wallet") + 1;
        double shannonEntropy = PasswordStrength.getShannonEntropy(new String(checkPassword));
        long shannonLong = (long) shannonEntropy;

        if (passLength < Constants.minPrivateKeyLength) {

            Dialog.showDialog("Minimum Length of phrase must be 35 characters.", "Note");

        }
        else if (TableContent.isAccountInTable(addressRs)) {

            Dialog.showDialog("Account already in wallet.", "Note");

        }
        else if (shannonLong <= 2) {

            Dialog.showDialog("Entropy is not high enough.", "Note");

        }
        else {
            if (enableApiMenuItem.isSelected()) {

                backgroundMenu = 3;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            char[] genSalt = null;
            try {
                genSalt = Aes.generateSaltOrIv(Constants.saltAndIvSize[0]);
            } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            char[] tmpSalt = genSalt;
            char[] genIV = null;
            try {
                genIV = Aes.generateSaltOrIv(Constants.saltAndIvSize[1]);
            } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            char[] tmpIV = genIV;

            char[] cipherPassPhrase = null;
            try {
                cipherPassPhrase = Aes.encrypt(String.valueOf(checkPassword), String.valueOf(globalPassword), new String(tmpSalt), new String(tmpIV));
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | DecoderException ex) {

                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            String label = Constants.accountLabel;
            int secretIteration;
            int secretKeySize;
            String encryptionStandard;

            if (Convert.stringToBoolean(Config.properties("keystash.enableExpertEncryption")) && Convert.stringToBoolean(Config.properties("keystash.enableProperties"))) {

                secretIteration = Convert.stringToInt(Config.properties("keystash.PBKDF2Iterations"));

                secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

                if (KeyLength.isUnlimitedKeyStrength() && secretKeySize == Constants.secretKeySize[1]) {

                    secretKeySize = Convert.stringToInt(Config.properties("keystash.secretKeySize"));

                }
                else {

                    secretKeySize = Constants.secretKeySize[0];

                }

                encryptionStandard = Config.properties("keystash.keyGeneratorAlgorithm");

            }
            else {

                secretIteration = Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1));
                secretKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
                encryptionStandard = Db.selectFromSettings("advanced_encryption_standard", "keystash_settings", "settings_id", 1);

            }

            int shareAccount = (Convert.stringToInt(Db.selectFromSettings("share_account", "keystash_settings", "settings_id", 1)));
            String accountType = Db.selectFromSettings("account_type", "keystash_settings", "settings_id", 1);

            String createdTimestamp = Date.getDate(false);

            Db.walletInsertNewAccount(accountId, addressClassic, addressRs, publicKey, label, new String(cipherPassPhrase), new String(tmpSalt), new String(tmpIV),
                    secretIteration, secretKeySize, encryptionStandard, shareAccount, accountType, createdTimestamp);

            TableContent.deleteGuiTables();
            TableContent.getTable();

            ApiRefresh.refreshApi();

            guiShowWarnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));
            if (guiShowWarnings) {

                Dialog.showDialog("Always backup your wallet after changes on a secure place!", "Note");

            }

            int countRows = Db.countRows("keystash_wallet");
            iconPaneAccountjLabel.setToolTipText("Accounts: " + countRows);
            confirmImportFrame.setVisible(false);
            accountMenuItem.setEnabled(true);
            MenuItem.enableMenu(true);

        }

        if (enableApiMenuItem.isSelected()) {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            backgroundMenu = 2;

        }
        Memory.clearPasswordMemory(checkPassword);
        Memory.clearPasswordMemory(globalPassword);
    }

    public static void showPhrase() {

        boolean showPhrase = confirmImportShowPhraseCheckBox.isSelected();

        if (showPhrase) {

            confirmImportPhraseField.setEchoChar((char) 0);

        }
        else {

            confirmImportPhraseField.setEchoChar('*');

        }

    }

    public static void closeFrame() {

        confirmImportOKButton.setEnabled(true);
        backgroundMenu = 0;
        confirmImportFrame.setVisible(false);
        accountMenuItem.setEnabled(true);
        confirmImportFrame.dispose();
        MenuItem.enableMenu(true);

    }

    public static void phraseLength() {

        char[] checkPassword = confirmImportPhraseField.getPassword();
        confirmImportLengthValue.setText(Convert.intToString(checkPassword.length));
        byte[] secretPhrase = Crypto.getPublicKey(new String(checkPassword));
        Long secretPhraseConvert = Convert.getId(secretPhrase);
        String addressRs = Convert.rsAccount(secretPhraseConvert);
        if (checkPassword.length == 0) {
            confirmImportShowAddressRsLabel.setText("");
        }
        else {
            confirmImportShowAddressRsLabel.setText(addressRs);
        }

    }

}
