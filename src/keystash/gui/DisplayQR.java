package keystash.gui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static keystash.KeyStashGUI.qrCodeBackgroundLabel;
import static keystash.KeyStashGUI.qrCodeFrame;
import static keystash.KeyStashGUI.row;
import keystash.util.Constants;
import keystash.util.Db;
import keystash.util.QRCode;
import org.apache.commons.lang.SystemUtils;

public class DisplayQR {

    public static void showQRCode()
            throws URISyntaxException,
            IOException {

        MenuItem.enableMenu(false);
        String getAccountRs = Db.selectFromWallet("address_rs", "keystash_wallet", "account_id", +row + 1);
        QRCode.createCode(getAccountRs);
        File f;

        if (SystemUtils.IS_OS_LINUX) {

            f = new File(Constants.qrCodeImagePathUnix);

        }
        else {

            f = new File(Constants.qrCodeImagePath);

        }

        ImageIcon imgThisImg = new ImageIcon(f.toString());
        imgThisImg.getImage().flush();
        qrCodeBackgroundLabel.setIcon(imgThisImg);
        qrCodeBackgroundLabel.setVerticalAlignment(JLabel.TOP);
        qrCodeFrame.pack();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (370 / 2),
                middle.y - (370 / 2));
        qrCodeFrame.setLocation(newLocation);
        qrCodeFrame.setTitle("QR Code: " + getAccountRs);

        if (SystemUtils.IS_OS_UNIX) {

            qrCodeFrame.setPreferredSize(new Dimension(355, 355));
            qrCodeFrame.setMinimumSize(new Dimension(355, 355));
            qrCodeFrame.setMaximumSize(new Dimension(355, 355));
            qrCodeFrame.pack();

        }
        
        qrCodeFrame.setVisible(true);
        qrCodeFrame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we) {

                MenuItem.enableMenu(true);

            }
        });

    }

}
