package keystash.gui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static keystash.KeyStashGUI.accountTable;
import static keystash.KeyStashGUI.enterPrimaryPasswordAccountDetailsFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordAccountDetailsPasswordField;
import static keystash.KeyStashGUI.enterPrimaryPasswordAccountDetailsShowPhraseCheckBox;
import static keystash.KeyStashGUI.enterPrimaryPasswordGenerateTokenFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordGenerateTokenPasswordField;
import static keystash.KeyStashGUI.enterPrimaryPasswordGenerateTokenShowPhraseCheckBox;
import static keystash.KeyStashGUI.enterPrimaryPasswordOfflineSigningFrame;
import static keystash.KeyStashGUI.enterPrimaryPasswordOfflineSigningPasswordField;
import static keystash.KeyStashGUI.enterPrimaryPasswordOfflineSigningShowPhraseCheckBox;

public class PopUpMenu {

    public static void doCopyAddress() {

        int getRow = accountTable.getSelectedRow();
        int column = accountTable.getSelectedColumn();
        Object rowValue;

        rowValue = accountTable.getValueAt(getRow, column);

        StringSelection stringSelection = new StringSelection(rowValue.toString());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);

    }

    public static void doAccountDetails() {
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (520 / 2),
                middle.y - (200 / 2));
        enterPrimaryPasswordAccountDetailsFrame.setLocation(newLocation);
        enterPrimaryPasswordAccountDetailsFrame.setVisible(true);
        enterPrimaryPasswordAccountDetailsPasswordField.setText("");
        enterPrimaryPasswordAccountDetailsShowPhraseCheckBox.setSelected(false);
        enterPrimaryPasswordAccountDetailsPasswordField.setEchoChar('*');
        enterPrimaryPasswordAccountDetailsPasswordField.requestFocus();
        MenuItem.enableMenu(false);
        enterPrimaryPasswordAccountDetailsFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                MenuItem.enableMenu(true);
            }
        });

    }

    public static void doGenerateTokenPrimaryPasswordFrame() {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (520 / 2),
                middle.y - (200 / 2));
        enterPrimaryPasswordGenerateTokenFrame.setLocation(newLocation);
        enterPrimaryPasswordGenerateTokenFrame.setVisible(true);
        enterPrimaryPasswordGenerateTokenPasswordField.setText("");
        enterPrimaryPasswordGenerateTokenShowPhraseCheckBox.setSelected(false);
        enterPrimaryPasswordGenerateTokenPasswordField.setEchoChar('*');
        enterPrimaryPasswordGenerateTokenPasswordField.requestFocus();
        MenuItem.enableMenu(false);
        enterPrimaryPasswordGenerateTokenFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                MenuItem.enableMenu(true);
            }
        });

    }

    public static void doOfflineSigning() {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point middle = new Point(screenSize.width / 2, screenSize.height / 2);
        Point newLocation = new Point(middle.x - (520 / 2),
                middle.y - (200 / 2));
        enterPrimaryPasswordOfflineSigningFrame.setLocation(newLocation);
        enterPrimaryPasswordOfflineSigningFrame.setVisible(true);
        enterPrimaryPasswordOfflineSigningPasswordField.setText("");
        enterPrimaryPasswordOfflineSigningShowPhraseCheckBox.setSelected(false);
        enterPrimaryPasswordOfflineSigningPasswordField.setEchoChar('*');
        enterPrimaryPasswordOfflineSigningPasswordField.requestFocus();
        MenuItem.enableMenu(false);
        enterPrimaryPasswordOfflineSigningFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                MenuItem.enableMenu(true);
            }
        });

    }

}
