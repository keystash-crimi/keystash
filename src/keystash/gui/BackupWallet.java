package keystash.gui;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.aboutFrame;
import static keystash.KeyStashGUI.isWalletEncrypted;
import keystash.util.Constants;
import keystash.util.Db;
import org.apache.commons.lang.SystemUtils;

public class BackupWallet {
    

    static public void backupWalletFile()
            throws InterruptedException {

        String afile;
        if (SystemUtils.IS_OS_LINUX) {
            
            afile = Constants.walletPathUnix;
            
        }
        else {
            
            afile = Constants.walletPath;
            
        }

        boolean cancel = false;

        JFileChooser chooser = new JFileChooser();
        chooser.setSelectedFile(new File("wallet.h2.db"));
        chooser.setDialogTitle("Specify a directory to save");
        
            
        int userSelection = chooser.showSaveDialog(aboutFrame);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            Db.walletClose();
            cancel = true;
            DataInputStream dis = null;
            try {
                dis = new DataInputStream(new FileInputStream(afile));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = null;
            try {
                data = new byte[dis.available()];
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                while ((nRead = dis.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, nRead);
                }
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                buffer.flush();
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dataOutStream = new DataOutputStream(baos);
            try {
                dataOutStream.write(data);
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            OutputStream outputStream = null;
            try {

                outputStream = new FileOutputStream(chooser.getSelectedFile());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                baos.writeTo(outputStream);
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                baos.close();
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                dataOutStream.close();
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                outputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                buffer.close();
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {

                dis.close();
                Dialog.showDialog("Wallet backup saved successfully!", "Note");
            } catch (IOException ex) {
                Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (cancel) {
            
            try {
                Db.connectToWallet();
            } catch (Exception e) {
                isWalletEncrypted = true;
            }

            if (isWalletEncrypted) {
                
                TableContent.deleteGuiTables();
                MenuItem.encryptedEnableMenu(false);
                Dialog.showDialog("Wallet file is encrypted. Connection was closed during backup. Please restart.", "Note");
                System.exit(0);
                
            }
            
        }
    }
    


}
