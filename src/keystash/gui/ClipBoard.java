package keystash.gui;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import static keystash.KeyStashGUI.accountDetailsCopyButton2;
import static keystash.KeyStashGUI.accountDetailsOKButton;
import static keystash.KeyStashGUI.accountDetailsjProgressBar;
import static keystash.KeyStashGUI.generateTokenTokenKeyField;
import static keystash.KeyStashGUI.lockClearClipboardButton;
import keystash.util.Convert;
import keystash.util.Db;

public class ClipBoard {

    public static void clearClipBoard() {

        while (accountDetailsjProgressBar.getValue() > 0) {

            long start_time = System.nanoTime();
            int clearClipBoard = Convert.stringToInt(Db.selectFromSettings("clear_clipboard", "keystash_settings", "settings_id", 1));
            long wait_time = clearClipBoard * 100000000;
            long end_time = start_time + wait_time;
            long passed_time;
            double seconds;
            int getProgressBarValue;
            int calcProgressBarValue;

            do {
                passed_time = start_time - System.nanoTime();
                seconds = Math.abs(passed_time / 1000000000.0);
                System.out.println("Wait For Clearing Passphrase: " + passed_time / 100000000);

                if (System.nanoTime() > end_time) {
                    getProgressBarValue = accountDetailsjProgressBar.getValue();
                    calcProgressBarValue = getProgressBarValue - 10;
                    System.out.println(getProgressBarValue);
                    accountDetailsjProgressBar.setValue(calcProgressBarValue);
                    start_time = System.nanoTime();
                }

            } while (System.nanoTime() < end_time && accountDetailsjProgressBar.getValue() > 0);

        }

        StringSelection stringSelection = new StringSelection("");
        java.awt.datatransfer.Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
        accountDetailsCopyButton2.setEnabled(true);
        accountDetailsOKButton.setEnabled(true);
        accountDetailsjProgressBar.setString("");
        lockClearClipboardButton = false;
    }

    public static void copyToken() {
        
        StringSelection stringSelection = new StringSelection(generateTokenTokenKeyField.getText());
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
        
    }

}
