package keystash.gui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static keystash.KeyStashGUI.importIconBackgroundLabel;
import static keystash.KeyStashGUI.newAccounIconBackgroundLabel;
import static keystash.KeyStashGUI.preferencesIconBackgroundLabel;
import static keystash.KeyStashGUI.showWarningsOnStartupCheckbox;
import static keystash.KeyStashGUI.welcomeFrame;
import static keystash.KeyStashGUI.welcomeShowOnStartupCheckbox;
import keystash.util.Convert;
import keystash.util.Db;
import org.apache.commons.lang.SystemUtils;

public class Welcome {

    public static void openFrame() {

        boolean warnings = Convert.stringToBoolean(Db.selectFromSettings("show_warnings", "keystash_settings", "settings_id", 1));
        boolean start = Convert.stringToBoolean(Db.selectFromSettings("welcome_screen", "keystash_settings", "settings_id", 1));
        FeelAndLook.centerFrame(welcomeFrame, 520, 300);
        MenuItem.enableMenu(false);
        welcomeFrame.setVisible(true);
        showWarningsOnStartupCheckbox.setSelected(warnings);
        welcomeShowOnStartupCheckbox.setSelected(start);
        welcomeFrame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we) {

                MenuItem.enableMenu(true);

            }
        });

    }

    public static void startUpCheckBox() {

        boolean checkBox = welcomeShowOnStartupCheckbox.isSelected();
        String checkBoxString = Boolean.toString(checkBox);
        int tableId = 1;
        String where = "welcome_screen";

        if (checkBox) {
            Db.walletUpdateSettings(tableId, where, checkBoxString);
        }
        else {
            Db.walletUpdateSettings(tableId, where, checkBoxString);
        }

    }

    public static void warningsCheckBox() {

        boolean checkBox = showWarningsOnStartupCheckbox.isSelected();
        String checkBoxString = Boolean.toString(checkBox);
        int tableId = 1;
        String where = "show_warnings";

        if (checkBox) {
            Db.walletUpdateSettings(tableId, where, checkBoxString);
        }
        else {
            Db.walletUpdateSettings(tableId, where, checkBoxString);
        }

    }

    public static void closeFrame() {

        MenuItem.enableMenu(true);
        welcomeFrame.setVisible(false);
        welcomeFrame.dispose();

    }

    public static void onHover(String imageName, float offset)
            throws IOException,
            URISyntaxException,
            InterruptedException {

        BufferedImage bufferedImage;
        String path = null;
        JLabel imageLabel = null;

        if ("New".equals(imageName)) {

            if (SystemUtils.IS_OS_LINUX) {

                path = "images/icon/newAccountIconBG.png";

            }
            else {

                path = "images\\icon\\newAccountIconBG.png";

            }

            imageLabel = newAccounIconBackgroundLabel;

        }
        else if ("Import".equals(imageName)) {

            if (SystemUtils.IS_OS_LINUX) {

                path = "images/icon/importIconBG.png";

            }
            else {

                path = "images\\icon\\importIconBG.png";

            }

            imageLabel = importIconBackgroundLabel;

        }
        else if ("Preferences".equals(imageName)) {

            if (SystemUtils.IS_OS_LINUX) {

                path = ("images/icon/preferencesIconBG.png");

            }
            else {

                path = ("images\\icon\\preferencesIconBG.png");

            }

            imageLabel = preferencesIconBackgroundLabel;

        }

        RescaleOp rescale;
        ImageIcon icon;
        File file = new File(path);
        bufferedImage = ImageIO.read(file);
        rescale = new RescaleOp(1.0f, offset, null);
        bufferedImage = rescale.filter(bufferedImage, null);
        icon = new ImageIcon(bufferedImage);
        imageLabel.setIcon(icon);
        Thread.sleep(100);

    }

}
