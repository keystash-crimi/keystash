package keystash.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import static keystash.KeyStashGUI.accountMenuItem;
import static keystash.KeyStashGUI.confirmImportEntropyValue;
import static keystash.KeyStashGUI.confirmImportFrame;
import static keystash.KeyStashGUI.confirmImportLengthValue;
import static keystash.KeyStashGUI.confirmImportOKButton;
import static keystash.KeyStashGUI.confirmImportPhraseField;
import static keystash.KeyStashGUI.confirmImportShowAddressRsLabel;
import static keystash.KeyStashGUI.confirmImportShowPhraseCheckBox;
import static keystash.KeyStashGUI.importFrame;
import static keystash.KeyStashGUI.importLengthValue;
import static keystash.KeyStashGUI.importPasswordConfirmField;
import static keystash.KeyStashGUI.importPasswordField;
import static keystash.KeyStashGUI.importShowPhraseCheckBox;
import static keystash.KeyStashGUI.backgroundMenu;
import static keystash.KeyStashGUI.globalPassword;
import keystash.util.Constants;
import keystash.util.Convert;

public class Import {

    public static void confirmPrimaryPassword()
            throws NoSuchProviderException {

        char[] checkPassword1 = importPasswordField.getPassword();
        char[] checkPassword2 = importPasswordConfirmField.getPassword();

        if (!Arrays.equals(checkPassword1, checkPassword2)) {
            Dialog.showDialog("Passwords do not match!", "Note");
        }
        else {
            if (checkPassword1.length < Constants.minPasswordChars) {
                Dialog.showDialog("Password must contain at least " + Constants.minPasswordChars + " characters!", "Note");
            }
            else {

                globalPassword = checkPassword1;
                confirmImportShowAddressRsLabel.setText("");
                confirmImportPhraseField.setText("");
                String passLengthString = "0";
                confirmImportEntropyValue.setText("0.00");
                confirmImportLengthValue.setText(passLengthString);
                importFrame.setVisible(false);
                FeelAndLook.centerFrame(confirmImportFrame, 520, 290);
                confirmImportFrame.setVisible(true);
                confirmImportShowPhraseCheckBox.setSelected(true);
                confirmImportPhraseField.setEchoChar((char) 0);
                confirmImportPhraseField.putClientProperty("JPasswordField.cutCopyAllowed", true);
                confirmImportOKButton.setEnabled(true);
                confirmImportOKButton.requestFocus();
                confirmImportFrame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent we) {
                        confirmImportOKButton.setEnabled(true);
                        backgroundMenu = 0;
                        accountMenuItem.setEnabled(true);
                        MenuItem.enableMenu(true);
                    }
                });
            }
        }

    }

    public static void phraseLength() {

        char[] checkPassword = importPasswordField.getPassword();
        importLengthValue.setText(Convert.intToString(checkPassword.length));

    }

    public static void showPhrase() {

        boolean showPhrase = importShowPhraseCheckBox.isSelected();

        if (showPhrase) {
            importPasswordField.setEchoChar((char) 0);
            importPasswordConfirmField.setEchoChar((char) 0);
        }
        else {
            importPasswordField.setEchoChar('*');
            importPasswordConfirmField.setEchoChar('*');
        }

    }

    public static void closeFrame() {

        importFrame.setVisible(false);
        importFrame.dispose();
        accountMenuItem.setEnabled(true);
        MenuItem.enableMenu(true);

    }

}
