package keystash.gui;

import java.awt.Color;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import keystash.KeyStashGUI;
import static keystash.KeyStashGUI.encryptDatabaseEntropyValue;
import static keystash.KeyStashGUI.encryptDatabaseLengthValue;
import static keystash.KeyStashGUI.encryptDatabasePasswordConfirmField;
import static keystash.KeyStashGUI.encryptDatabasePasswordField;
import static keystash.KeyStashGUI.encryptDatabaseShowPhraseCheckBox;
import static keystash.KeyStashGUI.encryptDatabasejProgressBar;
import static keystash.KeyStashGUI.importEntropyValue;
import static keystash.KeyStashGUI.importLengthValue;
import static keystash.KeyStashGUI.importPasswordConfirmField;
import static keystash.KeyStashGUI.importPasswordField;
import static keystash.KeyStashGUI.importShowPhraseCheckBox;
import static keystash.KeyStashGUI.importjProgressBar;
import static keystash.KeyStashGUI.newAccountEntropyValue;
import static keystash.KeyStashGUI.newAccountLengthValue;
import static keystash.KeyStashGUI.newAccountPasswordConfirmField;
import static keystash.KeyStashGUI.newAccountPasswordField;
import static keystash.KeyStashGUI.newAccountShowPhraseCheckBox;
import static keystash.KeyStashGUI.newAccountjProgressBar;
import keystash.crypto.WordBook;
import keystash.util.PasswordStrength;

public class Memorable {

    public static void generateMemorable(String type) {

        
        JPasswordField passwordField = null;
        JPasswordField passwordFieldConfirm = null;
        JLabel lengthValue = null;
        JLabel entropyValue = null;
        JProgressBar progressBar = null;
        JCheckBox checkBox = null;

        if ("New".equals(type)) {

            passwordField = newAccountPasswordField;
            passwordFieldConfirm = newAccountPasswordConfirmField;
            lengthValue = newAccountLengthValue;
            entropyValue = newAccountEntropyValue;
            progressBar = newAccountjProgressBar;
            checkBox = newAccountShowPhraseCheckBox;

        }
        else if ("Import".equals(type)) {

            passwordField = importPasswordField;
            passwordFieldConfirm = importPasswordConfirmField;
            lengthValue = importLengthValue;
            entropyValue = importEntropyValue;
            progressBar = importjProgressBar;
            checkBox = importShowPhraseCheckBox;

        }
        else if ("Encrypt".equals(type)) {

            passwordField = encryptDatabasePasswordField;
            passwordFieldConfirm = encryptDatabasePasswordConfirmField;
            lengthValue = encryptDatabaseLengthValue;
            entropyValue = encryptDatabaseEntropyValue;
            progressBar = encryptDatabasejProgressBar;
            checkBox = encryptDatabaseShowPhraseCheckBox;

        }
        
        
        try {
            passwordField.setText(WordBook.generateRememberWordPhrase());
        } catch (NoSuchAlgorithmException | NoSuchProviderException ex) {
            Logger.getLogger(KeyStashGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        progressBar.setStringPainted(true);
        passwordField.setEchoChar((char) 0);
        passwordFieldConfirm.setEchoChar((char) 0);
        passwordFieldConfirm.setText("");
        checkBox.setSelected(true);
        progressBar.setValue(50);
        progressBar.setString("Good");
        progressBar.setForeground(Color.blue.brighter());
        char[] getPassword = passwordField.getPassword();
        String checkPasswordString = new String(getPassword);
        int passLength = checkPasswordString.length();
        String passLengthString = String.valueOf(passLength);
        lengthValue.setText(passLengthString);
        double shannonEntropy = PasswordStrength.getShannonEntropy(checkPasswordString);
        String shannonEntropyString = String.valueOf(shannonEntropy);
        entropyValue.setText(shannonEntropyString);

    }

}
