package keystash.gui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import static keystash.KeyStashGUI.showPreferencesANSICheckbox;
import static keystash.KeyStashGUI.showPreferencesClearClipboardSpinner;
import static keystash.KeyStashGUI.showPreferencesFrame;
import static keystash.KeyStashGUI.showPreferencesIterationSpinner;
import static keystash.KeyStashGUI.showPreferencesKeySizeComboBox;
import static keystash.KeyStashGUI.showPreferencesLengthMaximumMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMaximumMinSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMinimumMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLengthMinimumMinSpinner;
import static keystash.KeyStashGUI.showPreferencesLowerCaseMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesLowerCaseMinSpinner;
import static keystash.KeyStashGUI.showPreferencesNumeraryMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesNumeraryMinSpinner;
import static keystash.KeyStashGUI.showPreferencesPhraseBlocksSpinner;
import static keystash.KeyStashGUI.showPreferencesRandomGenerationCheckbox;
import static keystash.KeyStashGUI.showPreferencesResetSettingsButton;
import static keystash.KeyStashGUI.showPreferencesSpecialCharMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesSpecialCharMinSpinner;
import static keystash.KeyStashGUI.showPreferencesUpperCaseMaxSpinner;
import static keystash.KeyStashGUI.showPreferencesUpperCaseMinSpinner;
import static keystash.KeyStashGUI.showPreferencesWarningsCheckbox;
import static keystash.KeyStashGUI.showPreferencesWhitespaceCheckbox;
import keystash.util.Convert;
import keystash.util.Db;

public class Preferences {

    public static void keySizeComboBox() {

        int getKeySize = showPreferencesKeySizeComboBox.getSelectedIndex();
        int keySizeComboBox;

        if (getKeySize == 0) {
            
            keySizeComboBox = 128;
            Db.walletUpdateSettings(1, "secret_key_size", Convert.intToString(keySizeComboBox));
            
        }
        else if (getKeySize == 1) {
            
            keySizeComboBox = 256;
            Db.walletUpdateSettings(1, "secret_key_size", Convert.intToString(keySizeComboBox));
            
        }

    }

    public static void iteration() {

        Component c = showPreferencesIterationSpinner.getEditor().getComponent(0);
        Object spinner = showPreferencesIterationSpinner.getValue();
        String spinnerString = String.valueOf(spinner);
        int spinnerInt = Convert.stringToInt(spinnerString);
        int tableId = 1;
        String where = "secret_iteration";

        Db.walletUpdateSettings(tableId, where, spinnerString);

        if (spinnerInt > 5000) {
            
            c.setForeground(Color.BLUE.darker());
            
        }
        else {
            
            c.setForeground(Color.BLACK);
            
        }
    }

    public static void checkBox(String type) {

        JCheckBox jCheckBox = null;
        String where = null;

        if ("Warnings".equals(type)) {

            jCheckBox = showPreferencesWarningsCheckbox;
            where = "show_warnings";

        }
        else if ("Whitespace".equals(type)) {

            jCheckBox = showPreferencesWhitespaceCheckbox;
            where = "enable_whitespace";

        }
        else if ("Ansi".equals(type)) {

            jCheckBox = showPreferencesANSICheckbox;
            where = "enable_ansi";

        }

        boolean checkBox = jCheckBox.isSelected();
        String checkBoxString = Boolean.toString(checkBox);
        int tableId = 1;

        if (checkBox) {
            
            Db.walletUpdateSettings(tableId, where, checkBoxString);
            
        }
        else {
            
            Db.walletUpdateSettings(tableId, where, checkBoxString);
            
        }

    }

    public static void clearClipBoard() {

        Object spinner = showPreferencesClearClipboardSpinner.getValue();
        String spinnerString = String.valueOf(spinner);
        int tableId = 1;
        String where = "clear_clipboard";
        Db.walletUpdateSettings(tableId, where, spinnerString);

    }

    public static void phraseBlocks() {

        Object spinner = showPreferencesPhraseBlocksSpinner.getValue();
        String spinnerString = String.valueOf(spinner);
        int tableId = 1;
        String where = "phrase_blocks";
        Db.walletUpdateSettings(tableId, where, spinnerString);

    }

    public static void spinnerValue(String type, String operator) {

        JSpinner spinnerObject = null;
        String where = null;
        String neighbour = null;

        if ("lengthMinimumMin".equals(type)) {

            spinnerObject = showPreferencesLengthMinimumMinSpinner;
            where = "length_minimum_min";
            neighbour = "length_minimum_max";

        }

        else if ("lengthMinimumMax".equals(type)) {

            spinnerObject = showPreferencesLengthMinimumMaxSpinner;
            where = "length_minimum_max";
            neighbour = "length_minimum_min";

        }

        else if ("lengthMaximumMin".equals(type)) {

            spinnerObject = showPreferencesLengthMaximumMinSpinner;
            where = "length_maximum_min";
            neighbour = "length_maximum_max";

        }
        else if ("lengthMaximumMax".equals(type)) {

            spinnerObject = showPreferencesLengthMaximumMaxSpinner;
            where = "length_maximum_max";
            neighbour = "length_maximum_min";

        }
        else if ("lowerCaseMin".equals(type)) {

            spinnerObject = showPreferencesLowerCaseMinSpinner;
            where = "lower_case_min";
            neighbour = "lower_case_max";

        }
        else if ("lowerCaseMax".equals(type)) {

            spinnerObject = showPreferencesLowerCaseMaxSpinner;
            where = "lower_case_max";
            neighbour = "lower_case_min";

        }
        else if ("upperCaseMin".equals(type)) {

            spinnerObject = showPreferencesUpperCaseMinSpinner;
            where = "upper_case_min";
            neighbour = "upper_case_max";

        }
        else if ("upperCaseMax".equals(type)) {

            spinnerObject = showPreferencesUpperCaseMaxSpinner;
            where = "upper_case_max";
            neighbour = "upper_case_min";

        }
        else if ("numeraryMin".equals(type)) {

            spinnerObject = showPreferencesNumeraryMinSpinner;
            where = "numerary_min";
            neighbour = "numerary_max";

        }
        else if ("numeraryMax".equals(type)) {

            spinnerObject = showPreferencesNumeraryMaxSpinner;
            where = "numerary_max";
            neighbour = "numerary_min";

        }
        else if ("specialCharMin".equals(type)) {

            spinnerObject = showPreferencesSpecialCharMinSpinner;
            where = "special_char_min";
            neighbour = "special_char_max";

        }
        else if ("specialCharMax".equals(type)) {

            spinnerObject = showPreferencesSpecialCharMaxSpinner;
            where = "special_char_max";
            neighbour = "special_char_min";

        }

        Object spinner = spinnerObject.getValue();
        String spinnerString = String.valueOf(spinner);
        int spinnerInt = Convert.stringToInt(spinnerString);
        int spinnerIntNeighbour = Convert.stringToInt(Db.selectFromSettings(neighbour, "keystash_settings", "settings_id", 1));
        int tableId = 1;

        if ("Min".equals(operator)) {

            if (spinnerInt > spinnerIntNeighbour) {
                
                Dialog.showDialog("Minimum cant be above maximum value.", "Note");
                spinnerObject.setValue(spinnerIntNeighbour);
                Db.walletUpdateSettings(tableId, where, Convert.intToString(spinnerIntNeighbour));
                
            }
            else {
                
                Db.walletUpdateSettings(tableId, where, spinnerString);
                
            }

        }
        else if ("Max".equals(operator)) {

            if (spinnerInt < spinnerIntNeighbour) {
                
                Dialog.showDialog("Maximum cant be below minimum value.", "Note");
                spinnerObject.setValue(spinnerIntNeighbour);
                System.out.println(spinnerObject.getValue());
                Db.walletUpdateSettings(tableId, where, Convert.intToString(spinnerIntNeighbour));
                
            }
            else {
                
                Db.walletUpdateSettings(tableId, where, spinnerString);
                
            }

        }

    }

    public static void randomGenerationCheckBox() {

        boolean checkBox = showPreferencesRandomGenerationCheckbox.isSelected();
        String checkBoxString = Boolean.toString(checkBox);
        int tableId = 1;
        String where = "expert_generation";
        int getKeySize = Convert.stringToInt(Db.selectFromSettings("secret_key_size", "keystash_settings", "settings_id", 1));
        int keySizeComboBox = 0;

        if (getKeySize == 128) {
            
            keySizeComboBox = 0;
            
        }
        else if (getKeySize == 256) {
            
            keySizeComboBox = 1;
            
        }

        if (checkBox) {
            
            Db.walletUpdateSettings(tableId, where, checkBoxString);
            showPreferencesPhraseBlocksSpinner.setEnabled(true);
            showPreferencesWhitespaceCheckbox.setEnabled(true);
            showPreferencesANSICheckbox.setEnabled(true);
            showPreferencesIterationSpinner.setEnabled(true);
            showPreferencesLengthMinimumMinSpinner.setEnabled(true);
            showPreferencesLengthMinimumMaxSpinner.setEnabled(true);
            showPreferencesLengthMaximumMinSpinner.setEnabled(true);
            showPreferencesLengthMaximumMaxSpinner.setEnabled(true);
            showPreferencesLowerCaseMinSpinner.setEnabled(true);
            showPreferencesLowerCaseMaxSpinner.setEnabled(true);
            showPreferencesUpperCaseMinSpinner.setEnabled(true);
            showPreferencesUpperCaseMaxSpinner.setEnabled(true);
            showPreferencesNumeraryMinSpinner.setEnabled(true);
            showPreferencesNumeraryMaxSpinner.setEnabled(true);
            showPreferencesSpecialCharMinSpinner.setEnabled(true);
            showPreferencesSpecialCharMaxSpinner.setEnabled(true);
            showPreferencesResetSettingsButton.setEnabled(true);
            showPreferencesKeySizeComboBox.setEnabled(true);
            showPreferencesPhraseBlocksSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("phrase_blocks", "keystash_settings", "settings_id", 1)));
            showPreferencesIterationSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("secret_iteration", "keystash_settings", "settings_id", 1)));
            showPreferencesWhitespaceCheckbox.setSelected(Convert.stringToBoolean(Db.selectFromSettings("enable_whitespace", "keystash_settings", "settings_id", 1)));
            showPreferencesANSICheckbox.setSelected(Convert.stringToBoolean(Db.selectFromSettings("enable_ansi", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMinimumMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_minimum_min", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMinimumMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_minimum_max", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMaximumMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_maximum_min", "keystash_settings", "settings_id", 1)));
            showPreferencesLengthMaximumMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("Length_maximum_max", "keystash_settings", "settings_id", 1)));
            showPreferencesLowerCaseMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("lower_case_min", "keystash_settings", "settings_id", 1)));
            showPreferencesLowerCaseMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("lower_case_max", "keystash_settings", "settings_id", 1)));
            showPreferencesUpperCaseMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("upper_case_min", "keystash_settings", "settings_id", 1)));
            showPreferencesUpperCaseMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("upper_case_max", "keystash_settings", "settings_id", 1)));
            showPreferencesNumeraryMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("numerary_min", "keystash_settings", "settings_id", 1)));
            showPreferencesNumeraryMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("numerary_max", "keystash_settings", "settings_id", 1)));
            showPreferencesSpecialCharMinSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("special_char_min", "keystash_settings", "settings_id", 1)));
            showPreferencesSpecialCharMaxSpinner.setValue(Convert.stringToInt(Db.selectFromSettings("special_char_max", "keystash_settings", "settings_id", 1)));
            showPreferencesKeySizeComboBox.setSelectedIndex(keySizeComboBox);
            
        }
        else {
            
            Db.walletUpdateSettings(tableId, where, checkBoxString);
            showPreferencesPhraseBlocksSpinner.setEnabled(false);
            showPreferencesIterationSpinner.setEnabled(false);
            showPreferencesWhitespaceCheckbox.setEnabled(false);
            showPreferencesANSICheckbox.setEnabled(false);
            showPreferencesLengthMinimumMinSpinner.setEnabled(false);
            showPreferencesLengthMinimumMaxSpinner.setEnabled(false);
            showPreferencesLengthMaximumMinSpinner.setEnabled(false);
            showPreferencesLengthMaximumMaxSpinner.setEnabled(false);
            showPreferencesLowerCaseMinSpinner.setEnabled(false);
            showPreferencesLowerCaseMaxSpinner.setEnabled(false);
            showPreferencesUpperCaseMinSpinner.setEnabled(false);
            showPreferencesUpperCaseMaxSpinner.setEnabled(false);
            showPreferencesNumeraryMinSpinner.setEnabled(false);
            showPreferencesNumeraryMaxSpinner.setEnabled(false);
            showPreferencesSpecialCharMinSpinner.setEnabled(false);
            showPreferencesSpecialCharMaxSpinner.setEnabled(false);
            showPreferencesResetSettingsButton.setEnabled(false);
            showPreferencesKeySizeComboBox.setEnabled(false);
            
        }

    }

    public static void closeFrame() {

        showPreferencesFrame.setVisible(false);
        showPreferencesFrame.dispose();
        MenuItem.enableMenu(true);

    }

}
