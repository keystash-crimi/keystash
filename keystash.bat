@ECHO OFF
SET PATH=%PATH%;%JAVA_HOME%;%JDK_HOME%\jre\bin;%PROGRAMFILES%\Java\jre7\bin;%PROGRAMFILES(X86)%\Java\jre7\bin;%PROGRAMFILES%\Java\jre8\bin;%PROGRAMFILES(X86)%\Java\jre8\bin
java -version >nul 2>&1 && (
	start "KeyStash" javaw -jar keystash.jar
) || (
	ECHO Java software not found on your system. Please go to http://java.com/en/ to download a copy of Java.
	PAUSE
)